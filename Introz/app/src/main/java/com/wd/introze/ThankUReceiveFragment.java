package com.wd.introze;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.chat.ui.activity.ChatActivity;
import com.quickblox.sample.chat.utils.chat.ChatHelper;
import com.quickblox.users.model.QBUser;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JSession;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.tables.TableContacts;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Gajendra on 9/27/2016.
 */

public class ThankUReceiveFragment extends Fragment {
    private static final String TAG = "ThankUReceive";
    private CustomProgress pd;
    private SharedPreferences spUser;
    //
    private SimpleDraweeView ivProfile;
    private TextView tvShortName;
    //
    private SimpleDraweeView ivProfile2;
    private TextView tvShortName2;
    //
    private String fullName;
    //
    private int chatId;
    //
    private ImageView ibChat;
    private String urlProfile;
    //
    private int id;

    //
    public static ThankUReceiveFragment getInstance(String data) {
        Bundle bundle = new Bundle();
        bundle.putString("data", data);
        //
        ThankUReceiveFragment fragment = new ThankUReceiveFragment();
        fragment.setArguments(bundle);
        //
        return fragment;
    }

    //
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_thank_u_received, null);
        TypefaceHelper.typeface(view);
        //
        TextView tvDollar = (TextView) view.findViewById(R.id.tvDollar);
        TextView tvName1 = (TextView) view.findViewById(R.id.tvName1);
        TextView tvName2 = (TextView) view.findViewById(R.id.tvName2);
        TextView tvName3 = (TextView) view.findViewById(R.id.tvName3);
        TextView tvMessage = (TextView) view.findViewById(R.id.tvMessage);
        ivProfile = (SimpleDraweeView) view.findViewById(R.id.ivProfile);
        tvShortName = (TextView) view.findViewById(R.id.tvShortName);
        ivProfile2 = (SimpleDraweeView) view.findViewById(R.id.ivByUser);
        tvShortName2 = (TextView) view.findViewById(R.id.tvByShortName);
        ibChat = (ImageView) view.findViewById(R.id.ibChat);
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        ProfileFragment.MyModel model = new Gson().fromJson(getArguments().getString("data"), ProfileFragment.MyModel.class);
        //
        fullName = model.fullName;
        id = model.id;
        //
        tvDollar.setText(model.money);
        tvName1.setText(JUtils.uppercaseFirstLetters(model.fullName));
        tvName2.setText(JUtils.uppercaseFirstLetters(model.fullName));
        tvName3.setText(JUtils.uppercaseFirstLetters(model.fullName));
        tvMessage.setText(model.feed);
        //
        getUserDetail(model.id);
        getThankUDetail(model.thankUId);
        //
        return view;
    }

    //
    private void getThankUDetail(int userId) {
        Ion.with(this).load("GET", AppUrls.MAIN + "/thank_you_detail/" + userId).setHeader("Authorization", JUtils.getHeader(spUser))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d(TAG, "Thank u detail result " + result);
            }
        });
    }

    //
    //
    private void getUserDetail(final int id) {
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/get_user_detail/" + id)//getActivity().getIntent().getStringExtra("user_id"))
                .setHeader("Authorization", JUtils.getHeader(spUser)).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                pd.dismiss();
                Log.d(TAG, "Thank U Receive Result " + result);
                if (e == null) {
                    try {//user_image fb_id
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            JSONObject joDetail = joResult.getJSONObject("user_details");
                            String userImage = joDetail.optString("user_image");
                            String fbId = joDetail.optString("fb_id");
                            String url = JUtils.getImageUrl(userImage, fbId);
                            //
                            urlProfile = url;
                            chatId = joDetail.optInt("user_chat_id");
                            boolean isMatch = joDetail.optInt("matched") == 1 ? true : false;
                            processChatIcon(isMatch);
                            //gotoP(url);
                            setUserProfile(Uri.parse(url), fullName);
                            setUserProfile2(Uri.parse(url), fullName);
                        } else {
                            //getUserDetail(id);
                        }
                    } catch (JSONException e1) {
                        //getUserDetail(id);
                    }
                } else {
                    //getUserDetail(id);
                }
            }
        });
    }

    //
    private void processChatIcon(boolean isMatch) {
        if (isMatch) {
            ibChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //
                    TableContacts tc = new TableContacts();
                    tc.firstName = fullName;
                    tc.lastName = "";
                    tc.introzeUserId = id;
                    tc.urlPic = urlProfile;
                    tc.chatId = chatId;
                    //
                    callChat(tc);//
                }
            });
            //
            ibChat.setVisibility(View.VISIBLE);
        }
    }

    //
    //
    private void callChat(TableContacts model) {
        if (spUser.getBoolean("is_chat_init", false)) {
            JSession.chatUserName = model.firstName + " " + model.lastName; //model.name;
            JSession.introzeId = model.introzeUserId + ""; //model.rel_user_id + "";
            JSession.urlRel = model.urlPic; //model.url;
            //Toast.makeText(getActivity(), "chat id " + model.chatId, Toast.LENGTH_SHORT).show();
            openChat(model.chatId);
        } else
            Toast.makeText(getActivity(), "Chat initializing please wait...", Toast.LENGTH_SHORT).show();
    }

    //
    public void openChat(int chatId) {
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        ArrayList<QBUser> selectedUsers = new ArrayList<>();
        //
        selectedUsers.add(new QBUser(chatId));
        //
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        pd.dismiss();
                        ChatActivity.startForResult(getActivity(), 5000, dialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        pd.dismiss();
                        Log.d("DialogError", "" + e);
                        //Toast.makeText(MainActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }


    //
    //
    private void setUserProfile(Uri uri, final String name) {
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(JUtils.getTwoUpperChar(name));
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener).setUri(uri).build();
        ivProfile.setController(controller);
    }

    //
    //
    private void setUserProfile2(Uri uri, final String name) {
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName2.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName2.setText(JUtils.getTwoUpperChar(name));
                tvShortName2.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener).setUri(uri).build();
        ivProfile2.setController(controller);
    }

}
