package com.wd.introze.match.chatdialogs;

import com.wd.introze.match.MatchModel;

/**
 * Created by flair on 16-08-2016
 */
public class DialogModel extends MatchModel {
    public int count;

    public DialogModel(int chatId, int count) {
        super.chatId = chatId;
        this.count = count;
    }

    //
    public DialogModel(int chatId, String name, String url, String byName, String date, int relationType, int relId, int rel_user_id,
                       int byUserId, String reason, String urlBy) {
        super.chatId = chatId;
        super.name = name;
        super.url = url;
        super.byName = byName;
        super.createOn = date;
        super.relationType = relationType;
        super.rel_id = relId;
        super.rel_user_id = rel_user_id;
        super.byUserId = byUserId;
        this.reason = reason;
        this.urlBy = urlBy;
    }

    //
    //
    public DialogModel(int chatId, String name, String url, String byName, String date, int relationType, int relId, int rel_user_id,
                       int byUserId, String reason, String urlBy, int user1ChatId, int user2ChatId) {
        super.chatId = chatId;
        super.name = name;
        super.url = url;
        super.byName = byName;
        super.createOn = date;
        super.relationType = relationType;
        super.rel_id = relId;
        super.rel_user_id = rel_user_id;
        super.byUserId = byUserId;
        this.reason = reason;
        this.urlBy = urlBy;
        this.user1ChatId = user1ChatId;
        this.user2ChatId = user2ChatId;
    }

    //
    public void setDetail(String name, String url, String byName, String date, int relationType, int relId, int rel_user_id) {
        super.name = name;
        super.url = url;
        super.byName = byName;
        super.createOn = date;
        super.relationType = relationType;
        super.rel_id = relId;
        super.rel_user_id = rel_user_id;
    }
}
