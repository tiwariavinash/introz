package com.wd.introze.customloader;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.ImageView;

import com.wd.introze.R;


public class CustomProgress extends Dialog {
    static CustomProgress dialog;

    public CustomProgress(Context context) {
        super(context);
        dialog = new CustomProgress(context, R.style.CustomProgress);
    }

    public CustomProgress(Context context, int theme) {
        super(context, theme);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        ImageView imageView = (ImageView) findViewById(R.id.spinnerImageView);
        AnimationDrawable spinner = (AnimationDrawable) imageView
                .getBackground();
        spinner.start();
    }

    // public void setMessage(CharSequence message) {
    // if (message != null && message.length() > 0) {
    // findViewById(R.id.message).setVisibility(View.VISIBLE);
    // TextView txt = (TextView) findViewById(R.id.message);
    // txt.setText(message);
    // txt.invalidate();
    // }
    // }

    public static CustomProgress show(Context context, CharSequence message, boolean indeterminate, boolean cancelable,
                                      OnCancelListener cancelListener) {
        dialog = new CustomProgress(context, R.style.CustomProgress);
        dialog.setTitle("");
        dialog.setContentView(R.layout.custom_progress);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.2f;
        dialog.getWindow().setAttributes(lp);
        // dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.show();
        return dialog;
    }

    public static void ProgressDismiss() {
        dialog.dismiss();
    }
}
