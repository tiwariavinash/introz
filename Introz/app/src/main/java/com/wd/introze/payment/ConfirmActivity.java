package com.wd.introze.payment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.AppUrls;
import com.wd.introze.R;
import com.wd.introze.SuccessModel;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;

import java.math.BigDecimal;

/**
 * Created by flair on 05-09-2016
 */
public class ConfirmActivity extends AppCompatActivity {
    //
    private TextView tvUserName, tvAmount, tvProcessingFeed, tvCardEnding, tvAmountCharge, tvProcessingFee;
    //
    private final String TAG = "ConfirmActivity";
    //
    private SharedPreferences spIntoze;
    private int amount;
    private String name;
    private String message;
    private int userId;
    private String last4;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private boolean isSuccess;
    //
    private CustomProgress pd;
    //

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        TypefaceHelper.typeface(this);
        //
        //
        TextView tvHeader = (TextView) findViewById(R.id.txtHeader);
        tvHeader.setTextSize(12);
        tvHeader.setText("CONFIRM");
        //
        tvUserName = (TextView) findViewById(R.id.tvRecipient2);
        tvAmount = (TextView) findViewById(R.id.tvAmount2);
        tvProcessingFeed = (TextView) findViewById(R.id.tvProcessingFee2);
        tvCardEnding = (TextView) findViewById(R.id.tvCardEnding2);
        tvAmountCharge = (TextView) findViewById(R.id.tvAmountCharge);
        tvProcessingFee = (TextView) findViewById(R.id.tvProcessingFee);
        //
        Intent intent = getIntent();
        name = intent.getStringExtra("user_name");
        amount = intent.getIntExtra("amount", 0);
        message = intent.getStringExtra("message");
        userId = intent.getIntExtra("user_id", 0);
        last4 = intent.getStringExtra("last_4");
        //
        FeedDetail model = new Gson().fromJson(intent.getStringExtra("fees_detail"), FeedDetail.class);
        //
        Log.d(TAG, amount + " " + userId + " " + message);
        //
        tvUserName.setText(name);
        tvAmount.setText("$" + amount);
        //
        tvProcessingFee.setText("Processing Fee (" + model.fees + "%):");
        //
        String pFees = String.format("%.2f", (amount * Float.parseFloat(model.fees)) / 100);
        //
        tvProcessingFeed.setText("$" + pFees);//(amount * Float.parseFloat(model.fees)) / 100);
        //
        BigDecimal number = new BigDecimal(model.totalAmount);
        //
        tvAmountCharge.setText("$" + number.stripTrailingZeros().toPlainString());//model.totalAmount);
        //
        tvCardEnding.setText(last4);
        //
        spIntoze = getSharedPreferences("user", MODE_PRIVATE);
        //
        builderError = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSuccess) finish();
                else
                    adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        //getFeedDetails();
    }

    //
    public void onClickBtnSendThankU(View view) {
        Log.d("user_id", "" + spIntoze.getString("user_id", null));
        Log.d("user_token", "" + spIntoze.getString("user_token", null));
        //
        pd = CustomProgress.show(this, null, false, false, null);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/send_thank_you").setHeader("Authorization", JUtils.getHeader(spIntoze))
                .setBodyParameter("message", message).setBodyParameter("to_user_id", "" + userId).setBodyParameter("amount", amount + "")
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d(TAG, result + "");
                pd.dismiss();
                if (result != null) {
                    SuccessModel model = new Gson().fromJson(result, SuccessModel.class);
                    if (TextUtils.equals("success", model.result)) {
                        isSuccess = true;
                        builderError.setErrorMessage("Success", "Your Thank You $$$ has been sent.");// model.message);
                    } else builderError.setErrorMessage("Error", model.message);
                    adError.show();
                    TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                } else {
                    builderError.setErrorMessage("Error", getString(R.string.error_no_response));
                    adError.show();
                    TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                }
            }
        });
    }

    ///fee_details
    private void getFeedDetails() {
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/fee_details").setHeader("Authorization", JUtils.getHeader(spIntoze))
                .setBodyParameter("amount", amount + "").asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d(TAG, "Result " + result);
                if (e == null) {
                    FeedDetail feedDetail = new Gson().fromJson(result, FeedDetail.class);
                    if (TextUtils.equals("success", feedDetail.result)) {
                        tvProcessingFee.setText("Processing Fee (" + feedDetail.fees + "%):");
                        tvProcessingFeed.setText("$" + (amount * Float.parseFloat(feedDetail.fees)) / 100);
                        tvAmountCharge.setText("$" + feedDetail.totalAmount);
                    } else {

                    }
                } else {
                    Log.d(TAG, "");
                }
            }
        });
    }

    //
    public class FeedDetail {

        @SerializedName("result")
        @Expose
        public String result;
        @SerializedName("total_amount")
        @Expose
        public Float totalAmount;
        @SerializedName("fees")
        @Expose
        public String fees;
    }
}
