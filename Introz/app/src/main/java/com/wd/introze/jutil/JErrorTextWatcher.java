package com.wd.introze.jutil;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.TextView;

/**
 * Created by Gajendra on 8/7/2016
 */
public class JErrorTextWatcher implements TextWatcher {
    //
    private TextView tvError;
    private String error;

    //
    public JErrorTextWatcher(TextView tvError, String error) {
        this.tvError = tvError;
        this.error = error;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(s)) {
            tvError.setText(error);
        } else tvError.setText("");
    }
}