package com.wd.introze;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.createintroze.NotificationMethodActivity;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JFile;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.qrcode.QRCodeActivity;
import com.wd.introze.selectcontact.SelectContactActivity;

import java.io.InputStream;

/**
 * Created by flair on 19-07-2016
 */
public class CreateIntrozeFragment extends Fragment implements View.OnClickListener {
    //
    //private ImageView ivPlusFirst, ivPlusSecond;
    //
    private SharedPreferences spUser;
    //
    private EditText etReason;
    //
    private boolean isIntroze1;
    String iid1;
    String name1;
    String userId1;
    String contactId1;
    String phone1;
    String email1;
    long _id1;
    //
    //
    private boolean isIntroze2;
    String iid2;
    String name2;
    String userId2;
    String contactId2;
    String email2;
    String phone2;
    long _id2;
    //
    private String urlPic1, urlPic2;
    //
    private LinearLayout formContainer;
    //
    private boolean isFirstSelected;
    //
    private TextView tvIntrozeFirst, tvIntrozeSecond;
    //
    private LinearLayout[] llCategories = new LinearLayout[4];
    private LinearLayout llSelected;
    private int selectIndex;
    //
    private SimpleDraweeView ivUser1;
    private SimpleDraweeView ivUser2;
    //
    private LinearLayout llReason;
    //
    private TextView tvUser1, tvUser2;
    //
    private LinearLayout llSelector1, llSelector2, llSelector3;
    //
    private TextView tv1, tv2, tv3;
    //
    private JSimpleBuilder builderCreateIntrozeSelDialog;
    private AlertDialog createIntrozeSelDialog;
    //
    private RelativeLayout rlFirst, rlSecond;

    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private LinearLayout llTopText;
    private TextView tvTopTitle;
    private TextView tvTopDescription;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_introze, null);
        //
        TypefaceHelper.typeface(view);
        //
        llTopText = (LinearLayout) view.findViewById(R.id.llTopText);
        tvTopTitle = (TextView) view.findViewById(R.id.tvTopTitle);
        tvTopDescription = (TextView) view.findViewById(R.id.tvTopDescription);
        //createIntrozeSelDialog = new CreateIntrozeSelDialog(getActivity()).create();
        builderCreateIntrozeSelDialog = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createIntrozeSelDialog.dismiss();
            }
        });
        createIntrozeSelDialog = builderCreateIntrozeSelDialog.create();
        //createIntrozeSelDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
        llSelector1 = (LinearLayout) view.findViewById(R.id.llSelector1);
        llSelector2 = (LinearLayout) view.findViewById(R.id.llSelector2);
        llSelector3 = (LinearLayout) view.findViewById(R.id.llSelector3);
        //
        tv1 = (TextView) view.findViewById(R.id.tv1);
        tv2 = (TextView) view.findViewById(R.id.tv2);
        tv3 = (TextView) view.findViewById(R.id.tv3);
        //
        ivUser1 = (SimpleDraweeView) view.findViewById(R.id.imgUser1);
        ivUser1.setBackgroundResource(R.drawable.user_dummy_img);
        //
        ivUser2 = (SimpleDraweeView) view.findViewById(R.id.imgUser2);
        ivUser2.setBackgroundResource(R.drawable.user_dummy_img);
        //
        tvUser1 = (TextView) view.findViewById(R.id.tvUser1);
        tvUser2 = (TextView) view.findViewById(R.id.tvUser2);
        //
        llCategories[0] = (LinearLayout) view.findViewById(R.id.llBusiness);
        llSelected = llCategories[0];
        //
        view.findViewById(R.id.llB).setOnClickListener(this);
        //
        llCategories[1] = (LinearLayout) view.findViewById(R.id.llDating);
        view.findViewById(R.id.llD).setOnClickListener(this);
        //
        llCategories[2] = (LinearLayout) view.findViewById(R.id.llFriendship);
        view.findViewById(R.id.llF).setOnClickListener(this);
        //
        llCategories[3] = (LinearLayout) view.findViewById(R.id.llMutualInterest);
        view.findViewById(R.id.llM).setOnClickListener(this);
        //
        tvIntrozeFirst = (TextView) view.findViewById(R.id.tvIntrozeFirst);
        tvIntrozeSecond = (TextView) view.findViewById(R.id.tvIntrozeSecond);
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        /*ivPlusFirst = (ImageView) view.findViewById(R.id.ivPlusFirst);
        ivPlusSecond = (ImageView) view.findViewById(R.id.ivPlusSecond);*/
        //
        rlFirst = (RelativeLayout) view.findViewById(R.id.rlFirst);
        rlSecond = (RelativeLayout) view.findViewById(R.id.rlSecond);
        rlFirst.setOnClickListener(this);
        rlSecond.setOnClickListener(this);
        //
        /*ivPlusFirst.setOnClickListener(this);
        ivPlusSecond.setOnClickListener(this);*/
        //
        Button btnCreateIntroze = (Button) view.findViewById(R.id.btnCreateIntroze);
        btnCreateIntroze.setOnClickListener(this);
        //
        etReason = (EditText) view.findViewById(R.id.etMessage);
        //
        llReason = (LinearLayout) view.findViewById(R.id.llReason);
        //
        formContainer = (LinearLayout) view.findViewById(R.id.formContainer);
        //
        builderError = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        //
        //TypefaceHelper.typeface((Button) adError.getButton(DialogInterface.BUTTON_POSITIVE));
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        if (HomeActivity.intent1 != null)
            onMyActivityResult(1000, getActivity().RESULT_OK, HomeActivity.intent1);
        //
        if (HomeActivity.intent2 != null)
            onMyActivityResult(1001, getActivity().RESULT_OK, HomeActivity.intent2);
        //initFirstPerson(HomeActivity.intent1);
        //
        view.findViewById(R.id.btnQRCodeGenerate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), QRCodeActivity.class);
                intent.putExtra("selected_tab", 0);
                startActivity(intent);
            }
        });
        view.findViewById(R.id.btnQRCodeScan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), QRCodeActivity.class);
                intent.putExtra("selected_tab", 1);
                startActivity(intent);
            }
        });
        return view;
    }

    //code
    public void onMyActivityResult(int reqCode, int resultCode, Intent data) {

        if (reqCode == 1000 || reqCode == 1001) {
            if (resultCode == Activity.RESULT_OK) {

                /*Uri contactData = data.getData();
                Cursor c = getActivity().managedQuery(contactData, null, null, null, null);
                if (c.moveToFirst()) {
*/

                //
                //
                /*ContentResolver cr = getActivity().getContentResolver();
                Cursor c = cr.query(ContactsContract.Contacts.CONTENT_URI,
                        null, ContactsContract.Contacts._ID + "=?", new String[]{data.getStringExtra("id")}, null);*/
                //
                String id = data.getStringExtra("id");//c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                //
                //Bitmap bitmap = loadContactPhoto(getActivity().getContentResolver(), Long.parseLong(id));
                String uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id)).toString();
                String urlPic = data.getStringExtra("url_profile");
                if (urlPic != null) uri = urlPic;
                //
                String email = getEmailId(getActivity().getContentResolver(), id);
                // Toast.makeText(getActivity(), "" + email, Toast.LENGTH_SHORT).show();

                //String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                String cNumber = null;
                //if (hasPhone.equalsIgnoreCase("1")) {
                Cursor phones = getActivity().getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                        null, null);
                if (phones.moveToFirst() && phones.getCount() > 0)
                    cNumber = phones.getString(phones.getColumnIndex("data1"));
                phones.close();

                //}
                String name = data.getStringExtra("fn");//c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                //
                if (reqCode == 1000) {
                    //
                    urlPic1 = urlPic;
                    //
                    _id1 = data.getLongExtra("_id", -1);
                    //
                    llSelector1.setBackgroundResource(R.drawable.layout_border_corner_left);
                    tv1.setTextColor(getResources().getColor(android.R.color.darker_gray));
                    //
                    llSelector2.setBackgroundResource(R.drawable.layout_border_corner_center_selected);
                    tv2.setTextColor(getResources().getColor(android.R.color.white));
                    //
                    isIntroze1 = data.getBooleanExtra("iu", false);
                    //if (isIntroze1) {
                    userId1 = data.getIntExtra("ui", 0) == 0 ? null : data.getIntExtra("ui", 0) + "";
                    //}
                    name1 = name;
                    //
                    //Log.d("IntrozePath", JFile.getRealPath(getActivity(), bitmap));
                    // Uri.parse(bitmap == null ? "" : JFile.getRealPath(getActivity(), bitmap));
                    loadImageFirst(uri);

                    /*if (bitmap == null) {
                        ivUser1.setImageResource(R.drawable.circle_gray);
                        tvUser1.setText(getTwoUpperChar(name1));
                        tvUser1.setVisibility(View.VISIBLE);
                    } else {
                        ivUser1.setImageBitmap(bitmap);
                        tvUser1.setVisibility(View.GONE);
                    }*/
                    isFirstSelected = true;
                    tvIntrozeFirst.setText(name);
                    phone1 = cNumber;
                    email1 = email;
                    contactId1 = data.getStringExtra("ci");
                    iid1 = id;
                } else if (reqCode == 1001) {
                    //
                    urlPic2 = urlPic;
                    //
                    _id2 = data.getLongExtra("_id", -1);
                    //
                    llSelector3.setBackgroundResource(R.drawable.layout_border_corner_right_selected);
                    tv3.setTextColor(getResources().getColor(android.R.color.white));
                    //
                    llSelector2.setBackgroundResource(R.drawable.layout_border_corner_center);
                    tv2.setTextColor(getResources().getColor(android.R.color.darker_gray));
                    //
                    isIntroze2 = data.getBooleanExtra("iu", false);
                    userId2 = data.getIntExtra("ui", 0) == 0 ? null : data.getIntExtra("ui", 0) + "";
                    contactId2 = data.getStringExtra("ci");
                    iid2 = id;
                    email2 = email;
                    name2 = name;
                    //
                    loadImageSecond(uri);
                    /*if (bitmap == null) {
                        ivUser2.setImageResource(R.drawable.circle_gray);
                        tvUser2.setText(getTwoUpperChar(name2));
                        tvUser2.setVisibility(View.VISIBLE);
                    } else {
                        ivUser2.setImageBitmap(bitmap);
                        tvUser2.setVisibility(View.GONE);
                    }*/
                    tvIntrozeSecond.setText(name);
                    //
                    phone2 = cNumber;
                    //
                    formContainer.setVisibility(View.VISIBLE);
                }
                //}
            }
        } else if (reqCode == 1003 && resultCode == getActivity().RESULT_OK) {
            clearScreen();
        }
    }

    //
    private void loadImageFirst(String uri) {
        //
        tvTopTitle.setText("Select Person 2");
        tvTopDescription.setText("Tap on Person 2 Circle below\nto search your contacts:");
        tvIntrozeSecond.setText("Choose\nPerson #2");
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvUser1.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                ivUser1.setBackgroundResource(R.drawable.circle_gray);
                tvUser1.setText(JUtils.getTwoUpperChar(name1));
                tvUser1.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(uri).build();
        ivUser1.setController(controller);
    }

    //
    private void loadImageSecond(String uri) {
        //
        llTopText.setVisibility(View.GONE);
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvUser2.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                ivUser2.setBackgroundResource(R.drawable.circle_gray);
                tvUser2.setText(JUtils.getTwoUpperChar(name2));
                tvUser2.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(uri).build();
        ivUser2.setController(controller);
    }


    //
    private void clearScreen() {
        //
        //Toast.makeText(getActivity(), "Screen Cleared", Toast.LENGTH_SHORT).show();
        //tvIntrozeSecond.setText("");
        //
        isFirstSelected = false;
        //
        _id1 = 0;
        _id2 = 0;
        //
        loadImageFirst(JFile.getUriFromResource(getActivity(), R.drawable.user_dummy_img).toString());
        loadImageSecond(JFile.getUriFromResource(getActivity(), R.drawable.user_dummy_img).toString());
        //
        ivUser1.setBackgroundResource(R.drawable.user_dummy_img);
        ivUser2.setBackgroundResource(R.drawable.user_dummy_img);
        //
        tvUser1.setText("");
        tvUser2.setText("");
        //
        tvIntrozeFirst.setText(getString(R.string.create_introze_dummy_user1));
        //tvIntrozeSecond.setText(getString(R.string.create_introze_dummy_user2));
        tvIntrozeSecond.setText("");
        //
        etReason.setText("");
        llReason.setVisibility(View.GONE);
        //
        formContainer.setVisibility(View.GONE);
        llSelected.setVisibility(View.GONE);
        //
        tvTopDescription.setText("Tap on Person 1 Circle below\nto search your contacts:");
        llTopText.setVisibility(View.VISIBLE);
        tvTopTitle.setText("Make an introduction!");
        //
    }

    //

    //
    public void initFirstPerson(Intent data) {
        String id = data.getStringExtra("id");//c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
        //
        //Bitmap bitmap = loadContactPhoto(getActivity().getContentResolver(), Long.parseLong(id));
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
        //
        String email = getEmailId(getActivity().getContentResolver(), id);
        // Toast.makeText(getActivity(), "" + email, Toast.LENGTH_SHORT).show();

        //String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
        String cNumber = null;
        //if (hasPhone.equalsIgnoreCase("1")) {
        Cursor phones = getActivity().getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                null, null);
        if (phones.moveToFirst() && phones.getCount() > 0)
            cNumber = phones.getString(phones.getColumnIndex("data1"));
        phones.close();

        //}
        String name = data.getStringExtra("fn");//c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        //

        _id1 = data.getLongExtra("_id", -1);
        //
        llSelector1.setBackgroundResource(R.drawable.layout_border_corner_left);
        tv1.setTextColor(getResources().getColor(android.R.color.darker_gray));
        //
        llSelector2.setBackgroundResource(R.drawable.layout_border_corner_center_selected);
        tv2.setTextColor(getResources().getColor(android.R.color.white));
        //
        isIntroze1 = data.getBooleanExtra("iu", false);
        //if (isIntroze1) {
        userId1 = data.getIntExtra("ui", 0) == 0 ? null : data.getIntExtra("ui", 0) + "";
        //}
        name1 = name;
        //
        //Log.d("IntrozePath", JFile.getRealPath(getActivity(), bitmap));
        // Uri.parse(bitmap == null ? "" : JFile.getRealPath(getActivity(), bitmap));
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvUser1.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                ivUser1.setBackgroundResource(R.drawable.circle_gray);
                tvUser1.setText(JUtils.getTwoUpperChar(name1));
                tvUser1.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(uri).build();
        ivUser1.setController(controller);

                    /*if (bitmap == null) {
                        ivUser1.setImageResource(R.drawable.circle_gray);
                        tvUser1.setText(getTwoUpperChar(name1));
                        tvUser1.setVisibility(View.VISIBLE);
                    } else {
                        ivUser1.setImageBitmap(bitmap);
                        tvUser1.setVisibility(View.GONE);
                    }*/
        isFirstSelected = true;
        tvIntrozeFirst.setText(name);
        phone1 = cNumber;
        email1 = email;
        contactId1 = data.getStringExtra("ci");
        iid1 = id;
        //
        HomeActivity.intent1 = null;
    }

    //
    //
    public void initSecondPerson(Intent data) {
        String id = data.getStringExtra("id");//c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
        //
        //Bitmap bitmap = loadContactPhoto(getActivity().getContentResolver(), Long.parseLong(id));
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
        //
        String email = getEmailId(getActivity().getContentResolver(), id);
        // Toast.makeText(getActivity(), "" + email, Toast.LENGTH_SHORT).show();

        //String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
        String cNumber = null;
        //if (hasPhone.equalsIgnoreCase("1")) {
        Cursor phones = getActivity().getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                null, null);
        if (phones.moveToFirst() && phones.getCount() > 0)
            cNumber = phones.getString(phones.getColumnIndex("data1"));
        phones.close();

        //}
        String name = data.getStringExtra("fn");//c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        //

        _id1 = data.getLongExtra("_id", -1);
        //
        llSelector1.setBackgroundResource(R.drawable.layout_border_corner_left);
        tv1.setTextColor(getResources().getColor(android.R.color.darker_gray));
        //
        llSelector2.setBackgroundResource(R.drawable.layout_border_corner_center_selected);
        tv2.setTextColor(getResources().getColor(android.R.color.white));
        //
        isIntroze1 = data.getBooleanExtra("iu", false);
        //if (isIntroze1) {
        userId1 = data.getIntExtra("ui", 0) == 0 ? null : data.getIntExtra("ui", 0) + "";
        //}
        name1 = name;
        //
        //Log.d("IntrozePath", JFile.getRealPath(getActivity(), bitmap));
        // Uri.parse(bitmap == null ? "" : JFile.getRealPath(getActivity(), bitmap));
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvUser1.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                ivUser1.setBackgroundResource(R.drawable.circle_gray);
                tvUser1.setText(JUtils.getTwoUpperChar(name1));
                tvUser1.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(uri).build();
        ivUser1.setController(controller);

                    /*if (bitmap == null) {
                        ivUser1.setImageResource(R.drawable.circle_gray);
                        tvUser1.setText(getTwoUpperChar(name1));
                        tvUser1.setVisibility(View.VISIBLE);
                    } else {
                        ivUser1.setImageBitmap(bitmap);
                        tvUser1.setVisibility(View.GONE);
                    }*/
        isFirstSelected = true;
        tvIntrozeFirst.setText(name);
        phone1 = cNumber;
        email1 = email;
        contactId1 = data.getStringExtra("ci");
        iid1 = id;
        //
        HomeActivity.intent1 = null;
    }

    //
    /*private String getTwoUpperChar(String name) {
        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }*/

    //
    private String getEmailId(ContentResolver cr, String id) {
        Cursor emailCur = cr.query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{id}, null);
        //
        String email = "";
        //
        while (emailCur.moveToNext()) {
            // This would allow you get several email addresses
            // if the email addresses were stored in an array
            email = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            /*String emailType = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));*/
        }
        emailCur.close();
        return email;
    }

    //
    public Bitmap loadContactPhoto(ContentResolver cr, long id) {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null) {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), SelectContactActivity.class);//new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        intent.putExtra("is_from_create_account", true);
        if (v.getId() == R.id.rlFirst) {
            //if (spUser.getBoolean("is_contact_sync", false)) {
            //intent.putExtra("title", "CHOOSE PERSON #1");
            //startActivityForResult(intent, 1000);
            ((HomeActivity) getActivity()).onCreateClicked(0, true);
            //} else
            //Toast.makeText(getActivity(), "Contacts is syncing please wait...", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.rlSecond) {
            if (!isFirstSelected) {
                builderCreateIntrozeSelDialog.setErrorMessage("Alert", "Please select person #1");
                createIntrozeSelDialog.show();
                TypefaceHelper.typeface((Button) createIntrozeSelDialog.getButton(DialogInterface.BUTTON_POSITIVE));
                return;
            }
            //if (spUser.getBoolean("is_contact_sync", false)) {
            /*intent.putExtra("title", "CHOOSE PERSON #2");
            startActivityForResult(intent, 1001);*/
            ((HomeActivity) getActivity()).onCreateClicked(0, false);
            //} else
            //Toast.makeText(getActivity(), "Contacts is syncing please wait...", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.btnCreateIntroze) {
            //createIntroze();
            if (_id1 == _id2) {
                builderError.setErrorMessage("Alert", "Please select different persons to create Introze.");
                adError.show();
                TypefaceHelper.typeface((Button) adError.getButton(DialogInterface.BUTTON_POSITIVE));
                return;
            }
            //
            String reason = etReason.getText().toString();
            if (TextUtils.isEmpty(reason)) {
                builderError.setErrorMessage("Please enter reason");
                adError.show();
                TypefaceHelper.typeface((Button) adError.getButton(DialogInterface.BUTTON_POSITIVE));
                return;
            }
            //
            Intent intent1 = new Intent(getActivity(), NotificationMethodActivity.class);
            String[] users = {iid1, name1, userId1, contactId1, phone1, email1, iid2, name2, userId2, contactId2,
                    phone2, email2};
            intent1.putExtra("is_user1_introze", isIntroze1);
            intent1.putExtra("is_user2_introze", isIntroze2);
            intent1.putExtra("users", users);
            intent1.putExtra("selected_index", selectIndex);
            intent1.putExtra("reason", reason);
            //
            intent1.putExtra("_id1", _id1);
            intent1.putExtra("_id2", _id2);
            //
            intent1.putExtra("url_profile1", urlPic1);
            intent1.putExtra("url_profile2", urlPic2);
            //
            startActivityForResult(intent1, 1003);
        }
        //
        else if (v.getId() == R.id.llB) {
            llReason.setVisibility(View.VISIBLE);
            llSelected.setVisibility(View.GONE);
            llSelected = llCategories[0];
            llSelected.setVisibility(View.VISIBLE);
            selectIndex = 0;
            etReason.requestFocus();
        } else if (v.getId() == R.id.llD) {
            llReason.setVisibility(View.VISIBLE);
            llSelected.setVisibility(View.GONE);
            llSelected = llCategories[1];
            llSelected.setVisibility(View.VISIBLE);
            selectIndex = 1;
            etReason.requestFocus();
        } else if (v.getId() == R.id.llF) {
            llReason.setVisibility(View.VISIBLE);
            llSelected.setVisibility(View.GONE);
            llSelected = llCategories[2];
            llSelected.setVisibility(View.VISIBLE);
            selectIndex = 2;
            etReason.requestFocus();
        } else if (v.getId() == R.id.llM) {
            llReason.setVisibility(View.VISIBLE);
            llSelected.setVisibility(View.GONE);
            llSelected = llCategories[3];
            llSelected.setVisibility(View.VISIBLE);
            selectIndex = 3;
            etReason.requestFocus();
        }
    }

    //
    private class CreateIntrozeSelDialog extends AlertDialog.Builder implements View.OnClickListener {

        public CreateIntrozeSelDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_introze_sel_vali, null);
            TextView tvEdValidation = (TextView) view.findViewById(R.id.tvEdValidation);
            setView(view, 50, 0, 50, 0);
            //

            //
            view.findViewById(R.id.btnYay).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            createIntrozeSelDialog.dismiss();

        }
    }
    //

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1003 && resultCode == getActivity().RESULT_OK) {
            clearScreen();
        }
    }
}
