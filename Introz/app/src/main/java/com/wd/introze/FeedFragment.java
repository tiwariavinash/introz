package com.wd.introze;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.FutureBuilder;
import com.koushikdutta.ion.builder.UrlEncodedBuilder;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.feed.FeedAdapter;
import com.wd.introze.feed.FeedModel;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.pagination.EndlessScrollListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by flair on 21-07-2016
 */
public class FeedFragment extends Fragment implements AdapterView.OnItemClickListener {
    //
    private SharedPreferences spUser;
    //
    private ProgressDialog pd;
    //
    private ArrayList<FeedModel> data;
    private FeedAdapter adapter;

    //
    TextView tvTextFeed, tvTextAfterFeed;
    //
    String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    CustomProgress customProgress;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private FeedProfileActivity.OnProfileClickListener profileClickListener;
    //
    private int totalPage;
    private int page = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    //

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, null);
        //
        tfText = Typeface.createFromAsset(getActivity().getAssets(), fontPathText);
        //
        tvTextFeed = (TextView) view.findViewById(R.id.tvTextFeed);
        tvTextAfterFeed = (TextView) view.findViewById(R.id.tvTextAfterFeed);
        //
        tvTextFeed.setTypeface(tfText);
        tvTextAfterFeed.setTypeface(tfText);
        //
        data = new ArrayList<>();
        /*data.add(new FeedModel(FeedType.BUSINESS));
        data.add(new FeedModel(FeedType.DATING));
        data.add(new FeedModel(FeedType.MUTUAL_FRIEND));
        data.add(new FeedModel(FeedType.FRIENDSHIP));*/
        //
        adapter = new FeedAdapter(getActivity(), data);
        //
        ListView lv = (ListView) view.findViewById(R.id.lv);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        //
        // Attach the listener to the AdapterView onCreate
        lv.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                ++page;
                if (page <= totalPage) {
                    getFeedsFromServer(page);
                    return true;
                }// or customLoadMoreDataFromApi(totalItemsCount);
                // ONLY if more data is actually being loaded; false otherwise.
                return false;
            }
        });
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait...");
        //
        builderError = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        getFeedsFromServer(page);

        return view;
    }

    //
    private void getFeedsFromServer(final int page) {
        //Toast.makeText(getActivity(), "" + page, Toast.LENGTH_SHORT).show();
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        //pd.show();
        customProgress = CustomProgress.show(getActivity(), null, false, true, null);
        UrlEncodedBuilder builder = Ion.with(this).load("POST", AppUrls.MAIN + "/feeds").setHeader("Authorization", "Basic " + basic);
        if (page != 0)
            builder.setBodyParameter("page", page + "");
        ((FutureBuilder) builder).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                // pd.dismiss();
                customProgress.dismiss();
                Log.d("FeedsResult", result + "");
                if (result != null) {
                    //
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat sdfDot = new SimpleDateFormat("MM.dd.yy");
                    //
                    try {
                        JSONObject joResult = new JSONObject(result);
                        //
                        FeedFragment.this.page = joResult.getInt("page");
                        FeedFragment.this.totalPage = joResult.getInt("total_pages");
                        //
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            if (joResult.has("feeds")) {
                                JSONArray joFeeds = joResult.getJSONArray("feeds");
                                for (int i = 0; i < joFeeds.length(); i++) {
                                    JSONObject joItem = joFeeds.getJSONObject(i);
                                    //
                                    Date d = null;
                                    try {
                                        d = sdf.parse(joItem.getString("posted_on"));
                                    } catch (ParseException e1) {

                                    }
                                    String dotDate = null;
                                    if (d == null) {

                                    } else {
                                        dotDate = sdfDot.format(d);
                                    }
                                    //String
                                    boolean isUser = Integer.parseInt(spUser.getString("user_id", "0")) == joItem.getInt("user_id");
                                    //
                                    data.add(new FeedModel(joItem.getInt("user_id"), joItem.getString("first_name"), joItem.getString("last_name"),
                                            joItem.getString("status_message"), dotDate, joItem.getInt("intro_type"),
                                            joItem.getString("image_url"), isUser, joItem.getString("fb_id"),
                                            joItem.getString("identifier")));
                                }
                                adapter.notifyDataSetChanged();
                                if (data.size() > 0) {
                                    tvTextFeed.setVisibility(View.GONE);
                                    tvTextAfterFeed.setVisibility(View.VISIBLE);
                                }
                            }/* else
                                Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();*/
                        } else
                            Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e1) {
                        //Toast.makeText(getActivity(), "" + e1, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    customProgress.dismiss();
                    //Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //
        FeedModel model = data.get(position);
        //
        if (!model.isUser) {
            //
            profileClickListener.onProfileClicked(model);
            /*Intent intent = new Intent(getActivity(), FeedProfileActivity.class);
            intent.putExtra("user_id", model.userid + "");
            intent.putExtra("feed_model", new Gson().toJson(model));
            startActivity(intent);*/
        }
    }
    //

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        profileClickListener = (FeedProfileActivity.OnProfileClickListener) context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (customProgress != null) customProgress.dismiss();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (customProgress != null) customProgress.dismiss();
    }
}
