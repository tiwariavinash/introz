package com.wd.introze.match;

import android.content.Context;
import android.graphics.drawable.Animatable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.wd.introze.R;

import java.util.ArrayList;

/**
 * Created by flair on 29-07-2016
 */
public class MatchAdapter extends RecyclerView.Adapter<MatchVH> {
    //
    private ArrayList<MatchModel> data;
    private Context context;
    //
    public static OnMatchClickListener listener;

    public MatchAdapter(Context context, ArrayList<MatchModel> data, OnMatchClickListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @Override
    public MatchVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MatchVH(LayoutInflater.from(context).inflate(R.layout.fragment_my_match_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final MatchVH holder, final int position) {
        //
        MatchModel model = data.get(position);
        //
        holder.tvFullName.setText(data.get(position).firstName);
        //
        if (model.isRead) {
            holder.llCircleRed.setVisibility(View.GONE);
        } else {
            holder.llCircleRed.setVisibility(View.VISIBLE);
        }
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                holder.tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                holder.tvShortName.setText(data.get(position).getTwoUpperChar());
                holder.tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(data.get(position).url).build();
        holder.iv.setController(controller);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    //
    public interface OnMatchClickListener {
        void onMatchClicked(int position);
    }
}
