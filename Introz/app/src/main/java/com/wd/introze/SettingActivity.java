package com.wd.introze;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JErrorTextWatcher;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.jutil.Validate;
import com.wd.introze.repository.server.rf.RFClient;
import com.wd.introze.repository.server.rf.RFService;
import com.wd.introze.repository.server.rf.model.user.Email;
import com.wd.introze.repository.server.rf.model.user.User;
import com.wd.introze.repository.server.rf.model.user.UserDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by flair on 20-07-2016
 */
public class SettingActivity extends Fragment {
    private static final String TAG = "SettingActivity";
    //
    private RFService rfService;
    //
    private EditText etFirstName, etLastName, etPassword;
    //
    private TextView tvEmail;
    private TextView tvPhone;

    LinearLayout linLayNotification, linLayPrivacyPolicy, linLayTermsOfService, linLayInviteFriend;
    //
    LinearLayout linLayContactUs, linLayManageContacts;
    //
    private SharedPreferences spUser;
    //
    private AlertDialog adEmail;
    //
    private LinearLayout llEmailContainer;

    //
    private ArrayList<EmailModel> alEmails = new ArrayList<>();
    //
    TextView tvSendMoney, tvReceiveMoney, tvNotification, tvPrivacyPolicy, tvTermsOfService, tvContactUs, tvManageContacts;
    //
    private OnSettingClickListener settingClickListener;
    //
    private String firstName, lastName, email, phone;
    //
    private User user;
    //
    private CustomProgress pd;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;

    private class EmailModel {
        //
        TextView tvEmail;
        //
        String email;
        int id;

        public EmailModel(String email, int id) {
            this.email = email;
            this.id = id;
        }
    }

    //


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_settings, null);
        TypefaceHelper.typeface(view);
        //
        rfService = RFClient.getClient().create(RFService.class);
        //
        etFirstName = (EditText) view.findViewById(R.id.etFirstName);
        etFirstName.setText(firstName);
        //
        etLastName = (EditText) view.findViewById(R.id.etLastName);
        etLastName.setText(lastName);
        //
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        //
        tvEmail = (TextView) view.findViewById(R.id.tvEmail);
        tvEmail.setText(email);
        //
        tvPhone = (TextView) view.findViewById(R.id.tvPhone);
        tvPhone.setText(phone);
        //
        linLayInviteFriend = (LinearLayout) view.findViewById(R.id.linLayInviteFriend);
        linLayInviteFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingClickListener.onInviteFriendsClicked();
            }
        });
        //
        linLayNotification = (LinearLayout) view.findViewById(R.id.linLayNotification);
        linLayNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                settingClickListener.onNotificationClicked();
                /*Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);*/
            }
        });
        //
        linLayPrivacyPolicy = (LinearLayout) view.findViewById(R.id.linLayPrivacyPolicy);
        linLayPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(getActivity(), PrivacyPolicyActivity.class);
                startActivity(intent);*/
                settingClickListener.onPrivacyPolicyClicked();
            }
        });
        //
        linLayTermsOfService = (LinearLayout) view.findViewById(R.id.linLayTermsOfService);
        linLayTermsOfService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(getActivity(), TermsOfServicesActivity.class);
                startActivity(intent);*/
                settingClickListener.onTermsOfServiceClicked();
            }
        });
        //
        linLayContactUs = (LinearLayout) view.findViewById(R.id.linLayContactUs);
        linLayContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingClickListener.onContactUsClicked();
                /*Intent intent = new Intent(getActivity(), ContactUsActivity.class);
                startActivity(intent);*/
            }
        });
        //
        linLayManageContacts = (LinearLayout) view.findViewById(R.id.linLayManageContacts);
        linLayManageContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(getActivity(), ManageContactsActivity.class);
                startActivity(intent);*/
                settingClickListener.onManageContactClicked();
            }
        });
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        adEmail = new AddEmailDialog(getActivity()).create();
        adEmail.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        llEmailContainer = (LinearLayout) view.findViewById(R.id.llEmailContainer);
        //
        tvSendMoney = (TextView) view.findViewById(R.id.tvSendMoney);
        tvReceiveMoney = (TextView) view.findViewById(R.id.tvReceiveMoney);
        //
        tvNotification = (TextView) view.findViewById(R.id.tvNotification);
        tvPrivacyPolicy = (TextView) view.findViewById(R.id.tvPrivacyPolicy);
        tvTermsOfService = (TextView) view.findViewById(R.id.tvTermsOfServices);
        //
        tvContactUs = (TextView) view.findViewById(R.id.tvContactUs);
        tvManageContacts = (TextView) view.findViewById(R.id.tvManageContacts);
        //
        Button btnLogout = (Button) view.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingClickListener.onLogoutClicked();
            }
        });

        //
        view.findViewById(R.id.llAddEmail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTvAddEmail(v);
            }
        });
        view.findViewById(R.id.llReceiveMoney).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTvReceiveMoney(v);
            }
        });
        view.findViewById(R.id.llSendMoney).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickEtSendMoney(v);
            }
        });
        view.findViewById(R.id.ivClearFirstName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFirstName(v);
            }
        });
        view.findViewById(R.id.ivClearLastName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearLastName(v);
            }
        });
        Button btnUpdate = (Button) view.findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = etFirstName.getText().toString();
                if (TextUtils.isEmpty(firstName)) {
                    builderError.setErrorMessage("Alert", "Please Enter First Name");
                    adError.show();
                    TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                    return;
                }
                String lastName = etLastName.getText().toString();
                if (TextUtils.isEmpty(lastName)) {
                    builderError.setErrorMessage("Alert", "Please Enter Last Name");
                    adError.show();
                    TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                    return;
                }
                updateInfo(v);
            }
        });
        //
        Log.d(TAG, "Local Response " + spUser.getString(User.class.getName(), null));
        //
        user = new Gson().fromJson(spUser.getString(User.class.getName(), null), User.class);
        //
        Log.d(TAG, "fb_id " + user.userDetails.fbId);
        //

        if (!TextUtils.equals(user.userDetails.fbId, "0") && !TextUtils.isEmpty(user.userDetails.fbId))
            etPassword.setFocusable(false);
        //
        builderError = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //btnUpdate.setTypeface(tfText3);
        //
        getProfileDetail();
        //getProfileDetailFromServer();
        //
        return view;
    }

    //
    private void addEmails() {
        //
        llEmailContainer.removeAllViews();
        for (final EmailModel memail : alEmails) {
            LinearLayout ll = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.activity_setting_email, null);
            TypefaceHelper.typeface(ll);
            final TextView tvEmail = (TextView) ll.findViewById(R.id.tvEmail);
            tvEmail.setText(memail.email);
            //
            memail.tvEmail = tvEmail;
            //
            ll.findViewById(R.id.ivClear).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //etEmail.setText("");
                    onDeleteEmail(memail.id);
                }
            });
            //
            llEmailContainer.addView(ll);
        }
    }

    //
    /*private void initHeader() {
        *//*TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("SETTINGS");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);*//*

    }*/

    private void onDeleteEmail(final int emailId) {
        Call<SuccessModel> call = rfService.onDeleteEmail(JUtils.getHeader(spUser), emailId);
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        call.enqueue(new Callback<SuccessModel>() {
            @Override
            public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                pd.dismiss();
                //
                if (response.isSuccessful()) {
                    for (int i = 0; i < user.userDetails.emails.size(); i++) {
                        Email em = user.userDetails.emails.get(i);
                        if (em.id == emailId) {
                            user.userDetails.emails.remove(i);
                            break;
                        }
                    }
                    //
                    //spUser.edit().putString(User.class.getName(), new Gson().toJson(user)).commit();
                    afterUpdate();
                } else {
                    builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();
                    TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                }
            }

            @Override
            public void onFailure(Call<SuccessModel> call, Throwable t) {
                pd.dismiss();
                //
                builderError.setErrorMessage(getString(R.string.error_no_response));
                adError.show();
                TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
            }
        });
    }

    //
    public void clearFirstName(View view) {
        etFirstName.setText("");
    }

    public void clearLastName(View view) {
        etLastName.setText("");
    }

    //
    public void updateInfo(View view) {
        //
        JUtils.hideSoftKeyboard(getActivity());
        //
        if (!JConnectionUtils.isConnectedToInternet(getActivity())) {
            builderError.setErrorMessage("No internet connection is available");
            adError.show();
            TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
            return;
        }
        //
        new UpdateAsyncTask().execute(etFirstName.getText().toString(), etLastName.getText().toString(),
                etPassword.getText().toString());
        /*pd = CustomProgress.show(getActivity(), null, false, false, null);
        //
        i = i + alEmails.size();
        //
        String password = etPassword.getText().toString();
        if (!TextUtils.isEmpty(password)) {
            i = i + 1;
            changePassword(password);
        }
        //
        updateFirstName();
        updateLastName();
        for (int i = 0; i < alEmails.size(); i++) {
            updateEmail(i);
        }*/
        //updatePassword();
    }

    //
    private void changePassword(String password) {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/change_password").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("password", password).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                etPassword.setText("");
                --i;
                if (i == 0) {
                    pd.dismiss();
                    //getProfileDetailFromServer();
                    afterUpdate();
                }
            }
        });
    }

    //
    int i = 2;

    //
    private void updateFirstName() {
        final String fn = etFirstName.getText().toString();
        if (TextUtils.isEmpty(fn)) {
            --i;
            return;
        }
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("PUT", AppUrls.MAIN + "/update_first_name").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("first_name", fn).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("FirstNameU", result + "");
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals(joResult.getString("result"), "success")) {
                            user.userDetails.firstName = fn;
                            --i;
                            if (i == 0) {
                                pd.dismiss();
                                afterUpdate();
                                //getProfileDetailFromServer();
                            }
                        } else {
                            updateFirstName();
                        }
                    } catch (JSONException e1) {
                        --i;
                        if (i == 0) {
                            pd.dismiss();
                            afterUpdate();
                            //getProfileDetailFromServer();
                        }
                        //updateFirstName();
                    }
                } else {
                    updateFirstName();
                }
            }
        });
    }

    //
    private void afterUpdate() {
        spUser.edit().putString(User.class.getName(), new Gson().toJson(user)).commit();
        getProfileDetail();
    }

    //
    private void updateLastName() {
        final String ln = etLastName.getText().toString();
        if (TextUtils.isEmpty(ln)) {
            --i;
            return;
        }
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("PUT", AppUrls.MAIN + "/update_last_name").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("last_name", ln).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                /*--i;

                if (i == 0) getProfileDetailFromServer();*/
                Log.d("LastNameU", "" + result);
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals(joResult.getString("result"), "success")) {
                            user.userDetails.lastName = ln;
                            --i;
                            if (i == 0) {
                                pd.dismiss();
                                afterUpdate();
                                //getProfileDetailFromServer();
                            }
                        } else {
                            updateLastName();
                        }
                    } catch (JSONException e1) {
                        --i;
                        if (i == 0) {
                            pd.dismiss();
                            afterUpdate();
                            //getProfileDetailFromServer();
                        }
                        //updateFirstName();
                    }
                } else {
                    updateLastName();
                }
            }
        });
    }

    //
    private void getProfileDetail() {
        //
        UserDetails userDetail = user.userDetails;
        //
        String fn = userDetail.firstName;
        String ln = userDetail.lastName;
        //
        etFirstName.setText(fn);
        etLastName.setText(ln);
        //
        String phone = userDetail.phone;
        String email = userDetail.email;
        tvEmail.setText(email);
        tvPhone.setText(phone);
        //
        alEmails.clear();
        //
        for (Email email1 : userDetail.emails) {
            //
            String mEmail = email1.email;
            Integer id = email1.id;
            //
            alEmails.add(new EmailModel(mEmail, id));
        }
        addEmails();
        //
        /*if (joUserDetail.optJSONArray("emails") != null) {
            JSONArray jaEmails = joUserDetail.getJSONArray("emails");
            alEmails.clear();
            for (int i = 0; i < jaEmails.length(); i++) {
                JSONObject joE = jaEmails.getJSONObject(i);
                alEmails.add(new EmailModel(joE.getString("email"), joE.getInt("id")));
            }
            addEmails();
        }*/
    }

    //
    private void getProfileDetailFromServer() {
        //
        /*Log.d("userid", spUser.getString("user_id", ""));
        Log.d("usertoken", spUser.getString("user_token", ""));*/
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/profile_tab").setHeader("Authorization", "Basic " + basic)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("ProfileResult", result + "");
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //
                           /* String inviteCode = joResult.getString("invite_code");
                            Log.d("inviteCodePro", "" + inviteCode);*/
                            //
                            JSONObject joUserDetail = joResult.getJSONObject("user_details");
                            //
                            String fn = joUserDetail.getString("first_name");
                            String ln = joUserDetail.getString("last_name");
                            //
                            //etFirstName.setText(fn);
                            //etLastName.setText(ln);
                            //
                            String phone = joUserDetail.getString("phone");
                            String email = joUserDetail.getString("email");
                            //tvEmail.setText(email);
                            //tvPhone.setText(phone);
                            //
                            //
                            UserDetails userDetails = user.userDetails;
                            List<Email> emails = userDetails.emails;
                            //
                            if (joUserDetail.optJSONArray("emails") != null) {
                                JSONArray jaEmails = joUserDetail.getJSONArray("emails");
                                alEmails.clear();
                                emails.clear();
                                for (int i = 0; i < jaEmails.length(); i++) {
                                    //
                                    JSONObject joE = jaEmails.getJSONObject(i);
                                    //
                                    String mEmail = joE.getString("email");
                                    Integer id = joE.optInt("id");
                                    //
                                    alEmails.add(new EmailModel(mEmail, id));
                                    emails.add(new Email(mEmail, id));
                                }
                                //addEmails();
                            }
                            //
                            userDetails.firstName = fn;
                            userDetails.lastName = ln;
                            userDetails.phone = phone;
                            userDetails.email = email;
                            //
                            spUser.edit().putString(User.class.getName(), new Gson().toJson(user)).commit();
                            //
                            Log.d(TAG, "MyFormat " + new Gson().toJson(user));
                            //
                            getProfileDetail();
                            //
                            //Toast.makeText(SettingActivity.this, "Information updated successfully", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e1) {
                        Toast.makeText(getActivity(), "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    //Log.d("ProfileFragment", "" + result);
                } else {
                    Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    public void onClickEtSendMoney(View view) {
        startActivity(new Intent(getActivity(), SendMoneyActivity.class));
    }

    //
    public void onClickTvReceiveMoney(View view) {
        startActivity(new Intent(getActivity(), ReceiveMoneyActivity.class));
    }

    /*private EditText etEmail;
    private TextView tvText1, tvText2, tvErrorEmail;*/

    //
    /*private class AddEmailDialog extends AlertDialog.Builder implements DialogInterface.OnClickListener {


        public AddEmailDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_add_email_new, null);
            TypefaceHelper.typeface(view);
            //
            setView(view);
            //
            etEmail = (EditText) view.findViewById(R.id.etEmail);
            tvErrorEmail = (TextView) view.findViewById(R.id.tvEmailError);
            etEmail.addTextChangedListener(new JErrorTextWatcher(tvErrorEmail, "Please enter email"));
            //
            tvText2 = (TextView) view.findViewById(R.id.tvText2);
            //
            setPositiveButton("add", this);
            setNegativeButton("cancel", this);
        }


        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    String email = etEmail.getText().toString();
                    if (TextUtils.isEmpty(email)) {
                        tvErrorEmail.setText("Please enter email");
                    } else if (!Validate.isEmailValid(email)) {
                        tvErrorEmail.setText("Please Enter Valid Email");
                    } else {
                        adEmail.dismiss();
                        addEmail(email);
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    adEmail.dismiss();
                    break;
            }
        }
    }*/
    //
    private class AddEmailDialog extends AlertDialog.Builder implements View.OnClickListener {

        EditText etEmail;
        TextView tvText1, tvText2, tvErrorEmail;

        public AddEmailDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_add_email, null);
            TypefaceHelper.typeface(view);
            //
            setView(view, 80, 0, 80, 0);
            //
            etEmail = (EditText) view.findViewById(R.id.etEmail);
            tvErrorEmail = (TextView) view.findViewById(R.id.tvEmailError);
            etEmail.addTextChangedListener(new JErrorTextWatcher(tvErrorEmail, "Please Enter Email Address"));
            //
            tvText1 = (TextView) view.findViewById(R.id.tvText1);
            //
            tvText2 = (TextView) view.findViewById(R.id.tvText2);
            //
            //tvText1.setTypeface(tfText1);
            //
            //tvText2.setTypeface(tfText);
            //
            view.findViewById(R.id.btnAdd).setOnClickListener(this);
            view.findViewById(R.id.btnCancel).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnAdd:
                    String email = etEmail.getText().toString();
                    if (TextUtils.isEmpty(email)) {
                        tvErrorEmail.setText("Please Enter Email Address");
                    } else if (!Validate.isEmailValid(email)) {
                        tvErrorEmail.setText("Please Enter Valid Email Address");
                    } else {
                        adEmail.dismiss();
                        addEmail(email);
                    }
                    break;
                case R.id.btnCancel:
                    adEmail.dismiss();
                    break;
            }
        }
    }

    //
    public void onClickTvAddEmail(View view) {
        adEmail.show();
    }

    //
    private void addEmail(String email) {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        pd = CustomProgress.show(getActivity(), null, false, true, null);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/add_email").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("email", email).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("SettingActivityResult", result + "");
                pd.dismiss();
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            //adEmail.dismiss();
                            getProfileDetailFromServer();
                        } else if (joResult.has("message")) {
                            builderError.setErrorMessage(joResult.getString("message"));
                            adError.show();
                            TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                        } else {
                            builderError.setErrorMessage(getString(R.string.error_no_response));
                            adError.show();
                            TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                        }
                        //Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e1) {
                        builderError.setErrorMessage(getString(R.string.error_no_response));
                        adError.show();
                        TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                        //Toast.makeText(getActivity(), "" + e1, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();
                    TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                    //Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private void updateEmail(final int position) {
        //
        final EmailModel model = alEmails.get(position);
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/update_email").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("email", model.tvEmail.getText().toString()).setBodyParameter("email_id", model.id + "")
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //
                Log.d(TAG, "Email Update " + result);
                //
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals(joResult.getString("result"), "success")) {
                            for (Email em : user.userDetails.emails) {
                                if (em.id == model.id) {
                                    em.email = model.email;
                                    break;
                                }
                            }
                            --i;
                            if (i == 0) {
                                pd.dismiss();
                                afterUpdate();
                                //getProfileDetailFromServer();
                            }
                        } else {
                            updateEmail(position);
                        }
                    } catch (JSONException e1) {
                        //
                        --i;
                        if (i == 0) {
                            pd.dismiss();
                            afterUpdate();
                            //getProfileDetailFromServer();
                        }
                        //updateFirstName();
                    }
                } else {
                    updateEmail(position);
                }
                //
               /* --i;
                if (i == 0) {
                    //
                    pd.dismiss();
                    afterUpdate();
                    //getProfileDetailFromServer();
                }*/
            }
        });
    }
    //

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        settingClickListener = (OnSettingClickListener) context;
    }

    public void onClickBtnLogout(View view) {
        //spUser.edit().putBoolean("is_verified", false).commit();
        /*setResult(RESULT_OK);
        finish();*/
    }

    //
    //
    public interface OnSettingClickListener {
        void onNotificationClicked();

        void onContactUsClicked();

        //
        void onLogoutClicked();

        void onManageContactClicked();

        void onPrivacyPolicyClicked();

        void onTermsOfServiceClicked();

        void onInviteFriendsClicked();
    }

    //
    private class UpdateAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pd == null) pd = CustomProgress.show(getActivity(), null, false, true, null);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            //
            if (!TextUtils.isEmpty(params[0]))
                updateFN(params[0]);
            if (!TextUtils.isEmpty(params[1]))
                updateLN(params[1]);
            if (!TextUtils.isEmpty(params[2]))
                updatePassword(params[2]);
            /*for (EmailModel emailModel : alEmails)
                updateE(emailModel);*/
            //
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            etPassword.setText("");
            spUser.edit().putString(User.class.getName(), new Gson().toJson(user)).commit();
            getProfileDetail();
            pd.dismiss();
        }
        //
    }

    //
    private void updateFN(String fn) {
        try {
            String result = Ion.with(this).load("PUT", AppUrls.MAIN + "/update_first_name")
                    .setHeader("Authorization", JUtils.getHeader(spUser)).setBodyParameter("first_name", fn).asString().get();
            //
            JSONObject joResult = new JSONObject(result);
            if (TextUtils.equals(joResult.getString("result"), "success")) {
                user.userDetails.firstName = fn;
            } else {

            }
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (JSONException e) {

        }
    }

    //
    private void updateLN(String ln) {
        try {
            String result = Ion.with(this).load("PUT", AppUrls.MAIN + "/update_last_name")
                    .setHeader("Authorization", JUtils.getHeader(spUser)).setBodyParameter("last_name", ln).asString().get();
            //
            JSONObject joResult = new JSONObject(result);
            if (TextUtils.equals(joResult.getString("result"), "success")) {
                user.userDetails.lastName = ln;
            } else {

            }
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (JSONException e) {

        }
    }

    //
    private void updatePassword(String password) {
        try {
            String result = Ion.with(this).load("POST", AppUrls.MAIN + "/change_password")
                    .setHeader("Authorization", JUtils.getHeader(spUser)).setBodyParameter("password", password).asString().get();
            //
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        }
    }

    //
    private void updateE(EmailModel model) {
        //
        try {
            String result = Ion.with(this).load("POST", AppUrls.MAIN + "/update_email").setHeader("Authorization",
                    JUtils.getHeader(spUser)).setBodyParameter("email", model.tvEmail.getText().toString())
                    .setBodyParameter("email_id", model.id + "").asString().get();
            //
            JSONObject joResult = new JSONObject(result);
            if (TextUtils.equals(joResult.getString("result"), "success")) {
                for (Email em : user.userDetails.emails) {
                    if (em.id == model.id) {
                        model.email = model.tvEmail.getText().toString();
                        em.email = model.email;
                        break;
                    }
                }
            } else {

            }
        } catch (InterruptedException e) {

        } catch (ExecutionException e) {

        } catch (JSONException e) {

        }
    }
}
