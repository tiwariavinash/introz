package com.wd.introze;

import android.graphics.Typeface;

import com.activeandroid.ActiveAndroid;
import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.norbsoft.typefacehelper.TypefaceCollection;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.quickblox.sample.chat.App;

/**
 * Created by Gajendra on 8/3/2016
 */
public class IntrozApplication extends App {
    @Override
    public void onCreate() {
        super.onCreate();
        //
        ActiveAndroid.initialize(this, false);
        //
        Fresco.initialize(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        // Initialize typeface helper
        TypefaceCollection typeface = new TypefaceCollection.Builder()
                .set(Typeface.NORMAL, Typeface.createFromAsset(getAssets(), "sourcesanspro-light.ttf"))
                .set(Typeface.BOLD, Typeface.createFromAsset(getAssets(), "itcavantgardestd-bold.ttf"))
                .create();
        TypefaceHelper.init(typeface);
        //
    }
}
