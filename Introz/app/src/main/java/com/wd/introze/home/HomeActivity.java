package com.wd.introze.home;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.activeandroid.query.Select;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.BaseServiceException;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.chat.ui.activity.ChatActivity;
import com.quickblox.sample.chat.utils.Consts;
import com.quickblox.sample.chat.utils.SharedPreferencesUtil;
import com.quickblox.sample.chat.utils.chat.ChatHelper;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.wd.introze.AppUrls;
import com.wd.introze.ContactUsActivity;
import com.wd.introze.CreateIntrozeFragment;
import com.wd.introze.FeedFragment;
import com.wd.introze.FeedProfileActivity;
import com.wd.introze.FragmentActivity;
import com.wd.introze.IntroductionTypeActivity;
import com.wd.introze.InviteFriendsActivity;
import com.wd.introze.LoginActivity;
import com.wd.introze.ManageContactsActivity;
import com.wd.introze.MatchDoneActivity;
import com.wd.introze.NewIntrozeInvitationActivity;
import com.wd.introze.PrivacyPolicyActivity;
import com.wd.introze.ProfileFragment;
import com.wd.introze.R;
import com.wd.introze.SettingActivity;
import com.wd.introze.SettingActivity.OnSettingClickListener;
import com.wd.introze.TermsOfServicesActivity;
import com.wd.introze.activities.ActivityModel;
import com.wd.introze.activities.byu.ByUModel;
import com.wd.introze.async.BadgeCountAsyncTask;
import com.wd.introze.feed.FeedAdapter;
import com.wd.introze.feed.FeedModel;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JSession;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.match.MatchFragment;
import com.wd.introze.mysyncadapter.SyncUtils;
import com.wd.introze.notification.NotificationActivity;
import com.wd.introze.payment.SendThankUActivity;
import com.wd.introze.pushnotification.GCMUtils;
import com.wd.introze.repository.server.rf.model.badge.BadgeUnreadCount;
import com.wd.introze.repository.server.rf.model.user.Email;
import com.wd.introze.repository.server.rf.model.user.User;
import com.wd.introze.repository.server.rf.model.user.UserDetails;
import com.wd.introze.selectcontact.SelectAdapter;
import com.wd.introze.selectcontact.SelectContactActivity;
import com.wd.introze.tables.TableContacts;
import com.wd.introze.tables.TableEmail;
import com.wd.introze.tables.TablePhone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Created by flair on 19-07-2016
 */
public class HomeActivity extends AppCompatActivity implements FeedAdapter.OnFeedBtnAddClickListener,
        FragmentActivity.OnAfterForULoadListener, OnSettingClickListener, ProfileFragment.OnStatusClickListener
        , FeedProfileActivity.OnProfileClickListener, IntroductionTypeActivity.OnIntroTypeClickListener,
        SelectAdapter.ContactClickListener {
    //
    private int selectedCurrentFragment = 2;
    //
    public static boolean isFromNotification;
    //
    private ArrayList<String> backstack = new ArrayList<>();
    //
    private String deviceToken = null;
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.example.android.datasync.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "example.com";
    // The account name
    public static final String ACCOUNT = "dummyaccount";
    //
    private SharedPreferences spUser;
    //
    //ImageView imgBtn_setting;
    //
    //ImageView imgBtn_search;
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    private int currentPosition = 2;
    private ArrayList<ViewModel> alView = new ArrayList<>(5);
    private String[] titles = {"activity", "my matches", "create introze", "feed", "account", "settings", "notification",
            "contact us", "manage contacts", "privacy policy", "terms of use", "NEW INTROZE INVITATION", "what i'm looking for", ""
            , "NEW INTROZE INVITATION", "", "choose person", "invite friends"};
    //private FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    private ImageView ibSetting, ibSearch, ibDollar;
    //
    private Fragment currentFragment, oldFragment;
    //
    private LinearLayout llActivityCount, llMatchCount, llAccountCount;
    private TextView tvActivityCount, tvMatchCount, tvAccountCount;
    //
    //
    private Timer timerCount;
    //
    private int activityCount = 2;
    //
    private int activity4UUnreadCount, activityByUUnreadCount;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private int backStackCount;
    //
    private int relationType;

    //
    private ImageButton ibBack;

    //
    private LinearLayout llBottomBar;
    private String TAG = "HomeActivity";
    //
    private boolean isForRedirection;
    private AlertDialog adInAppRedirection;

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //
        TypefaceHelper.typeface(this);
        //
        ibSetting = (ImageView) findViewById(R.id.imgBtn_setting);
        ibSearch = (ImageView) findViewById(R.id.imgBtn_search);
        //
        ibDollar = (ImageView) findViewById(R.id.imgBtn_dollar);
        //
        ibBack = (ImageButton) findViewById(R.id.ibBack);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        initHeader("ACTIVITY");
        //
        spUser = getSharedPreferences("user", MODE_PRIVATE);
        //
        String userDetail = spUser.getString(User.class.getName(), null);
        User user = null;
        if (userDetail != null) {
            user = new Gson().fromJson(userDetail, User.class);
            //
            userDetails = user.userDetails;
        }
        //
        if (TextUtils.isEmpty(spUser.getString(User.class.getName(), null)))
            getProfileFromServer1();
        else {
            //
            //sessionExpirationCheck(user.userDetails.userChatId);
            createSession(user.userDetails.userChatId);
        }
        //
        //
        SharedPreferences.Editor editorUser = spUser.edit();
        editorUser.putBoolean("is_verified", true).commit();
        //
        //if (spUser.getBoolean("is_contact_sync_db", false) && !spUser.getBoolean("is_contact_sync", false)) {
        if (spUser.getBoolean("is_contact_sync_db", false))
            SyncUtils.CreateSyncAccount(this);
        //}
        //
        ImageView ivCreateIntroze = (ImageView) findViewById(R.id.ivCreateIntroze);
        ivCreateIntroze.setColorFilter(getResources().getColor(R.color.colorPrimary));
        //
        int[] idTV = {R.id.tvActivity, R.id.tvMatches, R.id.tvCreateIntroze, R.id.tvFeed, R.id.tvAccount};
        int[] idIV = {R.id.ivActivity, R.id.ivMatches, R.id.ivCreateIntroze, R.id.ivFeed, R.id.ivAccount};
        //
        for (int i = 0; i < 5; i++) {
            ViewModel model = new ViewModel((TextView) findViewById(idTV[i]), (ImageView) findViewById(idIV[i]));
            alView.add(model);
        }
        //
        //
        setMyFragment(2);
        //
        llActivityCount = (LinearLayout) findViewById(R.id.llActivityCount);
        tvActivityCount = (TextView) findViewById(R.id.tvActivityCount);
        //
        llAccountCount = (LinearLayout) findViewById(R.id.llAccountCount);
        tvAccountCount = (TextView) findViewById(R.id.tvAccountCount);
        //
        llMatchCount = (LinearLayout) findViewById(R.id.llMatchCount);
        tvMatchCount = (TextView) findViewById(R.id.tvMatchCount);
        //
        final String category = getIntent().getStringExtra("from_notification");
        switchScreen(category);
        //
        //registerDevice();
        getDeviceToken();
        //
        initCountTimer();
        //
        builderError = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isForRedirection) {
                    switchScreen(HomeActivity.this.category);
                    isForRedirection = false;
                }
                adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //beforeRegisterDevice();
        JUtils.setAppStatus(this, true);
        //
        llBottomBar = (LinearLayout) findViewById(R.id.llBottomBar);
        //
        adInAppRedirection = new InAppRedirectionBuilder(this).create();
        adInAppRedirection.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    //
    private void sessionExpirationCheck(int chatId) {
        Date expirationDate = new Date(spUser.getLong("qb_expiration_date", 0));
        String token = spUser.getString("qb_token", "");
        // recreate session on next start app
        Date currentDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.HOUR, 2);
        Date twoHoursAfter = cal.getTime();

        if (expirationDate.before(twoHoursAfter)) {
            try {
                QBAuth.createFromExistentToken(token, expirationDate);
                //
                spUser.edit().putBoolean("is_chat_init", true).commit();
                //loadDialogsFromQb();
                if (currentFragment instanceof MatchFragment) {
                    ((MatchFragment) currentFragment).loadDialogsFromQb();
                }
            } catch (BaseServiceException e) {
                createSession(chatId);
            }
        } else {
            // create a session
            createSession(chatId);
        }
    }

    //
    private String byFullName, relFullName, activityModel;

    //
    public void onGotoMatched(String byFullName, String relFullName, String activityModel) {
        this.byFullName = byFullName;
        this.relFullName = relFullName;
        this.activityModel = activityModel;
        onBackPressed();
        setMyFragment(14);
    }

    //
    private void createSession(final int chatId) {
        //
        spUser.edit().putBoolean("is_chat_init", false).commit();
        //
        Log.d("OwerChatId", "" + "Chat Id" + chatId);
        //pd = CustomProgress.show(getActivity(), null, false, false, null);
        //
        QBAuth.createSessionV2(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession result, Bundle params) {
                //
                /*try {
                    String token = BaseService.getBaseService().getToken();
                    Date expirationDate = BaseService.getBaseService().getTokenExpirationDate();
                    //
                    spUser.edit().putString("qb_token", token).putLong("qb_expiration_date", expirationDate.getTime()).commit();
                } catch (BaseServiceException e) {

                }*/
                //
                /*if (SharedPreferencesUtil.hasQbUser()) {
                    //
                    spUser.edit().putBoolean("is_chat_init", true).commit();
                    //loadDialogsFromQb();
                    if (currentFragment instanceof MatchFragment) {
                        ((MatchFragment) currentFragment).loadDialogsFromQb();
                    }
                } else {*/
                getCurrentUser(chatId);
                //}
            }

            @Override
            public void onError(QBResponseException e) {
                //
                Log.d("SessionError", "" + e);
                //
                createSession(chatId);
                //pd.dismiss();
            }
        });
    }

    //
    private void getCurrentUser(final int chatId) {
        Log.d("OwerChatId", "" + "Chat Id" + chatId);
        //
        QBUsers.getUserV2(chatId, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                //
                login(qbUser);
            }

            @Override
            public void onError(QBResponseException e) {
                //
                getCurrentUser(chatId);
                //pd.dismiss();
                //Toast toast = Toast.makeText(getActivity(), "Chat initialization failed. No chat_id provided from server side", Toast.LENGTH_SHORT);
                //  if (toast != null) toast.show();
            }
        });
    }

    private void login(final QBUser user) {
        Log.d("LoginSuccessBe", "" + user);
        user.setPassword(Consts.QB_USERS_PASSWORD);
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.d("LoginSuccess", "" + user);
                //
                user.setPassword(Consts.QB_USERS_PASSWORD);
                user.setLogin("");
                SharedPreferencesUtil.saveQbUser(user);
                //
                spUser.edit().putBoolean("is_chat_init", true).commit();
                //loadDialogsFromQb();
                if (currentFragment instanceof MatchFragment) {
                    ((MatchFragment) currentFragment).loadDialogsFromQb();
                }
            }

            @Override
            public void onError(QBResponseException e) {
                //
                login(user);
                //pd.dismiss();
                Log.d("LoginError", "" + e);
            }
        });
    }

    //
    private void initCountTimer() {
        //
        timerCount = new Timer("count");
        timerCount.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //new ActivityCountAsyncTask().execute();
                getAmountDetailFromServer();
                getMatchCount();
                //getAccountUnreadCount();
                new BadgeCountAsyncTask(HomeActivity.this).execute(JUtils.getHeader(spUser));
                //
            }
        }, new Date(), 1000 * 10);
    }

    //
    private void switchScreen(String category) {
        //
        Log.d(TAG, "Category " + category);
        //
        if (TextUtils.equals("NEW_INTROZE_INVITATION", category) || TextUtils.equals("INTROZE_ACCEPTED", category)) {
            isFromNotification = true;
            //
            JSession.category = category;
            //
            onClickLlActivity(null);//
        } else if (TextUtils.equals("THANK_YOU_RECEIVED", category)) {
            //Toast.makeText(this, "OnClickLlAccount", Toast.LENGTH_SHORT).show();
            JSession.category = category;
            onClickLlAccount(null);
        }
    }

    //
    public void setActivityCount(int count) {
        if (count <= 0) {
            llActivityCount.setVisibility(View.GONE);
        } else {
            tvActivityCount.setText(count + "");
            llActivityCount.setVisibility(View.VISIBLE);
        }
    }
    //

    //
    //
    public void setAccountCount(int count) {
        if (count <= 0) {
            llAccountCount.setVisibility(View.GONE);
        } else {
            tvAccountCount.setText(count + "");
            llAccountCount.setVisibility(View.VISIBLE);
        }
    }

    //
    public void setMatchCount(int count) {
        if (count <= 0) {
            llMatchCount.setVisibility(View.GONE);
        } else {
            tvMatchCount.setText(count + "");
            llMatchCount.setVisibility(View.VISIBLE);
        }
    }

    //
    private void clearCurrentView(int position) {
        ViewModel model = alView.get(position);
        model.iv.clearColorFilter();
        model.tv.setTextColor(getResources().getColor(android.R.color.darker_gray));
    }

    //
    private void setCurrentView(int position) {
        ViewModel model = alView.get(position);
        model.iv.setColorFilter(getResources().getColor(R.color.home_footer));
        model.tv.setTextColor(getResources().getColor(R.color.home_footer));
    }

    //
    public void onClickLlActivity(View view) {
        JUtils.hideSoftKeyboard(this);
        if (currentFragment instanceof FragmentActivity) return;
        //
        selectedCurrentFragment = 0;
        setMyFragment(0);
    }

    //
    //
    public void onClickLlMatches(View view) {
        JUtils.hideSoftKeyboard(this);
        if (currentFragment instanceof MatchFragment) return;
        //
        setMyFragment(1);
    }

    //
    //
    public void onClickLlCreateIntroze(View view) {
        JUtils.hideSoftKeyboard(this);
        if (currentFragment instanceof CreateIntrozeFragment) return;
        setMyFragment(2);
    }

    //
    //
    public void onClickLlFeeds(View view) {
        JUtils.hideSoftKeyboard(this);
        if (currentFragment instanceof FeedFragment) return;
        //
        selectedCurrentFragment = 3;
        setMyFragment(3);
    }

    //
    //
    public void onClickLlAccount(View view) {
        JUtils.hideSoftKeyboard(this);
        if (currentFragment instanceof ProfileFragment) return;
        setMyFragment(4);
    }

    //
    private void setMyFragment(int position) {
        //
        if (position < 5 && currentPosition >= 5) {
            if (currentPosition == 13) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("13");
                if (fragment instanceof FeedFragment) {
                    currentPosition = 3;
                } else if (fragment instanceof NewIntrozeInvitationActivity)
                    currentPosition = 0;
                else if (fragment instanceof MatchFragment) currentPosition = 1;
                else if (fragment instanceof SelectContactActivity) {
                    if (isFromCreate == 1) currentPosition = 3;
                    else if (isFromCreate == 2) currentPosition = 0;
                    else currentPosition = 2;
                }
                //}
            } else if (currentPosition == 11 || currentPosition == 14) {
                currentPosition = 0;
            } else if (currentPosition == 16) {
                if (isFromCreate == 1) currentPosition = 3;
                else if (isFromCreate == 2) currentPosition = 0;
                else currentPosition = 2;
            } else
                currentPosition = 4;
            //
            for (String id : backstack) {
                FragmentManager fm = getSupportFragmentManager();
                fm.popBackStack(id, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
        if (position < 5) {
            clearCurrentView(currentPosition);
            setCurrentView(position);
        }
        //
        currentPosition = position;
        ////
        //titles[16] = titles[16] + (isFromFirst ? "#1" : "#2");
        initHeader(titles[currentPosition].toUpperCase() + (currentPosition == 17 ? (isFromFirst ? "#1" : "#2") : ""));
        Log.d("CurrentPosition", "Curent Positon " + currentPosition);
        switch (position) {
            case 0:
                currentFragment = new FragmentActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment).commit();
                break;
            case 1:
                currentFragment = new MatchFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment, "13").commit();
                break;
            case 3:
                currentFragment = new FeedFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment, "13").commit();
                break;
            case 4:
                currentFragment = new ProfileFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment).commit();
                break;
            case 5:
                currentFragment = new SettingActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("5").commit();
                backstack.add("5");
                break;
            case 6:
                currentFragment = new NotificationActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("6").commit();
                backstack.add("6");
                break;
            case 7:
                currentFragment = new ContactUsActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("7").commit();
                backstack.add("7");
                break;
            case 8:
                currentFragment = new ManageContactsActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("8").commit();
                backstack.add("8");
                break;
            case 9:
                currentFragment = new PrivacyPolicyActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("9").commit();
                backstack.add("9");
                break;
            case 10:
                currentFragment = new TermsOfServicesActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("10").commit();
                backstack.add("10");
                break;
            case 11:
                currentFragment = NewIntrozeInvitationActivity.getInstance(activityModelData);
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment, "13")
                        .addToBackStack("11").commit();
                backstack.add("11");
                break;
            case 12:
                currentFragment = IntroductionTypeActivity.getInstance(relationType);
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("12").commit();
                backstack.add("12");
                break;
            case 13:
                currentFragment = FeedProfileActivity.getInstance(feedModel);
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("13").commit();
                backstack.add("13");
                break;
            case 14:
                currentFragment = MatchDoneActivity.getInstance(byFullName, relFullName, activityModel);
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("14").commit();
                backstack.add("14");
                break;
            case 15:
                currentFragment = MatchDoneActivity.getInstance(byFullName, relFullName, activityModel);
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("15").commit();
                backstack.add("15");
                break;
            case 16:
                currentFragment = SelectContactActivity.getInstance(isFromCreate == 0 ? true : false);
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment, "13")
                        .addToBackStack("16").commit();
                backstack.add("16");
                break;
            case 17:
                currentFragment = new InviteFriendsActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment)
                        .addToBackStack("17").commit();
                backstack.add("17");
                initHeader("Invite Friends");
                break;
            default:
                currentFragment = new CreateIntrozeFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, currentFragment).commit();
        }
    }

    //
    public void hideBottom(boolean isForHide) {
        if (isForHide) llBottomBar.setVisibility(View.GONE);
        else llBottomBar.setVisibility(View.VISIBLE);
    }

    //
    public boolean isFromFirst;
    //
    private int isFromCreate;

    //
    public void onCreateClicked(int isFromCreate, boolean isFromFirst) {
        this.isFromCreate = isFromCreate;
        this.isFromFirst = isFromFirst;
        //
        setMyFragment(16);
    }

    private TextView txtHeader;

    private void initHeader(String text) {
        txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText(text);
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
        //
        ibBack.setVisibility(View.GONE);
        //
        if (currentPosition == 4) {
            ibSearch.setVisibility(View.GONE);
            ibSetting.setVisibility(View.VISIBLE);
            ibDollar.setVisibility(View.GONE);
        } else if (currentPosition == 0 || currentPosition == 3) {
            ibSetting.setVisibility(View.GONE);
            ibSearch.setVisibility(View.VISIBLE);
            ibDollar.setVisibility(View.GONE);
        } else {
            ibSearch.setVisibility(View.GONE);
            ibSetting.setVisibility(View.GONE);
            ibDollar.setVisibility(View.GONE);
        }
        //
        initActions();
    }

    //
    private void initActions() {
        //
        ibSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMyFragment(5);
            }
        });
        //
        ibSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                //Toast.makeText(HomeActivity.this, "Search Clicked", Toast.LENGTH_SHORT).show();
                //
                if (selectedCurrentFragment == 3)
                    onCreateClicked(1, true);
                else if (selectedCurrentFragment == 0)
                    onCreateClicked(2, true);
                /*Intent intent = new Intent(HomeActivity.this, SelectContactActivity.class);
                startActivityForResult(intent, 2001);*/
            }
        });
        //
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentFragment instanceof ProfileFragment) {
                    ((ProfileFragment) currentFragment).onClickBtnBack();
                    ibBack.setVisibility(View.GONE);
                }
            }
        });
    }

    //
    public static int amount;

    //
    private void getAmountDetailFromServer() {
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/profile_tab").setHeader("Authorization", JUtils.getHeader(spUser))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d(TAG, "Amount " + result + "");
                if (result != null) {
                    //
                    User user = new Gson().fromJson(result, User.class);
                    //
                    if (TextUtils.equals("success", user.result)) {
                        amount = user.userDetails.balance;
                        if (currentFragment instanceof ProfileFragment) {
                            ((ProfileFragment) currentFragment).setTvAmount(user.userDetails.balance);
                        }
                        //tvAmount.setText("$" + user.userDetails.balance);
                    } else {
                    }
                } else {

                }
            }
        });
    }

    //
    private void saveEmails(String result) {

    }

    //
    public static Intent intent1, intent2;
    //
    private String category;
    //
    private BroadcastReceiver pushBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            //
            category = intent.getStringExtra("from_notification");
            JSession.rel_id = intent.getIntExtra("rel_id", 0);
            //getIntent().putExtra("rel_id", intent.getIntExtra("rel_id", 0));
            //
            isForRedirection = true;
            builderError.setErrorMessage("Alert", message);
            adError.show();
            TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
        }
    };

    //
    //
    private class InAppRedirectionBuilder extends AlertDialog.Builder implements View.OnClickListener {
        //
        private Button btnYes, btnRedirection;

        public InAppRedirectionBuilder(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_reject_confirm, null);
            TypefaceHelper.typeface(view);
            //
            btnYes = (Button) view.findViewById(R.id.btnYes);
            btnYes.setOnClickListener(this);
            //
            btnRedirection = (Button) view.findViewById(R.id.btnNo);
            btnRedirection.setOnClickListener(this);
            //
            setView(view, 50, 0, 50, 0);
            //
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnNo:
                    break;
                case R.id.btnYes:
                    break;
            }
        }
    }

    //
    @Override
    public void onFeedBtnAddClicked(int identifier) {
        //
        List<TableContacts> item = new Select().from(TableContacts.class).where("Identifier =?", identifier).execute();
        //
        if (item.size() > 0) {
            TableContacts c = item.get(0);
            //
            intent1 = new Intent();
            intent1.putExtra("url_profile", c.urlPic);
            intent1.putExtra("_id", c.getId());
            intent1.putExtra("id", c.identifier + "");//new Gson().toJson(data.get(position)));
            intent1.putExtra("fn", c.firstName + " " + c.lastName);
            intent1.putExtra("iu", c.isIntroze);
            intent1.putExtra("ui", c.introzeUserId);
            if (!c.isIntroze)
                intent1.putExtra("ci", c.identifier + "");
            //
            onClickLlCreateIntroze(null);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2000 && resultCode == RESULT_OK) {
            if (LoginManager.getInstance() != null) {
                LoginManager.getInstance().logOut();
            }
            spUser.edit().putBoolean("is_verified", false).commit();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        } /*else if (requestCode == 3001 && resultCode == RESULT_OK) {//goto match fragment
            //Toast.makeText(HomeActivity.this, "OnMatchDone", Toast.LENGTH_SHORT).show();
            onClickLlMatches(null);
        }*/
    }

    //
    //
    private void getProfileFromServer1() {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/profile_tab").setHeader("Authorization", "Basic " + basic)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("SettingProfileResult", result + "");
                if (result != null) {
                    //
                    User user = new Gson().fromJson(result, User.class);
                    //
                    /*try {
                        JSONObject joResult = new JSONObject(result);*/
                    if (TextUtils.equals("success", user.result)) {
                        //
                        /*JSONObject joUserDetail = joResult.getJSONObject("user_details");

                        //
                        ProfileModel model = new ProfileModel();
                        model.phones = joUserDetail.getString("phone");
                        model.email.add("email");
                        //
                        JSONArray joEmails = joUserDetail.optJSONArray("emails");
                        if (joEmails != null) {
                            for (int i = 0; i < joEmails.length(); i++)
                                model.email.add(joEmails.getString(i) + "");
                        }
                        //
                        int chatId = joUserDetail.getInt("user_chat_id");*/
                        //
                        try {
                            JSONObject joResult = new JSONObject(result);
                            JSONObject joUserDetail = joResult.getJSONObject("user_details");
                            List<Email> emails = user.userDetails.emails;
                            //
                            if (joUserDetail.optJSONArray("emails") != null) {
                                JSONArray jaEmails = joUserDetail.getJSONArray("emails");
                                emails.clear();
                                for (int i = 0; i < jaEmails.length(); i++) {
                                    //
                                    JSONObject joE = jaEmails.getJSONObject(i);
                                    //
                                    String mEmail = joE.getString("email");
                                    Integer id = joE.optInt("id");
                                    //
                                    emails.add(new Email(mEmail, id));
                                }
                                //addEmails();
                            }
                        } catch (JSONException e1) {

                        }
                        //
                        spUser.edit().putString(User.class.getName(), new Gson().toJson(user)).commit();
                        //
                        //JUtils.removeMyProfileFromDB(user.userDetails.phone);
                        //
                        createSession(user.userDetails.userChatId);
                        //sessionExpirationCheck(user.userDetails.userChatId);
                    } else getProfileFromServer1();
                    /*} catch (JSONException e1) {

                    }*/
                    //Log.d("ProfileFragment", "" + result);
                } else {
                    getProfileFromServer1();
                }
            }
        });
    }

    //
    private void getProfileFromServer() {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/profile_tab").setHeader("Authorization", "Basic " + basic)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("SettingProfileResult", result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            JSONObject joUserDetail = joResult.getJSONObject("user_details");
                            //
                            ProfileModel model = new ProfileModel();
                            model.phones = joUserDetail.getString("phone");
                            model.email.add("email");
                            //
                            JSONArray joEmails = joUserDetail.optJSONArray("emails");
                            if (joEmails != null) {
                                for (int i = 0; i < joEmails.length(); i++)
                                    model.email.add(joEmails.getString(i) + "");
                            }
                            //
                            int chatId = joUserDetail.getInt("user_chat_id");
                            //
                            spUser.edit().putInt("chat_id", chatId)
                                    .putString("profile_detail", new Gson().toJson(model)).commit();
                            //
                            spUser.edit().putBoolean("is_chat_init", false).commit();
                            //
                            //createSession(chatId);
                        }
                    } catch (JSONException e1) {

                    }
                    //Log.d("ProfileFragment", "" + result);
                } else {

                }
            }
        });
    }
    //

    @Override
    public void onAfterForULoaded(ArrayList<ActivityModel> data) {
        //Toast.makeText(this, "OnAfterForULoaded", Toast.LENGTH_SHORT).show();
        ArrayList<Integer> ids = new ArrayList<>();
        for (ActivityModel model : data) {
            ids.add(model.relId);
        }
        if (ids.size() > 0 && activity4UUnreadCount > 0)
            readForUCount(new Gson().toJson(ids).replace("[", "").replace("]", ""));
    }

    @Override
    public void onAfterByULoaded(ArrayList<ByUModel> data) {
        //
        ArrayList<Integer> ids = new ArrayList<>();
        for (ByUModel model : data) {
            ids.add(model.relId);
        }
        if (ids.size() > 0 && activityByUUnreadCount > 0)
            readByUCount(new Gson().toJson(ids).replace("[", "").replace("]", ""));
    }

    //
    private String activityModelData;

    @Override
    public void onGotoInvitation(ActivityModel model) {
        activityModelData = new Gson().toJson(model);
        setMyFragment(11);
    }

    @Override
    public void onNotificationClicked() {
        setMyFragment(6);
    }

    @Override
    public void onContactUsClicked() {
        setMyFragment(7);
    }

    @Override
    public void onLogoutClicked() {
        //
        if (LoginManager.getInstance() != null) {
            LoginManager.getInstance().logOut();
        }
        spUser.edit().clear().commit();//.putBoolean("is_verified", false).commit();
        //
        spUser.edit().putBoolean("is_first_time", false).commit();
        //
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onManageContactClicked() {
        setMyFragment(8);
    }

    @Override
    public void onPrivacyPolicyClicked() {
        setMyFragment(9);
    }

    @Override
    public void onTermsOfServiceClicked() {
        setMyFragment(10);
    }

    @Override
    public void onInviteFriendsClicked() {

        setMyFragment(17);
        /*titles[17] = "Invite Friends";*/
    }

    @Override
    public void onStatusClicked(int relationType) {
        hideBottom(true);
        this.relationType = relationType;
        setMyFragment(12);
    }

    @Override
    public void onMoneyClicked() {
        ibBack.setVisibility(View.VISIBLE);
    }

    //
    private String feedModel;

    @Override
    public void onProfileClicked(FeedModel model) {
        titles[13] = model.firstName + " " + model.lastName;
        feedModel = new Gson().toJson(model);
        setMyFragment(13);
    }

    @Override
    public void onIntrozeTypeCancelClicked() {
        hideBottom(false);
        onBackPressed();
    }

    //
    private UserDetails userDetails;

    //
    private boolean isUser(long _id) {
        if (userDetails == null) return false;
        List<TablePhone> phones = new Select().from(TablePhone.class).where("Contacts =? AND Phone =?", _id, userDetails.phone).execute();
        if (phones.size() > 0) return true;
        //
        List<TableEmail> emails = new Select().from(TableEmail.class).where("Contacts =? AND Email =?", _id, userDetails.email).execute();
        if (emails.size() > 0) return true;
        //
        return false;
    }

    //
    public static TableContacts model1, model2;
    //
    //public boolean

    @Override
    public void onContactClicked(TableContacts model, boolean is4Profile) {
        JUtils.hideSoftKeyboard(this);
        if (isUser(model.getId())) {
            builderError.setErrorMessage("Alert", "You cannot create Introze for yourself.");
            adError.show();
            return;
        }

        //model1 = model;
        //
        if (is4Profile && model.isIntroze) {
            //
            FeedModel model1 = new FeedModel(model.introzeUserId, model.firstName, model.lastName, model.urlPic, model.identifier + "");
            model1.isMatched = model.isMatched;
            model1.chatId = model.chatId;
            //
            onProfileClicked(model1);
            /*Intent intent = new Intent(this, FeedProfileActivity.class);
            intent.putExtra("user_id", model.introzeUserId + "");
            //
            intent.putExtra("feed_model", new Gson().toJson(model1));
            startActivity(intent);*/
        } else {
            Intent intent = new Intent();
            intent.putExtra("url_profile", model.urlPic);
            intent.putExtra("_id", model.getId());
            intent.putExtra("id", model.identifier + "");//new Gson().toJson(data.get(position)));
            intent.putExtra("fn", model.firstName + " " + model.lastName);
            intent.putExtra("iu", model.isIntroze);
            intent.putExtra("ui", model.introzeUserId);
            if (!model.isIntroze)
                intent.putExtra("ci", model.identifier + "");
            //
            if (isFromFirst)
                HomeActivity.intent1 = intent;
            else HomeActivity.intent2 = intent;
            //
            //
            if (isFromCreate == 0) onBackPressed();
            else onClickLlCreateIntroze(null);

            //Toast.makeText(SelectContactActivity.this, "Contact id" + data.get(position).identifier, Toast.LENGTH_SHORT).show();
        /*setResult(RESULT_OK, intent);
        finish();*/
        }
    }


    @Override
    public void onPaymentClickListener(TableContacts model) {
        //
        JUtils.hideSoftKeyboard(this);
//
        Intent intent = new Intent(this, SendThankUActivity.class);
        intent.putExtra("user_id", model.introzeUserId);
        intent.putExtra("user_name", model.firstName + " " + model.lastName);
        intent.putExtra("url_user_pic", model.urlPic == null ?
                ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, model.identifier).toString() : model.urlPic);
        //
        startActivity(intent);
    }

    @Override
    public void onChatClicked(TableContacts contacts) {
        JUtils.hideSoftKeyboard(this);
        callChat(contacts);
    }

    //
    //
    private void callChat(TableContacts model) {
        if (spUser.getBoolean("is_chat_init", false)) {
            JSession.chatUserName = model.firstName + " " + model.lastName; //model.name;
            JSession.introzeId = model.introzeUserId + ""; //model.rel_user_id + "";
            JSession.urlRel = model.urlPic; //model.url;
            openChat(model.chatId);
        } else
            Toast.makeText(this, "Chat initializing please wait...", Toast.LENGTH_SHORT).show();
    }

    //
    public void openChat(int chatId) {
        ArrayList<QBUser> selectedUsers = new ArrayList<>();
        //
        selectedUsers.add(new QBUser(chatId));
        //
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        ChatActivity.startForResult(HomeActivity.this, 5000, dialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Log.d("DialogError", "" + e);
                        //Toast.makeText(MainActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    //
    private class ViewModel {
        TextView tv;
        ImageView iv;

        public ViewModel(TextView tv, ImageView iv) {
            this.tv = tv;
            this.iv = iv;
        }
    }

    //
    public class ProfileModel {
        public String phones;
        public ArrayList<String> email = new ArrayList<>();
    }

    //
    @Override
    protected void onResume() {
        super.onResume();
        JUtils.setAppStatus(this, true);
        //
        registerReceiver(brBadgeCount, new IntentFilter(BadgeCountAsyncTask.BADGE_COUNT_RECEIVER));
        //
        registerReceiver(mBroadcastReceiver, new IntentFilter("permission_access"));
        registerReceiver(pushBroadcastReceiver, new IntentFilter("push_messages_dialog"));
        //
    }

    //
    BroadcastReceiver brBadgeCount = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setBadgeCount(intent.getStringExtra(BadgeCountAsyncTask.BADGE_COUNT));
        }
    };

    //
    private void setBadgeCount(String badgeCount) {
        BadgeUnreadCount unreadCount = new Gson().fromJson(badgeCount, BadgeUnreadCount.class);
        //activity count
        if (unreadCount.getActivityUnreadCount() <= 0) {
            llActivityCount.setVisibility(View.GONE);
        } else {
            activity4UUnreadCount = unreadCount.activityForUUnreadCount;
            activityByUUnreadCount = unreadCount.activityByUUnreadCount;
            //
            tvActivityCount.setText(unreadCount.getActivityUnreadCount() + "");
            llActivityCount.setVisibility(View.VISIBLE);
        }
        //account count
        if (unreadCount.accountUnreadCount <= 0) {
            llAccountCount.setVisibility(View.GONE);
        } else {
            //
            receiveCount = unreadCount.accountUnreadCount;
            //
            if (currentFragment instanceof ProfileFragment) {
                ((ProfileFragment) currentFragment).setAccountCount(unreadCount.accountUnreadCount);
            }
            //
            tvAccountCount.setText(unreadCount.accountUnreadCount + "");
            llAccountCount.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        //
        unregisterReceiver(brBadgeCount);
        //
        unregisterReceiver(mBroadcastReceiver);
        unregisterReceiver(pushBroadcastReceiver);
        timerCount.cancel();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //
        JUtils.setAppStatus(this, false);
    }

    //
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String permission = intent.getStringExtra("permission");
            Log.d("PermissionBR", permission + "");
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(HomeActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, permission)) {
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(HomeActivity.this, new String[]{permission}, 3000);
                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        }
    };

    //
    private void getDeviceToken() {
        //
        new GCMUtils(this) {
            @Override
            public void getGCMId(String gcmid) {
                deviceToken = gcmid;
                //
                registerDevice();
            }
        };
        //
    }

    //
    public void registerDevice() {
        //
        //Toast.makeText(this, "registerDevice", Toast.LENGTH_SHORT).show();

        String device_os = System.getProperty("os.version");
        String device_model = "" + android.os.Build.MODEL;
        //
        TelephonyManager telephonyManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        String tmDevice, tmSerial, androidId;
        tmDevice = "" + telephonyManager.getDeviceId();
        tmSerial = "" + telephonyManager.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        //
        String deviceType = "Android";
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/device").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("device_unique_id", deviceId).setBodyParameter("device_type", deviceType)
                .setBodyParameter("device_model", device_model).setBodyParameter("device_os", device_os)
                .setBodyParameter("device_token", deviceToken).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //
                Log.d(TAG, "Device Register " + result);
                //
                if (result != null) {

                } else {

                }
            }
        });
    }

    //
    //
    public void beforeRegisterDevice() {
        //
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_PHONE_STATE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 3000);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        } else {
            registerDevice();
        }
        //
    }

    //
    //
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 3000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    registerDevice();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    //
    @Override
    public void onBackPressed() {
        //
        /*if (currentPosition == 6) {
            currentPosition = 5;
        }*/
        if (backstack.size() > 0) {//currentPosition > 5) {
            int pos = Integer.parseInt(backstack.remove(backstack.size() - 1));
            currentPosition = pos;
            //
            if (currentPosition == 11 || currentPosition == 14) {
                currentPosition = 0;
                initHeader(titles[0].toUpperCase());
            } else if (currentPosition == 5 || currentPosition == 12) {
                hideBottom(false);
                currentPosition = 4;
                initHeader(titles[4].toUpperCase());
            } else if (currentPosition == 13) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("13");
                if (fragment instanceof NewIntrozeInvitationActivity) {
                    currentPosition = 11;
                    txtHeader.setText(titles[11].toUpperCase());
                } else if (fragment instanceof FeedFragment) {
                    currentPosition = 3;
                    initHeader(titles[3].toUpperCase());
                } else if (fragment instanceof MatchFragment) {
                    currentPosition = 1;
                    initHeader(titles[3].toUpperCase());
                } else if (fragment instanceof SelectContactActivity) {
                    currentPosition = 16;
                    initHeader(titles[16].toUpperCase());
                }
            } else if (currentPosition == 16) {
                switch (isFromCreate) {
                    case 1:
                        currentPosition = 3;
                        break;
                    case 2:
                        currentPosition = 0;
                        break;
                    default:
                        currentPosition = 2;
                }
                initHeader(titles[currentPosition].toUpperCase());
            } else
                txtHeader.setText(titles[currentPosition - 1].toUpperCase());
            /*if (currentPosition <= 5)
                initHeader(titles[currentPosition - 1].toUpperCase());*/
        }
        super.onBackPressed();
    }
    //

    //
    private void readByUCount(final String ids) {
        //
        Log.d(TAG, "Read By U Count " + ids);
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("PUT", AppUrls.MAIN + "/mark_read_by").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("rel_id", ids).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("ByUCount", "ForUCount " + result);
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            activityByUUnreadCount = 0;
                            setActivityCount(activity4UUnreadCount + activityByUUnreadCount);
                        } else {
                            readByUCount(ids);
                        }
                    } catch (JSONException e1) {
                        readByUCount(ids);
                    }
                } else {
                    readByUCount(ids);
                }
            }
        });
    }

    //
    private void readForUCount(final String ids) {
        //
        Log.d(TAG, "Ids " + ids);
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("PUT", AppUrls.MAIN + "/mark_read").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("rel_id", ids).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.e("ForUCount", "ForUCount " + result);
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            activity4UUnreadCount = 0;
                            setActivityCount(activity4UUnreadCount + activityByUUnreadCount);
                        } else {
                            readForUCount(ids);
                        }
                    } catch (JSONException e1) {
                        readForUCount(ids);
                    }
                } else {
                    readForUCount(ids);
                }
            }
        });
    }

    //
    private class ActivityCountAsyncTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            int unreadCount = 0;
            //
            byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
            String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
            //
            try {
                String result = Ion.with(HomeActivity.this).load("GET", AppUrls.MAIN + "/for_you_count")
                        .setHeader("Authorization", "Basic " + basic).asString().get();
                Log.e(TAG, "For U Count Result " + result);
                JSONObject joForUResult = new JSONObject(result);
                if (TextUtils.equals("success", joForUResult.getString("result"))) {
                    activity4UUnreadCount = joForUResult.getInt("unread_count");
                }
            } catch (InterruptedException e) {

            } catch (ExecutionException e) {

            } catch (JSONException e) {

            }
            //
            try {
                String result = Ion.with(HomeActivity.this).load("GET", AppUrls.MAIN + "/by_you_count")
                        .setHeader("Authorization", "Basic " + basic).asString().get();
                //
                Log.d(TAG, "By You Count Result " + result);
                //
                JSONObject joForUResult = new JSONObject(result);
                if (TextUtils.equals("success", joForUResult.getString("result"))) {
                    activityByUUnreadCount = joForUResult.getInt("unread_count");
                }
            } catch (InterruptedException e) {

            } catch (ExecutionException e) {

            } catch (JSONException e) {

            }
            //
            Log.d(TAG, "For U " + activity4UUnreadCount + " By U " + activityByUUnreadCount);
            //
            unreadCount = activity4UUnreadCount + activityByUUnreadCount;

            return unreadCount;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            setActivityCount(integer);
        }
    }

    //
    public void getMatchCount() {
        //
        if (!JConnectionUtils.isConnectedToInternet(this)) return;
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(HomeActivity.this).load("GET", AppUrls.MAIN + "/new_match")
                .setHeader("Authorization", "Basic " + basic).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //
                Log.e(TAG, "Match Count Result " + result);
                //
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            if (joResult.has("new_match")) {
                                JSONArray jaMatch = joResult.getJSONArray("new_match");
                                int unreadCount = 0;
                                for (int i = 0; i < jaMatch.length(); i++) {
                                    JSONObject joItem = jaMatch.getJSONObject(i);
                                    if (joItem.getInt("read_flag") == 0) {
                                        unreadCount++;
                                    }
                                }
                                //
                                setMatchCount(unreadCount);
                            } else {
                                setMatchCount(0);
                            }
                        } else {
                            getMatchCount();
                        }
                    } catch (JSONException e1) {
                        getMatchCount();
                        //Toast.makeText(HomeActivity.this, "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    getMatchCount();
                }
            }
        });
    }

    //
    public static int receiveCount;

    //
    private void getAccountUnreadCount() {
        Ion.with(this).load("GET", AppUrls.MAIN + "/my_money").setHeader("Authorization", JUtils.getHeader(spUser))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //
                Log.d(TAG, "Account Unread " + result);
                //
                if (result != null) {
                    AccountUnreadCountModel model = new Gson().fromJson(result, AccountUnreadCountModel.class);
                    //
                    if (TextUtils.equals("success", model.result)) {
                        receiveCount = model.unreadRecCount;
                        //
                        setAccountCount(model.unreadRecCount);
                        //
                        if (currentFragment instanceof ProfileFragment) {
                            ((ProfileFragment) currentFragment).setAccountCount(model.unreadRecCount);
                        }
                    } else {
                        /*builderError.setErrorMessage(getString(R.string.error_no_response));
                        adError.show();*/
                    }
                } else {
                    /*builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();*/
                }
            }
        });
    }

    //
    public void markAccountRead(ArrayList<ProfileFragment.MyModel> data) {
        //
        ArrayList<Integer> ids = new ArrayList<>();
        for (ProfileFragment.MyModel model : data) {
            ids.add(model.thankUId);
        }
        if (ids.size() <= 0) return;
        //
        String d = new Gson().toJson(ids).replace("[", "").replace("]", "");

        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/mark_send_to").setHeader("Authorization", JUtils.getHeader(spUser))
                .setBodyParameter("to_id", d).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d(TAG, "Account Read " + result);
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            receiveCount = 0;
                            setAccountCount(0);
                        } else {

                        }
                    } catch (JSONException e1) {

                    }
                } else {

                }
            }
        });
    }

    //
    public class AccountUnreadCountModel {

        @SerializedName("result")
        @Expose
        public String result;
        @SerializedName("unread_count")
        @Expose
        public Integer unreadCount;
        @SerializedName("unread_sent_count")
        @Expose
        public Integer unreadSentCount;
        @SerializedName("unread_rec_count")
        @Expose
        public Integer unreadRecCount;
        @SerializedName("amount")
        @Expose
        public Integer amount;

    }
    //
}
