package com.wd.introze.activities.byu;

import android.content.Context;
import android.graphics.drawable.Animatable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;

/**
 * Created by flair on 27-07-2016
 */
public class ByUDialog extends AlertDialog.Builder implements View.OnClickListener {
    //
    private TextView tvShortName, tvReason;
    private SimpleDraweeView ivProfile;
    //
    private String shortName;
    //
    private DialogDismiss listener;
    //


    public ByUDialog(Context context, DialogDismiss listener) {
        super(context);
        //
        this.listener = listener;
        //
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_by_u, null);
        TypefaceHelper.typeface(view);
        //
        tvShortName = (TextView) view.findViewById(R.id.tvShortName);
        tvReason = (TextView) view.findViewById(R.id.tvReason);
        ImageView img_close = (ImageView) view.findViewById(R.id.img_close);
        img_close.setOnClickListener(this);
        //
        ivProfile = (SimpleDraweeView) view.findViewById(R.id.ivProfile);
        //
        setView(view, 20, 0, 20, 0);
    }

    //
    public void setValues(final String shortName, String reason, String url) {
        this.shortName = shortName;
        tvReason.setText(reason);
        //
        ControllerListener controllerListener1 = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(shortName);
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller1 = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener1)
                .setUri(url).build();
        ivProfile.setController(controller1);
    }
    //

    @Override
    public void onClick(View v) {
        listener.dialogDismiss();
    }

    //
    public interface DialogDismiss {
        void dialogDismiss();
    }
}