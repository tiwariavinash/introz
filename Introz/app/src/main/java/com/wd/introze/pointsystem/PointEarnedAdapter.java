package com.wd.introze.pointsystem;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;

/**
 * Created by Admin on 4/7/2018.
 */

public class PointEarnedAdapter extends RecyclerView.Adapter<PointEarnedAdapter.PointEarnedVH> {

    @Override
    public PointEarnedVH onCreateViewHolder(ViewGroup parent, int viewType) {
        //
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_point_earned, parent, false);
        TypefaceHelper.typeface(view);
        //
        return new PointEarnedVH(view);
    }

    @Override
    public void onBindViewHolder(PointEarnedVH holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    //
    public class PointEarnedVH extends RecyclerView.ViewHolder {

        public PointEarnedVH(View itemView) {
            super(itemView);
        }
    }
}
