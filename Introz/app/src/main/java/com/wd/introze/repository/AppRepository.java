package com.wd.introze.repository;

import com.wd.introze.repository.local.LocalRepository;
import com.wd.introze.repository.server.ServerRepository;

/**
 * Created by Admin on 4/9/2018.
 */

public class AppRepository {
    private static AppRepository appRepository;
    private LocalRepository localRepository;
    private ServerRepository serverRepository;

    private AppRepository() {
        localRepository = new LocalRepository();
        serverRepository = new ServerRepository();
    }

    public static AppRepository getInstance() {
        if (appRepository == null) appRepository = new AppRepository();
        return appRepository;
    }
}
