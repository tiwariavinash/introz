package com.wd.introze.home;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.wd.introze.CreateIntrozeFragment;
import com.wd.introze.FeedFragment;
import com.wd.introze.FragmentActivity;
import com.wd.introze.ProfileFragment;
import com.wd.introze.match.MatchFragment;

/**
 * Created by flair on 19-07-2016
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {
    //
    private String title[];

    public HomePagerAdapter(FragmentManager fm, String[] title) {
        super(fm);
        this.title = title;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentActivity();
            case 1:
                return new MatchFragment();
            case 3:
                return new FeedFragment();
            case 4:
                return new ProfileFragment();
        }
        return new CreateIntrozeFragment();
    }

    @Override
    public int getCount() {
        return title.length;
    }
    //

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
