package com.wd.introze.jutil;

import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.widget.TextView;

/**
 * Created by Gajendra on 8/28/2016.
 */
public class JPhoneNumberTextWatcher extends PhoneNumberFormattingTextWatcher {
    //
    private TextView tvError;
    private String error;

    //
    public JPhoneNumberTextWatcher(TextView tvError, String error) {
        this.tvError = tvError;
        this.error = error;
    }

    @Override
    public synchronized void afterTextChanged(Editable s) {
        super.afterTextChanged(s);
        if (TextUtils.isEmpty(s)) {
            tvError.setText(error);
        } else tvError.setText("");
    }
}
