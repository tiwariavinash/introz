package com.wd.introze;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.norbsoft.typefacehelper.TypefaceHelper;

/**
 * Created by dell on 8/13/2016.
 */
public class ContactUsActivity extends Fragment {
    //
    /*String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_contact_us, null);
        //
        TypefaceHelper.typeface(view);
        //
        //tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        //
        WebView wv = (WebView) view.findViewById(R.id.webView);
        wv.loadUrl("file:///android_asset/contact_us.html");
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("TERMS OF SERVICES");*/
        //initHeader();
        return view;
    }

    /*private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("CONTACT US");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }
*/
}
