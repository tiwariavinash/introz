package com.wd.introze.payment;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.AppUrls;
import com.wd.introze.R;
import com.wd.introze.SuccessModel;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.repository.server.rf.model.user.User;


/**
 * Created by flair on 05-09-2016
 */
public class AmountActivity extends AppCompatActivity {
    //
    private final String TAG = "AmountActivity";
    //
    private TextView tvLast4, tvAmount;
    //
    private SharedPreferences spIntroze;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private CustomProgress pd;
    //
    private boolean isSuccess;

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount);
        TypefaceHelper.typeface(this);
        //
        TextView tvTitle = (TextView) findViewById(R.id.txtHeader);
        tvTitle.setText("Amount");
        //
        tvLast4 = (TextView) findViewById(R.id.tvLast4);
        tvAmount = (TextView) findViewById(R.id.tvAmount);
        //
        spIntroze = getSharedPreferences("user", MODE_PRIVATE);
        //
        tvLast4.setText(getIntent().getStringExtra("last_4"));
        tvAmount.setText("$" + getIntent().getIntExtra("amount", 0));
        //Toast.makeText(AmountActivity.this, "" + getIntent().getStringExtra("last_4"), Toast.LENGTH_SHORT).show();
        //
        builderError = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSuccess) finish();
                else
                    adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        //getAmountDetailFromServer();
    }

    //
    private void getAmountDetailFromServer() {
        //
        pd = CustomProgress.show(this, null, false, false, null);
        Ion.with(this).load("GET", AppUrls.MAIN + "/profile_tab").setHeader("Authorization", JUtils.getHeader(spIntroze))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("SettingProfileResult", result + "");
                pd.dismiss();
                if (result != null) {
                    //
                    User user = new Gson().fromJson(result, User.class);
                    //
                    if (TextUtils.equals("success", user.result)) {
                        tvAmount.setText("$" + user.userDetails.balance);
                    } else {
                    }
                } else {

                }
            }
        });
    }

    //
    public void onClickBtnConfirm(View view) {
        Ion.with(this).load("PUT", AppUrls.MAIN + "/withdraw_amount").setHeader("Authorization", JUtils.getHeader(spIntroze))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d(TAG, "" + result);
                if (result != null) {
                    SuccessModel model = new Gson().fromJson(result, SuccessModel.class);
                    if (TextUtils.equals(model.result, "success")) {
                        isSuccess = true;
                        builderError.setErrorMessage("Success", "You will receive your money within 3 to 5 business days.");//model.message);
                    } else {
                        builderError.setErrorMessage(model.error);
                    }
                    adError.show();
                } else {
                    builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();
                }
            }
        });
    }

    //
    public void onClickBtnCancel(View view) {
        finish();
    }
}
