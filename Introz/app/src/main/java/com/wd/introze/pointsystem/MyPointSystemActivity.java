package com.wd.introze.pointsystem;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wd.introze.IntrozeLevelActivity;
import com.wd.introze.R;

/**
 * Created by Admin on 4/7/2018.
 */

public class MyPointSystemActivity extends AppCompatActivity implements View.OnClickListener {
    //
    private Button btnPointEarnedForMe;
    private Button btnPointIHaveEarned;
    //
    private TextView txtHeader;
    private Typeface tfHeader;
    String fontPathHeader = "itcavantgardestd-bold.ttf";

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_point_system);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        initHeader("INTROZE");
        //
        btnPointIHaveEarned = findViewById(R.id.btnPointIHaveEarned);
        btnPointIHaveEarned.setOnClickListener(this);
        //
        btnPointEarnedForMe = findViewById(R.id.btnPointEarnedForMe);
        btnPointEarnedForMe.setOnClickListener(this);
        //
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new PointEarnedFragment()).commit();
    }

    public void onClickBtnEarn(View view) {
        startActivity(new Intent(this, IntrozeLevelActivity.class));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPointEarnedForMe:
                btnPointEarnedForMe.setBackgroundColor(getResources().getColor(R.color.color_btn_green));
                btnPointIHaveEarned.setBackgroundColor(getResources().getColor(R.color.color_btn_white));
                //
                btnPointEarnedForMe.setTextColor(getResources().getColor(R.color.color_btn_white));
                btnPointIHaveEarned.setTextColor(getResources().getColor(android.R.color.darker_gray));
                //change fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new PointEarnedFragment()).commit();
                //
                break;
            case R.id.btnPointIHaveEarned:
                //
                btnPointEarnedForMe.setBackgroundColor(getResources().getColor(R.color.color_btn_white));
                btnPointIHaveEarned.setBackgroundColor(getResources().getColor(R.color.color_btn_green));
                //
                btnPointEarnedForMe.setTextColor(getResources().getColor(android.R.color.darker_gray));
                btnPointIHaveEarned.setTextColor(getResources().getColor(R.color.color_btn_white));
                //change fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new PointEarnedFragment()).commit();
                break;
        }
    }

    private void initHeader(String text) {
        txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(text);
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }
}
