package com.wd.introze.activities.byu;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Animatable;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;
import com.wd.introze.jutil.JUtils;

import java.util.ArrayList;

/**
 * Created by flair on 22-07-2016
 */
public class ByUAdapter extends BaseAdapter {
    //
    private ArrayList<ByUModel> data;
    private Context context;
    //
    private OnRelationClickListener listener;

    //
    public ByUAdapter(Context context, ArrayList<ByUModel> data, OnRelationClickListener listener) {
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    //
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        //
        LinearLayout ll = (LinearLayout) convertView;
        if (ll == null) {
            ll = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.introze_by_u_list_item, null);
            TypefaceHelper.typeface(ll);
        }
        //
        final ByUModel model = data.get(position);
        //
        TextView tvUser1 = (TextView) ll.findViewById(R.id.tvUser1);
        tvUser1.setText(model.user1firstname + "\n" + model.user1lastname);
        if (model.acceptedStatus != 1 && model.acceptedStatus != 2)
            tvUser1.setTextColor(context.getResources().getColor(android.R.color.darker_gray));
        else {
            tvUser1.setTextColor(context.getResources().getColor(android.R.color.black));
        }
        //
        TextView tvUser2 = (TextView) ll.findViewById(R.id.tvUser2);
        tvUser2.setText(model.user2firstname + "\n" + model.user2lastname);
        if (model.acceptedStatus != 1 && model.acceptedStatus != 3)
            tvUser2.setTextColor(context.getResources().getColor(android.R.color.darker_gray));
        else {
            tvUser2.setTextColor(context.getResources().getColor(android.R.color.black));
        }
        //
        ImageView ivRelation = (ImageView) ll.findViewById(R.id.ivRelationType);
        ivRelation.setImageResource(JUtils.getRelationTypeIcon(model.relationType));
        //if (model.acceptedStatus == 1) {
        //ivRelation.clearColorFilter();
        //} else {
        //ivRelation.setColorFilter(getFilter());
        //}
        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();
                listener.onRelationClicked(JUtils.getTwoUpperChar(model.by_first_name + " " + model.by_last_name),
                        model.reason, model.by_user_image);
            }
        });
        //
        Button btnMatch = (Button) ll.findViewById(R.id.btnMatch);
        btnMatch.setText(model.acceptedStatusName);
        if (TextUtils.equals(model.acceptedStatusName, "MATCHED"))
            btnMatch.setBackgroundColor(Color.parseColor("#1E498D"));
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                btnMatch.setBackgroundColor(context.getColor(android.R.color.darker_gray));
            } else
                btnMatch.setBackgroundColor(context.getResources().getColor(android.R.color.darker_gray));
        }
        //user1

        //Log.d("ByUProfile1", model.urlProfile1 + "");
        final SimpleDraweeView ivProfile1 = (SimpleDraweeView) ll.findViewById(R.id.ivProfile1);
        final TextView tvShortName1 = (TextView) ll.findViewById(R.id.tvShortName1);
        //

        //
        ControllerListener controllerListener1 = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName1.setVisibility(View.GONE);
                /*switch (model.acceptedStatus) {
                    case 1:
                *//*        ivProfile2.setColorFilter(getFilter());
                        break;
                    case 2:
                        ivProfile1.setColorFilter(getFilter());
                        break;
                }*/
                if (model.acceptedStatus != 1 && model.acceptedStatus != 2)
                    ivProfile1.setColorFilter(getFilter());
                else {
                    ivProfile1.clearColorFilter();
                }
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName1.setText(model.getTwoUpperChar(ByUModel.TypeUserByU.TYPE_USER_1));
                tvShortName1.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller1 = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener1)
                .setUri(model.urlProfile1).build();
        ivProfile1.setController(controller1);

        /*if (model.urlProfile1 == null) {
            ivProfile1.setImageResource(R.drawable.circle_gray);
            tvShortName1.setText(model.getTwoUpperChar(ByUModel.TypeUserByU.TYPE_USER_1));
        } else {
            Ion.with(ivProfile1).load(model.urlProfile1);
            tvShortName1.setVisibility(View.GONE);
        }*/
        //
        //user2
        //Log.d("ByUProfile2", model.urlProfile2 + "");
        //
        final SimpleDraweeView ivProfile2 = (SimpleDraweeView) ll.findViewById(R.id.ivProfile2);
        final TextView tvShortName2 = (TextView) ll.findViewById(R.id.tvShortName2);
        //
        //
        ControllerListener controllerListener2 = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName2.setVisibility(View.GONE);
                if (model.acceptedStatus != 1 && model.acceptedStatus != 3)
                    ivProfile2.setColorFilter(getFilter());
                else {
                    ivProfile2.clearColorFilter();
                }
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName2.setText(model.getTwoUpperChar(ByUModel.TypeUserByU.TYPE_USER_2));
                tvShortName2.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller2 = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener2)
                .setUri(model.urlProfile2).build();
        ivProfile2.setController(controller2);

        /*if (model.urlProfile2 == null) {
            ivProfile2.setImageResource(R.drawable.circle_gray);
            tvShortName2.setText(model.getTwoUpperChar(ByUModel.TypeUserByU.TYPE_USER_2));
        } else {
            Ion.with(ivProfile2).load(model.urlProfile2);
            tvShortName2.setVisibility(View.GONE);
        }*/
        //
        return ll;
    }

    public interface OnRelationClickListener {
        void onRelationClicked(String name, String reason, String url);
    }

    //
    private ColorMatrixColorFilter getFilter() {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);

        return filter;
    }
}
