package com.wd.introze.selectcontact;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.mysyncadapter.SyncUtils;
import com.wd.introze.payment.SendThankUActivity;

import com.wd.introze.repository.server.rf.model.user.Email;
import com.wd.introze.repository.server.rf.model.user.User;
import com.wd.introze.repository.server.rf.model.user.UserDetails;
import com.wd.introze.tables.TableContacts;
import com.wd.introze.tables.TableEmail;
import com.wd.introze.tables.TablePhone;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flair on 28-07-2016
 */
public class SelectContactActivity extends Fragment implements TextWatcher, SelectAdapter.ContactClickListener {
    //
    private LinearLayout llEmptyView, llContacts;
    //
    static boolean isFromCreateAccount;
    //
    private List<TableContacts> data = new ArrayList<>();
    private SelectAdapter adapter;

    /*//
    String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //*/
    //private ProgressDialog pd;
    //
    //private CustomProgress pd;
    //
    private SharedPreferences spUser;
    //
    private UserDetails userDetails;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;


    //
    public static SelectContactActivity getInstance(boolean isFromCreateAccount) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("is_from_create_account", isFromCreateAccount);
        //
        SelectContactActivity fragment = new SelectContactActivity();
        fragment.setArguments(bundle);
        //
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_select_contact, null);
        TypefaceHelper.typeface(view);
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        /*pd = new ProgressDialog(this);
        pd.setMessage("Please wait...");*/
        //
        isFromCreateAccount = getArguments().getBoolean("is_from_create_account", false);
        //
        llEmptyView = (LinearLayout) view.findViewById(R.id.llEmptyView);
        //
        llContacts = (LinearLayout) view.findViewById(R.id.llContacts);
        //
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setHasFixedSize(true);
        //
        adapter = new SelectAdapter(getActivity(), data);
        rv.setAdapter(adapter);
        //
        //lv.setOnItemClickListener(this);
        //
        EditText et = (EditText) view.findViewById(R.id.et);
        et.addTextChangedListener(this);
        //
        builderError = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        String userDetail = spUser.getString(User.class.getName(), null);
        if (userDetail != null) {
            User user = new Gson().fromJson(userDetail, User.class);
            userDetails = user.userDetails;
        }
        //
        loadContactsFromDB();
        //
        return view;
    }

    //
    private void loadContactsFromDB() {
        if (!spUser.getBoolean("is_contact_sync_db", false)) {
            //pd = CustomProgress.show(getActivity(), null, false, true, null);
        }
        List<TableContacts> items = new Select().from(TableContacts.class).orderBy("FirstName COLLATE NOCASE ASC").execute();
        data.clear();
        data.addAll(items);
        //
        adapter.notifyDataSetChanged();
        //
        setEmptyView();
        /*if (spUser.getBoolean("is_contact_sync_db", false) && pd != null && pd.isShowing()) {
            pd.dismiss();
        }*/
    }

    //
    private void setEmptyView() {
        if (data.size() > 0) {
            llContacts.setVisibility(View.VISIBLE);
            llEmptyView.setVisibility(View.GONE);
        } /*else if (pd == null || !pd.isShowing()) {
            llContacts.setVisibility(View.GONE);
            llEmptyView.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        data.clear();
        if (TextUtils.isEmpty(s)) {
            List<TableContacts> items = new Select().from(TableContacts.class).orderBy("FirstName ASC").execute();
            data.addAll(items);
        } else {
            List<TableContacts> items = new Select().from(TableContacts.class).
                    where("FirstName LIKE ?", "%" + s.toString() + "%").orderBy("FirstName ASC").execute();
            data.addAll(items);
        }
        adapter.notifyDataSetChanged();
    }

    //
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadContactsFromDB();
            //if (pd.isShowing()) pd.dismiss();
        }
    };

    //
    //
    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(mBroadcastReceiver, new IntentFilter("load_contacts_from_db"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mBroadcastReceiver);
    }

    //
    public void onClickBtnSyncContact(View view) {
        //pd = CustomProgress.show(this, "", false, false, null);
        llEmptyView.setVisibility(View.GONE);
        SyncUtils.TriggerRefresh();
    }

    @Override
    public void onContactClicked(TableContacts model, boolean is4Profile) {
        if (isUser(model.getId())) {
            builderError.setErrorMessage("Alert", "You cannot create Introze for yourself.");
            adError.show();
            return;
        }
        //
        /*if (is4Profile && model.isIntroze) {
            Intent intent = new Intent(this, FeedProfileActivity.class);
            intent.putExtra("user_id", model.introzeUserId + "");
            //
            FeedModel model1 = new FeedModel(0, model.firstName, model.lastName, null, null, 0, model.urlPic, false, null, null);
            //
            intent.putExtra("feed_model", new Gson().toJson(model1));
            startActivity(intent);
        } else {*/
        Intent intent = new Intent();
        intent.putExtra("url_profile", model.urlPic);
        intent.putExtra("_id", model.getId());
        intent.putExtra("id", model.identifier + "");//new Gson().toJson(data.get(position)));
        intent.putExtra("fn", model.firstName + " " + model.lastName);
        intent.putExtra("iu", model.isIntroze);
        intent.putExtra("ui", model.introzeUserId);
        if (!model.isIntroze)
            intent.putExtra("ci", model.identifier + "");
        //Toast.makeText(SelectContactActivity.this, "Contact id" + data.get(position).identifier, Toast.LENGTH_SHORT).show();
        /*setResult(RESULT_OK, intent);
        finish();*/
        //}
    }

    //
    private boolean isUser(long _id) {
        List<TablePhone> phones = new Select().from(TablePhone.class).where("Contacts =? AND Phone =?", _id, userDetails.phone).execute();
        if (phones.size() > 0) return true;
        //
        List<TableEmail> emails = new Select().from(TableEmail.class).where("Contacts =? AND Email =?", _id, userDetails.email).execute();
        if (emails.size() > 0) return true;
        //
        //
        boolean isExists = false;
        for (Email e : userDetails.emails) {
            List<TableEmail> em = new Select().from(TableEmail.class).where("Contacts =? AND Email =?", _id, e).execute();
            if (em.size() > 0) {
                isExists = true;
                break;
            }
        }
        //
        return isExists;
    }

    @Override
    public void onPaymentClickListener(TableContacts model) {
        //
        Intent intent = new Intent(getActivity(), SendThankUActivity.class);
        intent.putExtra("user_id", model.introzeUserId);
        intent.putExtra("user_name", model.firstName + " " + model.lastName);
        intent.putExtra("url_user_pic", model.urlPic == null ?
                ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, model.identifier).toString() : model.urlPic);
        //
        startActivity(intent);
        //finish();
    }

    @Override
    public void onChatClicked(TableContacts contacts) {

    }
}
