package com.wd.introze;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.chat.ui.activity.ChatActivity;
import com.quickblox.sample.chat.utils.chat.ChatHelper;
import com.quickblox.users.model.QBUser;
import com.wd.introze.activities.ActivityModel;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JSession;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.match.MatchModel;
import com.wd.introze.payment.SendThankUActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by flair on 08-08-2016
 */
public class MatchDoneActivity extends Fragment implements View.OnClickListener {
    //
    private final String TAG = "MatchDone";
    //
    private Button btnSendMessage, btnSendThankU;
    //
    private int userId1, userId2;
    //
    private ActivityModel model;
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    private String byFullName, relFullName;

    //
    private SharedPreferences spIntroze;
    //
    private CustomProgress pd;

    //
    public static MatchDoneActivity getInstance(String byFullName, String relFullName, String activityModel) {
        Bundle bundle = new Bundle();
        bundle.putString("by_full_name", byFullName);
        bundle.putString("rel_full_name", relFullName);
        bundle.putString(ActivityModel.class.getName(), activityModel);
        //
        MatchDoneActivity fragment = new MatchDoneActivity();
        fragment.setArguments(bundle);
        //
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_match_done, null);
        //
        //tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        //initHeader("NEW INTROZE INVITATION");
        //
        //Intent intent = getIntent();
        Bundle bundle = getArguments();
        String byFullName = bundle.getString("by_full_name");
        String relFullName = bundle.getString("rel_full_name");
        model = new Gson().fromJson(bundle.getString(ActivityModel.class.getName()), ActivityModel.class);
        //
        ((TextView) view.findViewById(R.id.btnSendMessage)).setText("Send Message to " + relFullName);
        //
        btnSendThankU = (Button) view.findViewById(R.id.btnThankYou);
        btnSendThankU.setText("Send Thank you $$$ to " + byFullName);
        btnSendThankU.setOnClickListener(this);
        //
        view.findViewById(R.id.llSendMessage).setOnClickListener(this);
        //
        spIntroze = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        return view;
    }

    /*//
    private void initHeader(String text) {
        TextView txtHeader = (TextView) view.findViewById(R.id.txtHeader);
        txtHeader.setText(text);
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnThankYou:
                sendThankU();
                break;
            case R.id.llSendMessage:
                sendMessage();
                break;
        }
    }

    //
    //
    private void markReadMatch(final MatchModel model) {
        //
        Log.d("RElId", model.rel_id + " Rel Id");
        if (!JConnectionUtils.isConnectedToInternet(getActivity())) return;
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        Ion.with(this).load("PUT", AppUrls.MAIN + "/mark_read").setHeader("Authorization", JUtils.getHeader(spIntroze))
                .setBodyParameter("rel_id", model.rel_id + "").asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("MarkResult", "" + result);
                //
                pd.dismiss();
                //
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            ((HomeActivity) getActivity()).getMatchCount();
                            callChat(model);
                        } else {
                            markReadMatch(model);
                            /*builderError.setErrorMessage(joResult.getString("message"));
                            adError.show();*/
                        }
                    } catch (JSONException e1) {
                        markReadMatch(model);
                        /*builderError.setErrorMessage(getString(R.string.error_no_response));
                        adError.show();*/
                    }
                } else {
                    markReadMatch(model);
                    /*builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();*/
                }
            }
        });
    }

    //
    private void sendThankU() {
        //TableContacts model = data.get(position);
        //
        Intent intent = new Intent(getActivity(), SendThankUActivity.class);
        intent.putExtra("user_id", model.byUserId);//model.introzeUserId);
        intent.putExtra("user_name", model.byFirstName + " " + model.byLastName);
        intent.putExtra("url_user_pic", model.urlByUser);
        //
        startActivity(intent);
        getActivity().onBackPressed();
    }

    //
    private void sendMessage() {
        //
        getUserDetail();
        //((HomeActivity) getActivity()).onClickLlMatches(null);
        /*Intent intent = new Intent();
        intent.putExtra("data", new Gson().toJson(model));*/
        /*setResult(RESULT_OK, intent);
        finish();*/
        /*MatchModel model = new MatchModel(this.model.getRelFullName(), null, this.model.urlProfile, 0,
                null, null, 0, 0, this.model.relId, this.model.relUserId);*/
        //callChat(model);
    }

    //
    private void callChat(MatchModel model) {
        if (spIntroze.getBoolean("is_chat_init", false)) {
            JSession.chatUserName = model.name;
            JSession.introzeId = model.rel_user_id + "";
            JSession.urlRel = model.url;
            openChat(model.chatId);
        } else
            Toast.makeText(getActivity(), "Chat initializing please wait...", Toast.LENGTH_SHORT).show();
    }

    //
    private void getUserDetail() {
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/get_user_detail/" + model.relUserId)//getActivity().getIntent().getStringExtra("user_id"))
                .setHeader("Authorization", JUtils.getHeader(spIntroze)).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                pd.dismiss();
                Log.d(TAG, result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            JSONObject joDetail = joResult.getJSONObject("user_details");
                            int chatId = joDetail.getInt("user_chat_id");
                            //
                            MatchModel model = new MatchModel(MatchDoneActivity.this.model.getRelFullName(),
                                    "", MatchDoneActivity.this.model.urlProfile, chatId, null, null, 0, 0, MatchDoneActivity.this.model.relId,
                                    MatchDoneActivity.this.model.relUserId, 0, null, null, null, "", "", "", 0, 0);
                            //callChat(model);
                            markReadMatch(model);
                        } else {
                            getUserDetail();
                        }
                    } catch (JSONException e1) {
                        getUserDetail();
                    }
                } else {
                    getUserDetail();
                }
            }
        });
    }

    //
    public void openChat(int chatId) {
        ArrayList<QBUser> selectedUsers = new ArrayList<>();
        //
        selectedUsers.add(new QBUser(chatId));
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        pd.dismiss();
                        ChatActivity.startForResult(getActivity(), 5000, dialog);
                        getActivity().onBackPressed();
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        pd.dismiss();
                        Log.d("DialogError", "" + e);
                        //Toast.makeText(MainActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}
