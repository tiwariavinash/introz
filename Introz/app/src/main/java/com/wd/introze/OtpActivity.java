package com.wd.introze;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JErrorTextWatcher;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by flair on 19-07-2016
 */
public class OtpActivity extends AppCompatActivity {
    //
    private EditText etOtp;
    //
    private TextView tvError;
    //
    private SharedPreferences spUser;
    //
    private ProgressDialog pd;
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    private AlertDialog confirmPhoneAlertDialog;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;

    //
    LinearLayout llClickResendOtp;
    CustomProgress customProgress;
    //
    private boolean isFromSuccess;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        //
        TypefaceHelper.typeface(this);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        confirmPhoneAlertDialog = new ConfirmPhoneAlertDialog(this).create();
        confirmPhoneAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
        /*invalidConfirmationAlertDialog = new InvalidConfirmationAlertDialog(this).create();
        invalidConfirmationAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));*/
        builderError = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
                if (isFromSuccess) {
                    startActivity(new Intent(OtpActivity.this, HomeActivity.class));
                    finish();
                }
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        initHeader();
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("CONFIRMATION CODE");*/
        //
        tvError = (TextView) findViewById(R.id.tvError);
        //
        etOtp = (EditText) findViewById(R.id.etOtp);
        etOtp.addTextChangedListener(new JErrorTextWatcher(tvError, "Please Enter Verification Code"));
        //
        llClickResendOtp = (LinearLayout) findViewById(R.id.llClickResendOtp);
        llClickResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //
        spUser = getSharedPreferences("user", MODE_PRIVATE);
        //
        pd = new ProgressDialog(this);
        pd.setMessage("Please wait...");
        JUser.setIsVerified(OtpActivity.this, true);
    }

    private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("CONFIRMATION CODE");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }

    //
    public void onClickBtnOtp(View view) {
        String otp = etOtp.getText().toString();
        if (TextUtils.isEmpty(otp)) {
            //Toast.makeText(OtpActivity.this, "Please enter otp", Toast.LENGTH_SHORT).show();
            tvError.setText("Please Enter Verification Code");
            return;
        }
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        //pd.show();
        customProgress = CustomProgress.show(OtpActivity.this, "", false, false, null);
        Ion.with(this).load("PUT", AppUrls.MAIN + "/verify_mobile").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("mobile", getIntent().getStringExtra("mobile")).setBodyParameter("otp", otp).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        //pd.dismiss();
                        customProgress.dismiss();
                        Log.d("UserResult", result + "");
                        if (e == null) {
                            //
                            try {
                                JSONObject joResult = new JSONObject(result);
                                //Toast.makeText(OtpActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                                if (TextUtils.equals("success", joResult.getString("result"))) {
                                    /*startActivity(new Intent(OtpActivity.this, HomeActivity.class));
                                    finish();*/
                                    isFromSuccess = true;
                                    JUser.setIsVerified(OtpActivity.this, false);
                                    showAlertDialog("Your phone number has been confirmed.");
                                    //confirmPhoneAlertDialog.show();
                                } else {
                                    //
                                    showAlertDialog(joResult.getString("message"));
                                    /*builderError.setErrorMessage(joResult.getString("message"));
                                    adError.show();*/
                                }
                            } catch (JSONException e1) {
                                //
                                showAlertDialog(getString(R.string.error_no_response));
                                /*builderError.setErrorMessage(getString(R.string.error_no_response));
                                adError.show();*/
                                //Toast.makeText(OtpActivity.this, "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //
                            showAlertDialog(getString(R.string.error_no_response));
                            /*builderError.setErrorMessage(getString(R.string.error_no_response));
                            adError.show();*/
                            //Toast.makeText(OtpActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    //
    private void showAlertDialog(String message) {
        builderError.setErrorMessage(message);
        adError.show();
        TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
    }

    //
    private class ConfirmPhoneAlertDialog extends AlertDialog.Builder implements View.OnClickListener {

        public ConfirmPhoneAlertDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_confirmation_code_thanks, null);
            TypefaceHelper.typeface(view);
            //
            setView(view, 50, 0, 50, 0);
            //
            view.findViewById(R.id.btnYay).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            confirmPhoneAlertDialog.dismiss();
            startActivity(new Intent(OtpActivity.this, HomeActivity.class));
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        /*LoginManager.getInstance().logOut();
        super.onBackPressed();*/
    }

    //
    public void onClickClickHere(View view) {
        //
        Intent intent = new Intent(this, PhoneVerificationActivity.class);
        startActivity(intent);
        finish();
        //
    }
}
