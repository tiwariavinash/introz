package com.wd.introze;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.facebook.login.widget.LoginButton;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.signup.PPActivity;
import com.wd.introze.signup.TOSActivity;

import java.util.Arrays;

public class MainActivity extends FBLoginActivity {

    Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        TypefaceHelper.typeface(this);
        //
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });
        //
        //
        loginButton = (LoginButton) findViewById(R.id.login_button);

        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        //loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, callback);

    }


    @Override
    public void onResume() {
        super.onResume();
        //

       /* Profile profile = Profile.getCurrentProfile();
        if (profile != null)
            Toast.makeText(MainActivity.this, "OnResume " + profile.getName(), Toast.LENGTH_SHORT).show();*/
    }

    //
    public void onClickTvLogin(View view) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void onClickTvLogins(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void onClickTermsService(View view) {
        //startActivity(new Intent(this, TermsOfServicesActivity.class));
        startActivity(new Intent(this, TOSActivity.class));
    }

    public void onClickPrivacyPolicy(View view) {
        //startActivity(new Intent(this, PrivacyPolicyActivity.class));
        startActivity(new Intent(this, PPActivity.class));
    }

    //
    public void onClickTvForgotPassword(View view) {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    //
    public void onClickBtnSignUp(View view) {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000 && resultCode == RESULT_OK) {
            Intent intent = new Intent(this, PhoneVerificationActivity.class);
            startActivity(intent);
        }
    }*/
}
