package com.wd.introze;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.jutil.SoftKeyboardLsnedRelativeLayout;
import com.wd.introze.repository.server.rf.model.user.Status;
import com.wd.introze.repository.server.rf.model.user.User;


import java.util.ArrayList;

/**
 * Created by flair on 21-07-2016
 */
//8776092233
public class IntroductionTypeActivity extends Fragment implements View.OnFocusChangeListener {
    private static final String TAG = "IntroductionType";
    //
    private EditText etBusiness, etDating, etFriendship, etMutualInterest;
    //
    private ImageView ivBusiness, ivDating, ivFriendship, ivMutualInterest;
    //
    private SharedPreferences spUser;
    //
    private ProgressDialog pd;
    //
    CustomProgress customProgress;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    /*String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;*/
    //
    private LinearLayout llBottomButton;
    //
    private User user;
    //
    private ArrayList<Integer> alChangeFeedType = new ArrayList<>();

    //
    public static IntroductionTypeActivity getInstance(int relationType) {
        Bundle bundle = new Bundle();
        bundle.putInt("relation_type", relationType);
        //
        IntroductionTypeActivity fragment = new IntroductionTypeActivity();
        fragment.setArguments(bundle);
        //
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_introduce_type, null);
        TypefaceHelper.typeface(view);
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait...");
        //
        user = new Gson().fromJson(spUser.getString(User.class.getName(), null), User.class);
        Status status = user.userDetails.status;
        //
        etBusiness = (EditText) view.findViewById(R.id.etBusiness);
        etBusiness.setOnFocusChangeListener(this);
        etBusiness.setText(status.business);//intent.getStringExtra("business"));
        etBusiness.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etBusiness.requestFocus();
                etBusiness.setCursorVisible(true);
                return false;
            }
        });
        //
        etBusiness.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!alChangeFeedType.contains(0))
                    alChangeFeedType.add(0);
            }
        });

        //JUtils.setCursorDrawableColor(etBusiness, Color.parseColor("#ed1d7a"));
        //
        etDating = (EditText) view.findViewById(R.id.etDating);
        etDating.setOnFocusChangeListener(this);
        etDating.setText(status.dating);//intent.getStringExtra("dating"));
        etDating.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etDating.requestFocus();
                etDating.setCursorVisible(true);
                return false;
            }
        });
        //
        etDating.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!alChangeFeedType.contains(1))
                    alChangeFeedType.add(1);
            }
        });
        //JUtils.setCursorDrawableColor(etDating, Color.parseColor("#ed1d7a"));
        //
        etFriendship = (EditText) view.findViewById(R.id.etFriendship);
        etFriendship.setOnFocusChangeListener(this);
        etFriendship.setText(status.friendship);//intent.getStringExtra("friendship"));
        etFriendship.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etFriendship.requestFocus();
                etFriendship.setCursorVisible(true);
                return false;
            }
        });
        //
        etFriendship.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!alChangeFeedType.contains(2))
                    alChangeFeedType.add(2);
            }
        });
        //JUtils.setCursorDrawableColor(etFriendship, Color.parseColor("#ed1d7a"));
        //
        etMutualInterest = (EditText) view.findViewById(R.id.etMutualInterest);
        etMutualInterest.setOnFocusChangeListener(this);
        etMutualInterest.setText(status.mutualInterests);//intent.getStringExtra("mutual_interest"));
        etMutualInterest.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                etMutualInterest.requestFocus();
                etMutualInterest.setCursorVisible(true);
                return false;
            }
        });
        //
        etMutualInterest.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!alChangeFeedType.contains(3))
                    alChangeFeedType.add(3);
            }
        });
        //JUtils.setCursorDrawableColor(etMutualInterest, Color.parseColor("#ed1d7a"));
        //

        //
        ivBusiness = (ImageView) view.findViewById(R.id.ivBusiness);
        ivDating = (ImageView) view.findViewById(R.id.ivDating);
        ivFriendship = (ImageView) view.findViewById(R.id.ivFriendship);
        ivMutualInterest = (ImageView) view.findViewById(R.id.ivMutualInterest);
        //
        switch (getArguments().getInt("relation_type")) {
            case 0:
                //ivBusiness.setVisibility(View.GONE);
                etBusiness.requestFocus();
                break;
            case 1:
                ///ivDating.setVisibility(View.GONE);
                etDating.requestFocus();
                break;
            case 2:
                //ivFriendship.setVisibility(View.GONE);
                etFriendship.requestFocus();
                break;
            case 3:
                //ivMutualInterest.setVisibility(View.GONE);
                etMutualInterest.requestFocus();
                break;
        }
        //
        llBottomButton = (LinearLayout) view.findViewById(R.id.llBottomButton);
        //
        SoftKeyboardLsnedRelativeLayout layout = (SoftKeyboardLsnedRelativeLayout) view.findViewById(R.id.skhl);
        layout.addSoftKeyboardLsner(new SoftKeyboardLsnedRelativeLayout.SoftKeyboardLsner() {
            @Override
            public void onSoftKeyboardShow() {
                //((HomeActivity) getActivity()).hideBottom(true);
                llBottomButton.setVisibility(View.VISIBLE);
                Log.d("SoftKeyboard", "Soft keyboard shown");
            }

            @Override
            public void onSoftKeyboardHide() {
                llBottomButton.setVisibility(View.GONE);
                //((HomeActivity) getActivity()).hideBottom(false);
                Log.d("SoftKeyboard", "Soft keyboard hidden");
            }
        });
        //
        view.findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickBtnSave(v);
            }
        });
        //
        view.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickBtnCancel(v);
            }
        });
        //
        builderError = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        return view;
    }

    //
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.etBusiness:
                if (hasFocus)
                    ivBusiness.setVisibility(View.GONE);
                else ivBusiness.setVisibility(View.VISIBLE);
                break;
            case R.id.etDating:
                if (hasFocus)
                    ivDating.setVisibility(View.GONE);
                else ivDating.setVisibility(View.VISIBLE);
                break;
            case R.id.etFriendship:
                if (hasFocus)
                    ivFriendship.setVisibility(View.GONE);
                else ivFriendship.setVisibility(View.VISIBLE);
                break;
            case R.id.etMutualInterest:
                if (hasFocus)
                    ivMutualInterest.setVisibility(View.GONE);
                else ivMutualInterest.setVisibility(View.VISIBLE);
                break;
        }
    }

    int j = 4;

    //
    public void onClickBtnSave(View view) {
        //
        j = alChangeFeedType.size();
        //
        final String[] feeds = {etBusiness.getText().toString(), etDating.getText().toString(), etFriendship.getText().toString(),
                etMutualInterest.getText().toString()};
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        //pd.show();
        customProgress = CustomProgress.show(getActivity(), null, false, true, null);
        for (int i = 0; i < 4; i++) {
            //
            if (!alChangeFeedType.contains(i)) {
                continue;
            }
            //
            Ion.with(this).load("POST", AppUrls.MAIN + "/add_feed").setHeader("Authorization", "Basic " + basic)
                    .setBodyParameter("feed_type", (i + 1) + "").setBodyParameter("feed_status", feeds[i])
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    Log.d(TAG, "Introduction Type Result " + result);
                    if (result != null) {
                        SuccessModel model = new Gson().fromJson(result, SuccessModel.class);
                        if (TextUtils.equals("success", model.result)) {
                            --j;
                            if (j == 0) {
                                //pd.dismiss();
                                customProgress.dismiss();
                                Status status = user.userDetails.status;
                                status.business = feeds[0];
                                status.dating = feeds[1];
                                status.friendship = feeds[2];
                                status.mutualInterests = feeds[3];
                                //
                                spUser.edit().putString(User.class.getName(), new Gson().toJson(user)).commit();
                                onClickBtnCancel(null);
                            }
                        } else if (TextUtils.isEmpty(model.message)) {
                            builderError.setErrorMessage(getString(R.string.error_no_response));
                            adError.show();
                        } else {
                            builderError.setErrorMessage(model.message);
                            adError.show();
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnIntroTypeClickListener) context;
    }

    //
    private OnIntroTypeClickListener listener;

    //
    public void onClickBtnCancel(View view) {
        JUtils.hideSoftKeyboard(getActivity());
        listener.onIntrozeTypeCancelClicked();
    }

    //
    public interface OnIntroTypeClickListener {
        void onIntrozeTypeCancelClicked();
    }
}
