package com.wd.introze.feed;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;
import com.wd.introze.jutil.JUtils;

import java.util.ArrayList;

/**
 * Created by flair on 21-07-2016
 */
public class FeedAdapter extends BaseAdapter {
    //
    private ArrayList<FeedModel> data;
    private Context context;
    //
    private OnFeedBtnAddClickListener listener;

    //
    public FeedAdapter(Context context, ArrayList<FeedModel> data) {
        this.context = context;
        this.data = data;
        //
        listener = (OnFeedBtnAddClickListener) context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        //
        LinearLayout ll = (LinearLayout) convertView;
        if (ll == null) {
            ll = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.activity_feed_list_item, null);
            TypefaceHelper.typeface(ll);
        }
        //
        LinearLayout llMain = (LinearLayout) ll.findViewById(R.id.llMain);
        if (position == 0) llMain.setBackgroundResource(R.drawable.feed_first_shape);
        else llMain.setBackgroundResource(R.drawable.feed_second_shape);
        //
        final FeedModel model = data.get(position);
        //
        ImageView ivFeedType = (ImageView) ll.findViewById(R.id.ivFeedType);
        ivFeedType.setImageResource(JUtils.getRelationTypeIcon(data.get(position).feedType));//data.get(position).getRelationTypeIcon());
        //
        TextView tvRelationType = (TextView) ll.findViewById(R.id.tvRelationType);
        tvRelationType.setText(data.get(position).getRelationTypeText());
        //
        TextView tvFeed = (TextView) ll.findViewById(R.id.tvFeed);
        tvFeed.setText(data.get(position).statusMessage);
        //
        TextView tvDate = (TextView) ll.findViewById(R.id.tvDate);
        tvDate.setText(data.get(position).postedOn);
        //profile pic
        final SimpleDraweeView ivProfile = (SimpleDraweeView) ll.findViewById(R.id.ivProfile);
        final TextView tvShortName = (TextView) ll.findViewById(R.id.tvShortName);
        //
        getProfilePicture(model.firstName + " " + model.lastName, model.urlProfile, ivProfile, tvShortName);
        //profile pic
        TextView tvName = (TextView) ll.findViewById(R.id.tvName);
        tvName.setText(JUtils.uppercaseFirstLetters(model.firstName + " " + model.lastName));
        //
        ImageView ivPlus = (ImageView) ll.findViewById(R.id.ivAdd);
        if (model.isUser) {
            //ivPlus.setVisibility(View.GONE);
            ivPlus.setImageResource(android.R.color.transparent);
        } else {
            //ivPlus.setVisibility(View.VISIBLE);
            ivPlus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_green_plus_icon));
            ivPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        int identifier = Integer.parseInt(model.identifier);
                        listener.onFeedBtnAddClicked(identifier);
                    } catch (NumberFormatException e) {

                    }
                }
            });
        } //
        return ll;
    }

    //
    public interface OnFeedBtnAddClickListener {
        void onFeedBtnAddClicked(int identifier);
    }

    //
    private void getProfilePicture(final String fullName, String url, final SimpleDraweeView ivProfile, final TextView tvShortName) {
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(JUtils.getTwoUpperChar(fullName));
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //AppUrls.MAIN + "/" + data.get(position).urlProfile
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(url).build();
        ivProfile.setController(controller);
    }
}
