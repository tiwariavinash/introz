
package com.wd.introze.repository.server.rf.model.introzeuser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("identifier")
    @Expose
    public String identifier;
    @SerializedName("contact_id")
    @Expose
    public Integer contactId;
    @SerializedName("introze_user_id")
    @Expose
    public Integer introzeUserId;
    @SerializedName("introze_user")
    @Expose
    public Integer introzeUser;
    @SerializedName("file_url")
    @Expose
    public String fileUrl;
    @SerializedName("facebook_id")
    @Expose
    public String facebookId;
    @SerializedName("matched")
    @Expose
    public Integer matched;
    @SerializedName("chat_id")
    @Expose
    public Integer chatId;
    @SerializedName("fb_id")
    @Expose
    public String fbId;
}
