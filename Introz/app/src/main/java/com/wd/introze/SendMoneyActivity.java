package com.wd.introze;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JErrorTextWatcher;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.payment.ConfirmActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by flair on 29-07-2016
 */
public class SendMoneyActivity extends AppCompatActivity implements View.OnClickListener {
    //
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private ArrayAdapter arrayAdapterYears;
    //
    private SharedPreferences spUser;
    private EditText etCardNumber, etExpirationDate, etYear, etCardHolderName;//, etCvv;
    private TextView tvECardNumber, tvEMonth, tvEYear, tvECardholderName;//, tvECvv;
    //
    private int month, year;
    //
    TextView tvText;
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    private AlertDialog adMonth, adYear;
    //
    private JSimpleBuilder builderE;
    private AlertDialog adE;
    //
    private CustomProgress pd;
    //
    private boolean isFromThankU;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);
        //
        TypefaceHelper.typeface(this);
        //
        tfText = Typeface.createFromAsset(getAssets(), fontPathText);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        tvECardNumber = (TextView) findViewById(R.id.tvECardNumber);
        tvEMonth = (TextView) findViewById(R.id.tvEMonth);
        tvEYear = (TextView) findViewById(R.id.tvEYear);
        tvECardholderName = (TextView) findViewById(R.id.tvECardholderName);
        //tvECvv = (TextView) findViewById(R.id.tvECvv);
        //
        etCardNumber = (EditText) findViewById(R.id.etCardNumber);
        etCardNumber.addTextChangedListener(new JErrorTextWatcher(tvECardNumber, "*"));
        //
        etExpirationDate = (EditText) findViewById(R.id.etDate);
        etExpirationDate.addTextChangedListener(new JErrorTextWatcher(tvEMonth, "*"));
        //
        etYear = (EditText) findViewById(R.id.etDate1);
        etYear.addTextChangedListener(new JErrorTextWatcher(tvEYear, "*"));
        //
        etCardHolderName = (EditText) findViewById(R.id.etCardHolderName);
        etCardHolderName.addTextChangedListener(new JErrorTextWatcher(tvECardholderName, "*"));
        //
        isFromThankU = getIntent().getBooleanExtra("is_from_thank_u", false);
        /*etCvv = (EditText) findViewById(R.id.etCvv);
        etCvv.addTextChangedListener(new JErrorTextWatcher(tvECvv, "*"));*/
        //
        initHeader();
        //
        spUser = getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        tvText = (TextView) findViewById(R.id.tvText);
        tvText.setTypeface(tfText);
        //
        findViewById(R.id.llMonth).setOnClickListener(this);
        findViewById(R.id.llYear).setOnClickListener(this);
        //
        final ArrayAdapter<String> arrayAdapterMonth = new ArrayAdapter<String>(this, R.layout.activity_send_money_month_list,
                R.id.tv) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TypefaceHelper.typeface(view);
                return view;
            }
        };
        for (int i = 0; i < 12; i++) {
            arrayAdapterMonth.add(months[i]);
        }
        //
        AlertDialog.Builder builderMonth = new AlertDialog.Builder(this);
        builderMonth.setAdapter(arrayAdapterMonth, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                month = which;
                etExpirationDate.setText(months[which]);
            }
        });
        adMonth = builderMonth.create();
        //
        GregorianCalendar gc = new GregorianCalendar();
        int cYear = gc.get(Calendar.YEAR);
        //
        arrayAdapterYears = new ArrayAdapter<String>(this, R.layout.activity_send_money_month_list,
                R.id.tv) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TypefaceHelper.typeface(view);
                return view;
            }
        };
        for (int i = 0; i < 10; i++) {
            arrayAdapterYears.add((cYear + i) + "");
        }
        //
        AlertDialog.Builder builderYear = new AlertDialog.Builder(this);
        builderYear.setAdapter(arrayAdapterYears, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String y = arrayAdapterYears.getItem(which).toString();
                year = Integer.parseInt(y);
                etYear.setText(y);
            }
        });
        adYear = builderYear.create();
        //
        builderE = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adE.dismiss();
            }
        });
        adE = builderE.create();
        //adE.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        String paymentDetail = spUser.getString(PaymentDetail.class.getName(), null);
        if (TextUtils.isEmpty(paymentDetail))
            getCardDetail();
        else {
            setBankDetail(new Gson().fromJson(paymentDetail, PaymentDetail.class));
        }
    }

    //
    private void setBankDetail(PaymentDetail model) {
        etCardNumber.setText(model.last4);
        //if (model.month != null) {
        month = model.month;
        etExpirationDate.setText(months[month]);
        //}
        // if (model.year != null)
        etYear.setText(model.year + "");
        //if (model.name != null)
        etCardHolderName.setText(model.name);
    }

    //
    private void saveBankDetail(PaymentDetail model) {
        spUser.edit().putString(PaymentDetail.class.getName(), new Gson().toJson(model)).commit();
        setBankDetail(model);
    }

    //
    private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("SEND MONEY SETTINGS");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }

    //
    private void showErrorMessage(String title, String message) {
        builderE.setErrorMessage(title, message);
        adE.show();
        TypefaceHelper.typeface(adE.getButton(DialogInterface.BUTTON_POSITIVE));
    }

    //
    public void onClickBtnConfirm(View view) {
        final String cardNumber = etCardNumber.getText().toString();
        if (TextUtils.isEmpty(cardNumber)) {
            tvECardNumber.setText("*");
            showErrorMessage("Alert", "Please enter card number");
            return;
        }
        //
        if (cardNumber.length() < 15 || cardNumber.length() > 16) {
            tvECardNumber.setText("*");
            showErrorMessage("Alert", "Card number must be 15 or 16 digits.");
            //Toast.makeText(SendMoneyActivity.this, "Please enter card number", Toast.LENGTH_SHORT).show();
            return;
        }
        //
        String expirationDate = etExpirationDate.getText().toString();
        if (TextUtils.isEmpty(expirationDate)) {
            tvEMonth.setText("*");
            showErrorMessage("Alert", "Please select month");
            //builderError.displayMessage();
            //Toast.makeText(SendMoneyActivity.this, "Please select month", Toast.LENGTH_SHORT).show();
            return;
        }
        //
        final String year = etYear.getText().toString();
        if (TextUtils.isEmpty(year)) {
            tvEYear.setText("*");
            showErrorMessage("Alert", "Please select year");
            //builderError.displayMessage();
            //Toast.makeText(SendMoneyActivity.this, "Please select year", Toast.LENGTH_SHORT).show();
            return;
        }
        //
        final String cardHolderName = etCardHolderName.getText().toString();
        if (TextUtils.isEmpty(cardHolderName)) {
            tvECardholderName.setText("*");
            showErrorMessage("Alert", "Please enter card holder name");
            //builderError.displayMessage();
            //Toast.makeText(SendMoneyActivity.this, "Please enter card holder name", Toast.LENGTH_SHORT).show();
            return;
        }
        //
        /*String cvv = etCvv.getText().toString();
        if (TextUtils.isEmpty(cvv)) {
            tvECvv.setText("*");
            builderError.displayMessage("Please enter cvv number");
            //Toast.makeText(SendMoneyActivity.this, "Please enter cvv number", Toast.LENGTH_SHORT).show();
            return;
        }*/
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        pd = CustomProgress.show(this, null, false, false, null);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/add_cc").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("card_number", cardNumber)//.setBodyParameter("cvv", cvv)
                .setBodyParameter("month", month + "")
                .setBodyParameter("year", year + "").setBodyParameter("name", cardHolderName)
                //.setBodyParameter("card_type", "")
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("PaymentDetail", "" + result);
                pd.dismiss();
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //
                            PaymentDetail model = new PaymentDetail(cardNumber.substring(cardNumber.length() - 4),
                                    cardHolderName, month, Integer.parseInt(year));
                            //
                            saveBankDetail(model);
                            /*//
                            spUser.edit().putString(PaymentDetail.class.getName(), new Gson().toJson(model)).commit();*/
                            //Toast.makeText(SendMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            if (isFromThankU) {
                                //
                                Intent intentData = getIntent();
                                //
                                Intent intent = new Intent(SendMoneyActivity.this, ConfirmActivity.class);
                                intent.putExtra("user_name", intentData.getStringExtra("user_name"));
                                intent.putExtra("amount", intentData.getIntExtra("amount", 0));
                                intent.putExtra("message", intentData.getStringExtra("message"));
                                intent.putExtra("user_id", intentData.getIntExtra("user_id", 0));
                                intent.putExtra("last_4", model.last4);
                                //
                                intent.putExtra("fees_detail", intentData.getStringExtra("fees_detail"));
                                startActivity(intent);
                                //
                                finish();
                            } else {
                                //
                                /*builderE.setErrorMessage("Success", joResult.getString("message"));
                                adE.show();*/
                                showErrorMessage("Success", joResult.getString("message"));
                                //setBankDetail(model);
                            }
                        } else {
                            //
                            showErrorMessage("Error", joResult.getString("message"));
                            /*builderE.setErrorMessage(joResult.getString("message"));
                            adE.show();*/
                        }
                        //Toast.makeText(SendMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e1) {
                        //
                        showErrorMessage("Error", getString(R.string.error_no_response));
                        /*builderE.setErrorMessage(getString(R.string.error_no_response));
                        adE.show();*/
                        //Toast.makeText(SendMoneyActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showErrorMessage("Error", getString(R.string.error_no_response));
                    /*builderE.setErrorMessage(getString(R.string.error_no_response));
                    adE.show();*/
                }
                //Toast.makeText(SendMoneyActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //
    public void getCardDetail() {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        pd = CustomProgress.show(this, null, false, false, null);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/get_cc").setHeader("Authorization", "Basic " + basic).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.d("CardDetail", result + "");
                        pd.dismiss();
                        if (result != null) {
                            //
                            PaymentDetail model = new Gson().fromJson(result, PaymentDetail.class);
                            //
                            if (TextUtils.equals("success", model.result)) {
                                //Toast.makeText(SendMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                                if (model.month == null || model.year == null || model.name == null) {
                                    /*if (!isFromThankU) {
                                        builderE.setErrorMessage(model.message);
                                        adE.show();
                                    }*/
                                } else {
                                    saveBankDetail(model);
                                }
                            } else {
                                //
                                showErrorMessage("Error", model.message);
                                /*builderE.setErrorMessage(model.message);
                                adE.show();*/
                            }
                            //Toast.makeText(SendMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();

                        } else {
                            //
                            showErrorMessage("Error", getString(R.string.error_no_response));
                            /*builderE.setErrorMessage(getString(R.string.error_no_response));
                            adE.show();*/
                        }
                        //Toast.makeText(SendMoneyActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMonth:
                adMonth.show();
                break;
            case R.id.llYear:
                adYear.show();
                break;
        }
    }

    //
    public class PaymentDetail {

        @SerializedName("result")
        @Expose
        public String result;
        @SerializedName("message")
        @Expose
        public String message;
        @SerializedName("last_4")
        @Expose
        public String last4;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("month")
        @Expose
        public Integer month;
        @SerializedName("year")
        @Expose
        public Integer year;
        @SerializedName("card_type")
        @Expose
        public String cardType;

        public PaymentDetail(String last4, String name, Integer month, Integer year) {
            this.last4 = last4;
            this.name = name;
            this.month = month;
            this.year = year;
        }
    }
}
