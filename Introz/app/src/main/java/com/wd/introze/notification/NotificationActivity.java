package com.wd.introze.notification;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.AppUrls;
import com.wd.introze.R;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JSimpleBuilder;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 2016-07-23
 */
public class NotificationActivity extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnTouchListener {
    //
    CustomProgress progress;
    //
    private SharedPreferences spUser;
    //
    private String[] values = {"0", "0", "0", "0", "0", "0", "0", "0"};
    //
    //
    private SwitchCompat cbPushInvitation;
    private SwitchCompat cbPush4U;
    private SwitchCompat cbPushByU, cbPushReceiveMoney, cbNotificationInvitation,
            cbNotification4U, cbNotificationByU, cbNotificationReceiveMoney;

    /*String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    String fontPathText1 = "sourcesanspro-regular.ttf";
    Typeface tfText1;
    //
    String fontPathText2 = "itcavantgardestd-md.ttf";
    Typeface tfText2;
    //
    String fontPathText3 = "itcavantgardestd-bold.ttf";
    Typeface tfText3;
    *///
    TextView tvInvitationP, tvIntForUP, tvIntByUP, tvReceiveMoneyP;
    //
    TextView tvInvitationE, tvIntForUE, tvIntByUE, tvReceiveMoneyE;
    //
    TextView tvPushNotification, tvEmailNotification;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private boolean isFromClicked;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_notification, null);
        //
        TypefaceHelper.typeface(view);
        //
       /* tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        tfText = Typeface.createFromAsset(getAssets(), fontPathText);
        //
        tfText1 = Typeface.createFromAsset(getAssets(), fontPathText1);
        //
        tfText2 = Typeface.createFromAsset(getAssets(), fontPathText2);
        //
        tfText3 = Typeface.createFromAsset(getAssets(), fontPathText3);*/
        //
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("NOTIFICATIONS");*/
        //
        //initHeader();
        //
        cbPushInvitation = (SwitchCompat) view.findViewById(R.id.cbPushInvitation);
        cbPushInvitation.setOnCheckedChangeListener(this);
        cbPushInvitation.setOnTouchListener(this);
        /*cbPushInvitation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isFromClicked = true;
                return false;
            }
        });*/
        //
        cbPush4U = (SwitchCompat) view.findViewById(R.id.cbPush4U);
        cbPush4U.setOnCheckedChangeListener(this);
        cbPush4U.setOnTouchListener(this);
        //
        cbPushByU = (SwitchCompat) view.findViewById(R.id.cbPushByU);
        cbPushByU.setOnCheckedChangeListener(this);
        cbPushByU.setOnTouchListener(this);
        //
        cbPushReceiveMoney = (SwitchCompat) view.findViewById(R.id.cbPushReceiveMoney);
        cbPushReceiveMoney.setOnCheckedChangeListener(this);
        cbPushReceiveMoney.setOnTouchListener(this);
        //
        cbNotificationInvitation = (SwitchCompat) view.findViewById(R.id.cbNotificationInvitation);
        cbNotificationInvitation.setOnCheckedChangeListener(this);
        cbNotificationInvitation.setOnTouchListener(this);
        //
        cbNotification4U = (SwitchCompat) view.findViewById(R.id.cbNotification4U);
        cbNotification4U.setOnCheckedChangeListener(this);
        cbNotification4U.setOnTouchListener(this);
        //
        cbNotificationByU = (SwitchCompat) view.findViewById(R.id.cbNotificationByU);
        cbNotificationByU.setOnCheckedChangeListener(this);
        cbNotificationByU.setOnTouchListener(this);
        //
        cbNotificationReceiveMoney = (SwitchCompat) view.findViewById(R.id.cbNotificationReceiveMoney);
        cbNotificationReceiveMoney.setOnCheckedChangeListener(this);
        cbNotificationReceiveMoney.setOnTouchListener(this);
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        tvInvitationP = (TextView) view.findViewById(R.id.tvInvitationP);
        tvIntForUP = (TextView) view.findViewById(R.id.tvIntForUP);
        tvIntByUP = (TextView) view.findViewById(R.id.tvIntByUP);
        tvReceiveMoneyP = (TextView) view.findViewById(R.id.tvReceiveMoneyP);
        //
        tvInvitationE = (TextView) view.findViewById(R.id.tvInvitationE);
        tvIntForUE = (TextView) view.findViewById(R.id.tvIntForUE);
        tvIntByUE = (TextView) view.findViewById(R.id.tvIntByUE);
        tvReceiveMoneyE = (TextView) view.findViewById(R.id.tvReceiveMoneyE);
        //
        tvPushNotification = (TextView) view.findViewById(R.id.tvPushNotification);
        tvEmailNotification = (TextView) view.findViewById(R.id.tvEmailNotification);
        //
        /*view.findViewById(R.id.btnUpdateSetting).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //onClickBtnUpdateSetting(v);
            }
        });*/
        /*tvInvitationP.setTypeface(tfText);
        tvIntForUP.setTypeface(tfText);
        tvIntByUP.setTypeface(tfText);
        tvReceiveMoneyP.setTypeface(tfText);
        //
        tvInvitationE.setTypeface(tfText);
        tvIntForUE.setTypeface(tfText);
        tvIntByUE.setTypeface(tfText);
        tvReceiveMoneyE.setTypeface(tfText);
        //
        tvPushNotification.setTypeface(tfText);
        tvEmailNotification.setTypeface(tfText);*/
        //
        builderError = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        getSetting1();
        //
        return view;
    }

    //
    private void getSetting1() {
        String val = spUser.getString("notification_setting", null);
        if (TextUtils.isEmpty(val)) {
            getSetting();
        } else {
            values = new Gson().fromJson(val, String[].class);
            initSettings();
        }
    }

    //
    private void initSettings() {
        /*//
        String valuesTemp = spUser.getString("notification_setting", null);
        //
        if (valuesTemp == null) {
            return;
        }
        values = new Gson().fromJson(valuesTemp, String[].class);
*/
        isFromClicked = false;
        //
        //
        boolean one = TextUtils.equals(values[0], "1") ? true : false;
        boolean two = TextUtils.equals(values[1], "1") ? true : false;
        boolean three = TextUtils.equals(values[2], "1") ? true : false;
        boolean four = TextUtils.equals(values[3], "1") ? true : false;
        boolean five = TextUtils.equals(values[4], "1") ? true : false;
        boolean six = TextUtils.equals(values[5], "1") ? true : false;
        boolean seven = TextUtils.equals(values[6], "1") ? true : false;
        boolean eight = TextUtils.equals(values[7], "1") ? true : false;
        //
        cbPushInvitation.setChecked(one);
        cbPush4U.setChecked(two);
        cbPushByU.setChecked(three);
        cbPushReceiveMoney.setChecked(four);
        //
        cbNotificationInvitation.setChecked(five);
        cbNotification4U.setChecked(six);
        cbNotificationByU.setChecked(seven);
        cbNotificationReceiveMoney.setChecked(eight);
        //
    }

    //
    /*private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("NOTIFICATIONS");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);

    }*/


    //
    public void onClickBtnUpdateSetting(final View view) {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        progress = CustomProgress.show(getActivity(), "", false, false, null);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/notification").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("push_invite", values[0]).setBodyParameter("push_for_success", values[1])
                .setBodyParameter("push_by_success", values[2]).setBodyParameter("push_money", values[3])
                .setBodyParameter("email_invite", values[4]).setBodyParameter("email_for_success", values[5])
                .setBodyParameter("email_by_success", values[6]).setBodyParameter("email_money", values[7])
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("SettingUpdate", result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //
                            spUser.edit().putString("notification_setting", new Gson().toJson(values)).commit();
                            //
                            progress.dismiss();
                            //Toast.makeText(NotificationActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            //finish();
                        } else {
                            onClickBtnUpdateSetting(view);
                            //Toast.makeText(NotificationActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e1) {
                        onClickBtnUpdateSetting(view);
                        //Toast.makeText(NotificationActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    onClickBtnUpdateSetting(view);
                    //Toast.makeText(NotificationActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private void getSetting() {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        final String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        progress = CustomProgress.show(getActivity(), "", false, false, null);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/notification").setHeader("Authorization", "Basic " + basic)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                progress.dismiss();
                Log.d("GetSetting", "" + result);
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //Toast.makeText(NotificationActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            //
                            isFromClicked = false;
                            //
                            JSONObject joSetting = joResult.getJSONObject("notification_setting");
                            //stPushSuccessfulIntroze.onClick(null);
                            values[0] = joSetting.getInt("push_invite") + "";
                            values[1] = joSetting.getInt("push_for_success") + "";
                            values[2] = joSetting.getInt("push_by_success") + "";
                            values[3] = joSetting.getInt("push_money") + "";
                            values[4] = joSetting.getInt("email_invite") + "";
                            values[5] = joSetting.getInt("email_for_success") + "";
                            values[6] = joSetting.getInt("email_by_success") + "";
                            values[7] = joSetting.getInt("email_money") + "";
                            //
                            spUser.edit().putString("notification_setting", new Gson().toJson(values)).commit();
                            //
                            initSettings();
                            /*boolean one = joSetting.getInt("push_invite") == 1 ? true : false;
                            boolean two = joSetting.getInt("push_for_success") == 1 ? true : false;
                            boolean three = joSetting.getInt("push_by_success") == 1 ? true : false;
                            boolean four = joSetting.getInt("push_money") == 1 ? true : false;
                            boolean five = joSetting.getInt("email_invite") == 1 ? true : false;
                            boolean six = joSetting.getInt("email_for_success") == 1 ? true : false;
                            boolean seven = joSetting.getInt("email_by_success") == 1 ? true : false;
                            boolean eight = joSetting.getInt("email_money") == 1 ? true : false;*/
                            //
                           /* cbPushInvitation.setChecked(one);
                            cbPush4U.setChecked(two);
                            cbPushByU.setChecked(three);
                            cbPushReceiveMoney.setChecked(four);
                            //
                            cbNotificationInvitation.setChecked(five);
                            cbNotification4U.setChecked(six);
                            cbNotificationByU.setChecked(seven);
                            cbNotificationReceiveMoney.setChecked(eight);*/
                            //
                        } else {
                            if (!TextUtils.equals("No settings available", joResult.getString("message"))) {
                                showAlertDialog("Error", joResult.getString("message"));
                                /*builderError.setErrorMessage(joResult.getString("message"));
                                adError.show();*/
                            }
                            //
                            //getSetting();
                            //initSettings();
                            //Toast.makeText(NotificationActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e1) {
                        //
                        showAlertDialog("Error", getString(R.string.error_no_response));
                        /*builderError.setErrorMessage(getString(R.string.error_no_response));
                        adError.show();*/
                        //getSetting();
                        //initSettings();
                        //Toast.makeText(NotificationActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //
                    showAlertDialog("Error", getString(R.string.error_no_response));
                    /*builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();*/
                    //getSetting();
                    //initSettings();
                    //Toast.makeText(NotificationActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private void showAlertDialog(String title, String message) {
        builderError.setErrorMessage(title, message);
        adError.show();
        TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cbPushInvitation:
                values[0] = isChecked ? "1" : "0";
                //Toast.makeText(NotificationActivity.this, "PushInvitation", Toast.LENGTH_SHORT).show();
                //updateSetting("push_invite", isChecked);
                break;
            case R.id.cbPush4U:
                values[1] = isChecked ? "1" : "0";
                //Toast.makeText(NotificationActivity.this, "Push Successful Introze", Toast.LENGTH_SHORT).show();
                //updateSetting("push_for_success", isChecked);
                break;
            case R.id.cbPushByU:
                values[2] = isChecked ? "1" : "0";
                //updateSetting("push_by_success", isChecked);
                break;
            case R.id.cbPushReceiveMoney:
                values[3] = isChecked ? "1" : "0";
                //updateSetting("push_money", isChecked);
                break;
            case R.id.cbNotificationInvitation:
                values[4] = isChecked ? "1" : "0";
                //updateSetting("email_invite", isChecked);
                break;
            case R.id.cbNotification4U:
                values[5] = isChecked ? "1" : "0";
                //updateSetting("email_for_success", isChecked);
                break;
            case R.id.cbNotificationByU:
                values[6] = isChecked ? "1" : "0";
                //updateSetting("email_by_success", isChecked);
                break;
            case R.id.cbNotificationReceiveMoney:
                values[7] = isChecked ? "1" : "0";
                //updateSetting("email_money", isChecked);
                break;
        }
        if (isFromClicked) onClickBtnUpdateSetting(null);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        isFromClicked = true;
        return false;
    }
}
