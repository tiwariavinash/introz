package com.wd.introze.jutil;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.activeandroid.Model;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.wd.introze.AppUrls;
import com.wd.introze.R;
import com.wd.introze.tables.TableContacts;
import com.wd.introze.tables.TableEmail;
import com.wd.introze.tables.TablePhone;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by flair on 17-08-2016
 */
public class JUtils {
    //
    public boolean isMatch(String identifier) {
        List<TableContacts> items = new Select().from(TableContacts.class).where("Identifier =?", identifier).execute();
        if (items.size() > 0) {
            return items.get(0).isMatched;
        }
        return false;
    }

    //
    public static void setCursorDrawableColor(EditText editText, int color) {
        try {
            Field fCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            fCursorDrawableRes.setAccessible(true);
            int mCursorDrawableRes = fCursorDrawableRes.getInt(editText);
            Field fEditor = TextView.class.getDeclaredField("mEditor");
            fEditor.setAccessible(true);
            Object editor = fEditor.get(editText);
            Class<?> clazz = editor.getClass();
            Field fCursorDrawable = clazz.getDeclaredField("mCursorDrawable");
            fCursorDrawable.setAccessible(true);
            Drawable[] drawables = new Drawable[2];
            drawables[0] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[1] = editText.getContext().getResources().getDrawable(mCursorDrawableRes);
            drawables[0].setColorFilter(color, PorterDuff.Mode.SRC_IN);
            drawables[1].setColorFilter(color, PorterDuff.Mode.SRC_IN);
            fCursorDrawable.set(editor, drawables);
        } catch (Throwable ignored) {
        }
    }

    //
    public static String getImageUrl(String url, String fbId) {
        if (TextUtils.isEmpty(url)) {
            return TextUtils.isEmpty(fbId) ? null : JUtils.getFacebookProfilePictureUrl(fbId);
        }
        return AppUrls.MAIN + "/" + url;
    }

    //
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat sdfDot = new SimpleDateFormat("MM.dd.yy");

    //
    public static long getMillis(String date) {
        try {
            return sdf.parse(date).getTime();
        } catch (ParseException e) {

        }
        return System.currentTimeMillis();
    }

    //
    public static boolean isEmail(String text) {
        if (text.contains("@") && (text.contains(".com") || text.contains(".co.in") || text.contains(".in")
                || text.contains(".net"))) {
            return true;
        }
        return false;
    }

    //
    public static String uppercaseFirstLetters(String str) {
        str = str.toLowerCase();
        boolean prevWasWhiteSp = true;
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (Character.isLetter(chars[i])) {
                if (prevWasWhiteSp) {
                    chars[i] = Character.toUpperCase(chars[i]);
                }
                prevWasWhiteSp = false;
            } else {
                prevWasWhiteSp = Character.isWhitespace(chars[i]);
            }
        }
        return new String(chars);
    }
    //

    //
    public static long getPrevMillis(String date, int prevDay, int prevMin) {
        try {
            Date date1 = sdf.parse(date);
            //
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date1.getTime());
            gc.set(Calendar.DAY_OF_MONTH, gc.get(Calendar.DAY_OF_MONTH) - prevDay);
            gc.set(Calendar.MINUTE, gc.get(Calendar.MINUTE) - prevMin);
            //
            return gc.getTimeInMillis();
        } catch (ParseException e) {

        }
        return 0;
    }

    //
    public static String getDateInDotFormat(String date) {
        //
        Date d = null;
        try {
            d = sdf.parse(date);
        } catch (ParseException e1) {

        }
        String dotDate = null;
        if (d == null) {

        } else {
            dotDate = sdfDot.format(d);
        }
        //
        return dotDate;
    }

    //
    public static String getDateFromLongToDotFormat(long date) {
        return sdfDot.format(new Date(date));
    }

    //
    public static void setAppStatus(Context context, boolean isAppOpen) {
        context.getSharedPreferences("user", Context.MODE_PRIVATE).edit().putBoolean("is_app_open", isAppOpen).commit();
    }

    //
    public static boolean getAppStatus(Context context) {
        return context.getSharedPreferences("user", Context.MODE_PRIVATE).getBoolean("is_app_open", false);
    }

    //
    public static Bitmap getFacebookProfilePicture(String userID) {
        Bitmap bitmap = null;
        try {
            URL imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
        } catch (MalformedURLException e) {

        } catch (IOException e) {

        }
        return bitmap;
    }

    //
    public static String getFacebookProfilePictureUrl(String userID) {
        return "https://graph.facebook.com/" + userID + "/picture?type=large";
    }

    //
    public static int getRelationTypeIcon(int relationType) {
        int relation;
        switch (relationType) {
            case 1:
                relation = R.drawable.ic_business;
                break;
            case 2:
                relation = R.drawable.ic_friendship;
                break;
            case 3:
                relation = R.drawable.ic_dating;
                break;
            default:
                relation = R.drawable.ic_mutual_interest;
        }
        return relation;
    }

    //
    public static String getRelationTypeText(int relationType) {
        String relation;
        switch (relationType) {
            case 1:
                relation = "Business";
                break;
            case 2:
                relation = "Dating";
                break;
            case 3:
                relation = "Friendship";
                break;
            default:
                relation = "Mutual Interest";
        }
        return relation;
    }

    //
    public static String getTwoUpperChar(String fullName) {

        String[] temp = fullName.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[temp.length - 1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }

    //
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    //
    public static String readContentFromAssetsFile(Context context, String fileName) {
//
        String html = "";
        //
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(fileName)));

            //

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                html += mLine;
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return html;
    }

    //
    public static String getNumeric(String content) {
        String temp = content.replaceAll("[^0-9]", "").replace(" ", "");
        return temp;
    }

    //
    public static String getHeader(SharedPreferences spUser) {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        return "Basic " + basic;
    }

    //
    public static void removeMyProfileFromDB(String contactNumber) {
        List<TablePhone> list = new Delete().from(TablePhone.class).where("Phone =?", contactNumber).execute();
        if (list.size() > 0) {
            long _id = list.get(0).contacts.getId();
            Model.delete(TableContacts.class, _id);
            //
            new Delete().from(TableEmail.class).where("Contacts =?", _id).execute();
        }
    }
}
