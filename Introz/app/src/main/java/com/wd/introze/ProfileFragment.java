package com.wd.introze;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JFile;
import com.wd.introze.jutil.JSession;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.payment.AmountActivity;
import com.wd.introze.payment.MoneyProfileActivity;
import com.wd.introze.payment.receivemodel.ReceiveModel;
import com.wd.introze.payment.sentmodel.SentResult;
import com.wd.introze.payment.sentmodel.ThankYouDatum;
import com.wd.introze.repository.server.rf.model.user.Status;
import com.wd.introze.repository.server.rf.model.user.User;
import com.wd.introze.repository.server.rf.model.user.UserDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by flair on 20-07-2016
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {
    //
    private final String TAG = "ProfileFragment";
    //
    private CustomProgress pd;
    //
    private SharedPreferences spUser;
    //
    private LinearLayout llContainer;
    //
    private LinearLayout llProfile, llMoney;
    //
    //private ImageView ivProfile;
    private SimpleDraweeView ivProfile;
    //
    private TextView tvName, tvBalance, tvBusiness, tvDating, tvFriendship, tvMutualFriend;
    //
    private String fn, ln, email, phone;
    //
    private Button btnProfile, btnMyMoney;
    //
    private TextView tvShortName;
    //
    TextView tvTextMyMoney1, tvTextMyMoney2;
    //
    //
    String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    LinearLayout llSentMoney, llReceiveMoney, llMain, llChildReceive, llChildSend;
    //
    private ImageView ivBComma, ivDComma, ivFComma, ivMComma;
    //
    private AlertDialog adMessage;
    private JSimpleBuilder builder;
    //
    private String fbID;
    //
    private OnStatusClickListener statusClickListener;
    //
    private RecyclerView rvMy;
    private TextView tvEmpty;
    private ArrayList<MyModel> dataMy = new ArrayList<>();
    //
    private ArrayList<MyModel> dataSend = new ArrayList<>();
    private TextView tvEmptySent;
    private RecyclerView rvSent;

    //
    private MyAdapter adapterMy;
    private MyAdapter adapterSent;
    //
    private TextView tvAmount;
    //
    private int withdrawalAmount;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, null);
        //
        TypefaceHelper.typeface(view);
        //
        tfText = Typeface.createFromAsset(getActivity().getAssets(), fontPathText);
        //
        llContainer = (LinearLayout) view.findViewById(R.id.container);
        //
        llProfile = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_profile_1, null);
        TypefaceHelper.typeface(llProfile);
        //
        llMoney = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_profile_money, null);
        TypefaceHelper.typeface(llMoney);
        //
        tvAmount = (TextView) llMoney.findViewById(R.id.tvAmount);
        //
        /*for (int i = 0; i < 10; i++) {
            dataMy.add(new MyModel("", "", "", ""));
        }*/
        //
        adapterMy = new MyAdapter(getActivity(), dataMy);
        //
        rvMy = (RecyclerView) llMoney.findViewById(R.id.rv);
        rvMy.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMy.setAdapter(adapterMy);
        //
        tvEmpty = (TextView) llMoney.findViewById(R.id.tvEmpty);
        setMyVisibility();
        //
        //
        adapterSent = new MyAdapter(getActivity(), dataSend);
        //
        rvSent = (RecyclerView) llMoney.findViewById(R.id.rvSent);
        rvSent.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSent.setAdapter(adapterSent);
        //
        tvEmptySent = (TextView) llMoney.findViewById(R.id.tvEmptySent);
        setSentVisibility();
        //
        tvName = (TextView) llProfile.findViewById(R.id.tvName);
        //
        tvBalance = (TextView) llProfile.findViewById(R.id.tvBalance);
        //
        tvBusiness = (TextView) llProfile.findViewById(R.id.tvBusiness);
        tvBusiness.setHint(R.string.placeholder_business);
        //
        tvDating = (TextView) llProfile.findViewById(R.id.tvDating);
        tvDating.setHint(R.string.placeholder_dating);
        //
        tvFriendship = (TextView) llProfile.findViewById(R.id.tvFriendship);
        tvFriendship.setHint(R.string.placeholder_friendship);
        //
        tvMutualFriend = (TextView) llProfile.findViewById(R.id.tvMutualFriend);
        tvMutualFriend.setHint(R.string.placeholder_mutual_friend);
        //
        ivBComma = (ImageView) llProfile.findViewById(R.id.ivBComma);
        //ivBComma.setVisibility(View.VISIBLE);
        //
        ivDComma = (ImageView) llProfile.findViewById(R.id.ivDComma);
        //ivDComma.setVisibility(View.VISIBLE);
        //
        ivFComma = (ImageView) llProfile.findViewById(R.id.ivFComma);
        //ivFComma.setVisibility(View.VISIBLE);
        //
        ivMComma = (ImageView) llProfile.findViewById(R.id.ivMComma);
        //ivMComma.setVisibility(View.VISIBLE);
        //
        ivProfile = (SimpleDraweeView) llProfile.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(this);
        //
        llProfile.findViewById(R.id.llBusiness).setOnClickListener(this);
        llProfile.findViewById(R.id.llDating).setOnClickListener(this);
        llProfile.findViewById(R.id.llFriendship).setOnClickListener(this);
        llProfile.findViewById(R.id.llMutualInterest).setOnClickListener(this);
        //
        tvShortName = (TextView) llProfile.findViewById(R.id.tvShortName);
        //
        llContainer.addView(llProfile);
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        btnProfile = (Button) view.findViewById(R.id.btnProfile);
        btnProfile.setOnClickListener(this);
        //
        btnMyMoney = (Button) view.findViewById(R.id.btnMoney);
        btnMyMoney.setOnClickListener(this);
        //btnMyMoney.setText("My Money (" + HomeActivity.receiveCount + ")");
        //
        tvTextMyMoney1 = (TextView) llMoney.findViewById(R.id.tvTextMyMoney1);
        tvTextMyMoney2 = (TextView) llMoney.findViewById(R.id.tvTextMyMoney2);
        //
        setAccountCount(HomeActivity.receiveCount);
        /*if (count <= 0) {
            tvTextMyMoney2.setText(getString(R.string.account_received_text));
            btnMyMoney.setText("My Money");
        } else {
            tvTextMyMoney2.setText(getString(R.string.account_received_text) + " (" + count + ")");
            btnMyMoney.setText("My Money (" + HomeActivity.receiveCount + ")");
        }*/
        //
        //tvTextMyMoney2.setText(getString(R.string.account_received_text) + " (" + HomeActivity.receiveCount + ")");
        //
        tvTextMyMoney1.setTypeface(tfText);
        tvTextMyMoney2.setTypeface(tfText);
        //
        llMain = (LinearLayout) llMoney.findViewById(R.id.llMain);
        llSentMoney = (LinearLayout) llMoney.findViewById(R.id.llSentMoney);
        llSentMoney.setOnClickListener(this);
        //
        llReceiveMoney = (LinearLayout) llMoney.findViewById(R.id.llReceiveMoney);
        llReceiveMoney.setOnClickListener(this);
        //
        llChildSend = (LinearLayout) llMoney.findViewById(R.id.llChildSent);
        llChildReceive = (LinearLayout) llMoney.findViewById(R.id.llChildReceived);
        //
        builder = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adMessage.dismiss();
            }
        });
        adMessage = builder.create();
        //adMessage.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        llMoney.findViewById(R.id.llWithdrawal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                //Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT).show();
                //
                if (withdrawalAmount <= 0) {
                    builder.setErrorMessage("Alert", "We're sorry but you don't have enough funds to withdraw the chosen amount.");
                    adMessage.show();
                    TypefaceHelper.typeface(adMessage.getButton(DialogInterface.BUTTON_POSITIVE));
                    return;
                }
                getBankDetail();
            }
        });
        //
        //getAmountDetailFromServer();
        if (TextUtils.equals("THANK_YOU_RECEIVED", JSession.category)) {
            onClick(btnMyMoney);
            JSession.category = null;
        }
        //
        getSentMoney();
        getReceiveMoney();
        //
        return view;
    }

    //
    public void setTvAmount(int amount) {
        tvAmount.setText("$" + amount);
    }

    //
    private void gotoWithdrawal() {
        ReceiveMoneyActivity.ReceiveModel receiveModel = null;
        String bankDetail = spUser.getString(ReceiveMoneyActivity.ReceiveModel.class.getName(), null);
        if (TextUtils.isEmpty(bankDetail)) {
            Intent intent = new Intent(getActivity(), ReceiveMoneyActivity.class);
            intent.putExtra("amount", withdrawalAmount);
            intent.putExtra("is_from_profile", true);
            startActivity(intent);
        } else {
            //
            Intent intent = new Intent(getActivity(), AmountActivity.class);
            intent.putExtra("amount", withdrawalAmount);
            receiveModel = new Gson().fromJson(bankDetail, ReceiveMoneyActivity.ReceiveModel.class);
            //
            intent.putExtra("last_4", receiveModel.routingNumber.substring(receiveModel.routingNumber.length() - 4));
            //
            startActivity(intent);
        }
    }

    //
    private void getBankDetail() {
//
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        pd = CustomProgress.show(getActivity(), null, false, false, null);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/check_bank_detail").setHeader("Authorization", "Basic " + basic).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        pd.dismiss();
                        Log.d(TAG, "" + result);
                        if (result != null) {
                            ReceiveMoneyActivity.ReceiveModel model = new Gson().fromJson(result, ReceiveMoneyActivity.ReceiveModel.class);
                            if (TextUtils.equals("success", model.result)) {
                                //
                                if (!(TextUtils.isEmpty(model.accountNumber) || TextUtils.isEmpty(model.routingNumber) || TextUtils.isEmpty(model.name)))
                                    spUser.edit().putString(ReceiveMoneyActivity.ReceiveModel.class.getName(), new Gson().toJson(model)).commit();
                                gotoWithdrawal();
                            } else {
                                builder.setErrorMessage(model.message);
                                adMessage.show();
                                TypefaceHelper.typeface(adMessage.getButton(DialogInterface.BUTTON_POSITIVE));
                            }
                            //Toast.makeText(ReceiveMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();

                        } else {
                            builder.setErrorMessage(getString(R.string.error_no_response));
                            adMessage.show();
                            TypefaceHelper.typeface(adMessage.getButton(DialogInterface.BUTTON_POSITIVE));
                            //Toast.makeText(ReceiveMoneyActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    //
    private void getAmountDetailFromServer() {
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/profile_tab").setHeader("Authorization", JUtils.getHeader(spUser))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("SettingProfileResult", result + "");
                if (result != null) {
                    //
                    User user = new Gson().fromJson(result, User.class);
                    //
                    if (TextUtils.equals("success", user.result)) {
                        withdrawalAmount = user.userDetails.balance;
                        tvAmount.setText("$" + user.userDetails.balance);
                    } else {
                    }
                } else {

                }
            }
        });
    }

    //
    private void initProfileDetail(UserDetails userDetails) {
        //
        phone = userDetails.phone;
        email = userDetails.email;
        fn = userDetails.firstName;
        ln = userDetails.lastName;
        //
        Status status = userDetails.status;
        //
        String business = status.business;
        String dating = status.dating;
        String friendship = status.friendship;
        String mutualInterest = status.mutualInterests;
        //
        tvName.setText(fn + " " + ln);
        tvBalance.setText(userDetails.balance + "");
        //
        if (TextUtils.isEmpty(business)) {
            //ivBComma.setVisibility(View.GONE);
            tvBusiness.setText("");
        } else {
            tvBusiness.setText(business);
            ivBComma.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(dating)) {
            tvDating.setText("");
            //ivDComma.setVisibility(View.GONE);
        } else {
            tvDating.setText(dating);
            ivDComma.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(friendship)) {
            tvFriendship.setText("");
            //ivFComma.setVisibility(View.GONE);
        } else {
            tvFriendship.setText(friendship);
            ivFComma.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(mutualInterest)) {
            tvMutualFriend.setText("");
            //ivMComma.setVisibility(View.GONE);
        } else {
            tvMutualFriend.setText(mutualInterest);
            ivMComma.setVisibility(View.VISIBLE);
        }
        //
        fbID = userDetails.fbId;
        //
        String urlPic = userDetails.userImage;
        /*String urlProfile = AppUrls.MAIN + "/" + urlPic; // ? AppUrls.MAIN + "/" :
        //
        if (TextUtils.isEmpty(urlPic) && !TextUtils.isEmpty(fbID)) {
            urlProfile = JUtils.getFacebookProfilePictureUrl(fbID);
        }*/
        //
        setUserProfile(Uri.parse(JUtils.getImageUrl(urlPic, fbID)), fn + " " + ln);
    }
    //


    @Override
    public void onResume() {
        super.onResume();
        //
        getAmountDetailFromServer();
        //
        String userDetail = spUser.getString(User.class.getName(), null);
        if (TextUtils.isEmpty(userDetail))
            getProfileFromServer1();
        else initProfileDetail(new Gson().fromJson(userDetail, User.class).userDetails);
    }

    //
    private void getProfileFromServer1() {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/profile_tab").setHeader("Authorization", "Basic " + basic)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("SettingProfileResult", result + "");
                pd.dismiss();
                if (result != null) {
                    //
                    User user = new Gson().fromJson(result, User.class);
                    //
                    /*try {
                        JSONObject joResult = new JSONObject(result);*/
                    if (TextUtils.equals("success", user.result)) {
                        /*JSONObject joUserDetail = joResult.getJSONObject("user_details");
                        //
                        ProfileModel model = new ProfileModel();
                        model.phones = joUserDetail.getString("phone");
                        model.email.add("email");
                        //
                        JSONArray joEmails = joUserDetail.optJSONArray("emails");
                        if (joEmails != null) {
                            for (int i = 0; i < joEmails.length(); i++)
                                model.email.add(joEmails.getString(i) + "");
                        }
                        //
                        int chatId = joUserDetail.getInt("user_chat_id");*/
                        //
                        initProfileDetail(user.userDetails);
                        spUser.edit().putString(User.class.getName(), new Gson().toJson(user)).commit();
                        //
                        //spUser.edit().putBoolean("is_chat_init", false).commit();
                        //
                        //createSession(user.userDetails.userChatId);
                    } else {
                        showErrorMessage(user.message);
                    }//getProfileFromServer1();
                    /*} catch (JSONException e1) {

                    }*/
                    //Log.d("ProfileFragment", "" + result);
                } else {
                    showErrorMessage(getString(R.string.error_no_response));
                    //getProfileFromServer1();
                }
            }
        });
    }

    //
    private void getProfileDetailFromServer() {
        //
        /*Log.d("userid", spUser.getString("user_id", ""));
        Log.d("usertoken", spUser.getString("user_token", ""));*/
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        pd = CustomProgress.show(getActivity(), "", false, false, null);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/profile_tab").setHeader("Authorization", "Basic " + basic)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //
                if (pd != null) pd.dismiss();
                //
                Log.d("SettingProfileResult", result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            JSONObject joUserDetail = joResult.getJSONObject("user_details");
                            //
                            phone = joUserDetail.getString("phone");
                            email = joUserDetail.getString("email");
                            fn = joUserDetail.getString("first_name");
                            ln = joUserDetail.getString("last_name");
                            tvName.setText(fn + " " + ln);
                            tvBalance.setText(joUserDetail.getInt("balance") + "");
                            //
                            JSONObject joStatus = joUserDetail.getJSONObject("status");
                            //
                            String business = joStatus.getString("business");
                            String dating = joStatus.getString("dating");
                            String friendship = joStatus.getString("friendship");
                            String mutualInterest = joStatus.getString("mutual_interests");
                            //
                            if (TextUtils.isEmpty(business)) {
                                //ivBComma.setVisibility(View.GONE);
                                tvBusiness.setText("");
                            } else {
                                tvBusiness.setText(business);
                                //ivBComma.setVisibility(View.VISIBLE);
                            }
                            if (TextUtils.isEmpty(dating)) {
                                tvDating.setText("");
                                //ivDComma.setVisibility(View.GONE);
                            } else {
                                tvDating.setText(dating);
                                //ivDComma.setVisibility(View.VISIBLE);
                            }
                            if (TextUtils.isEmpty(friendship)) {
                                tvFriendship.setText("");
                                //ivFComma.setVisibility(View.GONE);
                            } else {
                                tvFriendship.setText(friendship);
                                //ivFComma.setVisibility(View.VISIBLE);
                            }
                            if (TextUtils.isEmpty(mutualInterest)) {
                                tvMutualFriend.setText("");
                                //ivMComma.setVisibility(View.GONE);
                            } else {
                                tvMutualFriend.setText(mutualInterest);
                                //ivMComma.setVisibility(View.VISIBLE);
                            }
                            //
                            fbID = joUserDetail.getString("fb_id");
                            //
                            String urlPic = joUserDetail.getString("user_image");
                            String urlProfile = AppUrls.MAIN + "/" + urlPic; // ? AppUrls.MAIN + "/" :
                            //
                            if (TextUtils.isEmpty(urlPic) && !TextUtils.isEmpty(fbID)) {
                                urlProfile = JUtils.getFacebookProfilePictureUrl(fbID);
                            }
                            //
                            setUserProfile(Uri.parse(urlProfile), fn + " " + ln);
                        }
                    } catch (JSONException e1) {
                        builder.setErrorMessage(getString(R.string.error_no_response));
                        adMessage.show();
                        TypefaceHelper.typeface(adMessage.getButton(DialogInterface.BUTTON_POSITIVE));
                    }
                } else {
                    builder.setErrorMessage(getString(R.string.error_no_response));
                    adMessage.show();
                    TypefaceHelper.typeface(adMessage.getButton(DialogInterface.BUTTON_POSITIVE));
                }
            }
        });
    }

    //
    //
    public String getTwoUpperChar(String name) {
        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }

    //
    private void setUserProfile(Uri uri, final String name) {
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(getTwoUpperChar(name));
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener).setUri(uri).build();
        ivProfile.setController(controller);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnProfile:
                llContainer.removeAllViews();
                llContainer.addView(llProfile);
                //
                btnProfile.setBackgroundColor(getResources().getColor(R.color.color_btn_green));
                btnMyMoney.setBackgroundColor(getResources().getColor(R.color.color_btn_white));
                //
                btnProfile.setTextColor(getResources().getColor(R.color.color_btn_white));
                btnMyMoney.setTextColor(getResources().getColor(android.R.color.darker_gray));

                break;
            case R.id.btnMoney:
                llContainer.removeAllViews();
                llContainer.addView(llMoney);
                //
                btnProfile.setBackgroundColor(getResources().getColor(R.color.color_btn_white));
                btnMyMoney.setBackgroundColor(getResources().getColor(R.color.color_btn_green));
                //
                btnProfile.setTextColor(getResources().getColor(android.R.color.darker_gray));
                btnMyMoney.setTextColor(getResources().getColor(R.color.color_btn_white));

                break;
            case R.id.llBusiness:
                introductionType(0);
                break;
            case R.id.llDating:
                introductionType(1);
                break;
            case R.id.llFriendship:
                introductionType(2);
                break;
            case R.id.llMutualInterest:
                introductionType(3);
                break;
            case R.id.ivProfile:
                //if (TextUtils.isEmpty(fbID)) {
                callGalleryIntent();
                /*} else {
                    builder.setErrorMessage("Oop's You are logged in from Facebook. You can not change profile picture here");
                    adMessage.show();
                }*/
                //imageChooser.show(getActivity().getSupportFragmentManager(), "");
                break;
            case R.id.llSentMoney:
                //Toast.makeText(getActivity(), "SendMoney", Toast.LENGTH_SHORT).show();
                llMain.setVisibility(View.GONE);
                llChildSend.setVisibility(View.VISIBLE);
                statusClickListener.onMoneyClicked();
                isFromSent = true;
                break;
            case R.id.llReceiveMoney:
                llMain.setVisibility(View.GONE);
                llChildReceive.setVisibility(View.VISIBLE);
                statusClickListener.onMoneyClicked();
                isFromSent = false;
                //Toast.makeText(getActivity(), "ReceiveMoney", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    //
    private void introductionType(int relationType) {
        //
        statusClickListener.onStatusClicked(relationType);
        /*Intent intent = new Intent(getActivity(), IntroductionTypeActivity.class);
        intent.putExtra("relation_type", relationType);
        //
        intent.putExtra("business", tvBusiness.getText().toString());
        intent.putExtra("dating", tvDating.getText().toString());
        intent.putExtra("friendship", tvFriendship.getText().toString());
        intent.putExtra("mutual_interest", tvMutualFriend.getText().toString());
        //
        startActivity(intent);*/
    }

    //
    private void uploadProfilePicture(final String path) {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/upload_picture").setHeader("Authorization", "Basic " + basic)
                .setMultipartFile("profile_picture", "image/*", new File(path))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("ImageUploadResult", result + "");
                pd.dismiss();
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            //setUserProfile(JFile.getImageUri(getActivity(), JFile.getBitmapFromPath(path)), "");

                            getProfileFromServer1();
                        } else {
                            showErrorMessage(joResult.getString("message"));
                            //uploadProfilePicture(path);
                            //Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e1) {
                        showErrorMessage(getString(R.string.error_no_response));
                        //uploadProfilePicture(path);
                        //Toast.makeText(getActivity(), "" + e1, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showErrorMessage(getString(R.string.error_no_response));
                    //uploadProfilePicture(path);
                    //Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private void showErrorMessage(String message) {
        builder.setErrorMessage(message);
        adMessage.show();
        TypefaceHelper.typeface(adMessage.getButton(DialogInterface.BUTTON_POSITIVE));
    }

    //
    public void callGalleryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, 1000);
    }

    //
    private String myBitmap;

    //
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //
        switch (requestCode) {
            case 1000:
                if (FragmentActivity.RESULT_OK == resultCode) {
                    Uri selectedImage = data.getData();
                    //
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    // Get the cursor
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    Bitmap bitmap = BitmapFactory.decodeFile(imgDecodableString);
                    //
                    String path = JFile.getRealPath(getActivity(), bitmap);
                    if (path != null) { //uploadProfilePicture(path);
                        cropImage(selectedImage);
                    }
                }
                break;
            case 7001:
                if (data != null) {
                    try {
                        // get the returned data
                        Bundle extras = data.getExtras();
                        // get the cropped bitmap
                        Bitmap photo = extras.getParcelable("data");
                        //
                        myBitmap = BitMapToString(photo);
                        //System.out.println("MyPathString : " + path);
                        //
                        uploadProfilePicture(JFile.getRealPath(getActivity(), photo));
                        //llBG11.setImageBitmap(photo);
                    } catch (Exception e) {

                    }
                    /*if (pic != null) {
                        // To delete original image taken by camera
                        if (pic.delete())
                            Toast.makeText(getActivity(), "Your device doesn't support the crop action!", Toast.LENGTH_SHORT).show();
                    }*/
                }
                break;
        }
    }

    //
    //Bitmap to string
    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (pd != null) pd.dismiss();
    }
    //


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        statusClickListener = (OnStatusClickListener) context;
    }

    //
    public void onClickBtnBack() {
        llChildReceive.setVisibility(View.GONE);
        llChildSend.setVisibility(View.GONE);
        llMain.setVisibility(View.VISIBLE);
    }

    //
    public interface OnStatusClickListener {
        void onStatusClicked(int relationType);

        void onMoneyClicked();
    }

    //
    private void setMyVisibility() {
        if (dataMy.size() > 0) {
            tvEmpty.setVisibility(View.GONE);
            rvMy.setVisibility(View.VISIBLE);
        } else {
            tvEmpty.setVisibility(View.VISIBLE);
            rvMy.setVisibility(View.GONE);
        }
    }

    //
    private void setSentVisibility() {
        if (dataSend.size() > 0) {
            tvEmptySent.setVisibility(View.GONE);
            rvSent.setVisibility(View.VISIBLE);
        } else {
            tvEmptySent.setVisibility(View.VISIBLE);
            rvSent.setVisibility(View.GONE);
        }
    }

    //
    private class MyAdapter extends RecyclerView.Adapter<MyVH> {
        //
        private Context context;
        private ArrayList<MyModel> data;

        //
        MyAdapter(Context context, ArrayList<MyModel> data) {
            this.context = context;
            this.data = data;
        }

        @Override
        public MyVH onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MyVH(LayoutInflater.from(context).inflate(R.layout.fragment_profile_my_item, parent, false));
        }

        @Override
        public void onBindViewHolder(MyVH holder, int position) {
            MyModel model = data.get(position);
            holder.tvFullName.setText(model.fullName);
            holder.tvDate.setText(JUtils.getDateInDotFormat(model.date));
            holder.tvMoney.setText(model.money);
            if (TextUtils.isEmpty(model.feed)) {
                holder.ivQuote.setVisibility(View.GONE);
                holder.tvFeed.setText(model.feed);
            } else {
                holder.ivQuote.setVisibility(View.VISIBLE);
                holder.tvFeed.setText(model.feed);
            }
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    //
    private class MyVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        //
        TextView tvFullName, tvFeed, tvMoney, tvDate;
        //
        private ImageView ivQuote;

        public MyVH(View itemView) {
            super(itemView);
            //
            TypefaceHelper.typeface(itemView);
            //
            tvFullName = (TextView) itemView.findViewById(R.id.tvFullName);
            tvFeed = (TextView) itemView.findViewById(R.id.tvFeed);
            tvMoney = (TextView) itemView.findViewById(R.id.tvMoney);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            //
            ivQuote = (ImageView) itemView.findViewById(R.id.ivQuotes);
            //
            tvFullName.setOnClickListener(this);
            //
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvFullName:
                    gotoProfile(getAdapterPosition());
                    break;
                default: {
                    if (isFromSent) gotoProfile(getAdapterPosition());
                    else
                        startDetail(getAdapterPosition());
                }
            }
        }
    }

    //
    private void startDetail(int position) {
        MyModel model = isFromSent ? dataSend.get(position) : dataMy.get(position);
        Intent intent = new Intent(getActivity(), ThankUReceiveActivity.class);
        intent.putExtra("data", new Gson().toJson(model));
        startActivity(intent);
    }

    //
    private class DateSorting implements Comparator<MyModel> {

        @Override
        public int compare(MyModel lhs, MyModel rhs) {
            long d1 = JUtils.getMillis(lhs.date);
            long d2 = JUtils.getMillis(rhs.date);
            //
            if (d1 < d2) return -1;
            else if (d1 == d2) return 0;
            else
                return 1;
        }
    }

    //
    boolean isFromSent;

    //
    private MyModel model;

    //
    private void gotoProfile(int position) {
        model = isFromSent ? dataSend.get(position) : dataMy.get(position);
        getUserDetail(model.id);
    }

    //
    private void gotoP(String url) {
        Intent intent = new Intent(getActivity(), MoneyProfileActivity.class);
        intent.putExtra("first_name", model.fullName);
        intent.putExtra("last_name", "");
        intent.putExtra("user_id", model.id);
        intent.putExtra("url_pic", url);
        startActivity(intent);
    }

    //
    //
    private void getUserDetail(final int id) {
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/get_user_detail/" + id)//getActivity().getIntent().getStringExtra("user_id"))
                .setHeader("Authorization", JUtils.getHeader(spUser)).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                pd.dismiss();
                Log.d(TAG, result + "");
                if (e == null) {
                    try {//user_image fb_id
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            JSONObject joDetail = joResult.getJSONObject("user_details");
                            String userImage = joDetail.optString("user_image");
                            String fbId = joDetail.optString("fb_id");
                            String url = TextUtils.isEmpty(userImage) ? JUtils.getImageUrl(userImage, fbId) : userImage;
                            gotoP(url);
                        } else {
                            getUserDetail(id);
                        }
                    } catch (JSONException e1) {
                        getUserDetail(id);
                    }
                } else {
                    getUserDetail(id);
                }
            }
        });
    }


    //
    public class MyModel {
        public int thankUId;
        String fullName;
        String feed;
        String money;
        String date;
        int id;

        public MyModel(int id, String fullName, String feed, String money, String date, int thankUId) {
            this.thankUId = thankUId;
            this.id = id;
            this.fullName = fullName;
            this.feed = feed;
            this.money = money;
            this.date = date;
        }
    }

    //
    public void setAccountCount(int count) {
        if (count <= 0) {
            tvTextMyMoney2.setText(getString(R.string.account_received_text));
            btnMyMoney.setText("My Money");
        } else {
            tvTextMyMoney2.setText(getString(R.string.account_received_text) + " (" + count + ")");
            btnMyMoney.setText("My Money (" + HomeActivity.receiveCount + ")");
        }
    }

    //
    private void getReceiveMoney() {
        Ion.with(this).load("GET", AppUrls.MAIN + "/thank_you_received").setHeader("Authorization", JUtils.getHeader(spUser))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d(TAG, "Receive Result " + result);
                if (result != null) {
                    ReceiveModel sentResult = new Gson().fromJson(result, ReceiveModel.class);
                    if (TextUtils.equals("success", sentResult.result)) {
                        List<com.wd.introze.payment.receivemodel.ThankYouDatum> items = sentResult.thankYouData;
                        //
                        dataMy.clear();
                        //
                        for (com.wd.introze.payment.receivemodel.ThankYouDatum model : items) {
                            MyModel model1 = new MyModel(model.byUserId, model.byFirstName + " " + model.byLastName, model.message,
                                    "$" + model.amount, model.date, model.thankYouId);
                            dataMy.add(model1);
                            //
                        }
                        for (int i = 0; i < dataMy.size(); i++) {
                            if (JSession.rel_id == dataMy.get(i).thankUId) {
                                startDetail(i);
                                break;
                            }
                        }
                        //
                        ((HomeActivity) getActivity()).markAccountRead(dataMy);
                        //
                        Collections.sort(dataMy, new DateSorting());
                        adapterMy.notifyDataSetChanged();
                    } else {

                    }
                    setMyVisibility();
                } else {
                    setMyVisibility();
                }
            }
        });
    }

    //
    //
    private void getSentMoney() {
        Ion.with(this).load("GET", AppUrls.MAIN + "/thank_you_sent").setHeader("Authorization", JUtils.getHeader(spUser))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d(TAG, "Sent " + result);
                if (result != null) {
                    SentResult sentResult = new Gson().fromJson(result, SentResult.class);
                    if (TextUtils.equals("success", sentResult.result)) {
                        List<ThankYouDatum> items = sentResult.thankYouData;
                        dataSend.clear();
                        for (ThankYouDatum model : items) {
                            MyModel model1 = new MyModel(model.toUserId, model.toFirstName + " " + model.toLastName, model.message,
                                    "$" + model.amount, model.date, model.thankYouId);
                            dataSend.add(model1);
                        }
                        Collections.sort(dataSend, new DateSorting());
                        adapterSent.notifyDataSetChanged();
                    } else {

                    }
                    setSentVisibility();
                } else {
                    setSentVisibility();
                }
            }
        });
    }

    //
    public void cropImage(Uri picUri) {
        try {
            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setDataAndType(picUri, "image/*");

            intent.putExtra("crop", "true");
            intent.putExtra("outputX", 200);
            intent.putExtra("outputY", 200);
            intent.putExtra("aspectX", 4);
            intent.putExtra("aspectY", 4);
            intent.putExtra("scaleUpIfNeeded", true);
            intent.putExtra("return-data", true);

            startActivityForResult(intent, 7001);

        } catch (ActivityNotFoundException e) {
            //Toast.showToast(this, "Your device doesn't support the crop action!");
            Toast.makeText(getActivity(), "Your device doesn't support the crop action!", Toast.LENGTH_SHORT).show();
        }
    }
}
