package com.wd.introze.match.chatdialogs;

import android.content.Context;
import android.graphics.drawable.Animatable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.wd.introze.R;
import com.wd.introze.jutil.JUtils;

import java.util.ArrayList;

/**
 * Created by flair on 29-07-2016.
 */
public class DialogAdapter extends RecyclerView.Adapter<DialogVH> {
    //
    private ArrayList<DialogModel> data;
    private Context context;
    //
    public static OnDialogClickListener listener;

    public DialogAdapter(Context context, ArrayList<DialogModel> data, OnDialogClickListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @Override
    public DialogVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DialogVH(LayoutInflater.from(context).inflate(R.layout.temp_activity_my_match, parent, false));
    }

    @Override
    public void onBindViewHolder(final DialogVH holder, final int position) {
        //
        final DialogModel model = data.get(position);
        //
        if (model.count > 0) {
            holder.tvCount.setText(model.count + "");
            holder.ll.setVisibility(View.VISIBLE);
        } else {
            holder.ll.setVisibility(View.GONE);
        }
        //
        holder.tvFullName.setText(model.name);

        //Html.fromHtml("<html><body>Messages (<font color='#8DC73F'>" + totalCounts
        //+ "</font>)</body></html>")
        //
        //String n = (" by " + model.byName).toUpperCase();
        holder.tvByName.setText(Html.fromHtml("<html><body>by <font color='#5d7bad'>" + model.byName
                + "</font></body></html>"));
        //
        holder.tvDate.setText(model.createOn);
        //
        holder.ivRelationType.setImageResource(JUtils.getRelationTypeIcon(model.relationType));
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                holder.tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                holder.tvShortName.setText(model.getTwoUpperChar());
                holder.tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(model.url).build();
        holder.iv.setController(controller);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    //
    public interface OnDialogClickListener {
        void onDialogClicked(int position, boolean isForChat);

        void onRelationshipClicked(int position);
    }
//
}
