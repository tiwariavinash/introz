package com.wd.introze.jutil;

import android.telephony.PhoneNumberUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by admin on 2016-07-27.
 */
public class Validate {


    //


    //
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    //
    public boolean isValidPhone(String target) {
        if (target.equals("")) {
            return false;
        } else {
            if (PhoneNumberUtils.isGlobalPhoneNumber(target)) {
                if (target.length() >= 10 && target.length() <= 14) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    //
    public boolean isValidPhoneNumber(String target) {


        String sPhoneNumber = "605-888-9999";
        //String sPhoneNumber = "605-88899991";
        //String sPhoneNumber = "605-888999A";

        Pattern pattern = Pattern.compile("\\d{3}-\\d{3}-\\d{4}");
        Matcher matcher = pattern.matcher(sPhoneNumber);

        if (matcher.matches())

        {
            System.out.println("Phone Number Valid");
        } else

        {
            System.out.println("Phone Number must be in the form XXX-XXX-XXXX");
        }
        return false;
    }

}
