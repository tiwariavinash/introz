
package com.wd.introze.repository.server.rf.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Status {

    @SerializedName("business")
    @Expose
    public String business;
    @SerializedName("dating")
    @Expose
    public String dating;
    @SerializedName("friendship")
    @Expose
    public String friendship;
    @SerializedName("mutual_interests")
    @Expose
    public String mutualInterests;

}
