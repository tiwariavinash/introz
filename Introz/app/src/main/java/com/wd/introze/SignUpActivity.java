package com.wd.introze;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JErrorTextWatcher;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.jutil.Validate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 2016-07-14
 */
public class SignUpActivity extends AppCompatActivity {
    //
    private EditText etFirstName, etLastName, etEmail, etPassword, etRePassword;
    //
    private TextView tvErrorFN, tvErrorLN, tvErrorEmail, tvErrorPassword, tvErrorRePassword;

    //
    private ProgressDialog pd;
    //
    private SharedPreferences.Editor editorUser;
    private SharedPreferences spUser;
    //
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    CustomProgress customProgress;
    //
    private JSimpleBuilder builder;
    private AlertDialog ad;

    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        //
        TypefaceHelper.typeface(this);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        initHeader();
        //
        pd = new ProgressDialog(this);
        pd.setMessage("Please wait...");
        //
        tvErrorFN = (TextView) findViewById(R.id.tvErrorFN);
        tvErrorLN = (TextView) findViewById(R.id.tvErrorLN);
        tvErrorEmail = (TextView) findViewById(R.id.tvErrorEmail);
        tvErrorPassword = (TextView) findViewById(R.id.tvErrorPassword);
        tvErrorRePassword = (TextView) findViewById(R.id.tvErrorRePassword);
        //
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etFirstName.addTextChangedListener(new JErrorTextWatcher(tvErrorFN, getString(R.string.validation_first_name)));
        //
        etLastName = (EditText) findViewById(R.id.etLastName);
        etLastName.addTextChangedListener(new JErrorTextWatcher(tvErrorLN, getString(R.string.validation_last_name)));
        //
        etEmail = (EditText) findViewById(R.id.etEmail);
        etEmail.addTextChangedListener(new JErrorTextWatcher(tvErrorEmail, getString(R.string.validation_email)));
        //
        /*if (!Validate.isEmailValid(etEmail.getText().toString().trim())) {
            etEmail.addTextChangedListener(new JErrorTextWatcher(tvErrorEmail, "Please enter valid email address"));
        }*/
        //
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPassword.addTextChangedListener(new JErrorTextWatcher(tvErrorPassword, getString(R.string.validation_password)));
        //
        etRePassword = (EditText) findViewById(R.id.etRePassword);
        etRePassword.addTextChangedListener(new JErrorTextWatcher(tvErrorRePassword, getString(R.string.validation_re_password)));
        //

        //
        spUser = getSharedPreferences("user", MODE_PRIVATE);
        editorUser = spUser.edit();
        //
        builder = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });
        ad = builder.create();
        //ad.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    //
    private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("SIGN UP");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }

    //
    public void onClickBtnCreateAccount(View view) {
        //
        JUtils.hideSoftKeyboard(this);
        //
        if (!JConnectionUtils.isConnectedToInternet(this)) {
            showAlertDialog(getString(R.string.error_no_response));
            return;
        }
        //
        final String firstName = etFirstName.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            etFirstName.setText("");
            return;
        }
        //last name
        final String lastName = etLastName.getText().toString();
        if (TextUtils.isEmpty(lastName)) {
            etLastName.setText("");
            return;
        }
        //email
        final String email = etEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            etEmail.setText("");
            return;
        }
        //
        if (!Validate.isEmailValid(etEmail.getText().toString().trim())) {
            tvErrorEmail.setText(getString(R.string.validation_email_valid));
            return;
        }
        //
        String password = etPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            etPassword.setText("");
            return;
        }
        //
        String rePassword = etRePassword.getText().toString();
        if (TextUtils.isEmpty(rePassword)) {
            etRePassword.setText("");
            return;
        }
        if (!TextUtils.equals(rePassword, password)) {
            tvErrorRePassword.setText(getString(R.string.validation_password_matching));
            return;
        }
        if (password.length() < 6 || password.length() > 32) {
            tvErrorPassword.setText(getString(R.string.validation_password_limit));
            return;
        }
        //
        customProgress = CustomProgress.show(SignUpActivity.this, "", false, false, null);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/user").setBodyParameter("first_name", firstName)
                .setBodyParameter("last_name", lastName).setBodyParameter("email", email).setBodyParameter("password", password)
                .setBodyParameter("c_password", rePassword).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //
                customProgress.dismiss();
                if (result != null) {
                    Log.d("SignUpResult", result + "");
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            JSONObject joUser = joResult.getJSONObject("user");
                            //
                            editorUser.putString("user_id", joUser.getInt("user_id") + "")
                                    .putString("user_token", joUser.getString("user_token"));
                            /*if (joUser.has("chat_id")) {
                                editorUser.putInt("chat_id", joUser.getInt("chat_id"));
                            }*/
                            editorUser.commit();
                            //
                            Intent intent = new Intent(SignUpActivity.this, PhoneVerificationActivity.class);
                            startActivity(intent);
                            finish();
                            //
                        } else {
                            //
                            String message = null;
                            //
                            if (TextUtils.equals("error", joResult.getString("result"))) {
                                if (TextUtils.equals("validation", joResult.optString("error_type"))) {
                                    if (joResult.has("message")) {
                                        JSONObject joError = joResult.optJSONObject("message");
                                        if (joError != null) {
                                            if (joError.has("mobile")) {
                                                JSONArray jaError = joError.optJSONArray("mobile");
                                                //
                                                message = jaError.getString(0);
                                                //builder.setErrorMessage(jaError.getString(0));
                                            } else if (joError.has("email")) {
                                                //
                                                message = joError.optJSONArray("email").getString(0);
                                                //builder.setErrorMessage(joError.optJSONArray("email").getString(0));
                                            } else if (joError.has("first_name")) {
                                                //
                                                message = joError.optJSONArray("first_name").getString(0);
                                                //builder.setErrorMessage(joError.optJSONArray("first_name").getString(0));
                                            } else if (joError.has("last_name")) {
                                                //
                                                message = joError.optJSONArray("last_name").getString(0);
                                                //builder.setErrorMessage(joError.optJSONArray("last_name").getString(0));
                                            } else if (joError.has("c_password")) {
                                                //
                                                message = joError.optJSONArray("c_password").getString(0);
                                                //builder.setErrorMessage(joError.optJSONArray("c_password").getString(0));
                                            } else {
                                                //
                                                message = getString(R.string.error_no_response);
                                                //builder.setErrorMessage(getString(R.string.error_no_response));
                                            }
                                        } else
                                            //
                                            message = getString(R.string.error_no_response);
                                        //builder.setErrorMessage(getString(R.string.error_no_response));
                                    } else
                                        message = getString(R.string.error_no_response);
                                    //builder.setErrorMessage(getString(R.string.error_no_response));
                                } else
                                    message = joResult.getString("message");
                                //builder.setErrorMessage(joResult.getString("message"));
                            } else
                                message = getString(R.string.error_no_response);
                            //builder.setErrorMessage(getString(R.string.error_no_response));
                            showAlertDialog(message);
                            //ad.show();
                            /*JSONObject joMessage = joResult.getJSONObject("message");
                            String errorMessage;
                            if (joMessage.has("email")) {
                                errorMessage = joMessage.getJSONArray("email").getString(0);
                            } else if (joMessage.has("first_name")) {
                                errorMessage = joMessage.getJSONArray("first_name").getString(0);
                            } else if (joMessage.has("c_password")) {
                                errorMessage = joMessage.getJSONArray("c_password").getString(0);
                            } else {
                                errorMessage = getString(R.string.error_no_response);//joResult.getString("error_type");
                            }
                            builder.setErrorMessage(errorMessage);
                            ad.show();*/
                        }
                    } catch (JSONException e1) {
                        //
                        showAlertDialog(getString(R.string.error_no_response));
                        /*builder.setMessage(getString(R.string.error_no_response));
                        ad.show();*/
                    }
                } else {
                    //
                    showAlertDialog(getString(R.string.error_no_response));
                    /*builder.setMessage(getString(R.string.error_no_response));
                    ad.show();*/
                }
            }
        });
    }

    //
    private void showAlertDialog(String message) {
        builder.setErrorMessage(message);
        ad.show();
        TypefaceHelper.typeface(ad.getButton(DialogInterface.BUTTON_POSITIVE));
    }
}
