package com.wd.introze;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JErrorTextWatcher;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.Validate;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 2016-07-14
 */
public class ForgotPasswordActivity extends AppCompatActivity {
    //
    private ProgressDialog pd;
    //
    EditText etEmail;
    private TextView tvErrorEmail;
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    String fontPath = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    CustomProgress customProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        //
        TypefaceHelper.typeface(this);
        //
        builderError = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
        tfText = Typeface.createFromAsset(getAssets(), fontPath);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Recover Password");*/
        initHeader();
        //
        pd = new ProgressDialog(this);
        pd.setMessage("Please wait...");
        //
        tvErrorEmail = (TextView) findViewById(R.id.tvEmail);
        //
        etEmail = (EditText) findViewById(R.id.etEmail);
        etEmail.addTextChangedListener(new JErrorTextWatcher(tvErrorEmail, "Please Enter Email Address"));
        //

        if (!Validate.isEmailValid(etEmail.getText().toString().trim())) {
            etEmail.addTextChangedListener(new JErrorTextWatcher(tvErrorEmail, "Please Enter Valid Email Address"));
        }
        //
    }

    //
    public void onClickTvAccount(View view) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
    //

    private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("RECOVER PASSWORD");
        txtHeader.setTextSize(13);
        txtHeader.setTypeface(tfHeader);
    }

    //
    public void onClickBtnRecoverPassword(View view) {
        if (!JConnectionUtils.isConnectedToInternet(this)) {
            showAlertMessage(getString(R.string.error_no_response));
            return;
        }
        //
        String email = etEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            tvErrorEmail.setText("Please Enter Email Address");
            //Toast.makeText(ForgotPasswordActivity.this, "Please enter email id", Toast.LENGTH_SHORT).show();
            return;
        }
        //
        if (!Validate.isEmailValid(etEmail.getText().toString().trim())) {
            tvErrorEmail.setText("Please Enter Valid Email Address");
            return;
        }
        //
        //pd.show();
        customProgress = CustomProgress.show(ForgotPasswordActivity.this, "", false, false, null);
        Ion.with(this).load("PUT", AppUrls.MAIN + "/forgot_password").setBodyParameter("email", email).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        //pd.dismiss();
                        customProgress.dismiss();
                        if (e == null) {
                            Log.d("ForgotPass", result + "");
                            try {
                                JSONObject joResult = new JSONObject(result);

                                String message = "";
                                if (TextUtils.equals("success", joResult.getString("result"))) {
                                    //
                                    message = "Password sent to your registered email.";
                                    //builderError.setErrorMessage("Thank You!", "Password sent to your registered email.");//joResult.getString("message"));
                                    etEmail.setText("");
                                    tvErrorEmail.setText("");
                                } else {
                                    //
                                    message = joResult.getString("message");
                                    //builderError.setErrorMessage(joResult.getString("message"));
                                }
                                showAlertMessage(message);
                            } catch (JSONException e1) {
                                //
                                showAlertMessage(getString(R.string.error_no_response));
                                /*builderError.setErrorMessage(getString(R.string.error_no_response));
                                adError.show();*/
                            }
                        } else {
                            //
                            showAlertMessage(getString(R.string.error_no_response));
                            /*builderError.setErrorMessage(getString(R.string.error_no_response));
                            adError.show();*/
                        }
                    }
                });
    }

    //

    private void showAlertMessage(String message) {
        builderError.setErrorMessage(message);
        adError.show();
        TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
    }

    //
    public void onClickTvLogin(View view) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
