package com.wd.introze;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;

import com.wd.introze.repository.server.rf.RFClient;
import com.wd.introze.repository.server.rf.RFService;
import com.wd.introze.services.InsertIntoDBService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 8/13/2016
 */
public class ManageContactsActivity extends Fragment implements View.OnClickListener {
    //
    private final String TAG = "ManageContact";
    //
    private RFService rfService = RFClient.getClient().create(RFService.class);
    ;
    //
    private CustomProgress pd;
    private SharedPreferences spIntroze;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
   /* String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
*/
    //

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_manage_contact, null);
        TypefaceHelper.typeface(view);
        //
        //tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("TERMS OF SERVICES");*/
        //initHeader();
        spIntroze = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        ((TextView) view.findViewById(R.id.tv)).setText(getString(R.string.manage_contact));//JUtils.readContentFromAssetsFile(getActivity(), "manage_contacts.txt"));
        //
        view.findViewById(R.id.btnReSync).setOnClickListener(this);
        //
        builderError = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        return view;
    }

    @Override
    public void onClick(View v) {
        //
        if (pd == null) pd = CustomProgress.show(getContext(), null, false, false, null);
        else pd.show();
        Call<SuccessModel> call = rfService.onDeleteContact(JUtils.getHeader(spIntroze));
        call.enqueue(new Callback<SuccessModel>() {
            @Override
            public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    //
                    SuccessModel successModel = response.body();
                    //
                    if (TextUtils.equals("success", successModel.result)) {
                        builderError.setErrorMessage("Syncing Contacts", "Introze is resyncing your contacts.Please wait up to 30 seconds before sending your next Introze.");
                        adError.show();
                        TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                        //
                        Intent i = new Intent(getActivity(), InsertIntoDBService.class);
                        getActivity().startService(i);
                    } else {
                        builderError.setErrorMessage(successModel.message);
                        adError.show();
                        TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                    }
                } else {
                    builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();
                    TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
                }
            }

            @Override
            public void onFailure(Call<SuccessModel> call, Throwable t) {
                pd.dismiss();
                builderError.setErrorMessage(getString(R.string.error_no_response));
                adError.show();
                TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
            }
        });
        /*pd = CustomProgress.show(getActivity(), null, false, false, null);
        //
        Ion.with(this).load("DELETE", AppUrls.MAIN + "/delete_contacts").setHeader("Authorization", JUtils.getHeader(spIntroze))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //
                Log.d(TAG, "Result " + result);
                //
                pd.dismiss();
                if (result != null) {
                    SuccessModel model = new Gson().fromJson(result, SuccessModel.class);
                    if (TextUtils.equals("success", model.result)) {
                        builderError.setErrorMessage("Syncing Contacts", "Introze is resyncing your contacts.Please wait up to 30 seconds before sending your next Introze.");
                        adError.show();
                        //
                        Intent i = new Intent(getActivity(), InsertIntoDBService.class);
                        getActivity().startService(i);
                    } else {
                        builderError.setErrorMessage(model.message);
                        adError.show();
                    }
                } else {
                    builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();
                }
            }
        });*/
    }

    /*private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("MANAGE CONTACTS");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }*/
}
