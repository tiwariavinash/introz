package com.wd.introze;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JConnectionUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by flair on 12-12-2016.
 */

public class InviteFriendsActivity extends Fragment {
    private AlertDialog adCode;
    //
    TextView tvCopy, tvEmailInvite, tvCode, tvTextInvite;
    //
    private SharedPreferences spUser;
    //
    String userName, inviteCode;
    //
    CustomProgress customProgress;
    JConnectionUtils cd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_invite_friends, null);
        //
        TypefaceHelper.typeface(view);
        //
        cd = new JConnectionUtils();
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        adCode = new InviteFriendsActivity.CopyCodeDialog(getActivity()).create();
        adCode.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        adCode.getWindow().setGravity(Gravity.CENTER);
        //
        tvCode = (TextView) view.findViewById(R.id.tvCode);
        //
        tvCopy = (TextView) view.findViewById(R.id.tvCopy);
        tvCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                CharSequence label = null;
                ClipData clip = ClipData.newPlainText("code", AppUrls.MAIN + "/" + inviteCode);
                clipboard.setPrimaryClip(clip);
                adCode.show();
            }
        });
        //
        tvEmailInvite = (TextView) view.findViewById(R.id.tvEmailInvite);
        tvEmailInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setType("plain/text");
                //sendIntent.setData(Uri.parse("test@gmail.com"));
                sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                //sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"test@gmail.com"});
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out Introze!");
                sendIntent.putExtra(Intent.EXTRA_TEXT, userName + " thinks you should get on Introze and start getting introduced, making introductions, and getting paid for them. Sign up using link:" + "\n" + AppUrls.MAIN + "/" + inviteCode);
                startActivity(sendIntent);
            }
        });
        //
        tvTextInvite = (TextView) view.findViewById(R.id.tvTextInvite);
        tvTextInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:"));
                sendIntent.putExtra("sms_body", userName + " thinks you should get on Introze and start getting introduced, making introductions, and getting paid for them. Sign up using link:" + "\n" + AppUrls.MAIN + "/" + inviteCode);
                startActivity(sendIntent);
                //
                /*Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setType("vnd.android-dir/mms-sms");
                int flags = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP;
                intent.setFlags(flags);
                intent.setData(Uri.parse("content://sms/inbox"));
                startActivity(intent);*/
            }
        });
        //
        if (cd.isConnectedToInternet(getActivity())) {
            getProfileDetailFromServer();
        } else {
            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();

        }
        //
        return view;
    }

    //
    private void getProfileDetailFromServer() {
        //
        /*Log.d("userid", spUser.getString("user_id", ""));
        Log.d("usertoken", spUser.getString("user_token", ""));*/
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        customProgress = CustomProgress.show(getActivity(), "", false, false, null);
        Ion.with(this).load("GET", AppUrls.MAIN + "/profile_tab").setHeader("Authorization", "Basic " + basic)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("ProfileResult ", "InviteFriends " + result);
                customProgress.dismiss();
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {

                            inviteCode = joResult.getString("invite_code");
                            Log.d("inviteCodeFriends", "" + inviteCode);
                            tvCode.setText(inviteCode);
                            JSONObject joUserDetails = joResult.getJSONObject("user_details");
                            userName = joUserDetails.getString("first_name");
                            Log.d("UserName", "" + userName);
                        }
                    } catch (JSONException e1) {

                    }

                } else {
                    Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private class CopyCodeDialog extends AlertDialog.Builder implements View.OnClickListener {

        public CopyCodeDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_code_copied, null);
            TypefaceHelper.typeface(view);
            //
            setView(view, 80, 0, 80, 0);
            //
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            adCode.dismiss();
        }
        //
    }
}
