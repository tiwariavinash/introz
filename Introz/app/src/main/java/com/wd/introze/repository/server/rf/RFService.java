package com.wd.introze.repository.server.rf;

import com.wd.introze.SuccessModel;
import com.wd.introze.repository.server.rf.model.badge.RFAccountUnreadCount;
import com.wd.introze.repository.server.rf.model.badge.RFActivityUnreadCount;
import com.wd.introze.sync.SyncModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RFService {
    /**
     * @param authorization
     * @param syncModel
     * @return
     */
    @Headers({
            "Accept: application/json", "Content-type: application/json"
    })
    @POST("contact_sync2")
    Call<SuccessModel> onContactSync2(@Header("Authorization") String authorization, @Body SyncModel syncModel);


    /**
     * @param auth
     * @param syncModel
     * @return
     */
    @Headers({
            "Accept: application/json", "Content-type: application/json"
    })
    @POST("contact_sync3")
    Call<SuccessModel> onContactSync3(@Header("Authorization") String auth, @Body SyncModel syncModel);

    /**
     * @param auth
     * @return
     */
    @GET("get_introze_users")
    Call<ResponseBody> getIntrozeUser(@Header("Authorization") String auth);

    /**
     * @param auth
     * @return
     */
    @DELETE("delete_contacts")
    Call<SuccessModel> onDeleteContact(@Header("Authorization") String auth);

    /**
     * @param auth
     * @param emailId
     * @return
     */
    @FormUrlEncoded
    @POST("delete_email")
    Call<SuccessModel> onDeleteEmail(@Header("Authorization") String auth, @Field("email_id") int emailId);

    /**
     * @param auth
     * @param id
     * @return
     */
    @GET("get_user_detail/{id}")
    Call<ResponseBody> getUserDetail(@Header("Authorization") String auth, @Path("id") int id);

    /**
     * @param auth
     * @return
     */
    @GET("for_you_count")
    Call<RFActivityUnreadCount> getForUCount(@Header("Authorization") String auth);

    /**
     * @param auth
     * @return
     */
    @GET("by_you_count")
    Call<RFActivityUnreadCount> getByUCount(@Header("Authorization") String auth);

    @GET("my_money")
    Call<RFAccountUnreadCount> getAccountCount(@Header("Authorization") String auth);

    @GET("new_match")
    Call<ResponseBody> getMatchList(@Header("Authorization") String auth);

    @GET("getEmployeeDetail/companyid/{companyid}/healthid/{healthid}")
    Call<ResponseBody> getDetails(@Path("companyid") String companyId, @Path("healthid") String healthId);
}
