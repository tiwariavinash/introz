
package com.wd.introze.repository.server.rf.model.user;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class User {

    @SerializedName("result")
    @Expose
    public String result;
    @SerializedName("user_details")
    @Expose
    public UserDetails userDetails;
    @SerializedName("message")
    @Expose
    public String message;
}
