package com.wd.introze.repository.server.rf.model.badge;

/**
 * Created by flair on 01-10-2016.
 */

public class BadgeUnreadCount {
    public int accountUnreadCount;
    //
    public int activityForUUnreadCount;
    public int activityByUUnreadCount;

    public BadgeUnreadCount(int accountUnreadCount, int activityForUUnreadCount, int activityByUUnreadCount) {
        this.accountUnreadCount = accountUnreadCount;
        //
        this.activityForUUnreadCount = activityForUUnreadCount;
        this.activityByUUnreadCount = activityByUUnreadCount;
    }

    //
    public int getActivityUnreadCount() {
        return activityForUUnreadCount + activityByUUnreadCount;
    }
}
