package com.wd.introze.repository.server.rf.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Email {

    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("id")
    @Expose
    public Integer id;

    public Email(String email, Integer id) {
        this.email = email;
        this.id = id;
    }
}