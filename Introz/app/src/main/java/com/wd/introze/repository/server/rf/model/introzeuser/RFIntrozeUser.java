
package com.wd.introze.repository.server.rf.model.introzeuser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RFIntrozeUser {

    @SerializedName("result")
    @Expose
    public String result;
    @SerializedName("total_contacts")
    @Expose
    public Integer totalContacts;
    @SerializedName("data")
    @Expose
    public List<Datum> data = new ArrayList<Datum>();
    @SerializedName("data_available")
    @Expose
    public Integer dataAvailable;

}
