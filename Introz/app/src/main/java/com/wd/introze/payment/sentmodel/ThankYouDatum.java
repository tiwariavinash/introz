
package com.wd.introze.payment.sentmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThankYouDatum {

    @SerializedName("amount")
    @Expose
    public Integer amount;
    @SerializedName("to_first_name")
    @Expose
    public String toFirstName;
    @SerializedName("to_last_name")
    @Expose
    public String toLastName;
    @SerializedName("to_user_id")
    @Expose
    public Integer toUserId;
    @SerializedName("your_id")
    @Expose
    public Integer yourId;
    @SerializedName("your_first_name")
    @Expose
    public String yourFirstName;
    @SerializedName("your_last_name")
    @Expose
    public String yourLastName;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("to_read")
    @Expose
    public Integer toRead;
    @SerializedName("by_read")
    @Expose
    public Integer byRead;
    @SerializedName("thank_you_id")
    @Expose
    public Integer thankYouId;
    @SerializedName("message")
    @Expose
    public String message;

}
