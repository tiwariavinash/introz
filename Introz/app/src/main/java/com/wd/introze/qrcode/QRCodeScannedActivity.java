package com.wd.introze.qrcode;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;

/**
 * Created by Admin on 4/2/2018
 */

public class QRCodeScannedActivity extends AppCompatActivity {
    //
    private TextView tvFullName;
    private TextView tvPhone;
    private TextView tvEmail;
    private ImageView iv;

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code_scanned);
        TypefaceHelper.typeface(this);
        //
        tvFullName = findViewById(R.id.tvFullName);
        tvPhone = findViewById(R.id.tvPhone);
        tvEmail = findViewById(R.id.tvEmail);
        //
        try {
            QRCodeModel model = new Gson().fromJson(getIntent().getStringExtra("data"), QRCodeModel.class);
            tvFullName.setText(model.firstName + " " + model.lastName);
            tvEmail.setText(model.email);
            tvPhone.setText(model.phone);
            //
            Glide.with(this).load(model.imageUrl).into(iv);
        } catch (Exception e) {
            Toast.makeText(this, "Please scan qr code provided by Introze App", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickBtnDownloadContactInfo(View view) {
        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        contactIntent.putExtra(ContactsContract.Intents.Insert.NAME, tvFullName.getText().toString())
                .putExtra(ContactsContract.Intents.Insert.PHONE, tvPhone.getText().toString())
                .putExtra(ContactsContract.Intents.Insert.EMAIL, tvEmail.getText().toString());
        startActivity(contactIntent);
    }
}
