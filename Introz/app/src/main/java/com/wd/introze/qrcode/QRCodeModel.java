package com.wd.introze.qrcode;

/**
 * Created by Admin on 4/3/2018.
 */

public class QRCodeModel {
    public String firstName;
    public String lastName;
    public String email;
    public String imageUrl;
    public String phone;

    public QRCodeModel(String firstName, String lastName, String email, String imageUrl, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imageUrl = imageUrl;
        this.phone = phone;
    }
}
