package com.wd.introze.jutil;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by flair on 13-12-2016.
 */

public class JUser {

    public static void setIsVerified(Context context, boolean isVerified) {
        context.getSharedPreferences("user", context.MODE_PRIVATE)
                .edit().putBoolean("is_verified_otp", isVerified).commit();
    }

    //
    public static boolean isVerified(Context context) {
        //
        SharedPreferences sp = context.getSharedPreferences("user", context.MODE_PRIVATE);
        return sp.getBoolean("is_verified_otp", false);
    }
}
