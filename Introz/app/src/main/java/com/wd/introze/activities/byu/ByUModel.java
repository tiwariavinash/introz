package com.wd.introze.activities.byu;

import android.content.ContentUris;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;

import com.wd.introze.R;
import com.wd.introze.jutil.JUtils;

/**
 * Created by flair on 22-07-2016
 */
public class ByUModel {
    //
    public int relId;
    //
    public String user1firstname;
    public String user1lastname;

    public String user2firstname;
    public String user2lastname;

    //
    public String urlProfile1;
    public String urlProfile2;
    //
    public String identifier1;
    public String identifier2;

    public int relationType;

    public int acceptedStatus;
    public String acceptedStatusName;

    public String reason;

    public String by_first_name;
    public String by_last_name;
    public String by_user_image;
    //

    public ByUModel(String user1firstname, String user1lastname, String user2firstname, String user2lastname, int relationType
            , int acceptedStatus, String reason, String by_first_name, String by_last_name, String urlProfile1, String urlProfile2,
                    String by_user_image, String oneFbId, String twoFbId, String byFbId, int relId, String identifier1, String identifier2) {
        //
        this.identifier1 = identifier1;
        this.identifier2 = identifier2;
        //
        this.user1firstname = user1firstname;
        this.user1lastname = user1lastname;
        this.user2firstname = user2firstname;
        this.user2lastname = user2lastname;
        this.relationType = relationType;
        //this.acceptedStatus = acceptedStatus;
        setAcceptedStatus(acceptedStatus);
        this.reason = reason;
        this.by_first_name = by_first_name;
        this.by_last_name = by_last_name;
        //
        this.by_user_image = JUtils.getImageUrl(by_user_image, byFbId); //AppUrls.MAIN + "/" + by_user_image;
        //
        String tempPic1 = JUtils.getImageUrl(urlProfile1, oneFbId);
        String tempPic2 = JUtils.getImageUrl(urlProfile2, twoFbId);
        //Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, data.get(position).identifier);
        this.urlProfile1 = getCorrectPic(tempPic1, identifier1); //AppUrls.MAIN + "/" + urlProfile1;
        this.urlProfile2 = getCorrectPic(tempPic2, identifier2); //AppUrls.MAIN + "/" + urlProfile2;
        //
        Log.d("ByUProf1M", this.urlProfile1 + " fdsdsd");
        Log.d("ByUProf2M", this.urlProfile2 + " dfdds");
        //
        /*if (TextUtils.isEmpty(urlProfile1) && !TextUtils.equals("0", oneFbId)) {
            this.urlProfile1 = JUtils.getFacebookProfilePictureUrl(oneFbId);
        }
        if (TextUtils.isEmpty(urlProfile2) && !TextUtils.equals("0", twoFbId)) {
            this.urlProfile2 = JUtils.getFacebookProfilePictureUrl(twoFbId);
        }
        if (TextUtils.isEmpty(by_user_image) && !TextUtils.equals("0", byFbId)) {
            this.by_user_image = JUtils.getFacebookProfilePictureUrl(byFbId);
        }*/
        //
        this.relId = relId;
    }

    //
    private String getCorrectPic(String pic, String identifier) {
        if (pic == null) {
            if (TextUtils.isEmpty(identifier)) return null;
            else
                return ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(identifier)).toString();
        }
        return pic;
    }

    public void setAcceptedStatus(int acceptedStatus) {
        this.acceptedStatus = acceptedStatus;
        switch (acceptedStatus) {
            case 0:
                acceptedStatusName = "PENDING";
                break;
            case 1:
                acceptedStatusName = "MATCHED";
                break;
            case 2:
                acceptedStatusName = "PENDING";
                break;
            case 3:
                acceptedStatusName = "PENDING";
                break;
            case 4:
                acceptedStatusName = "PENDING";
                break;
            case 5:
                acceptedStatusName = "PENDING";
                break;
            case 6:
                acceptedStatusName = "PENDING";
                break;
            case 7:
                acceptedStatusName = "PENDING";
                break;
            case 8:
                acceptedStatusName = "MATCHED";
                break;
        }

    }
    //

    public String getShortFullName() {
        String name = by_first_name + " " + by_last_name;
        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
        //return by_first_name + " " + by_last_name;
    }

    //
    public boolean isMatched() {
        if (acceptedStatus == 1) return true;
        else return false;
    }

    //
    public int getRelationTypeIcon() {
        int relation;
        switch (relationType) {
            case 0:
                relation = R.drawable.ic_business_1;
                break;
            case 1:
                relation = R.drawable.ic_dating_round;
                break;
            case 2:
                relation = R.drawable.ic_friendship_round;
                break;
            default:
                relation = R.drawable.ic_mutual_round;
        }
        return relation;
    }

    //
    public String getTwoUpperChar(int typeUser) {

        String name = null;
        switch (typeUser) {
            case TypeUserByU.TYPE_USER_1:
                name = user1firstname + " " + user1lastname;
                break;
            case TypeUserByU.TYPE_USER_2:
                name = user2firstname + " " + user2lastname;
                break;
        }

        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }

    //
    public String getFullName1() {
        return user1firstname + " " + user1lastname;
    }

    //
    public interface TypeUserByU {
        int TYPE_USER_1 = 0;
        int TYPE_USER_2 = 1;
    }
}
