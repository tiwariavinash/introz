package com.wd.introze;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.activities.ActivityAdapter;
import com.wd.introze.activities.ActivityModel;
import com.wd.introze.activities.byu.ByUAdapter;
import com.wd.introze.activities.byu.ByUDialog;
import com.wd.introze.activities.byu.ByUModel;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by flair on 22-07-2016
 */
public class FragmentActivity extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener
        , ByUAdapter.OnRelationClickListener, ByUDialog.DialogDismiss {
    //
    private ArrayList<ActivityModel> data;
    private ActivityAdapter adapter;
    //
    private LinearLayout llContainer, llIntroze4u, llIntrozeByu;
    //
    private Button btnIntroze4u, btnIntrozeByu;
    //
    private SharedPreferences spUser;
    //
    private ArrayList<ByUModel> alDataByU;
    private ByUAdapter adapterByU;
    //
    private AlertDialog dialog;
    //
    ByUDialog builder;
    //
    String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    TextView tvText4u;
    //
    TextView tvTextInByU;
    //
    private int relId;
    //
    private OnAfterForULoadListener forULoadedListerner;
    //
    private ListView lv;
    private LinearLayout llForUProgressBar, llForUListContainer;
    private TextView tvForUEmpty;
    //
    private ListView lvByU;
    private LinearLayout llByUProgressBar, llByUListContainer;
    private TextView tvByUEmpty;
    //
    private ImageView ivSpine;
    private Animation animRotate;
    private CustomProgress pd;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        forULoadedListerner = (OnAfterForULoadListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity, null);
        //
        TypefaceHelper.typeface(view);
        //
        tfText = Typeface.createFromAsset(getActivity().getAssets(), fontPathText);
        //
        builder = new ByUDialog(getActivity(), this);
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        llContainer = (LinearLayout) view.findViewById(R.id.container);
        //
        llIntroze4u = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_activity_introze_4_u, null);
        llIntrozeByu = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_activity_introze_by_you, null);
        //
        TypefaceHelper.typeface(llIntroze4u);
        TypefaceHelper.typeface(llIntrozeByu);
        //
        //
        ivSpine = (ImageView) llIntroze4u.findViewById(R.id.ivSpin);
        //
        ImageView ivSpin4U = (ImageView) llIntrozeByu.findViewById(R.id.ivSpin4U);
        //
        animRotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        //
        ivSpine.startAnimation(animRotate);
        //
        ivSpin4U.startAnimation(animRotate);
        //
        llContainer.addView(llIntroze4u, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        //
        data = new ArrayList<>();
        //
        adapter = new ActivityAdapter(getActivity(), data);
        /*
        list view for for u
         */
        lv = (ListView) llIntroze4u.findViewById(R.id.lv);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        //
        llForUProgressBar = (LinearLayout) llIntroze4u.findViewById(R.id.llProgressBar);
        tvForUEmpty = (TextView) llIntroze4u.findViewById(R.id.tvEmpty);
        llForUListContainer = (LinearLayout) llIntroze4u.findViewById(R.id.llForUListContainer);
        //
        tvForUEmpty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invitationSentForYou();
            }
        });
        //
        alDataByU = new ArrayList<>();
        //by u data
        lvByU = (ListView) llIntrozeByu.findViewById(R.id.lv);
        adapterByU = new ByUAdapter(getActivity(), alDataByU, this);
        lvByU.setAdapter(adapterByU);
        //
        llByUProgressBar = (LinearLayout) llIntrozeByu.findViewById(R.id.llProgressBar);
        tvByUEmpty = (TextView) llIntrozeByu.findViewById(R.id.tvEmpty);
        llByUListContainer = (LinearLayout) llIntrozeByu.findViewById(R.id.llByUListContainer);
        //
        tvByUEmpty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invitationSentByYou();
            }
        });
        //
        btnIntroze4u = (Button) view.findViewById(R.id.btnProfile);
        btnIntroze4u.setOnClickListener(this);
        //
        btnIntrozeByu = (Button) view.findViewById(R.id.btnMoney);
        btnIntrozeByu.setOnClickListener(this);
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        tvText4u = (TextView) llIntroze4u.findViewById(R.id.tvText4u);
        tvText4u.setTypeface(tfText);
        //
        tvTextInByU = (TextView) llIntrozeByu.findViewById(R.id.tvTextInByU);
        tvTextInByU.setTypeface(tfText);
        //
        String category = getActivity().getIntent().getStringExtra("from_notification");
        if (TextUtils.isEmpty(category) && !TextUtils.isEmpty(JSession.category))
            category = JSession.category;
        //
        if (category != null) {
            if (TextUtils.equals("NEW_INTROZE_INVITATION", category)) {
                onClick(btnIntroze4u);
                relId = getActivity().getIntent().getIntExtra("rel_id", 0);
                if (relId == 0 && JSession.rel_id != 0) relId = JSession.rel_id;
            } else if (TextUtils.equals("INTROZE_ACCEPTED", category))
                onClick(btnIntrozeByu);
        }
        /*invitationSentByYou();
        invitationSentForYou();*/
        //
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //
        forULoadedListerner.onGotoInvitation(data.get(position));
        /*Intent intent = new Intent(getActivity(), NewIntrozeInvitationActivity.class);
        intent.putExtra("data", new Gson().toJson(data.get(position)));
        startActivity(intent);*/
    }

    //
    private void gotoInvitation(int relId) {
        ActivityModel model = null;
        for (ActivityModel model1 : data) {
            Log.d("BothRelId", relId + " " + model1.relUserId);
            if (model1.relId == relId) {
                model = model1;
                break;
            }
        }
        relId = 0;
        if (model != null) {
            //
            forULoadedListerner.onGotoInvitation(model);
            /*Intent intent = new Intent(getActivity(), NewIntrozeInvitationActivity.class);
            intent.putExtra("data", new Gson().toJson(model));
            startActivity(intent);*/
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnProfile:
                llContainer.removeAllViews();
                llContainer.addView(llIntroze4u);
                //
                btnIntroze4u.setBackgroundColor(getResources().getColor(R.color.color_btn_green));
                btnIntrozeByu.setBackgroundColor(getResources().getColor(R.color.color_btn_white));
                //
                btnIntroze4u.setTextColor(getResources().getColor(R.color.color_btn_white));
                btnIntrozeByu.setTextColor(getResources().getColor(android.R.color.darker_gray));
                break;
            case R.id.btnMoney:
                llContainer.removeAllViews();
                llContainer.addView(llIntrozeByu, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                //
                btnIntroze4u.setBackgroundColor(getResources().getColor(R.color.color_btn_white));
                btnIntrozeByu.setBackgroundColor(getResources().getColor(R.color.color_btn_green));
                //
                btnIntroze4u.setTextColor(getResources().getColor(android.R.color.darker_gray));
                btnIntrozeByu.setTextColor(getResources().getColor(R.color.color_btn_white));
                break;
        }
    }

    //
    private void invitationSentByYou() {
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        llByUListContainer.setVisibility(View.GONE);
        llByUProgressBar.setVisibility(View.VISIBLE);
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, true, null);
        else pd.show();
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/by_you").setHeader("Authorization", "Basic " + basic).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        //
                        pd.dismiss();
                        //
                        llByUProgressBar.setVisibility(View.GONE);
                        Log.d("ByYou", result + "");
                        if (result != null) {
                            try {
                                JSONObject joResult = new JSONObject(result);
                                //
                                if (TextUtils.equals("success", joResult.getString("result"))) {
                                    if (joResult.has("invitations")) {
                                        JSONArray joInvitation = joResult.getJSONArray("invitations");
                                        //
                                        alDataByU.clear();
                                        for (int i = 0; i < joInvitation.length(); i++) {
                                            JSONObject joItem = joInvitation.getJSONObject(i);
                                            //
                                            ByUModel model = new ByUModel(joItem.getString("user1_first_name"),
                                                    joItem.getString("user1_last_name"), joItem.getString("user2_first_name"),
                                                    joItem.getString("user2_last_name"), joItem.getInt("relation_type"),
                                                    joItem.getInt("accepted_status"), joItem.getString("reason"),
                                                    joItem.getString("by_first_name"), joItem.getString("by_last_name"),
                                                    joItem.getString("user_image_1"), joItem.getString("user_image_2"),
                                                    joItem.getString("by_user_image"), joItem.optString("fb_id_1"),
                                                    joItem.optString("fb_id_2"), joResult.optString("fb_id"), joItem.getInt("rel_id"),
                                                    joItem.optString("identifier_1"), joItem.optString("identifier_2"));
                                            alDataByU.add(model);
                                        }
                                        adapterByU.notifyDataSetChanged();
                                        //
                                        setEmptyViewByU(alDataByU.size());
                                        //
                                        forULoadedListerner.onAfterByULoaded(alDataByU);
                                    } else if (joResult.has("message")) {
                                        tvByUEmpty.setText(joResult.getString("message") + " Please tap to refresh");
                                        setEmptyViewByU(0);
                                    }
                                } else {
                                    //
                                    tvByUEmpty.setText(getString(R.string.network_empty_view));
                                    setEmptyViewByU(0);
                                    //invitationSentByYou();
                                    //Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                //
                                tvByUEmpty.setText(getString(R.string.network_empty_view));
                                setEmptyViewByU(0);
                                //invitationSentByYou();
                                //Toast.makeText(getActivity(), "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //
                            //
                            tvByUEmpty.setText(getString(R.string.network_empty_view));
                            setEmptyViewByU(0);
                            //invitationSentByYou();
                            //Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    //
    private void setEmptyViewForU(int forUCount) {
        llForUListContainer.setVisibility(View.VISIBLE);
        //
        if (forUCount <= 0) {
            lv.setVisibility(View.GONE);
            tvForUEmpty.setVisibility(View.VISIBLE);
        } else {
            tvForUEmpty.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
        }
    }

    //
    //
    private void setEmptyViewByU(int byUCount) {
        llByUListContainer.setVisibility(View.VISIBLE);
        //
        if (byUCount <= 0) {
            lvByU.setVisibility(View.GONE);
            tvByUEmpty.setVisibility(View.VISIBLE);
        } else {
            tvByUEmpty.setVisibility(View.GONE);
            lvByU.setVisibility(View.VISIBLE);
        }
    }

    //
    private void invitationSentForYou() {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        llForUListContainer.setVisibility(View.GONE);
        llForUProgressBar.setVisibility(View.VISIBLE);
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, true, null);
        else pd.show();
        //
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/for_you").setHeader("Authorization", "Basic " + basic).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        //
                        pd.dismiss();
                        //
                        llForUProgressBar.setVisibility(View.GONE);
                        //
                        Log.d("InvitationForU", result + "");
                        if (result != null) {
                            try {
                                JSONObject joResult = new JSONObject(result);
                                if (TextUtils.equals("success", joResult.getString("result"))) {
                                    if (joResult.has("invitations")) {
                                        JSONArray joInvitations = joResult.getJSONArray("invitations");
                                        //
                                        data.clear();
                                        for (int i = 0; i < joInvitations.length(); i++) {
                                            JSONObject joItem = joInvitations.getJSONObject(i);
                                            //
                                            Log.d("RelFbId", joItem.optString("rel_fb_id") + "");
                                            //
                                            data.add(new ActivityModel(joItem.getInt("rel_id") + "", joItem.getString("userBy_first_name"),
                                                    joItem.getString("userBy_last_name"), joItem.getInt("relation_type")
                                                    , joItem.getString("rel_user_first_name"), joItem.getString("rel_user_last_name")
                                                    , joItem.getString("reason"), joItem.getString("rel_user_image"),
                                                    joItem.getString("by_user_image"), joItem.getInt("accepted_status"),
                                                    joItem.getInt("person"), joItem.optString("rel_fb_id"),
                                                    joItem.optString("by_fb_id"), joItem.getInt("user_1_status"),
                                                    joItem.getInt("user_2_status"), joItem.getInt("by_user_id"),
                                                    joItem.getInt("rel_user_id"), joItem.getInt("rel_id"),
                                                    joItem.getString("created_on"), joItem.getString("rel_identifier"),
                                                    joItem.getString("by_identifier")));
                                        }
                                        adapter.notifyDataSetChanged();
                                        //
                                        setEmptyViewForU(data.size());
                                        //
                                        forULoadedListerner.onAfterForULoaded(data);
                                        if (HomeActivity.isFromNotification) {
                                            HomeActivity.isFromNotification = false;
                                            gotoInvitation(relId);
                                        }
                                    } else if (joResult.has("message")) {
                                        tvForUEmpty.setText(joResult.getString("message") + " Please tap to refresh");
                                        setEmptyViewForU(0);
                                    }
                                } else {
                                    tvForUEmpty.setText(getString(R.string.network_empty_view));
                                    setEmptyViewForU(0);
                                }
                                //invitationSentForYou();
                                //Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e1) {
                                //
                                tvForUEmpty.setText(getString(R.string.network_empty_view));
                                setEmptyViewForU(0);
                                //invitationSentForYou();
                                //Toast.makeText(getActivity(), "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //
                            tvForUEmpty.setText(getString(R.string.network_empty_view));
                            setEmptyViewForU(0);
                            //invitationSentForYou();
                            /*Toast toast = Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT);
                            if (toast != null) {
                                toast.show();
                            }*/
                        }
                    }
                });
    }

    @Override
    public void onRelationClicked(String shortName, String reason, String url) {
        /*Bundle b = new Bundle();
        b.putString("name", name);
        b.putString("reason", reason);*/
        //dialog.setArguments(b);
        builder.setValues(shortName, reason, url);
        dialog.show();
    }
    //

    @Override
    public void onResume() {
        super.onResume();
        invitationSentByYou();
        invitationSentForYou();
    }
    //


    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000 && resultCode == getActivity().RESULT_OK) {
            invitationSentByYou();
            invitationSentForYou();
        }
    }*/

    @Override
    public void dialogDismiss() {
        dialog.dismiss();
    }
    //

    public interface OnAfterForULoadListener {
        void onAfterForULoaded(ArrayList<ActivityModel> data);

        void onAfterByULoaded(ArrayList<ByUModel> data);

        //
        void onGotoInvitation(ActivityModel model);
    }
}
