package com.wd.introze.qrcode;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
//import com.google.zxing.WriterException;
import com.google.zxing.WriterException;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;
import com.wd.introze.repository.server.rf.model.user.User;
import com.wd.introze.repository.server.rf.model.user.UserDetails;

import java.io.IOException;

/**
 * Created by Admin on 3/31/2018.
 */

public class QRCodeFragment extends Fragment {
    private static final String TAG = "dQRCodeFragment";
    //
    private TextView tvFirstName;
    //
    private SharedPreferences sp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_qr_code, null);
        TypefaceHelper.typeface(view);
        //
        tvFirstName = view.findViewById(R.id.tvFirstName);
        //
        sp = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        String userDetail = sp.getString(User.class.getName(), null);
        //
        UserDetails userDetails = new Gson().fromJson(userDetail, User.class).userDetails;
        //
        final ImageView ivQRCode = view.findViewById(R.id.ivQRCode);
        try {
            QRCodeModel data = new QRCodeModel(userDetails.firstName, userDetails.lastName, userDetails.email, userDetails.userImage, userDetails.phone);
            Bitmap bitmap = QRCodeGenerator.getQRCodeImage(new Gson().toJson(data), 350, 350);
            if (bitmap != null)
                ivQRCode.setImageBitmap(bitmap);
            else Log.d(TAG, "Bitmap is null");
        } /*catch (WriterException e) {
            e.printStackTrace();
        }*/ catch (IOException e) {
            e.printStackTrace();
        } catch (WriterException e) {
            e.printStackTrace();
        }
        //
        tvFirstName.setText(userDetails.firstName);
        //
        return view;
    }
}
