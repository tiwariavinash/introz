package com.wd.introze.selectcontact;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.provider.ContactsContract;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.wd.introze.R;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.tables.TableContacts;

import java.io.InputStream;
import java.util.List;

/**
 * Created by flair on 28-07-2016
 */
public class SelectAdapter extends RecyclerView.Adapter<SelectVH> {
    public static List<TableContacts> data;
    private Context context;
    //
    static ContactClickListener listener;

    //
    public SelectAdapter(Context context, List<TableContacts> data) {
        this.context = context;
        this.data = data;
        listener = (ContactClickListener) context;
    }

    @Override
    public SelectVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SelectVH(LayoutInflater.from(context).inflate(R.layout.activity_select_contact_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(SelectVH holder, int position) {
        //
        final TableContacts model = data.get(position);
        //
        TextView tvName = holder.tvName;
        tvName.setTextSize(18);
        //
        //
        String fullName = model.firstName + " " + model.lastName;
        tvName.setText(JUtils.uppercaseFirstLetters(fullName));
        //
        if (data.get(position).isIntroze) {
            holder.viewBlueCircle.setVisibility(View.VISIBLE);
        } else {
            holder.viewBlueCircle.setVisibility(View.GONE);
        }
        //
        //Bitmap bitmap = loadContactPhoto(context.getContentResolver(), data.get(position).identifier);
        //
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, data.get(position).identifier);//Uri.parse("");
        /*if (bitmap != null) {
            uri = JFile.getImageUri(context, loadContactPhoto(context.getContentResolver(), data.get(position).identifier));
        }*/

        //
        final TextView tvShortName = holder.tvShortName;
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(JUtils.getTwoUpperChar(model.firstName + " " + model.lastName));
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //

        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(model.urlPic == null ? uri.toString() : model.urlPic).build();
        holder.ivProfile.setController(controller);
        //
        if (!SelectContactActivity.isFromCreateAccount && model.isIntroze) {
            if (fullName.length() > 20)
                tvName.setTextSize(14);
            holder.ivDollar.setVisibility(View.VISIBLE);
            if (model.isMatched) {
                holder.ivChat.setVisibility(View.VISIBLE);
            } else {
                holder.ivChat.setVisibility(View.GONE);
            }
        } else {
            holder.ivDollar.setVisibility(View.GONE);
            holder.ivChat.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    //
    /*public String getTwoUpperChar(String name) {
        //
        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }*/

    //
    public Bitmap loadContactPhoto(ContentResolver cr, long id) {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null) {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }

    //
    public interface ContactClickListener {
        void onContactClicked(TableContacts contacts, boolean is4Profile);

        void onPaymentClickListener(TableContacts contacts);

        //
        void onChatClicked(TableContacts contacts);
    }
}
