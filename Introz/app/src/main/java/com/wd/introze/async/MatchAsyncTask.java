package com.wd.introze.async;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.match.MatchModel;
import com.wd.introze.repository.server.rf.RFClient;
import com.wd.introze.repository.server.rf.RFService;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Gajendra on 10/4/2016
 */

public class MatchAsyncTask extends AsyncTask<String, Void, List<ArrayList<MatchModel>>> {
    //
    public static final String MATCH_RECEIVER = "match_receiver";
    public static final String MATCH_DATA = "data";
    private static final String TAG = "MatchAsyncTask";

    //
    private Context context;

    public MatchAsyncTask(Context context) {
        this.context = context;
    }

    //
    @Override
    protected List<ArrayList<MatchModel>> doInBackground(String... params) {
        //
        List<ArrayList<MatchModel>> data = new ArrayList<>();
        //
        ArrayList<MatchModel> alMatchList = getMatchList(params[0]);
        data.add(alMatchList);
        //
        ArrayList<QBChatDialog> alDialogList = getDialogList();
        //
        ArrayList<MatchModel> alDialogs = new ArrayList<>();
        //
        for (QBChatDialog dialog : alDialogList) {
            MatchModel model = containChatId(alMatchList, dialog);
            if (model != null) {
                alDialogs.add(model);
            }
        }
        data.add(alDialogs);
        //
        return data;
    }

    //
    //
    private MatchModel containChatId(ArrayList<MatchModel> alMatch, QBChatDialog dialog) {
        ArrayList<Integer> chatIds = new ArrayList<>();
        chatIds.add(dialog.getOccupants().get(0));
        chatIds.add(dialog.getOccupants().get(1));
        //
        for (MatchModel model : alMatch) {
            //
            if (chatIds.contains(model.user1ChatId) && chatIds.contains(model.user2ChatId)) {
                model.count = dialog.getUnreadMessageCount();
                return model;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<ArrayList<MatchModel>> data) {
        super.onPostExecute(data);
        //
        Intent intent = new Intent(MATCH_RECEIVER);
        intent.putExtra(MATCH_DATA, new Gson().toJson(new MatchData(data)));
        context.sendBroadcast(intent);
    }

    //
    private ArrayList<MatchModel> getMatchList(final String auth) {
        //
        if (!JConnectionUtils.isConnectedToInternet(context)) return new ArrayList<>();
        //
        ArrayList<MatchModel> data = new ArrayList<>();
        //
        try {
            Response<ResponseBody> response = RFClient.getClient().create(RFService.class).getMatchList(auth).execute();
            //
            if (response.isSuccessful()) {
                try {
                    String result = response.body().string();
                    //
                    Log.d(TAG, "Match List " + result);
                    //
                    JSONObject joResult = new JSONObject(result);
                    if (TextUtils.equals("success", joResult.getString("result"))) {
                        if (joResult.has("new_match")) {
                            JSONArray joNewMatch = joResult.getJSONArray("new_match");
                            for (int i = 0; i < joNewMatch.length(); i++) {
                                JSONObject joItem = joNewMatch.getJSONObject(i);
                                String fn = joItem.getString("rel_first_name");
                                String ln = joItem.getString("rel_last_name");
                                //
                                String byName = joItem.getString("by_first_name") + " " + joItem.getString("by_last_name");
                                //
                                String createdOn = JUtils.getDateInDotFormat(joItem.getString("created_on"));
                                //
                                Log.d("MyCreateOn", createdOn + "hhkkkjj");
                                //
                                MatchModel model = new MatchModel(fn + " " + ln, fn, joItem.getString("rel_user_image"),
                                        joItem.getInt("rel_chat_id"), byName, createdOn, joItem.getInt("relation_type"),
                                        joItem.getInt("read_flag"), joItem.getInt("rel_id"), joItem.getInt("rel_user_id"),
                                        joItem.getInt("by_user_id"), joItem.getString("reason"),
                                        joItem.optString("by_user_image"), joItem.optString("by_fb_id"),
                                        joItem.optString("rel_identifier"), joItem.optString("by_identifier"),
                                        joItem.optString("rel_fb_id"), joItem.optInt("user1_chat_id"),
                                        joItem.optInt("user2_chat_id"));
                                //
                                data.add(model);
                            }
                        }
                    } else {
                        getMatchList(auth);
                    }
                    //
                } catch (IOException e) {
                    getMatchList(auth);
                } catch (JSONException e) {
                    getMatchList(auth);
                }
            } else {
                getMatchList(auth);
            }
        } catch (IOException e) {
            getMatchList(auth);
        }
        return data;
    }

    //
    private ArrayList<QBChatDialog> getDialogList() {
        //
        //
        if (!JConnectionUtils.isConnectedToInternet(context)) return new ArrayList<>();
        //
        //
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.setLimit(100);
        //
        try {
            ArrayList<QBChatDialog> dialogs = QBChatService.getChatDialogs(QBDialogType.PRIVATE, requestBuilder, new Bundle());
            return dialogs;
        } catch (QBResponseException e) {
            getDialogList();
        }
        //
        return new ArrayList<>();
    }

    //
    public class MatchData {
        public List<ArrayList<MatchModel>> data = new ArrayList<>();

        public MatchData(List<ArrayList<MatchModel>> data) {
            this.data = data;
        }
    }
}
