package com.wd.introze;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;

/**
 * Created by admin on 2016-07-23.
 */
public class PrivacyPolicyActivity extends Fragment {

    //
    /*String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
*/
    //
    private TextView tv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_privacy_policy, null);
        TypefaceHelper.typeface(view);
        //
        //tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("PRIVACY POLICY");*/
        //initHeader();
        //
        WebView wv = (WebView) view.findViewById(R.id.webView);
        wv.loadUrl("file:///android_asset/privacy.html");
        /*tv = (TextView) view.findViewById(R.id.tv);
        tv.setText(Html.fromHtml(JUtils.readContentFromAssetsFile(getActivity(), "privacy.html")));*/
        //getHtmlFromServer();
        return view;
    }

    /*private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("PRIVACY POLICY");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }*/

    //
    private void getHtmlFromServer() {
        Ion.with(this).load("http://introze.com/PrivacyPolicy").asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                tv.setText(Html.fromHtml(result));
            }
        });
    }

}
