package com.wd.introze;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JErrorTextWatcher;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.payment.AmountActivity;

/**
 * Created by flair on 30-07-2016
 */
public class ReceiveMoneyActivity extends AppCompatActivity {
    //
    private final String TAG = "ReceiveMoneyResult";
    //
    private SharedPreferences spUser;
    //
    private EditText etNameOfBank, etRoutingNumber, etBankAccountNumber;
    private TextView tvEBankName, tvERoutingNumber, tvEAccountNumber;

    //
    TextView tvText;
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;
    ///
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private CustomProgress pd;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    //
    private boolean isFromProfile;

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_money);
        TypefaceHelper.typeface(this);
        //
        isFromProfile = getIntent().getBooleanExtra("is_from_profile", false);
        //
        tfText = Typeface.createFromAsset(getAssets(), fontPathText);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        initHeader();
        //
        tvEBankName = (TextView) findViewById(R.id.tvENameOfBank);
        etNameOfBank = (EditText) findViewById(R.id.etNameOfBank);
        etNameOfBank.addTextChangedListener(new JErrorTextWatcher(tvEBankName, "Please enter bank name"));
        //
        tvERoutingNumber = (TextView) findViewById(R.id.tvERoutingNumber);
        etRoutingNumber = (EditText) findViewById(R.id.etRoutingNumber);
        etRoutingNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    tvERoutingNumber.setText("Please enter routing number");
                } else if (s.length() != 9) {
                    tvERoutingNumber.setText("Routing number must be 9 digit long");
                } /*else if (s.length() > 9) {
                    tvEAccountNumber.setText("Card number should not be greater than 9 digit");
                }*/ else tvERoutingNumber.setText("");
            }
        });
        //etRoutingNumber.addTextChangedListener(new JErrorTextWatcher(tvEBankName, "Please enter rout number name"));
        //
        tvEAccountNumber = (TextView) findViewById(R.id.tvEAccountNumber);
        etBankAccountNumber = (EditText) findViewById(R.id.etBankAccountNumber);
        //etBankAccountNumber.addTextChangedListener(new JErrorTextWatcher(tvEAccountNumber, "Please enter bank number"));
        //
        etBankAccountNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    tvEAccountNumber.setText("Please enter bank account");
                } else if (s.length() < 6) {
                    tvEAccountNumber.setText("Bank account should not be less than 6 digit");
                } /*else if (s.length() > 9) {
                    tvEAccountNumber.setText("Card number should not be greater than 9 digit");
                }*/ else tvEAccountNumber.setText("");
            }
        });
        //
        spUser = getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        tvText = (TextView) findViewById(R.id.tvText);
        //
        tvText.setTypeface(tfText);
        //
        builderError = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srl);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        //
                        //mySwipeRefreshLayout.setRefreshing(true);
                        //Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout");

                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        getBankDetail();
                    }
                }
        );
        //String bankDetail = spUser.getString()
        //
        String bankDetail = spUser.getString(ReceiveModel.class.getName(), null);
        if (TextUtils.isEmpty(bankDetail))
            getBankDetail();
        else setBankDetail(new Gson().fromJson(bankDetail, ReceiveModel.class));
    }

    private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("RECEIVE MONEY SETTINGS");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }

    //
    private void setBankDetail(ReceiveModel model) {
        //
        String name = model.name;
        String accountNumber = model.accountNumber;
        String routingNumber = model.routingNumber;
        //
        if (!TextUtils.isEmpty(accountNumber)) {
            etBankAccountNumber.setText(accountNumber);
        }
        if (!TextUtils.isEmpty(routingNumber)) {
            etRoutingNumber.setText(routingNumber);
        }
        if (!TextUtils.isEmpty(name)) {
            etNameOfBank.setText(name);
        }
    }


    //
    public void onClickBtnSave(View view) {
        final String name = etNameOfBank.getText().toString();
        if (TextUtils.isEmpty(name)) {
            tvEBankName.setText("Please enter bank name");
            //Toast.makeText(ReceiveMoneyActivity.this, "Please enter bank name", Toast.LENGTH_SHORT).show();
            return;
        }
        //
        final String rout = etRoutingNumber.getText().toString();
        if (TextUtils.isEmpty(rout)) {
            tvERoutingNumber.setText("Please enter Routing Number");
            return;
        } else if (rout.length() != 9) {
            tvERoutingNumber.setText("Routing number must be 9 digit long");
            return;
        }
        //
        final String bankNumber = etBankAccountNumber.getText().toString();
        if (TextUtils.isEmpty(bankNumber)) {
            tvEAccountNumber.setText("Please enter bank number");
            return;
        } else if (bankNumber.length() < 6) {
            tvEAccountNumber.setText("Bank number should not be less then 6 digit");
            return;
        }
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        pd = CustomProgress.show(this, null, false, false, null);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/add_bank_detail").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("account_number", bankNumber).setBodyParameter("routing_number", rout)
                .setBodyParameter("name", name).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                pd.dismiss();
                Log.d(TAG, "" + result);
                if (result != null) {
                    SuccessModel model = new Gson().fromJson(result, SuccessModel.class);
                    //
                    if (TextUtils.equals("success", model.result)) {
                        ReceiveModel receiveModel = new ReceiveModel(bankNumber, rout, name);
                        spUser.edit().putString(ReceiveModel.class.getName(), new Gson().toJson(receiveModel)).commit();
                        if (isFromProfile) {
                            //
                            Intent intent = new Intent(ReceiveMoneyActivity.this, AmountActivity.class);
                            //
                            intent.putExtra("amount", getIntent().getIntExtra("amount", 0));
                            intent.putExtra("last_4", receiveModel.routingNumber.substring(receiveModel.routingNumber.length() - 4));
                            //
                            startActivity(intent);
                            finish();
                        } else {
                            //
                            showAlertMessage("Success", model.message);
                            //builderError.setErrorMessage("Success", model.message);
                            //Toast.makeText(ReceiveMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            //adError.show();
                        }
                    } else {
                        //
                        showAlertMessage("Error", model.message);
                        /*builderError.setErrorMessage(model.message);
                        adError.show();*/
                    }
                    //Toast.makeText(ReceiveMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                } else {
                    //
                    showAlertMessage("Error", getString(R.string.error_no_response));
                    /*builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();*/
                    //Toast.makeText(ReceiveMoneyActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private void showAlertMessage(String title, String message) {
        builderError.setErrorMessage(message);
        adError.show();
        TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
    }

    //
    public class ReceiveModel {

        @SerializedName("result")
        @Expose
        public String result;
        @SerializedName("account_number")
        @Expose
        public String accountNumber;
        @SerializedName("routing_number")
        @Expose
        public String routingNumber;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("message")
        @Expose
        public String message;

        public ReceiveModel(String accountNumber, String routingNumber, String name) {
            this.accountNumber = accountNumber;
            this.routingNumber = routingNumber;
            this.name = name;
        }
    }

    //
    private void getBankDetail() {
//
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        pd = CustomProgress.show(this, null, false, false, null);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/check_bank_detail").setHeader("Authorization", "Basic " + basic).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        mySwipeRefreshLayout.setRefreshing(false);
                        pd.dismiss();
                        Log.d(TAG, "" + result);
                        if (result != null) {
                            ReceiveModel model = new Gson().fromJson(result, ReceiveModel.class);
                            if (TextUtils.equals("success", model.result)) {
                                //
                                if (TextUtils.isEmpty(model.routingNumber) || TextUtils.isEmpty(model.accountNumber)) {
                                    if (!isFromProfile) {
                                        /*builderError.setErrorMessage(model.message);
                                        adError.show();*/
                                    }
                                } else {
                                    spUser.edit().putString(ReceiveModel.class.getName(), new Gson().toJson(model)).commit();
                                    setBankDetail(model);
                                }
                                //builderError.setErrorMessage(model.message);
                                /*String name = joResult.getString("name");
                                String accountNumber = joResult.getString("account_number");
                                String routingNumber = joResult.getString("routing_number");
                                //
                                if (!TextUtils.isEmpty(accountNumber)) {
                                    etBankAccountNumber.setText(accountNumber);
                                }
                                if (!TextUtils.isEmpty(routingNumber)) {
                                    etRoutingNumber.setText(routingNumber);
                                }
                                if (!TextUtils.isEmpty(name)) {
                                    etNameOfBank.setText(name);
                                }*/
                            } else {
                                showAlertMessage("Error", model.message);
                                /*builderError.setErrorMessage(model.message);
                                adError.show();*/
                            }
                            //Toast.makeText(ReceiveMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();

                        } else {
                            //
                            showAlertMessage("Error", getString(R.string.error_no_response));
                           /* builderError.setErrorMessage(getString(R.string.error_no_response));
                            adError.show();*/
                            //Toast.makeText(ReceiveMoneyActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
