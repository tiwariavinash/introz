package com.wd.introze;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Gajendra on 9/27/2016
 */

public class ThankUReceiveActivity extends AppCompatActivity {
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_u_receive);
        // TypefaceHelper.typeface(this);
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        TextView tvTitle = (TextView) findViewById(R.id.txtHeader);
        tvTitle.setText("thank you $$$ received".toUpperCase());
        tvTitle.setTextSize(12);
        tvTitle.setTypeface(tfHeader);
        //
        String data = getIntent().getStringExtra("data");
        //
        getSupportFragmentManager().beginTransaction().add(R.id.container, ThankUReceiveFragment.getInstance(data)).commit();
    }
}
