package com.wd.introze.feed;


import com.activeandroid.query.Select;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.tables.TableContacts;

import java.util.List;

/**
 * Created by flair on 21-07-2016
 */
public class FeedModel {
    //
    public boolean isMatched;
    public int chatId;
    //
    public String identifier;
    //
    public String fb_id;
    //
    public int userid;
    //
    public int feedType;
    //
    public String firstName;
    public String lastName;

    public String statusMessage;
    public String postedOn;
    //
    public String urlProfile;
    //
    public boolean isUser;

    public FeedModel(int userid, String firstName, String lastName, String statusMessage, String postedOn, int feedType,
                     String urlProfile, boolean isUser, String fb_id, String identifier) {
        this.userid = userid;
        this.feedType = feedType;
        this.firstName = firstName;
        this.lastName = lastName;
        this.statusMessage = statusMessage;
        this.postedOn = postedOn;
        this.isUser = isUser;
        this.fb_id = fb_id;
        this.urlProfile = JUtils.getImageUrl(urlProfile, fb_id); /*urlProfile; //AppUrls.MAIN + "/" + urlProfile;
        if (TextUtils.isEmpty(urlProfile) && !TextUtils.isEmpty(fb_id) && !TextUtils.equals("0", fb_id)) {
            this.urlProfile = JUtils.getFacebookProfilePictureUrl(fb_id);
        }*/
        this.identifier = identifier;
    }

    //
    public FeedModel(int userId, String firstName, String lastName, String urlRel) {
        this.userid = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.urlProfile = urlRel;
    }

    //
    public FeedModel(int userId, String firstName, String lastName, String urlRel, String identifier) {
        this(userId, firstName, lastName, urlRel);
        this.identifier = identifier;
        isMatched = setMatched(this.identifier);
    }
    //

    //
    private boolean setMatched(String identifier) {
        List<TableContacts> items = new Select().from(TableContacts.class).where("Identifier =?", identifier).execute();
        if (items.size() > 0) {
            return items.get(0).isMatched;
        }
        return false;
    }

    //
    public String getRelationTypeText() {
        String relation;
        switch (feedType) {
            case 0:
                relation = "Business";
                break;
            case 1:
                relation = "Dating";
                break;
            case 2:
                relation = "Friendship";
                break;
            default:
                relation = "Mutual Interest";
        }
        return relation;
    }

    //
    public String getTwoUpperChar() {
        String name = firstName + " " + lastName;
        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }
    //
}
