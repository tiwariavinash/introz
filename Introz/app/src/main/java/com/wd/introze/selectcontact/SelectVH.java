package com.wd.introze.selectcontact;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;

/**
 * Created by Gajendra on 8/31/2016
 */
public class SelectVH extends RecyclerView.ViewHolder implements View.OnClickListener {
    SimpleDraweeView ivProfile;
    TextView tvName, tvShortName;
    View viewBlueCircle;
    ImageView ivDollar, ivChat;

    public SelectVH(View itemView) {
        super(itemView);
        //
        TypefaceHelper.typeface(itemView);
        //
        ivProfile = (SimpleDraweeView) itemView.findViewById(R.id.iv);
        ivProfile.setOnClickListener(this);
        //
        tvName = (TextView) itemView.findViewById(R.id.tvName);
        tvShortName = (TextView) itemView.findViewById(R.id.tvShortName);
        viewBlueCircle = itemView.findViewById(R.id.view);
        //
        ivDollar = (ImageView) itemView.findViewById(R.id.ivDoller);
        ivDollar.setOnClickListener(this);
        //
        ivChat = (ImageView) itemView.findViewById(R.id.ivChat);
        ivChat.setOnClickListener(this);
        //
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv) {
            SelectAdapter.listener.onContactClicked(SelectAdapter.data.get(getAdapterPosition()), true);
        } else if (v.getId() == R.id.ivDoller) {
            SelectAdapter.listener.onPaymentClickListener(SelectAdapter.data.get(getAdapterPosition()));
        } else if (v.getId() == R.id.ivChat) {
            SelectAdapter.listener.onChatClicked(SelectAdapter.data.get(getAdapterPosition()));
        } else
            SelectAdapter.listener.onContactClicked(SelectAdapter.data.get(getAdapterPosition()), false);
    }
}
