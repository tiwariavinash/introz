package com.wd.introze.jutil;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wd.introze.R;

/**
 * Created by admin on 10-10-2015
 */
public class JImageChooser extends DialogFragment {

    //
    private static final int REQUEST_IMAGE_CAPTURE = 100;
    private static final int RESULT_LOAD_IMG = 120;

    public OnImageLoadListener listener;

    public JImageChooser() {
    }

    public void setOnImageLoadListener(Object object) {
        listener = (OnImageLoadListener) object;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.j_image_chooser, null);
        builder.setView(view);
        //
        ImageButton btnGallery = (ImageButton) view.findViewById(R.id.btnGallery);
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGalleryIntent();
            }
        });
        //
        ImageButton btnCamera = (ImageButton) view.findViewById(R.id.btnCapture);
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callCameraIntent();
            }
        });
        //
        TextView tvGallery = (TextView) view.findViewById(R.id.tvGallery);
        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGalleryIntent();
            }
        });
        //
        TextView tvCancel = (TextView) view.findViewById(R.id.tvCancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // builder.setCancelable(true);
                getDialog().dismiss();
            }
        });
        //
        return builder.create();
    }

    public void callCameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    public void callGalleryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //
        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE:
                if (resultCode == FragmentActivity.RESULT_OK) {
                    Bundle extras = data.getExtras();
                    Bitmap bitmap = (Bitmap) extras.get("data");
                    //
                    listener.onImageLoaded(bitmap);
                }
                break;
            case RESULT_LOAD_IMG:
                if (FragmentActivity.RESULT_OK == resultCode) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    // Get the cursor
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // Move to first row
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    Bitmap bitmap = BitmapFactory.decodeFile(imgDecodableString);
                    //
                    listener.onImageLoaded(bitmap);
                }
                break;
        }
        dismiss();
    }

    public interface OnImageLoadListener {
        void onImageLoaded(Bitmap bitmap);
    }
}
