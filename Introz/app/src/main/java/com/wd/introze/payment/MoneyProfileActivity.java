package com.wd.introze.payment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wd.introze.FeedProfileActivity;
import com.wd.introze.R;
import com.wd.introze.feed.FeedModel;

/**
 * Created by flair on 15-09-2016
 */
public class MoneyProfileActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_profile);
        //
        TextView tvTitle = (TextView) findViewById(R.id.txtHeader);
        tvTitle.setTextSize(12);
        //
        Intent intent = getIntent();
        //
        int id = intent.getIntExtra("user_id", 0);
        String fn = intent.getStringExtra("first_name");
        String ln = intent.getStringExtra("last_name");
        String url = intent.getStringExtra("url_pic");
        //
        tvTitle.setText(fn + " " + ln);
        //
        FeedModel model = new FeedModel(id, fn, ln, url);
        //
        getSupportFragmentManager().beginTransaction().replace(R.id.container,
                FeedProfileActivity.getInstance(new Gson().toJson(model))).commit();
    }
}
