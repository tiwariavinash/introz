package com.wd.introze.activities;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;
import com.wd.introze.jutil.JUtils;

import java.util.ArrayList;

/**
 * Created by flair on 22-07-2016
 */
public class ActivityAdapter extends BaseAdapter {
    //
    private ArrayList<ActivityModel> data;
    private Context context;

    //
    public ActivityAdapter(Context context, ArrayList<ActivityModel> data) {
        this.data = data;
        this.context = context;
    }

    //
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //
        LinearLayout ll = (LinearLayout) convertView;
        if (ll == null) {
            ll = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.fragment_activity_list_item, null);
            TypefaceHelper.typeface(ll);
        }
        //
        ActivityModel model = data.get(position);
        //
        TextView tvByUser = (TextView) ll.findViewById(R.id.tvByUser);
        tvByUser.setText("By " + model.byFirstName + " " + model.byLastName);
        //
        TextView tvName = (TextView) ll.findViewById(R.id.tvName);
        tvName.setText(model.relFirstName + "\n" + model.relLastName);
        //
        ImageView iv = (ImageView) ll.findViewById(R.id.ivRelationType);
        iv.setImageResource(JUtils.getRelationTypeIcon(model.relationType));
        //
        SimpleDraweeView ivProfile = (SimpleDraweeView) ll.findViewById(R.id.ivProfile);
        final TextView tvShortName = (TextView) ll.findViewById(R.id.tvShortName);
        //
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(data.get(position).getTwoUpperChar(ActivityModel.TypeUser.TYPE_REL_USER));
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(data.get(position).urlProfile).build();
        ivProfile.setController(controller);

        //
        TextView tvDate = (TextView) ll.findViewById(R.id.tvDate);
        tvDate.setText(JUtils.getDateInDotFormat(model.createdOn));
        /*if (model.urlProfile == null) {
            ivProfile.setImageResource(R.drawable.circle_gray);
            tvShortName.setText(model.getTwoUpperChar(ActivityModel.TypeUser.TYPE_REL_USER));
        } else {
            Ion.with(ivProfile).load(model.urlProfile);
            tvShortName.setVisibility(View.GONE);
        }*/
        return ll;
    }
}
