
package com.wd.introze.repository.server.rf.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserDetails {

    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("balance")
    @Expose
    public Integer balance;
    @SerializedName("user_chat_id")
    @Expose
    public Integer userChatId;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("status")
    @Expose
    public Status status;
    @SerializedName("identifire")
    @Expose
    public String identifire;
    @SerializedName("user_image")
    @Expose
    public String userImage;
    @SerializedName("fb_id")
    @Expose
    public String fbId;
    /*@SerializedName("emails")
    @Expose
    public String emails;*/
    @SerializedName("extra_emails")
    @Expose
    public List<Email> emails = new ArrayList<>();
    //
    @SerializedName("level")
    @Expose
    public String level;
    @SerializedName("profile_points")
    @Expose
    public Integer profilePoints;
    @SerializedName("invite_code")
    @Expose
    public String inviteCode;
}
