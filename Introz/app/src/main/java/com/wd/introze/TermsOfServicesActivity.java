package com.wd.introze;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.norbsoft.typefacehelper.TypefaceHelper;

/**
 * Created by admin on 2016-07-23
 */
public class TermsOfServicesActivity extends Fragment {
    //
   /* String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;*/

    //

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_terms_of_services, null);
        TypefaceHelper.typeface(view);
        //
        //tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("TERMS OF SERVICES");*/
        //initHeader();
        //
        WebView wv = (WebView) view.findViewById(R.id.webView);
        wv.loadUrl("file:///android_asset/tos.html");
        /*TextView tv = (TextView) view.findViewById(R.id.tv);
        tv.setText(Html.fromHtml(JUtils.readContentFromAssetsFile(getActivity(), "tos.html")));*/
        //
        return view;
    }

   /* private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("TERMS OF SERVICES");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }*/
}
