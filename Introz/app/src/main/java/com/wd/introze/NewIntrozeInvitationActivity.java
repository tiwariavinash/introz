package com.wd.introze;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.activities.ActivityModel;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.feed.FeedModel;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.repository.server.rf.RFClient;
import com.wd.introze.repository.server.rf.RFService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by flair on 22-07-2016
 */
public class NewIntrozeInvitationActivity extends Fragment {
    //
    private SharedPreferences spUser;
    //
    private String introzeid;
    //
    private TextView tvAcceptedMessage;
    private LinearLayout llBottomButton;
    //
    private String typeByUser;
    //
    /*String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
*/
    //
    //
    /*String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;*/
    //
    //private AlertDialog adThanks;
    //
    private ActivityModel model;
    //
    private JSimpleBuilder builder;
    private AlertDialog ad;
    //
    private AlertDialog adReject;
    private CustomProgress pd;
    //
    private RFService rfService;

    //
    public static NewIntrozeInvitationActivity getInstance(String data) {
        Bundle bundle = new Bundle();
        bundle.putString("data", data);
        //
        NewIntrozeInvitationActivity fragment = new NewIntrozeInvitationActivity();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_new_introze_invitation, null);
        TypefaceHelper.typeface(view);
        //
        //tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        //tfText = Typeface.createFromAsset(getAssets(), fontPathText);
        rfService = RFClient.getClient().create(RFService.class);
        //
        //adThanks = new ThanksDialog(getActivity()).create();
        //adThanks.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
        //initHeader("NEW INTROZE INVITATION");
        //
        model = new Gson().fromJson(getArguments().getString("data", "{}"), ActivityModel.class);//getActivity().getIntent().getStringExtra("data"), ActivityModel.class);
        //
        typeByUser = model.getByFullName();
        //
        introzeid = model.introzId;
        //
        TextView tvUser = (TextView) view.findViewById(R.id.tvByUser);
        tvUser.setText(model.getByFullName());//model.byFirstName + " " + model.byLastName);
        tvUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProfile(v);
            }
        });
        //
        TextView tvRelName = (TextView) view.findViewById(R.id.tvRelName);
        tvRelName.setText(model.getRelFullName());
        tvRelName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoRelProfile(v);
            }
        });
        //
        ImageView ivRelationType = (ImageView) view.findViewById(R.id.ivRelationType);
        ivRelationType.setImageResource(JUtils.getRelationTypeIcon(model.relationType));
        //
        TextView tvRelationType = (TextView) view.findViewById(R.id.tvRelationType);
        tvRelationType.setText(JUtils.getRelationTypeText(model.relationType));
        //
        TextView tvByUser1 = (TextView) view.findViewById(R.id.tvByUser1);
        tvByUser1.setText(model.getByFullName());//model.byFirstName + " " + model.byLastName);
        tvByUser1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProfile(v);
            }
        });
        view.findViewById(R.id.flProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProfile(v);
            }
        });
        //
        TextView tvReason = (TextView) view.findViewById(R.id.tvReason);
        tvReason.setText(model.reason);
        //
        SimpleDraweeView ivProfile = (SimpleDraweeView) view.findViewById(R.id.ivProfile);
        final TextView tvShortName = (TextView) view.findViewById(R.id.tvShortName);
        //
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(model.getTwoUpperChar(ActivityModel.TypeUser.TYPE_REL_USER));
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(model.urlProfile).build();
        ivProfile.setController(controller);

        /*if (model.urlProfile == null) {
            ivProfile.setImageResource(R.drawable.circle_gray);
            tvShortName.setText(model.getTwoUpperChar(ActivityModel.TypeUser.TYPE_REL_USER));
        } else {
            Ion.with(ivProfile).load(model.urlProfile);
            tvShortName.setVisibility(View.GONE);
        }*/
        //
        SimpleDraweeView ivByUser = (SimpleDraweeView) view.findViewById(R.id.ivByUser);
        final TextView tvByShortName = (TextView) view.findViewById(R.id.tvByShortName);
        //
        //
        ControllerListener byControllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvByShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvByShortName.setText(model.getTwoUpperChar(ActivityModel.TypeUser.TYPE_BY_USER));
                tvByShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController byController = Fresco.newDraweeControllerBuilder().setControllerListener(byControllerListener)
                .setUri(model.urlByUser).build();
        ivByUser.setController(byController);

        /*if (model.urlByUser == null) {
            ivByUser.setImageResource(R.drawable.circle_gray);
            tvByShortName.setText(model.getTwoUpperChar(ActivityModel.TypeUser.TYPE_BY_USER));
        } else {
            Ion.with(ivProfile).load(model.urlByUser);
            tvByShortName.setVisibility(View.GONE);
        }*/
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);

        //
        tvAcceptedMessage = (TextView) view.findViewById(R.id.tvAcceptedMessage);
        llBottomButton = (LinearLayout) view.findViewById(R.id.llBottomButton);
        /*if (model.acceptedStatus == 1) {
            llBottomButton.setVisibility(View.GONE);
            tvAcceptedMessage.setVisibility(View.VISIBLE);
            tvAcceptedMessage.setTypeface(tfText);
        }*/
        setAcceptedStatus(model.acceptedStatus, model.isPersonOne);
        //
        builder = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });
        ad = builder.create();
        //ad.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        adReject = new RejectDialog(getActivity()).create();
        //adReject.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        view.findViewById(R.id.flRelProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoRelProfile(v);
            }
        });
        //
        view.findViewById(R.id.btnReject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickBtnReject(v);
            }
        });
        //
        view.findViewById(R.id.btnAccept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickBtnAccept(v);
            }
        });
        //
        return view;
    }

    //
    public void gotoRelProfile(View view) {
        /*if (TextUtils.isEmpty(model.relIdentifier)) {
            builder.setErrorMessage("Alert", "Sorry no user found with the details you provided.");
            ad.show();
            return;
        }*/
        Call<ResponseBody> call = rfService.getUserDetail(JUtils.getHeader(spUser), model.relUserId);
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        JSONObject joResult = new JSONObject(response.body().string());
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //
                            //
                            Intent intent = new Intent(getActivity(), FeedProfileActivity.class);

                            intent.putExtra("user_id", model.relUserId + "");
                            //
                            Log.d("ProfileUserId", "Rel User Id " + model.relUserId);
                            //

                            //
                            FeedModel model1 = new FeedModel(model.relUserId, model.relFirstName, model.relLastName, model.urlProfile,
                                    model.relIdentifier);
                            //
                            profileClickListener.onProfileClicked(model1);
                            //intent.putExtra("feed_model", new Gson().toJson(model1));
                            //startActivity(intent);
                            //
                        } else {
                            //
                            showAlertMessage("Alert", "Sorry no user found with the details you provided.");
                            /*builder.setErrorMessage("Alert", "Sorry no user found with the details you provided.");
                            ad.show();*/
                        }
                    } catch (JSONException e) {
                        //
                        showAlertMessage("Error", getString(R.string.error_no_response));
                       /* builder.setErrorMessage(getString(R.string.error_no_response));
                        ad.show();*/
                    } catch (IOException e) {
                        //
                        showAlertMessage("Error", getString(R.string.error_no_response));
                        /*builder.setErrorMessage(getString(R.string.error_no_response));
                        ad.show();*/
                    }
                } else {
                    //
                    showAlertMessage("Error", getString(R.string.error_no_response));
                    /*builder.setErrorMessage(getString(R.string.error_no_response));
                    ad.show();*/
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                //
                showAlertMessage("Error", getString(R.string.error_no_response));
                /*builder.setErrorMessage(getString(R.string.error_no_response));
                ad.show();*/
            }
        });
    }

    //
    private void showAlertMessage(String title, String message) {
        builder.setErrorMessage(getString(R.string.error_no_response));
        ad.show();
        TypefaceHelper.typeface(ad.getButton(DialogInterface.BUTTON_POSITIVE));
    }

    //
    public void gotoProfile(View view) {

        Intent intent = new Intent(getActivity(), FeedProfileActivity.class);

        intent.putExtra("user_id", model.byUserId + "");
        //
        Log.d("ProfileUserId", "User Id " + model.byUserId);
        //
        FeedModel model1 = new FeedModel(model.byUserId, model.byFirstName, model.byLastName, model.urlByUser, model.byIdentifier);
        //
        profileClickListener.onProfileClicked(model1);
        //intent.putExtra("feed_model", new Gson().toJson(model1));
        //startActivity(intent);
    }

    //
    private class RejectDialog extends AlertDialog.Builder implements DialogInterface.OnClickListener {

        public RejectDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_reject_confirm_new, null);
            TypefaceHelper.typeface(view);
            //
            setView(view);
            /*view.findViewById(R.id.btnYes).setOnClickListener(this);
            view.findViewById(R.id.btnNo).setOnClickListener(this);
            setView(view, 50, 0, 50, 0);*/
            //
            setPositiveButton("yes", this);
            setNegativeButton("no", this);
        }

        /*@Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnNo:
                    adReject.dismiss();
                    break;
                case R.id.btnYes:
                    onReject();
                    break;
            }
        }*/

        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    onReject();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    adReject.dismiss();
                    break;
            }
        }
    }
    //
    /*private class RejectDialog extends AlertDialog.Builder implements View.OnClickListener {

        public RejectDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_reject_confirm, null);
            TypefaceHelper.typeface(view);
            //
            view.findViewById(R.id.btnYes).setOnClickListener(this);
            view.findViewById(R.id.btnNo).setOnClickListener(this);
            setView(view, 50, 0, 50, 0);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnNo:
                    adReject.dismiss();
                    break;
                case R.id.btnYes:
                    onReject();
                    break;
            }
        }
    }*/

    //
    public void onClickBtnReject(View view) {
        adReject.show();
        TypefaceHelper.typeface(adReject.getButton(DialogInterface.BUTTON_POSITIVE));
        TypefaceHelper.typeface(adReject.getButton(DialogInterface.BUTTON_NEGATIVE));
    }

    private void onReject() {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("PUT", AppUrls.MAIN + "/reject_introze/" + introzeid).setHeader("Authorization", "Basic " + basic).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.d("RejectResult", result + "");
                        if (e == null) {
                            try {
                                JSONObject joResult = new JSONObject(result);
                                if (TextUtils.equals("success", joResult.getString("result"))) {
                                    //if (isForReject)
                                    adReject.dismiss();
                                    getActivity().onBackPressed();
                                    //Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                                    //finish();
                                } else {
                                    showMessage(joResult.getString("message"));
                                    //Toast.makeText(NewIntrozeInvitationActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                Toast.makeText(getActivity(), "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    //
    private void showMessage(String message) {
        builder.setErrorMessage(message);
        ad.show();
    }

    //
    public void onClickBtnAccept(View view) {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        Ion.with(this).load("PUT", AppUrls.MAIN + "/accept_introze/" + introzeid).setHeader("Authorization", "Basic " + basic)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("AcceptResult", result + "");
                pd.dismiss();
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //Toast.makeText(NewIntrozeInvitationActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            //
                            if (joResult.getInt("accepted_status") == 1) {
                                /*Intent intent = new Intent(getActivity(), MatchDoneActivity.class);
                                intent.putExtra(ActivityModel.class.getName(), new Gson().toJson(model));
                                intent.putExtra("by_full_name", typeByUser).putExtra("rel_full_name", model.getRelFullName());
                                getActivity().startActivityForResult(intent, 3001);
                                getActivity().onBackPressed();*/
                                ((HomeActivity) getActivity()).onGotoMatched(typeByUser, model.getRelFullName(), new Gson().toJson(model));
                                //finish();
                            } else {
                                setAcceptedStatus(joResult.getInt("accepted_status"), model.isPersonOne);
                                    /*llBottomButton.setVisibility(View.GONE);
                                    tvAcceptedMessage.setVisibility(View.VISIBLE);*/
                                //adThanks.show();
                            }
                            //finish();
                        } else {
                            showMessage(joResult.getString("message"));
                            //Toast.makeText(NewIntrozeInvitationActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e1) {
                        showMessage(getString(R.string.error_no_response));
                        //Toast.makeText(NewIntrozeInvitationActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showMessage(getString(R.string.error_no_response));
                    //Toast.makeText(NewIntrozeInvitationActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /*private void initHeader(String text) {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText(text);
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }*/

    //
    private class ThanksDialog extends AlertDialog.Builder implements View.OnClickListener {

        public ThanksDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_send_introze_accept_new, null);
            setView(view, 50, 0, 50, 0);
            //
            view.findViewById(R.id.btnYay).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //adThanks.dismiss();

        }
    }
    //backup
    /*private class ThanksDialog extends AlertDialog.Builder implements View.OnClickListener {

        public ThanksDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_send_introze_accept, null);
            setView(view, 50, 0, 50, 0);
            //
            view.findViewById(R.id.btnYay).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            adThanks.dismiss();

        }
    }*/
    //backup

    //
    private void setAcceptedStatus(int acceptedStatus, boolean isPersonOne) {
        switch (acceptedStatus) {
            case AcceptedStatus.PENDING:
            case AcceptedStatus.PERSON_1_REJECTED:
            case AcceptedStatus.PERSON_2_REJECTED:
                // message visibility Gone. buttons visibility Visible
                return;

            case AcceptedStatus.PERSON_1_ACCEPTED:
                if (isPersonOne) {
                    tvAcceptedMessage.setVisibility(View.VISIBLE);
                    llBottomButton.setVisibility(View.GONE);
                } else {
                    tvAcceptedMessage.setVisibility(View.GONE);
                    llBottomButton.setVisibility(View.VISIBLE);
                }
                return;

            case AcceptedStatus.PERSON_2_ACCEPTED:
                if (!isPersonOne) {
                    tvAcceptedMessage.setVisibility(View.VISIBLE);
                    llBottomButton.setVisibility(View.GONE);
                } else {
                    tvAcceptedMessage.setVisibility(View.GONE);
                    llBottomButton.setVisibility(View.VISIBLE);
                }
                return;

            case AcceptedStatus.BOTH_ACCEPTED:
                tvAcceptedMessage.setVisibility(View.GONE);
                llBottomButton.setVisibility(View.GONE);
                return;

            default:
                if (isPersonOne) {
                    if (model.user_1_status == 1) {
                        tvAcceptedMessage.setVisibility(View.VISIBLE);
                        llBottomButton.setVisibility(View.GONE);
                        break;
                    }
                } else if (!isPersonOne) {
                    if (model.user_2_status == 1) {
                        tvAcceptedMessage.setVisibility(View.VISIBLE);
                        llBottomButton.setVisibility(View.GONE);
                        break;
                    }
                }
                tvAcceptedMessage.setVisibility(View.GONE);
                llBottomButton.setVisibility(View.GONE);
        }// switch
    }

    //
    private interface AcceptedStatus {
        int PENDING = 0;
        //
        int BOTH_ACCEPTED = 1;
        //
        int PERSON_1_ACCEPTED = 2;
        int PERSON_2_ACCEPTED = 3;
        //
        int PERSON_1_REJECTED = 4;
        int PERSON_2_REJECTED = 5;
        //
        int BOTH_REJECTED = 6;
    }

    //
    private FeedProfileActivity.OnProfileClickListener profileClickListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        profileClickListener = (FeedProfileActivity.OnProfileClickListener) context;
    }
}
