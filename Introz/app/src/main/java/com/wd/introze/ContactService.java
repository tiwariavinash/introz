package com.wd.introze;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.wd.introze.mysyncadapter.SyncUtils;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by flair on 30-07-2016
 */
public class ContactService extends Service {


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //
        Timer timer = new Timer("introze_sync");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                SyncUtils.CreateSyncAccount(ContactService.this);
            }
        }, new Date(), 1000 * 120);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }
}