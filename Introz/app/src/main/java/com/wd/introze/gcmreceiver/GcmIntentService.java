/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wd.introze.gcmreceiver;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.core.app.NotificationCompat;
import android.text.Html;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.wd.introze.R;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JUtils;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {
    public static int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;


    public GcmIntentService() {
        super("GcmIntentService");
    }

    public static final String TAG = "GcmIntentService";


    @Override
    protected void onHandleIntent(Intent intent) {

        Log.i(TAG, "============$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                // sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                // sendNotification("Deleted messages on server: " + extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                Log.i(TAG, "Notification ... ");
               /* for (int i = 0; i < 5; i++) {
                    Log.i(TAG, "Working... " + (i + 1)
                            + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                    }
                }*/
                Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.

                sendNotification(extras);
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }


    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(Bundle bundle) {

        String from, title, message = "", category = null;
        int relId = 0;
        Log.i(TAG, " Notification Bundle " + bundle);
        // database = new MyDatabaseHandler(getApplicationContext());
        if (bundle != null) {
            from = bundle.getString("from");
            title = bundle.getString("title");
            message = bundle.getString("message");
            //   database.addNotificationMessage(new NotificationBean(from,title,message));
            try {
                JSONObject joCustom = new JSONObject(bundle.getString("custom"));
                if (joCustom.has("thank_you_id")) relId = joCustom.getInt("thank_you_id");
                else if (joCustom.has("rel_id")) relId = joCustom.getInt("rel_id");
                category = joCustom.getString("category");
            } catch (JSONException e) {
                Log.d(TAG, "JsonException " + e.getMessage());
            }
            //relId =joCustom.getIn
        }
        //
        /*if (TextUtils.equals("NEW_INTROZE_INVITATION", category) || TextUtils.equals("INTROZE_ACCEPTED", category)) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("from_notification", category);
            startActivity(intent);
        } *//*else if (TextUtils.equals("INTROZE_ACCEPTED", category)) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.putExtra("from_notification", category);
            startActivity(intent);
        }*//* else if (TextUtils.equals("INTROZE_CHAT", category)) {

        } else if (TextUtils.equals("THANK_YOU_RECEIVED", category)) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.putExtra("from_notification", category);
            startActivity(intent);
        }*/
        //

        //

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Intent notificationIntent = new Intent();
        if (JUtils.getAppStatus(this)) {
            Intent intent = new Intent("push_messages_dialog");
            intent.putExtra("message", message);
            //
            intent.putExtra("from_notification", category);
            intent.putExtra("rel_id", relId);
            //
            sendBroadcast(intent);
            return;
        }
        Intent notificationIntent = new Intent(this, HomeActivity.class);

        Log.d(TAG, "My new category" + category);
        notificationIntent.putExtra("from_notification", category);
        notificationIntent.putExtra("rel_id", relId);
        //notificationIntent.putExtra("msg", "" + message);

        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                null;
        try {
            mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Introze")
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(Html.fromHtml(message)))
                    .setContentText(Html.fromHtml(message))
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setLights(Color.RED, 3000, 3000)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setAutoCancel(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        try {
//            Uri notification = RingtoneManager
//                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
//                    notification);
//            r.play();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        //Ton
        // mBuilder.setSound(Uri.parse("uri://sadfasdfasdf.mp3"));
        mBuilder.setContentIntent(pendingNotificationIntent);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        if (NOTIFICATION_ID > 100) {
            NOTIFICATION_ID = 1;
        }
        NOTIFICATION_ID++;
    }

}
