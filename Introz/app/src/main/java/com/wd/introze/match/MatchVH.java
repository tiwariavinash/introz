package com.wd.introze.match;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;

/**
 * Created by flair on 29-07-2016
 */
public class MatchVH extends RecyclerView.ViewHolder implements View.OnClickListener {
    //
    TextView tvFullName, tvShortName;
    SimpleDraweeView iv;
    LinearLayout llCircleRed;

    public MatchVH(View itemView) {
        super(itemView);
        //
        TypefaceHelper.typeface(itemView);
        //
        tvShortName = (TextView) itemView.findViewById(R.id.tvShortName);
        tvFullName = (TextView) itemView.findViewById(R.id.tvFullName);
        llCircleRed = (LinearLayout) itemView.findViewById(R.id.llCircleRed);
        //
        iv = (SimpleDraweeView) itemView.findViewById(R.id.iv);
        //
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        MatchAdapter.listener.onMatchClicked(getAdapterPosition());
    }
}
