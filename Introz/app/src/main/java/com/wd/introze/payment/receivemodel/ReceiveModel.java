
package com.wd.introze.payment.receivemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class ReceiveModel {

    @SerializedName("result")
    @Expose
    public String result;
    @SerializedName("thank_you_data")
    @Expose
    public List<ThankYouDatum> thankYouData = new ArrayList<ThankYouDatum>();

}
