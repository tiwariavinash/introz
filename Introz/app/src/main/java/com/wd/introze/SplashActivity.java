package com.wd.introze;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.facebook.login.LoginManager;
import com.wd.introze.Introduction.IntroActivity;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JUser;
import com.wd.introze.services.InsertIntoDBService;

/**
 * Created by admin on 2016-07-01
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!isMyServiceRunning(ContactService.class)) {
                Intent intent = new Intent(this, ContactService.class);
                startService(intent);
            }
        }*/
        //
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences spUser = getSharedPreferences("user", MODE_PRIVATE);
                //
                if (JUser.isVerified(SplashActivity.this)) {
                    startActivity(new Intent(SplashActivity.this, PhoneVerificationActivity.class));
                } else if (spUser.getBoolean("is_verified", false)) {
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                } else if (spUser.getBoolean("is_first_time", true)) {
                    //
                    Log.d("insertIntoDB "," isFirstTimeTrue ");
                    if (!spUser.getBoolean("is_contact_sync_db", false))
                        insertIntoDBFirstTime();
                    //SyncUtils.CreateSyncAccount(SplashActivity.this);
                    spUser.edit().putBoolean("is_first_time", false).commit();
                    LoginManager.getInstance().logOut();
                    startActivity(new Intent(SplashActivity.this, IntroActivity.class));
                } else {
                    //
                    Log.d("insertIntoDB "," isFirstTimeFalse ");
                    if (!spUser.getBoolean("is_contact_sync_db", false))
                        insertIntoDBFirstTime();
                    //SyncUtils.CreateSyncAccount(SplashActivity.this);
                    LoginManager.getInstance().logOut();
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }
                finish();
            }
        }, 3000);
    }

    //
    private void insertIntoDBFirstTime() {
        Intent i = new Intent(this, InsertIntoDBService.class);
        startService(i);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
