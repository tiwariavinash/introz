
package com.wd.introze.sync;

import java.util.ArrayList;
import java.util.List;

public class Update {

    public List<String> email = new ArrayList<String>();
    public String first_name;
    public String identifier;
    public String last_name;
    public int contact_id;
    public List<String> phone = new ArrayList<String>();

}
