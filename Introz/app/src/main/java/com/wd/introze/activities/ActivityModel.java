package com.wd.introze.activities;


import com.wd.introze.R;
import com.wd.introze.jutil.JUtils;

/**
 * Created by flair on 22-07-2016.
 */
public class ActivityModel {
    //
    public int user_1_status;
    public int user_2_status;
    //
    public boolean isPersonOne;
    //
    public String byFirstName;
    public String byLastName;
    public int byUserId;
    public String byFBId;
    public String byIdentifier;

    //
    public int relationType;

    //
    public String relFirstName;
    public String relLastName;
    public int relUserId;
    public int relId;
    public String rel_fb_id;
    public String relIdentifier;
    //
    public String reason;
    //
    public String introzId;
    //
    public String urlProfile;
    public String urlByUser;

    public int acceptedStatus;
    //
    public String createdOn;

    public ActivityModel(String introzId, String byFirstName, String byLastName, int relationType, String relFirstName,
                         String relLastName, String reason, String urlProfile, String urlByUser, int acceptedStatus, int person,
                         String relFbId, String byFbId, int user_1_status, int user_2_status, int byUserId, int relUserId,
                         int relId, String createdOn, String relIdentifier, String byIdentifier) {
        this.introzId = introzId;
        this.byFirstName = byFirstName;
        this.byLastName = byLastName;
        this.relationType = relationType;
        this.relFirstName = relFirstName;
        this.relLastName = relLastName;
        this.reason = reason;
        //this.urlProfile = AppUrls.MAIN + "/" + urlProfile;
        //this.urlByUser = AppUrls.MAIN + "/" + urlByUser;
        this.acceptedStatus = acceptedStatus;
        isPersonOne = person == 1 ? true : false;
        this.rel_fb_id = relFbId;
        //if (TextUtils.isEmpty(urlProfile) && !TextUtils.equals("0", relFbId)) {
        this.urlProfile = JUtils.getImageUrl(urlProfile, relFbId); //getFacebookProfilePictureUrl(relFbId);
        //}
        this.byFBId = byFbId;
        //if (TextUtils.isEmpty(urlByUser) && !TextUtils.equals("0", byFbId)) {
        this.urlByUser = JUtils.getImageUrl(urlByUser, byFbId); //JUtils.getFacebookProfilePictureUrl(byFbId);
        //}
        //
        this.user_1_status = user_1_status;
        this.user_2_status = user_2_status;
        //
        this.byUserId = byUserId;
        //
        this.relUserId = relUserId;
        this.relId = relId;
        this.createdOn = createdOn;
        //
        this.relIdentifier = relIdentifier;
        this.byIdentifier = byIdentifier;
    }

    //
    //
    public int getRelationTypeIcon() {
        int relation;
        switch (relationType) {
            case 0:
                relation = R.drawable.ic_business_1;
                break;
            case 1:
                relation = R.drawable.ic_dating_round;
                break;
            case 2:
                relation = R.drawable.ic_friendship_round;
                break;
            default:
                relation = R.drawable.ic_mutual_round;
        }
        return relation;
    }

    //
    public String getRelationTypeText() {
        String relation;
        switch (relationType) {
            case 0:
                relation = "Business";
                break;
            case 1:
                relation = "Dating";
                break;
            case 2:
                relation = "Friendship";
                break;
            default:
                relation = "Mutual Interest";
        }
        return relation;
    }

    //
    public String getTwoUpperChar(int typeUser) {

        String name = null;
        switch (typeUser) {
            case TypeUser.TYPE_REL_USER:
                name = relFirstName + " " + relLastName;
                break;
            case TypeUser.TYPE_BY_USER:
                name = byFirstName + " " + byLastName;
                break;
        }

        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }

    //
    public String getByFullName() {
        return byFirstName + " " + byLastName;
    }

    //
    public String getRelFullName() {
        return relFirstName + " " + relLastName;
    }

    //
    public interface TypeUser {
        int TYPE_BY_USER = 0;
        int TYPE_REL_USER = 1;
    }
}
