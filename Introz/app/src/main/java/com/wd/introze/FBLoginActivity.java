package com.wd.introze;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JSimpleBuilder;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by flair on 09-08-2016.
 */
public class FBLoginActivity extends AppCompatActivity {
    //
    public LoginButton loginButton;
    //
    public CustomProgress customProgress1;
    //
    //
    private SharedPreferences.Editor editorUser;
    private SharedPreferences spUser;
    //
    public boolean isVerified;
    //
    private AlertDialog ad;
    private JSimpleBuilder builder;
    /*
    facebook
     */
    public CallbackManager callbackManager;

    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    public FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(final LoginResult loginResult) {
            //
            if (Profile.getCurrentProfile() == null) {
                profileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                        //
                        getEmailFromFacebook(loginResult, profile2);
                        // profile2 is the new profile
                        Log.v("facebook - profile", profile2.getFirstName());
                        //Toast.makeText(MainActivity.this, "Profile2 " + profile2.getName(), Toast.LENGTH_SHORT).show();
                        //onFacebookLogin(profile2.getId(), profile2.getFirstName(), profile2.getLastName(), "");
                        profileTracker.stopTracking();
                    }
                };
                // no need to call startTracking() on mProfileTracker
                // because it is called by its constructor, internally.
            } else {
                Profile profile = Profile.getCurrentProfile();
                getEmailFromFacebook(loginResult, profile);
                //Toast.makeText(MainActivity.this, "else " + profile.getName(), Toast.LENGTH_SHORT).show();
                Log.v("facebook - profile", profile.getFirstName());
            }
            /*AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            Log.d("FacebookDetail", profile + "");
            //displayMessage(profile);
            if (profile != null)
                Toast.makeText(MainActivity.this, "Facebook login successful " + profile.getName(), Toast.LENGTH_SHORT).show();*/
        }

        @Override
        public void onCancel() {
            customProgress1.dismiss();
            builder.setErrorMessage("Facebook login canceled by User");
            ad.show();
        }

        @Override
        public void onError(FacebookException e) {
            customProgress1.dismiss();
            builder.setErrorMessage("" + e.getMessage());
            ad.show();
        }
    };

    //
    private void getEmailFromFacebook(LoginResult loginResult, final Profile profile) {
        AccessToken accessToken = loginResult.getAccessToken();
        //
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                String email = "";
                try {
                    email = object.getString("email");
                } catch (JSONException e) {

                }
                //
                Log.d("FacebookEmail", "" + email);
                //
                onFacebookLogin(profile.getId(), profile.getFirstName(), profile.getLastName(), email);
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
        request.setParameters(parameters);
        request.executeAsync();
    }

    /*
    facebook
     */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        spUser = getSharedPreferences("user", MODE_PRIVATE);
        editorUser = spUser.edit();
        /*
        facebook
         */
        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                //displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();
        /*
        facebook
         */
        builder = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
            }
        });
        //
        ad = builder.create();
        ad.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    /*//
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 3000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    registerDevice();
                } else {
                    afterLoginSuccessStartActivity();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }*/

    //


    //
    /*public void registerDevice() {
        //
        String device_os = System.getProperty("os.version");
        String device_model = "" + android.os.Build.MODEL;
        //
        TelephonyManager telephonyManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        String tmDevice, tmSerial, androidId;
        tmDevice = "" + telephonyManager.getDeviceId();
        tmSerial = "" + telephonyManager.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        //
        String deviceType = "Android";
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/device").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("device_unique_id", deviceId).setBodyParameter("device_type", deviceType)
                .setBodyParameter("device_model", device_model).setBodyParameter("device_os", device_os)
                .setBodyParameter("device_token", deviceToken).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //
                //
                *//*Intent intent = new Intent(FBLoginActivity.this, PhoneVerificationActivity.class);
                startActivity(intent);
                finish();*//*
                afterLoginSuccessStartActivity();
                //Log.d("DeviceRegistration", result + "");
                *//*if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                    } catch (JSONException e1) {

                    }
                } else Toast.makeText(LoginActivity.this, "" + e, Toast.LENGTH_SHORT).show();*//*
            }
        });
    }*/

    /*//
    public void afterFacebookLoginSuccess() {
//
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_PHONE_STATE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3000);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        } else {
            registerDevice();
        }
        //
    }
    //*/

    //
    public void afterLoginSuccessStartActivity() {
        //
        if (isVerified) {
            //Toast.makeText(LoginActivity.this, "phone is not verified", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(FBLoginActivity.this, HomeActivity.class));
            finish();
        } else {
            Intent intent = new Intent(FBLoginActivity.this, PhoneVerificationActivity.class);
            startActivity(intent);
            finish();
        }
        //
    }

    public void onFacebookLogin(String id, String fn, String ln, String email1) {
        final String firstName = fn;
        //last name
        final String lastName = ln;
        //email
        final String email = TextUtils.isEmpty(email1) ? fn.toLowerCase() + "" + ln.toLowerCase() + "@gmail.com" : email1;
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/user").setBodyParameter("first_name", firstName)
                .setBodyParameter("last_name", lastName).setBodyParameter("email", email.replace(" ", ""))
                .setBodyParameter("facebook_id", id).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //pd.dismiss();
                Log.d("SignUpResult", result + "");
                customProgress1.dismiss();
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            JSONObject joUser = joResult.getJSONObject("user");
                            //
                            isVerified = joUser.optInt("mobile_verified") == 1 ? true : false;
                            //
                            editorUser.putString("user_id", joUser.getString("user_id"))
                                    .putString("user_token", joUser.getString("user_token"));
                            /*if (joUser.has("chat_id"))
                                editorUser.putInt("chat_id", joUser.getInt("chat_id"));*/
                            editorUser.commit();
                            //
                            afterLoginSuccessStartActivity();
                            //
                        } else {
                            LoginManager.getInstance().logOut();
                            //JSONObject joMessage = joResult.getJSONObject("message");
                            //if (joMessage.has("email"))
                            //Toast.makeText(FBLoginActivity.this, "" + joMessage.getJSONArray("email").getString(0), Toast.LENGTH_SHORT).show();
                            // else if (joMessage.has("first_name"))
                            //Toast.makeText(FBLoginActivity.this, "" + joMessage.getJSONArray("first_name").getString(0), Toast.LENGTH_SHORT).show();
                            //else
                            //Toast.makeText(FBLoginActivity.this, "" + joResult.getString("error_type"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e1) {
                        LoginManager.getInstance().logOut();
                        //Toast.makeText(FBLoginActivity.this, "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    LoginManager.getInstance().logOut();
                    //Toast.makeText(FBLoginActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    //
    public void onClickFB(View view) {
        //
        Log.d("Internet", "FBClicked " + JConnectionUtils.isConnectedToInternet(this));
        if (!JConnectionUtils.isConnectedToInternet(this)) {
            builder.setErrorMessage("Please check your Network and try again later");
            ad.show();
            return;
        }
        //
        loginButton.performClick();
        customProgress1 = CustomProgress.show(this, "", false, false, null);
    }
}
