package com.wd.introze.match;

import com.wd.introze.jutil.JUtils;

/**
 * Created by flair on 29-07-2016.
 */
public class MatchModel {
    //
    public int count;
    //
    public int rel_user_id;
    //
    public int rel_id;
    //
    public boolean isRead;
    //
    public String byName;
    public int byUserId;
    public String urlBy;
    public String byIdentifier;
    //
    public String name;
    public String firstName;
    //
    public String url;
    public int chatId;
    public String createOn;
    //
    public int relationType;
    //
    public String reason;
    //
    public String relIdentifier;
    //
    public int user1ChatId;
    public int user2ChatId;

    public MatchModel() {
    }

    public MatchModel(String name, String firstName, String url, int chatId, String byName, String createOn, int relationType,
                      int readFlag,
                      int rel_id, int rel_user_id, int byUserId, String reason, String urlBy, String byFbId,
                      String relIdentifier, String byIdentifier, String relFbId, int user1ChatId, int user2ChatId) {
        this.firstName = firstName;
        this.name = name;
        this.url = JUtils.getImageUrl(url, relFbId);
        this.chatId = chatId;
        this.byName = byName;
        this.createOn = createOn;
        this.relationType = relationType;
        this.isRead = readFlag == 1 ? true : false;
        this.rel_id = rel_id;
        this.rel_user_id = rel_user_id;
        //
        this.byUserId = byUserId;
        //
        this.reason = reason;
        //
        this.urlBy = JUtils.getImageUrl(urlBy, byFbId);
        //
        this.relIdentifier = relIdentifier;
        this.byIdentifier = byIdentifier;
        //
        this.user1ChatId = user1ChatId;
        this.user2ChatId = user2ChatId;
    }

    //
    public String getTwoUpperChar() {
        if (name == null) return "";
        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }
}
