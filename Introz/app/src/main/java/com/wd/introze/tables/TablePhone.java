package com.wd.introze.tables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by flair on 26-07-2016.
 */
@Table(name = "TablePhone")
public class TablePhone extends Model{
    @Column(name = "Phone")
    public String phone;
    @Column(name = "Contacts")
    public TableContacts contacts;
}
