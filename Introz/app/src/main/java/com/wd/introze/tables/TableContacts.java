package com.wd.introze.tables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by flair on 26-07-2016
 */
@Table(name = "TableContacts")
public class TableContacts extends Model {
    //
    @Column(name = "Identifier")
    public long identifier;
    //
    @Column(name = "FirstName")
    public String firstName;
    @Column(name = "LastName")
    public String lastName;
    @Column(name = "IsIntroze")
    public boolean isIntroze;
    //
    @Column(name = "ChatId")
    public Integer chatId;
    //
    @Column(name = "ContactId")
    public int contactId;
    @Column(name = "IntrozeUserId")
    public int introzeUserId;
    @Column(name = "IsMatched")
    public boolean isMatched;
    @Column(name = "UrlPic")
    public String urlPic;
}
