package com.wd.introze.jutil;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;

/**
 * Created by flair on 27-08-2016
 */
//
public class JSimpleBuilder extends AlertDialog.Builder implements View.OnClickListener, DialogInterface.OnClickListener {
    //
    private TextView tvMessage, tvTitle;
    //
    private View.OnClickListener listener;

    //
    public JSimpleBuilder(Context context, View.OnClickListener onClickListener) {
        super(context);
        listener = onClickListener;
        //
        View view = LayoutInflater.from(context).inflate(R.layout.dilaog_ed_validation_new, null);
        TypefaceHelper.typeface(view);
        //
        tvMessage = (TextView) view.findViewById(R.id.tvEdValidation);
        //
        /*View viewTitle = LayoutInflater.from(context).inflate(R.layout.dialog_tv_title, null);
        tvTitle = (TextView) viewTitle.findViewById(R.id.tvTitle);
        tvTitle.setText("Error");
        TypefaceHelper.typeface(viewTitle);
        setCustomTitle(viewTitle);*/
        //
        setView(view);
        //
        setPositiveButton("ok", this);
    }

    //
    @Override
    public void onClick(View v) {
        listener.onClick(v);
    }

    //
    public void setErrorMessage(String title, String message) {
        //tvTitle.setText(title);
        tvMessage.setText(message);
    }

    //
    public void setErrorMessage(String message) {
        tvMessage.setText(message);
    }


    //backup
/*
    //
    public JSimpleBuilder(Context context, View.OnClickListener onClickListener) {
        super(context);
        listener = onClickListener;
        //
        View view = LayoutInflater.from(context).inflate(R.layout.dilaog_ed_validation_new, null);
        TypefaceHelper.typeface(view);
        //
        tvMessage = (TextView) view.findViewById(R.id.tvEdValidation);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        //
        setView(view, 50, 0, 50, 0);
        //
        view.findViewById(R.id.btnYay).setOnClickListener(this);
        //
    }

    @Override
    public void onClick(View v) {
        listener.onClick(v);
    }

    //
    public void setErrorMessage(String title, String message) {
        tvTitle.setText(title);
        tvMessage.setText(message);
    }

    //
    public void setErrorMessage(String message) {
        tvMessage.setText(message);
    }
*/
//backup
    @Override
    public void onClick(DialogInterface dialog, int which) {
        listener.onClick(null);
    }
}
