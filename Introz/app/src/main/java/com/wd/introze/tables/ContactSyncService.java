package com.wd.introze.tables;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.IBinder;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.wd.introze.AppUrls;
import com.wd.introze.sync.Add;
import com.wd.introze.sync.Contacts;
import com.wd.introze.sync.Delete;
import com.wd.introze.sync.SyncModel;
import com.wd.introze.sync.Update;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flair on 30-07-2016
 */
public class ContactSyncService extends Service {
    //
    private SharedPreferences spUser;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("ContactSyncStarted", "ContactSyncStarted");
        Toast.makeText(ContactSyncService.this, "ContactSyncServiceStarted", Toast.LENGTH_SHORT).show();
        //
        spUser = getSharedPreferences("user", MODE_PRIVATE);
        syncContact();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    //
    private void syncContact() {
        //
        //
        Contacts contacts = new Contacts();
        //
        List<Add> adds = contacts.add;
        List<Update> update = contacts.update;
        List<Delete> delete = contacts.delete;
        //
        SyncModel model = new SyncModel();
        model.contacts = contacts;
        //
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            int i = 0;
            while (cur.moveToNext()) {
                //
                //
                Add add = new Add();
                List<String> email = add.email;
                List<String> phone = add.phone;
                //
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                //
                add.identifier = id;
                //
                String name = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                //
                add.first_name = name;
                add.last_name = name;
                //
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //Query phone here.  Covered next
                    getPhoneNumber(cur, id, phone);
                }
                //
                getEmail(cur, id, email);
                //
                adds.add(add);
            }
            //
            Log.d("GsonOutput", new Gson().toJson(model));
        }
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(new Gson().toJson(model));
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/contact_sync2").setTimeout(120000).setHeader("Authorization", "Basic " + basic)
                .setHeader("Accept", "application/json").setHeader("Content-type", "application/json")
                .setJsonObjectBody(jo).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {

                Log.d("HomeResult", result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            Toast.makeText(ContactSyncService.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            //
                            getIntrozeUsers();
                        } else {
                            if (joResult.has("message")) {
                                Toast.makeText(ContactSyncService.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            } else if (joResult.has("errorType")) {
                                Toast.makeText(ContactSyncService.this, "" + joResult.getString("errorType"), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (JSONException e1) {
                        Toast.makeText(ContactSyncService.this, "" + e1, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("HomeError", e + "");
                    Toast.makeText(ContactSyncService.this, "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private void getIntrozeUsers() {
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/get_introze_users").setHeader("Authorization", "Basic " + basic).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("iusers", result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            JSONArray joData = joResult.getJSONArray("data");
                            //
                            ArrayList<TableContacts> ids = new ArrayList<>();

                            for (int i = 0; i < joData.length(); i++) {
                                TableContacts c = new TableContacts();
                                JSONObject ii = joData.getJSONObject(i);
                                c.identifier = Long.parseLong(ii.getString("identifier"));
                                c.contactId = ii.getInt("contact_id");
                                c.introzeUserId = ii.getInt("introze_user_id");
                                //
                                ids.add(c);
                                //ids.add(joData.getJSONObject(i).getString("identifier"));
                            }
                            //
                            syncContact2(ids);
                        } else {
                        }
                    } catch (JSONException e1) {
                        Toast.makeText(ContactSyncService.this, "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ContactSyncService.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void syncContact2(ArrayList<TableContacts> ids) {
        //
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                //
                TableContacts contacts = new TableContacts();
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                contacts.identifier = Long.parseLong(id);
                //
                boolean contains = false;
                for (TableContacts con : ids) {
                    if (TextUtils.equals(con.identifier + "", id)) {
                        contains = true;
                        contacts.contactId = con.contactId;
                        contacts.introzeUserId = con.introzeUserId;
                        break;
                    }
                }
                //
                if (contains) {
                    contacts.isIntroze = true;
                } else {
                    contacts.isIntroze = false;
                }
                //
                String name = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                //
                contacts.firstName = name;
                contacts.lastName = name;
                contacts.save();
                //
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //Query phone here.  Covered next
                    getPhoneNumber2(cur, id, contacts);
                }
                //
                getEmail2(id, contacts);
            }
        }
        spUser.edit().putBoolean("is_contact_sync", true).commit();
        Toast.makeText(ContactSyncService.this, "Contact Synced", Toast.LENGTH_SHORT).show();
        stopSelf();
    }

    //
    private void getEmail2(String id, TableContacts contacts) {
        Cursor emailCur = getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{id}, null);
        while (emailCur.moveToNext()) {
            // This would allow you get several email addresses
            // if the email addresses were stored in an array
            String email = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            //
            TableEmail tableEmail = new TableEmail();
            tableEmail.email = email;
            tableEmail.contacts = contacts;
            tableEmail.save();
            /*String emailType = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));*/
        }
        emailCur.close();
    }


    //
    private void getPhoneNumber2(Cursor cur, String id, TableContacts contacts) {
        if (Integer.parseInt(cur.getString(
                cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
            Cursor pCur = getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                    new String[]{id}, null);
            while (pCur.moveToNext()) {
                // Do something with phones
                TablePhone tablePhone = new TablePhone();
                tablePhone.phone = pCur.getString(pCur.getColumnIndex("data1"));
                tablePhone.contacts = contacts;
                //
                tablePhone.save();
                //Log.d("PhoneNumber", pCur.getString(pCur.getColumnIndex("data1")) + "");
            }
            pCur.close();
        }
    }

    //
    private void getPhoneNumber(Cursor cur, String id, List<String> phone) {
        if (Integer.parseInt(cur.getString(
                cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
            Cursor pCur = getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                    new String[]{id}, null);
            while (pCur.moveToNext()) {
                // Do something with phones
                phone.add(pCur.getString(pCur.getColumnIndex("data1")));
                //
                //Log.d("PhoneNumber", pCur.getString(pCur.getColumnIndex("data1")) + "");
            }
            pCur.close();
        }
    }

    //
    private void getEmail(Cursor cur, String id, List<String> alEmail) {
        Cursor emailCur = getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{id}, null);
        while (emailCur.moveToNext()) {
            // This would allow you get several email addresses
            // if the email addresses were stored in an array
            String email = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            //
            alEmail.add(email);
            /*String emailType = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));*/
        }
        emailCur.close();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
