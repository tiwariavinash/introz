package com.wd.introze;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.jutil.PhoneNumberTextWatcher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 2016-07-14
 */
public class PhoneVerificationActivity extends AppCompatActivity {
    //
    private AlertDialog adThanks;
    //
    private SharedPreferences spUser;
    //
    private EditText etMobile;
    private TextView tvError;
    //
    private ProgressDialog pd;
    //
    String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    TextView tvPhoneVerText;
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    private AlertDialog invalidPhoneDialog;
    //
    TextView tvTextInvalidPhone;
    //
    CustomProgress customProgress;
    //
    private String mobile;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private boolean isFromSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);
        //
        TypefaceHelper.typeface(this);
        //
        initHeader();
        //
        tfText = Typeface.createFromAsset(PhoneVerificationActivity.this.getAssets(), fontPathText);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        invalidPhoneDialog = new InvalidPhoneDialog(this).create();
        invalidPhoneDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Mobile Number");*/
        //
        spUser = getSharedPreferences("user", MODE_PRIVATE);
        //
        adThanks = new ThanksDialog(this).create();
        //adThanks.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
        tvError = (TextView) findViewById(R.id.tvError);
        //
        etMobile = (EditText) findViewById(R.id.etMobile);
        etMobile.addTextChangedListener(new PhoneNumberTextWatcher(etMobile, tvError));
        //new JPhoneNumberTextWatcher(tvError, "Mobile Number must be 10 digits long"));//new JErrorTextWatcher(tvError, "Please enter mobile number"));
        //
        /*if (etMobile.getText().toString().length() != 10) {
            etMobile.addTextChangedListener(new JErrorTextWatcher(tvError, "Mobile Number must be 10 digits long"));
        }*/
        //
        pd = new ProgressDialog(this);

        tvPhoneVerText = (TextView) findViewById(R.id.tvPhoneVerText);
        tvPhoneVerText.setTypeface(tfText);
        //
        builderError = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
                if (isFromSuccess) {
                    Intent intent = new Intent(PhoneVerificationActivity.this, OtpActivity.class);
                    intent.putExtra("mobile", mobile);
                    startActivity(intent);
                    finish();
                }
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    //
    private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("MOBILE PHONE NUMBER");
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);

    }

    //
    private String formatPhone(String phone) {
        String temp = JUtils.getNumeric(phone);
        if (temp.length() > 10) {
            temp = temp.substring(temp.length() - 10);
        }
        return temp;
    }

    //
    public void onClickBtnAddNumber(View view) {
        //
        JUtils.hideSoftKeyboard(this);
        //
        if (!JConnectionUtils.isConnectedToInternet(this)) {
            showAlertDialog(getString(R.string.error_no_response));
            return;
        }
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        String mobileTemp = JUtils.getNumeric(etMobile.getText().toString());
        //Log.d("user_basic", basic + "");
        mobile = formatPhone(mobileTemp);
        /*if (TextUtils.isEmpty(mobile) || !TextUtils.equals(mobileTemp, mobile) || mobile.length() != 10) {*/
        if (TextUtils.isEmpty(mobile)) {
            tvError.setText("Please Enter Mobile Number");
            //Toast.makeText(PhoneVerificationActivity.this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!TextUtils.equals(mobile, mobileTemp) || mobile.length() != 10) {
            tvError.setText("Mobile Number must be 10 digits long");
            //Toast.makeText(PhoneVerificationActivity.this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d("MobileNumber", "" + mobile);
        //final String mobile = formatPhone(mobile1);
        //
        //pd.show();
        customProgress = CustomProgress.show(PhoneVerificationActivity.this, "", false, false, null);
        Ion.with(this).load("PUT", AppUrls.MAIN + "/add_mobile").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("mobile", mobile).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("user_Result", result + "");
                customProgress.dismiss();
                if (result != null) {
                    // pd.dismiss();
                    try {
                        JSONObject joResult = new JSONObject(result);
                        //
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //Toast.makeText(PhoneVerificationActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            spUser.edit().putString("phone", mobile).commit();
                            //
                            isFromSuccess = true;
                            showAlertDialog("We've sent you a confirmation code.");
                            //adThanks.show();
                        } else {
                            //
                            String message = null;
                            /*{"result":"error","message":"The 'To' number +11234567891 is not a valid phone number."}*/
                            if (TextUtils.equals("error", joResult.getString("result"))) {
                                if (TextUtils.equals("validation", joResult.optString("error_type"))) {
                                    if (joResult.has("message")) {
                                        JSONObject joError = joResult.optJSONObject("message");
                                        if (joError != null && joError.has("mobile")) {
                                            JSONArray jaError = joError.optJSONArray("mobile");
                                            //
                                            message = jaError.getString(0);
                                            //builderError.setErrorMessage(jaError.getString(0));
                                        }
                                    }
                                } else
                                    message = joResult.getString("message");
                                //builderError.setErrorMessage(joResult.getString("message"));
                            } else
                                message = getString(R.string.error_no_response);
                            //builderError.setErrorMessage(getString(R.string.error_no_response));
                            //
                            showAlertDialog(message);
                            //adError.show();
                        }
                    } catch (JSONException e1) {
                        //
                        showAlertDialog(getString(R.string.error_no_response));
                        /*builderError.setErrorMessage(getString(R.string.error_no_response));
                        adError.show();*/
                        //Toast.makeText(PhoneVerificationActivity.this, "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //
                    showAlertDialog(getString(R.string.error_no_response));
                    /*builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();*/
                    //Toast.makeText(PhoneVerificationActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private void showAlertDialog(String message) {
        builderError.setErrorMessage(message);
        adError.show();
        TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
    }

    //
    private class ThanksDialog extends AlertDialog.Builder implements View.OnClickListener {

        public ThanksDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_phone_verification_thanks, null);
            TypefaceHelper.typeface(view);
            //
            setView(view, 50, 0, 50, 0);
            //
            view.findViewById(R.id.btnYay).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            adThanks.dismiss();
            Intent intent = new Intent(PhoneVerificationActivity.this, OtpActivity.class);
            intent.putExtra("mobile", mobile);
            startActivity(intent);
            finish();
        }
    }

    //
    private class InvalidPhoneDialog extends AlertDialog.Builder implements View.OnClickListener {

        public InvalidPhoneDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_invalid_phone_number, null);
            TypefaceHelper.typeface(view);
            //
            tvTextInvalidPhone = (TextView) view.findViewById(R.id.tvTextInvalidPhone);
            tvTextInvalidPhone.setTypeface(tfText);
            setView(view, 50, 0, 50, 0);
            //
            view.findViewById(R.id.btnYay).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            invalidPhoneDialog.dismiss();
            tvTextInvalidPhone.setText("");
            /*Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
            intent.putExtra("mobile", etMobile.getText().toString());
            startActivity(intent);
            finish();*/
        }
    }
    //

    @Override
    public void onBackPressed() {
        LoginManager.getInstance().logOut();
        super.onBackPressed();
    }
}
