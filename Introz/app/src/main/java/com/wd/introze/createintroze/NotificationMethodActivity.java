package com.wd.introze.createintroze;

import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.FutureBuilder;
import com.koushikdutta.ion.builder.UrlEncodedBuilder;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.AppUrls;
import com.wd.introze.R;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.tables.TableEmail;
import com.wd.introze.tables.TablePhone;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by flair on 26-07-2016
 */
public class NotificationMethodActivity extends AppCompatActivity {
    //
    private LinearLayout llUser1Container, llUser2Container;
    //
    private SharedPreferences spUser;
    //
    private String urlPic1, urlPic2;
    //
    String id1;
    String name1;
    String userId1;
    String contactId1;
    String phone1;
    String email1;
    //
    String id2;
    String name2;
    String userId2;
    String contactId2;
    String email2;
    String phone2;
    //
    //
    private CustomProgress pd;
    //
    private RadioGroup rgG1, rgPhone1, rgG2, rgPhone2;
    //
    private boolean isSuccess;
    //
    private JSimpleBuilder builderIntrozeSuccessCreAlertDialog;
    private AlertDialog introzeSuccessCreAlertDialog;
    //
    private AlertDialog ad;
    //
    private TextView tvSync;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_method);
        //
        TypefaceHelper.typeface(this);
        //
        initHeader();
        //
        builderIntrozeSuccessCreAlertDialog = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                introzeSuccessCreAlertDialog.dismiss();
                if (isSuccess) {
                    setResult(RESULT_OK);
                    finish();
                }
                //introzeSuccessCreAlertDialog.dismiss();
            }
        });
        introzeSuccessCreAlertDialog = builderIntrozeSuccessCreAlertDialog.create();
        //introzeSuccessCreAlertDialog = new IntrozeSuccessCreAlertDialog(this).create();
        //introzeSuccessCreAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
        llUser1Container = (LinearLayout) findViewById(R.id.llUser1Container);
        llUser2Container = (LinearLayout) findViewById(R.id.llUser2Container);
        //
        urlPic1 = getIntent().getStringExtra("url_profile1");
        urlPic2 = getIntent().getStringExtra("url_profile2");
        //
        String[] users = getIntent().getStringArrayExtra("users");
        //first user
        boolean isIntrozeUser1 = getIntent().getBooleanExtra("is_user1_introze", false);
        id1 = users[0];
        name1 = users[1];
        userId1 = users[2];
        contactId1 = users[3];
        phone1 = users[4];
        email1 = users[5];
        if (isIntrozeUser1) {
            isIntrozeUser(llUser1Container, name1, id1, urlPic1);
        } else {
            LinearLayout ll1 = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.is_not_introze_user, null);
            TypefaceHelper.typeface(ll1);
            //
            rgG1 = (RadioGroup) ll1.findViewById(R.id.radioGender);
            //
            rgPhone1 = (RadioGroup) ll1.findViewById(R.id.rgPhone);
            //
            isNotIntrozeUser(llUser1Container, ll1, rgG1, rgPhone1, id1, name1, getIntent().getLongExtra("_id1", -1));
        }
        //
        //second user
        boolean isIntrozeUser2 = getIntent().getBooleanExtra("is_user2_introze", false);
        id2 = users[6];
        name2 = users[7];
        userId2 = users[8];
        contactId2 = users[9];
        email2 = users[10];
        phone2 = users[11];
        if (isIntrozeUser2) {
            isIntrozeUser(llUser2Container, name2, id2, urlPic2);
        } else {
            LinearLayout ll2 = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.is_not_introze_user, null);
            TypefaceHelper.typeface(ll2);
            //
            rgG2 = (RadioGroup) ll2.findViewById(R.id.radioGender);
            //
            rgPhone2 = (RadioGroup) ll2.findViewById(R.id.rgPhone);
            //
            isNotIntrozeUser(llUser2Container, ll2, rgG2, rgPhone2, id2, name2, getIntent().getLongExtra("_id2", -1));
        }
        //
        //
        spUser = getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        //
        ad = new SyncBuilder(this).create();
        ad.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
    }

    //
    private void initHeader() {
        TextView txtHeader = (TextView) findViewById(R.id.txtHeader);
        txtHeader.setText("NOTIFICATION METHOD");
        txtHeader.setTextSize(12);

    }


    //
    private void createIntroze() {

       /* Log.d("contactid1", "" + contactId1);
        Log.d("email1", "" + email1);
        Log.d("phone1", "" + phone1);

        Log.d("contactid2", "" + contactId2);
        Log.d("email2", "" + email2);
        Log.d("phone2", "" + phone2);
*/

        String relationType = (getIntent().getIntExtra("selected_index", 0) + 1) + "";

        //
        //Log.d("relationtype", relationType + "");
        //
        //Log.d("reason", reason + "");

        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        /*Log.d("userid", "" + spUser.getString("user_id", ""));
        Log.d("usertoken", "" + spUser.getString("user_token", ""));*/
        //

        //
        UrlEncodedBuilder builder = Ion.with(this).load("POST", AppUrls.MAIN + "/create_introze")
                .setHeader("Authorization", "Basic " + basic);
        if (userId1 != null)
            builder.setBodyParameter("user_id_1", userId1);
        else {
            builder.setBodyParameter("contact_id_1", contactId1)
                    .setBodyParameter("email_1", email1).setBodyParameter("phone_1", phone1);
        }
        if (userId2 != null) {
            builder.setBodyParameter("user_id_2", userId2);
        } else {
            builder.setBodyParameter("contact_id_2", contactId2)
                    .setBodyParameter("email_2", email2)
                    .setBodyParameter("phone_2", phone2);
        }
        builder.setBodyParameter("relation_type", relationType)
                .setBodyParameter("reason", getIntent().getStringExtra("reason"));
        //
        pd = CustomProgress.show(this, "", false, false, null);
        ((FutureBuilder) builder).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                pd.dismiss();
                Log.d("CreateIntrozeResult", "" + result);
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //Toast.makeText(NotificationMethodActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            /*setResult(RESULT_OK);
                            finish();*/
                            HomeActivity.intent1 = null;
                            HomeActivity.intent2 = null;
                            //
                            isSuccess = true;
                            showDialog("Your Introze has been sent!",
                                    "Your contacts will be notified of the Introze you created.");
                        } else {
                            if (joResult.has("message")) {
                                /*tvSync.setText(joResult.getString("message"));
                                ad.show();*/
                                showDialog("", joResult.getString("message"));
                                //Toast.makeText(NotificationMethodActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                            if (joResult.has("errorCode") && TextUtils.equals("401", joResult.getString("errorCode"))) {
                                //
                                showDialog("Authentication", "Authentication Problem");
                                //Toast.makeText(NotificationMethodActivity.this, "Authentication Problem", Toast.LENGTH_SHORT).show();
                            }
                            //finish();
                        }
                    } catch (JSONException e1) {
                        //
                        showDialog("Error", getString(R.string.error_no_response));
                        //Toast.makeText(NotificationMethodActivity.this, "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showDialog("Error", getString(R.string.error_no_response));
                    //Toast.makeText(NotificationMethodActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    public void onClickBtnDone(View view) {
        if (userId1 == null) {
            //
            RadioButton rb = ((RadioButton) rgG1.findViewById(rgG1.getCheckedRadioButtonId()));
            email1 = rb == null ? null : rb.getText().toString();
            //
            RadioButton rbP = ((RadioButton) rgPhone1.findViewById(rgPhone1.getCheckedRadioButtonId()));
            phone1 = rbP == null ? null : rbP.getText().toString();
            //
            if (email1 == null && phone1 == null) {
                showDialog("Alert", "Please select email or phone for user 1");
                //Toast.makeText(NotificationMethodActivity.this, "Please select email or phone for user 1", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        //
        //
        if (userId2 == null) {
            RadioButton rbE2 = ((RadioButton) rgG2.findViewById(rgG2.getCheckedRadioButtonId()));
            email2 = rbE2 == null ? null : rbE2.getText().toString();
            //
            RadioButton rbP2 = ((RadioButton) rgPhone2.findViewById(rgPhone2.getCheckedRadioButtonId()));
            phone2 = rbP2 == null ? null : rbP2.getText().toString();
            //
            if (email2 == null && phone2 == null) {
                //
                showDialog("Alert", "Please select email or phone for user 2");
                //Toast.makeText(NotificationMethodActivity.this, "Please select email or phone for user 2", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        createIntroze();
    }

    //
    private void showDialog(String title, String message) {
        builderIntrozeSuccessCreAlertDialog.setErrorMessage(title, message);
        introzeSuccessCreAlertDialog.show();
        TypefaceHelper.typeface(introzeSuccessCreAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE));
    }

    //
    private void isIntrozeUser(View view, final String name, String id, String urlPic) {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.is_introze_user, null);
        TypefaceHelper.typeface(ll);
        //
        TextView tv = (TextView) ll.findViewById(R.id.tv);
        tv.setText(name);
        //
        final TextView tvShortName = (TextView) ll.findViewById(R.id.tvShortName);
        //
        SimpleDraweeView iv = (SimpleDraweeView) ll.findViewById(R.id.iv);
        //
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
        if (!TextUtils.isEmpty(urlPic)) {
            uri = Uri.parse(urlPic);
        }
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(getTwoUpperChar(name));
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(uri).build();
        iv.setController(controller);

        /*Bitmap bitmap = loadContactPhoto(getContentResolver(), Long.parseLong(id));
        if (bitmap == null) {
            iv.setImageResource(R.drawable.account_icon);
        } else {
            iv.setImageBitmap(bitmap);
        }*/
        //
        ((LinearLayout) view).addView(ll);
    }

    //
    private void isNotIntrozeUser(View view, LinearLayout ll, RadioGroup rgG, RadioGroup rgP, String id, final String name, long _id) {
        //
        List<TableEmail> emails = new Select().from(TableEmail.class).where("Contacts =?", _id).execute();
        //
        //ArrayList<String> emails = getEmailId(getContentResolver(), id);
        for (int i = 0; i < emails.size(); i++) {
            RadioButton rv = (RadioButton) LayoutInflater.from(this).inflate(R.layout.radio, null);
            TypefaceHelper.typeface(rv);
            //
            rv.setText(emails.get(i).email);
            rgG.addView(rv, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            //
            if (i < emails.size() - 1) {
                View hr = new View(this);
                hr.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                //
                rgG.addView(hr, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            }
        }
        //
        if (emails.size() > 0) {
            ll.findViewById(R.id.llEmail).setVisibility(View.VISIBLE);
        }
        //
        List<TablePhone> phones = new Select().from(TablePhone.class).where("Contacts =?", _id).execute();
        //
        //ArrayList<String> phones = getPhoneNumber(id);
        for (int i = 0; i < phones.size(); i++) {
            RadioButton rv = (RadioButton) LayoutInflater.from(this).inflate(R.layout.radio, null);
            TypefaceHelper.typeface(rv);
            //
            rv.setText(phones.get(i).phone);
            rgP.addView(rv, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            //
            if (i < phones.size() - 1) {
                View hr = new View(this);
                hr.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                //
                rgP.addView(hr, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            }
        }
        //tvNameTemp
        //
        if (phones.size() > 0) {
            ll.findViewById(R.id.llSMS).setVisibility(View.VISIBLE);
        }
        //
        final TextView tvNameTemp = (TextView) ll.findViewById(R.id.tvNameTemp);
        SimpleDraweeView iv = (SimpleDraweeView) ll.findViewById(R.id.iv);
        //
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvNameTemp.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvNameTemp.setText(JUtils.getTwoUpperChar(name));
                tvNameTemp.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(uri).build();
        iv.setController(controller);

        /*ImageView iv = (ImageView) ll.findViewById(R.id.iv);
        Bitmap bitmap = loadContactPhoto(getContentResolver(), Long.parseLong(id));
        if (bitmap == null) {
            tvNameTemp.setText(getTwoUpperChar(name));
        } else {
            tvNameTemp.setVisibility(View.GONE);
            iv.setImageBitmap(bitmap);
        }*/
        //
        TextView tvName = (TextView) ll.findViewById(R.id.tvName);
        tvName.setText(name);
        /*//
        TextView tvEmail = (TextView) ll.findViewById(R.id.tvEmail);
        tvEmail.setText(email);
        //
        TextView tvPhone = (TextView) ll.findViewById(R.id.tvPhone);
        tvPhone.setText(phone);*/
        //
        ((LinearLayout) view).addView(ll);
    }

    //
    private String getTwoUpperChar(String name) {
        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }

    //
    private class IntrozeSuccessCreAlertDialog extends AlertDialog.Builder implements View.OnClickListener {

        public IntrozeSuccessCreAlertDialog(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_create_introze_success, null);
            TypefaceHelper.typeface(view);
            // tvEdValidation = (TextView) view.findViewById(R.id.tvEdValidation);
            setView(view, 50, 0, 50, 0);
            //

            //
            view.findViewById(R.id.btnOk).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            introzeSuccessCreAlertDialog.dismiss();
            setResult(RESULT_OK);
            finish();
        }
    }

    //
    //
    private class SyncBuilder extends AlertDialog.Builder implements View.OnClickListener {

        public SyncBuilder(Context context) {
            super(context);
            //
            View view = LayoutInflater.from(context).inflate(R.layout.dilaog_ed_validation, null);
            TypefaceHelper.typeface(view);
            //
            tvSync = (TextView) view.findViewById(R.id.tvEdValidation);
            setView(view, 50, 0, 50, 0);
            //

            //
            view.findViewById(R.id.btnYay).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ad.dismiss();
            finish();
            /*Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
            intent.putExtra("mobile", etMobile.getText().toString());
            startActivity(intent);
            finish();*/
        }
    }
    //
}
