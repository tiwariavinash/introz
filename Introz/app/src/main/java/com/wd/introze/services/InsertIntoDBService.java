package com.wd.introze.services;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.google.gson.Gson;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.mysyncadapter.SyncUtils;

import com.wd.introze.repository.server.rf.model.user.Email;
import com.wd.introze.repository.server.rf.model.user.User;
import com.wd.introze.tables.TableContacts;
import com.wd.introze.tables.TableEmail;
import com.wd.introze.tables.TablePhone;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flair on 05-09-2016.
 */
public class InsertIntoDBService extends IntentService {
    //
    private SharedPreferences spUser;
    //
    private String userPhone;
    private ArrayList<String> userEmails = new ArrayList<>();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public InsertIntoDBService() {
        super(InsertIntoDBService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        spUser = getSharedPreferences("user", MODE_PRIVATE);
        //
        spUser.edit().putBoolean("is_first_time_sync", true).putBoolean("is_contact_sync_db", false).commit();
        //
        insertIntoDBFirstTime();
    }

    //
    private void insertIntoDBFirstTime() {
        Log.d("insertIntoDB "," inside1 ");
        new com.activeandroid.query.Delete().from(TableEmail.class).where("Email <>?", "-1").execute();
        new com.activeandroid.query.Delete().from(TablePhone.class).where("Phone <>?", "-1").execute();
        new com.activeandroid.query.Delete().from(TableContacts.class).where("Identifier <>?", -1).execute();
        //
        ArrayList<CModel> name = getContactsFromPhoneBook();
        //
        ArrayList<List<CModel>> alNewBatch = getBatches(name, 100);
        //
        for (List<CModel> list : alNewBatch) {
            //
            ActiveAndroid.beginTransaction();
            try {
                //
                for (CModel model : list) {
                    insertContact(model);
                /*Intent intent = new Intent("load_contacts_from_db");
                sendBroadcast(intent);*/
                }
                //
                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
            }
            Intent intent = new Intent("load_contacts_from_db");
            sendBroadcast(intent);
        }
        //
        //
        /*if (spUser.getBoolean("is_verified", false)) {
            //
            withTransaction(name);
        } else {
            withoutTransaction(name);
        }*/
        //
        spUser.edit().putBoolean("is_contact_sync_db", true).commit();
        //
        Log.d("SyncDBSyncContact2", "Completed");
        //SyncUtils.CreateSyncAccount(context);
        Intent intent = new Intent("load_contacts_from_db");
        sendBroadcast(intent);
        //
        if (spUser.getBoolean("is_verified", false))
            SyncUtils.CreateSyncAccount(this);
        //
        //syncFirstTime();
    }

    //
    //
    public ArrayList<List<CModel>> getBatches(ArrayList<CModel> collection, int batchSize) {
        int i = 0;
        ArrayList<List<CModel>> batches = new ArrayList<>();
        while (i < collection.size()) {
            int nextInc = Math.min(collection.size() - i, batchSize);
            List<CModel> batch = collection.subList(i, i + nextInc);
            batches.add(batch);
            i = i + nextInc;
        }
        return batches;
    }

    //
    private void withoutTransaction(ArrayList<CModel> name) {
        //
        for (CModel model : name) {
            insertContact(model);
            //
            Intent intent = new Intent("load_contacts_from_db");
            sendBroadcast(intent);
        }
        //
    }

    //
    private void withTransaction(ArrayList<CModel> name) {
        //
        ActiveAndroid.beginTransaction();
        try {
            //
            for (CModel model : name) {
                insertContact(model);
            }
            //
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    //
    //
    private void insertContact(CModel model) {
        //
        TableContacts contacts = new TableContacts();
        String id = model.id;
        //
        contacts.identifier = Long.parseLong(id);
        //
        String[] name = model.displayName.split(" ");
        //
        if (name.length > 1) {
            contacts.lastName = name[name.length - 1];
            //
            String temp = name[0];
            for (int i = 1; i < name.length - 1; i++) {
                temp = temp + " " + name[i];
            }
            contacts.firstName = temp;
        } else {
            contacts.firstName = model.displayName;
            contacts.lastName = "";
        }
        //
        contacts.save();
        //
        ArrayList<String> phones = model.phones;
        //
        ActiveAndroid.beginTransaction();
        try {
            //
            for (int i = 0; i < phones.size(); i++) {
                TablePhone modelPhone = new TablePhone();
                modelPhone.phone = phones.get(i);
                modelPhone.contacts = contacts;
                modelPhone.save();
            }
            //
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
        //
        ArrayList<String> emails = model.emails;
        //
        ActiveAndroid.beginTransaction();
        try {
            //
            for (int i = 0; i < emails.size(); i++) {
                TableEmail modelEmail = new TableEmail();
                modelEmail.email = emails.get(i);
                modelEmail.contacts = contacts;
                modelEmail.save();
            }
            //
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    //
    //
    private ArrayList<CModel> getContactsFromPhoneBook() {
        //
        /*HomeActivity.ProfileModel profileModel = new Gson().fromJson(spUser.getString("profile_detail", ""),
                HomeActivity.ProfileModel.class);*/
        //
        Log.d("insertIntoDB "," inside1PhoneBook ");
        String userDetail = spUser.getString(User.class.getName(), null);
        if (!TextUtils.isEmpty(userDetail)) {
            User user = new Gson().fromJson(userDetail, User.class);
            userPhone = user.userDetails.phone;
            //
            for (Email e : user.userDetails.emails) {
                userEmails.add(e.email);
            }
            userEmails.add(user.userDetails.email);
            //for ( em:user.userDetails.emails)
            //userEmails.addAll(user.userDetails.emails);
        }
        //
        //
        ContentResolver cr = getContentResolver();
        //
        Cursor curContact = cr.query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts.HAS_PHONE_NUMBER}, null, null, null);
        //
        ArrayList<CModel> alContacts = new ArrayList<>();
        //
        ArrayList<String> alNames = new ArrayList<>();
        //
        //
        while (curContact.moveToNext()) {
            String displayName = curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            if (!alNames.contains(displayName) && !TextUtils.isEmpty(displayName) && isAlpha(displayName)
                    && !JUtils.isEmail(displayName)) {
                alNames.add(displayName);
                //
                String _id = curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts._ID));
                //
                String[] data = getContactDetails(this, _id);
                //
                if (data != null) {
                    //
                    CModel model = new CModel(curContact.getString(
                            curContact.getColumnIndex(ContactsContract.Contacts._ID))
                            ,
                            curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)),
                            curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                    //
                    for (String phone : new Gson().fromJson(data[0], String[].class)) {
                        model.phones.add(phone);
                    }
                    //
                    for (String email : new Gson().fromJson(data[1], String[].class)) {
                        model.emails.add(email);
                    }
                    //
                    alContacts.add(model);
                }
            }
        }
        curContact.close();
        //
        userPhone = null;
        userEmails.clear();
        //
        return alContacts;
    }

    //
    public boolean isAlpha(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (Character.isLetter(c)) {
                return true;
            }
        }
        return false;
    }

    //
    public String[] getContactDetails(Context context, String _id) {
        Cursor cursorDetail = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{ContactsContract.Data.MIMETYPE, ContactsContract.Data.DATA1},
                ContactsContract.Data.CONTACT_ID + "=?", new String[]{_id}, null);
        //
        ArrayList<String> phones = new ArrayList<>();
        ArrayList<String> emails = new ArrayList<>();
        //
        while (cursorDetail.moveToNext()) {
            String item = cursorDetail.getString(cursorDetail.getColumnIndex(ContactsContract.Data.DATA1));
            //
            if (TextUtils.equals(cursorDetail.getString(cursorDetail.getColumnIndex(ContactsContract.Data.MIMETYPE)),
                    ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
                String phone = formatPhone(item);
                if (TextUtils.equals(phone, userPhone)) {
                    cursorDetail.close();
                    return null;
                }
                if (!phones.contains(phone)) {
                    phones.add(phone);
                }
            } else if (TextUtils.equals(cursorDetail.getString(cursorDetail.getColumnIndex(ContactsContract.Data.MIMETYPE)),
                    ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
                if (userEmails.contains(item)) {
                    cursorDetail.close();
                    return null;
                }
                if (!emails.contains(item)) {
                    emails.add(item);
                }
            }
        }
        cursorDetail.close();
        if (phones.size() <= 0 && emails.size() <= 0) return null;
        return new String[]{new Gson().toJson(phones), new Gson().toJson(emails)};
    }

    //
    private String formatPhone(String phone) {
        String temp = phone.replaceAll("[^0-9]", "").replace(" ", "");
        if (temp.length() > 10) {
            temp = temp.substring(temp.length() - 10);
        }
        return temp;
    }

    //
    public class CModel {
        //
        //public boolean isUser;
        //
        public String id;
        public String hasPhoneNumber;
        public String displayName;
        //
        public int contactId;
        //
        public ArrayList<String> phones = new ArrayList<>();
        public ArrayList<String> emails = new ArrayList<>();

        //
        public CModel() {
        }

        //
        public CModel(String id) {
            this.id = id;
        }

        //
        public CModel(String id, String hasPhoneNumber, String displayName) {
            this.id = id;
            this.hasPhoneNumber = hasPhoneNumber;
            this.displayName = displayName;
        }
    }
    //
}
