package com.wd.introze;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.chat.ui.activity.ChatActivity;
import com.quickblox.sample.chat.utils.chat.ChatHelper;
import com.quickblox.users.model.QBUser;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.feed.FeedModel;
import com.wd.introze.jutil.JSession;
import com.wd.introze.payment.SendThankUActivity;
import com.wd.introze.tables.TableContacts;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flair on 29-07-2016
 */
public class FeedProfileActivity extends Fragment {
    //
    private TextView tvName, tvBusiness, tvDating, tvFriendship, tvMutualFriend;
    //
    private SharedPreferences spUser;
    //
    private SimpleDraweeView ivProfile;
    //
    private TextView tvShortName, tvIntroducedText;
    //
    Button btnSendThanku;
    //
    private ImageView ivBComma, ivDComma, ivFComma, ivMComma;
    //
    private FeedModel model;
    private CustomProgress pd;

    //
    public static FeedProfileActivity getInstance(String data) {
        Bundle bundle = new Bundle();
        bundle.putString("data", data);
        //
        Log.d("FeedProData", "data " + data);
        //
        FeedProfileActivity fragment = new FeedProfileActivity();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_feed_profile, null);
        TypefaceHelper.typeface(view);
        //
        view.findViewById(R.id.ivEditB).setVisibility(View.GONE);
        view.findViewById(R.id.ivEditD).setVisibility(View.GONE);
        view.findViewById(R.id.ivEditF).setVisibility(View.GONE);
        view.findViewById(R.id.ivEditMI).setVisibility(View.GONE);
        //
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvBusiness = (TextView) view.findViewById(R.id.tvBusiness);
        tvDating = (TextView) view.findViewById(R.id.tvDating);
        tvFriendship = (TextView) view.findViewById(R.id.tvFriendship);
        tvMutualFriend = (TextView) view.findViewById(R.id.tvMutualFriend);
        //
        tvIntroducedText = (TextView) view.findViewById(R.id.tvIntroducedText);
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        ivProfile = (SimpleDraweeView) view.findViewById(R.id.ivProfile);
        tvShortName = (TextView) view.findViewById(R.id.tvShortName);
        //
        btnSendThanku = (Button) view.findViewById(R.id.btnSendThanku);
        btnSendThanku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SendThankUActivity.class);
                intent.putExtra("user_id", model.userid);//introzeUserId);
                intent.putExtra("user_name", model.firstName + " " + model.lastName);
                intent.putExtra("url_user_pic", model.urlProfile == null ?
                        ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI,
                                Long.parseLong(model.identifier)).toString() : model.urlProfile);
                //
                startActivity(intent);
            }
        });
        //
        ivBComma = (ImageView) view.findViewById(R.id.ivBComma);
        //
        ivDComma = (ImageView) view.findViewById(R.id.ivDComma);
        //
        ivFComma = (ImageView) view.findViewById(R.id.ivFComma);
        //
        ivMComma = (ImageView) view.findViewById(R.id.ivMComma);
        //
        model = new Gson().fromJson(getArguments().getString("data"), FeedModel.class); //new Gson().fromJson(getIntent().getStringExtra("feed_model"), FeedModel.class);
        //
        setUserProfile(model.urlProfile, model.firstName + " " + model.lastName);
        //
        if (model.isMatched) {
            ImageButton ibChat = (ImageButton) view.findViewById(R.id.ibChat);
            ibChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("FeeddfdslksAct", "Identifier " + model.identifier);
                    Log.d("FeedProfileChat", "Identifier " + model.chatId);
                    //
                    //
                    if (TextUtils.isEmpty(model.identifier) || TextUtils.equals("null", model.identifier)) {
                        //
                        //
                        TableContacts tc = new TableContacts();
                        tc.firstName = model.firstName;
                        tc.lastName = model.lastName;
                        tc.introzeUserId = model.userid;
                        tc.urlPic = model.urlProfile;
                        tc.chatId = model.chatId;
                        //
                        //Toast.makeText(getActivity(), "chat id my " + tc.chatId, Toast.LENGTH_SHORT).show();
                        //
                        callChat(tc);//items.get(0));
                    } else {
                        //
                        List<TableContacts> items = new Select().from(TableContacts.class).where("Identifier =?", model.identifier).execute();
                        if (items.size() > 0) {
                            //
                            /*TableContacts tc = new TableContacts();
                            tc.firstName = model.firstName;
                            tc.lastName = model.lastName;
                            tc.introzeUserId = model.userid;
                            tc.urlPic = model.urlProfile;
                            tc.chatId = model.chatId;*/
                            //
                            callChat(items.get(0));
                        } /*else
                            Toast.makeText(getActivity(), "item clicked", Toast.LENGTH_SHORT).show();*/
                    }
                }
            });
            //
            ibChat.setVisibility(View.VISIBLE);
        }
        //
        getUserDetail();
        //
        return view;
    }

    //
    //
    private void callChat(TableContacts model) {
        if (spUser.getBoolean("is_chat_init", false)) {
            JSession.chatUserName = model.firstName + " " + model.lastName; //model.name;
            JSession.introzeId = model.introzeUserId + ""; //model.rel_user_id + "";
            JSession.urlRel = model.urlPic; //model.url;
            //Toast.makeText(getActivity(), "chat id " + model.chatId, Toast.LENGTH_SHORT).show();
            openChat(model.chatId);
        } else
            Toast.makeText(getActivity(), "Chat initializing please wait...", Toast.LENGTH_SHORT).show();
    }

    //
    public void openChat(int chatId) {
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        ArrayList<QBUser> selectedUsers = new ArrayList<>();
        //
        selectedUsers.add(new QBUser(chatId));
        //
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        pd.dismiss();
                        ChatActivity.startForResult(getActivity(), 5000, dialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        pd.dismiss();
                        Log.d("DialogError", "" + e);
                        //Toast.makeText(MainActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    //
    private void setUserProfile(String uriProfile, final String name) {
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(getTwoUpperChar(name));
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(uriProfile).build();
        ivProfile.setController(controller);
        //
    }

    //
    public String getTwoUpperChar(String name) {
        String[] temp = name.split(" ");
        if (temp.length >= 2) {
            return temp[0].substring(0, 1) + temp[1].substring(0, 1);
        }
        return temp[0].substring(0, 1);
    }

    //
    private void getUserDetail() {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/get_user_detail/" + model.userid)//getActivity().getIntent().getStringExtra("user_id"))
                .setHeader("Authorization", "Basic " + basic).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("FeedProfile", result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        //
                        JSONObject joUserDetail = joResult.getJSONObject("user_details");
                        //
                        String name = joUserDetail.getString("first_name") + " " + joUserDetail.getString("last_name");
                        String firstName = joUserDetail.getString("first_name");
                        //
                        tvName.setText(name);
                        //
                        btnSendThanku.setText("send thank you $$$ to " + name);
                        tvIntroducedText.setText(firstName + " wants to be introduced to these types of people:");

                        // txtHeader.setText(name);
                        //
                        JSONObject joStatus = joUserDetail.getJSONObject("status");
                        //
                        String business = joStatus.getString("business");
                        String dating = joStatus.getString("dating");
                        String friendship = joStatus.getString("friendship");
                        String mutualInterest = joStatus.getString("mutual_interests");
                        //
                        if (!TextUtils.isEmpty(business)) {
                            tvBusiness.setText(business);
                            ivBComma.setVisibility(View.VISIBLE);
                        }
                        //
                        if (!TextUtils.isEmpty(dating)) {
                            tvDating.setText(dating);
                            ivDComma.setVisibility(View.VISIBLE);
                        }
                        //
                        if (!TextUtils.isEmpty(friendship)) {
                            tvFriendship.setText(friendship);
                            ivFComma.setVisibility(View.VISIBLE);
                        }
                        //
                        if (!TextUtils.isEmpty(mutualInterest)) {
                            tvMutualFriend.setText(mutualInterest);
                            ivMComma.setVisibility(View.VISIBLE);
                        }
                        //
                        //FeedProfileActivity.this.name = name;
                        //ivProfile.setImageURI(Uri.parse(AppUrls.MAIN + "/" + joUserDetail.getString("user_image")));
                        //setUserProfile(Uri.parse(AppUrls.MAIN + "/" + joUserDetail.getString("user_image")), name);
                    } catch (JSONException e1) {
                        //Toast.makeText(FeedProfileActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //Toast.makeText(FeedProfileActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    public interface OnProfileClickListener {
        void onProfileClicked(FeedModel model);
    }
}
