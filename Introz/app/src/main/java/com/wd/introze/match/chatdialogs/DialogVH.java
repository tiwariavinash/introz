package com.wd.introze.match.chatdialogs;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.R;

/**
 * Created by flair on 29-07-2016
 */
public class DialogVH extends RecyclerView.ViewHolder implements View.OnClickListener {
    //
    TextView tvFullName;
    TextView tvShortName;
    TextView tvCount;
    TextView tvByName;
    TextView tvDate;
    //
    ImageView ivRelationType;
    //
    SimpleDraweeView iv;
    //
    LinearLayout ll;

    public DialogVH(View itemView) {
        super(itemView);
        //
        TypefaceHelper.typeface(itemView);
        //
        tvFullName = (TextView) itemView.findViewById(R.id.tvName);
        tvCount = (TextView) itemView.findViewById(R.id.tvCount);
        ll = (LinearLayout) itemView.findViewById(R.id.ll);
        tvShortName = (TextView) itemView.findViewById(R.id.tvShortName);
        iv = (SimpleDraweeView) itemView.findViewById(R.id.iv);
        //
        tvByName = (TextView) itemView.findViewById(R.id.tvByName);
        tvByName.setOnClickListener(this);
        //
        tvDate = (TextView) itemView.findViewById(R.id.tvDate);
        //
        ivRelationType = (ImageView) itemView.findViewById(R.id.ivRelationType);
        ivRelationType.setOnClickListener(this);
        //
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvByName)
            DialogAdapter.listener.onDialogClicked(getAdapterPosition(), false);
        else if (v.getId()==R.id.ivRelationType)
            DialogAdapter.listener.onRelationshipClicked(getAdapterPosition());
        else DialogAdapter.listener.onDialogClicked(getAdapterPosition(), true);
    }
}
