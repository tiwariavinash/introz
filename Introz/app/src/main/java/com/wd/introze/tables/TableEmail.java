package com.wd.introze.tables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by flair on 26-07-2016.
 */
@Table(name = "TableEmail")
public class TableEmail extends Model{
    @Column(name = "Email")
    public String email;
    @Column(name = "Contacts")
    public TableContacts contacts;
}
