package com.wd.introze;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import com.norbsoft.typefacehelper.TypefaceHelper;

/**
 * Created by Admin on 4/6/2018.
 */

public class IntrozeLevelActivity extends AppCompatActivity {

    private TextView txtHeader;
    private Typeface tfHeader;
    String fontPathHeader = "itcavantgardestd-bold.ttf";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introze_level1);
        TypefaceHelper.typeface(this);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        initHeader("INTROZE LEVELS");
    }

    private void initHeader(String text) {
        txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(text);
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }
}
