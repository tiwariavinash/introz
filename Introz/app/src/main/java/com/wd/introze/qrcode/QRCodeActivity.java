package com.wd.introze.qrcode;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.norbsoft.typefacehelper.TypefaceHelper;
import com.tobrun.vision.qr.QRScanFragment;
import com.wd.introze.R;

/**
 * Created by Admin on 3/31/2018.
 */

public class QRCodeActivity extends AppCompatActivity implements QRScanFragment.OnScanCompleteListener {

    private TextView txtHeader;
    private Typeface tfHeader;
    String fontPathHeader = "itcavantgardestd-bold.ttf";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);
        //
        tfHeader = Typeface.createFromAsset(getAssets(), fontPathHeader);
        //
        ViewPager vp = findViewById(R.id.vp);
        vp.setAdapter(new QRViewPageAdapter(getSupportFragmentManager()));
        //
        TabLayout tabLayout = findViewById(R.id.tl);
        tabLayout.setupWithViewPager(vp);
        //
        changeTabsFont(tabLayout);
        //
        tabLayout.getTabAt(getIntent().getIntExtra("selected_tab", 0)).select();
        //
        initHeader("QR CODE");
    }

    private void changeTabsFont(TabLayout tabLayout) {
        ViewGroup childTabLayout = (ViewGroup) tabLayout.getChildAt(0);
        for (int i = 0; i < childTabLayout.getChildCount(); i++) {
            ViewGroup viewTab = (ViewGroup) childTabLayout.getChildAt(i);
            for (int j = 0; j < viewTab.getChildCount(); j++) {
                View tabTextView = viewTab.getChildAt(j);
                if (tabTextView instanceof TextView) {
                    TypefaceHelper.typeface(tabTextView);
                }
            }
        }
    }

    private void initHeader(String text) {
        txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(text);
        txtHeader.setTextSize(12);
        txtHeader.setTypeface(tfHeader);
    }

    @Override
    public void onScanComplete(@NonNull String qrCode) {
        Intent intent = new Intent(this, QRCodeScannedActivity.class);
        intent.putExtra("data", qrCode);
        startActivity(intent);
    }

    @Override
    public void onCameraError(int errorTextRes) {

    }

    private class QRViewPageAdapter extends FragmentStatePagerAdapter {

        private String[] titles = {"QR Code", "Scanner"};

        public QRViewPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) return new QRCodeFragment();
            return new QRScanFragment();
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            super.getPageTitle(position);
            return titles[position];
        }
    }
}
