package com.wd.introze.signup;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import com.wd.introze.R;

/**
 * Created by flair on 19-09-2016.
 */
public class PPActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pp);
        //
        TextView tvTitle = (TextView) findViewById(R.id.txtHeader);
        tvTitle.setText("Privacy Policy".toUpperCase());
    }
}
