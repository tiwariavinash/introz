package com.wd.introze.Introduction;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.wd.introze.LoginActivity;
import com.wd.introze.MainActivity;
import com.wd.introze.R;

/**
 * Created by flair on 21-07-2016
 */
public class IntroActivity extends AppCompatActivity {
    //
    private RadioGroup rg;
    private int[] rgBtnIDs = {R.id.rb1, R.id.rb2, R.id.rb3};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        //
        //TypefaceHelper.typeface(this);
        //
        rg = (RadioGroup) findViewById(R.id.rg);
        //
        whiteAllRadio();
        ((RadioButton) rg.findViewById(rgBtnIDs[0])).setButtonDrawable(R.drawable.radio_button_black);
        // view pager
        final ViewPager vp = (ViewPager) findViewById(R.id.vp);
        vp.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        vp.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                //
                whiteAllRadio();
                rg.check(rgBtnIDs[position]);
                ((RadioButton) rg.findViewById(rgBtnIDs[position])).setButtonDrawable(R.drawable.radio_button_black);
            }
        });
    }

    //
    private void whiteAllRadio() {
        ((RadioButton) rg.findViewById(rgBtnIDs[0])).setButtonDrawable(R.drawable.radio_button_white);
        ((RadioButton) rg.findViewById(rgBtnIDs[1])).setButtonDrawable(R.drawable.radio_button_white);
        ((RadioButton) rg.findViewById(rgBtnIDs[2])).setButtonDrawable(R.drawable.radio_button_white);

    }

    //
    private class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fm = null;
            switch (position) {
                case 0:
                    fm = new First();
                    break;
                case 1:
                    fm = new Second();
                    break;
                case 2:
                    fm = new Third();
                    break;
            }
            return fm;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    //
    public void onClickBtnSignUp(View view) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    //
    public void onClickBtnLogin(View view) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
