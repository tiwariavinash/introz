package com.wd.introze.match;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.sample.chat.ui.activity.ChatActivity;
import com.quickblox.sample.chat.utils.chat.ChatHelper;
import com.quickblox.users.model.QBUser;
import com.wd.introze.AppUrls;
import com.wd.introze.FeedProfileActivity;
import com.wd.introze.R;
import com.wd.introze.activities.byu.ByUDialog;
import com.wd.introze.async.MatchAsyncTask;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.feed.FeedModel;
import com.wd.introze.home.HomeActivity;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JSession;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.match.chatdialogs.DialogAdapter;
import com.wd.introze.match.chatdialogs.DialogModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by flair on 29-07-2016
 */
public class MatchFragment extends Fragment implements MatchAdapter.OnMatchClickListener,
        DialogAdapter.OnDialogClickListener, ByUDialog.DialogDismiss {
    //
    private boolean isMergeNow = false, isForDialogUpdate = false;
    //
    private ArrayList<MatchModel> alAllData = new ArrayList<>();
    //
    private ArrayList<MatchModel> data;
    private MatchAdapter adapter;
    //
    private ArrayList<DialogModel> alDialog = new ArrayList<>();
    private DialogAdapter adapterDialog;
    //
    private SharedPreferences spUser;

    //
    String fontPathText = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    TextView tvMatches, tvMatches1;
    //
    TextView tvMessage, tvMessage1;
    //
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //
    private TextView tvMessageCount, tvMatchesCount;
    private int matchCount;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private LinearLayout llMatches, llMatchesEmpty, llMessages, llMessagesEmpty;
    //
    private CustomProgress pd;
    //
    private boolean isRunning;
    //
    private ByUDialog builder;
    private AlertDialog ad;
    //
    private EditText et;
    //
    private String searchText;
    private static final String TAG = "MatchFragment";

    //
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_match, null);
        //
        TypefaceHelper.typeface(view);
        //
        et = (EditText) view.findViewById(R.id.et);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchText = s.toString();
                filterData();
            }
        });
        //
        builder = new ByUDialog(getActivity(), this);
        ad = builder.create();
        ad.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
        //
        llMatches = (LinearLayout) view.findViewById(R.id.llMatches);
        llMatchesEmpty = (LinearLayout) view.findViewById(R.id.llMatchesEmpty);
        llMessages = (LinearLayout) view.findViewById(R.id.llMessages);
        llMessagesEmpty = (LinearLayout) view.findViewById(R.id.llMessagesEmpty);
        //
        tfText = Typeface.createFromAsset(getActivity().getAssets(), fontPathText);
        //
        data = new ArrayList<>();
        adapter = new MatchAdapter(getActivity(), data, this);
        //
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv.setLayoutManager(llm);
        //
        rv.setAdapter(adapter);
        //

        //
        adapterDialog = new DialogAdapter(getActivity(), alDialog, this);
        //
        RecyclerView rvDialog = (RecyclerView) view.findViewById(R.id.rv1);
        rvDialog.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDialog.setAdapter(adapterDialog);
        //
        spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        tvMatches = (TextView) view.findViewById(R.id.tvMatches);
        tvMatches1 = (TextView) view.findViewById(R.id.tvMatches1);
        tvMessage = (TextView) view.findViewById(R.id.tvMessage);
        tvMessage1 = (TextView) view.findViewById(R.id.tvMessage1);
        //

        tvMatches.setTypeface(tfText);
        tvMatches1.setTypeface(tfText);
        tvMessage.setTypeface(tfText);
        tvMessage1.setTypeface(tfText);
        //
        tvMessageCount = (TextView) view.findViewById(R.id.tvMessagesCount);
        tvMatchesCount = (TextView) view.findViewById(R.id.tvMatchesCount);
        //
        builderError = new JSimpleBuilder(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        //
        if (spUser.getBoolean("is_chat_init", false)) {
            //
            tvMessageCount.setTextColor(getResources().getColor(android.R.color.darker_gray));
            tvMessageCount.setText(Html.fromHtml("<html><body>Messages (<font color='#8DC73F'>0</font>)</body></html>"));
            tvMessageCount.setVisibility(View.VISIBLE);
            //tvMessageCount.setVisibility(View.GONE);
        } else {
            tvMessageCount.setVisibility(View.VISIBLE);
        }
        //
        return view;
    }

    //
    private void filterData() {
        data.clear();
        alDialog.clear();
        if (TextUtils.isEmpty(searchText)) {
            for (MatchModel model1 : alAllData) {
                data.add(model1);
                //Log.d("ChatIdRel", joItem.getInt("rel_chat_id") + "");
                if (model1.isRead) {
                    //
                    alDialog.add(new DialogModel(model1.chatId, model1.name, model1.url,
                            model1.byName, model1.createOn, model1.relationType, model1.rel_id,
                            model1.rel_user_id, model1.byUserId, model1.reason, model1.urlBy, model1.user1ChatId, model1.user2ChatId));
                } else matchCount++;
            }
        } else {
            for (MatchModel model1 : alAllData) {
                if (model1.name.contains(searchText)) {
                    data.add(model1);
                    //Log.d("ChatIdRel", joItem.getInt("rel_chat_id") + "");
                    if (model1.isRead) {
                        //
                        alDialog.add(new DialogModel(model1.chatId, model1.name, model1.url,
                                model1.byName, model1.createOn, model1.relationType, model1.rel_id,
                                model1.rel_user_id, model1.byUserId, model1.reason, model1.urlBy, model1.user1ChatId, model1.user2ChatId));
                    } else matchCount++;
                }
            }
        }
        adapter.notifyDataSetChanged();
        //adapterDialog.notifyDataSetChanged();
        //
        initCounts();
    }

    Timer timer;
    //

    @Override
    public void onResume() {
        super.onResume();
        //
        getActivity().registerReceiver(brMessage, new IntentFilter("message_count"));
        //
        //
        getActivity().registerReceiver(brMatch, new IntentFilter(MatchAsyncTask.MATCH_RECEIVER));
        //
        //
        if (!JConnectionUtils.isConnectedToInternet(getActivity())) {
            builderError.setErrorMessage(getString(R.string.no_internet));
            adError.show();
            return;
        }
        //
        /*if (!isRunning)
            getMatchesListFromServer();*/
        //
        //if (timer == null) {
        timer = new Timer("matches");
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!isRunning)
                    //getMatchesListFromServer();
                    //
                    new MatchAsyncTask(getContext()).execute(JUtils.getHeader(spUser));
            }
        }, new Date(), 1000 * 5);
        //}
        /*else {

        }*/
        //
        //

        /*if (spUser.getBoolean("is_chat_init", false))
            loadDialogsFromQb();*/
        //
    }

    //
    private BroadcastReceiver brMatch = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            MatchAsyncTask.MatchData data = new Gson().fromJson(intent.getStringExtra(MatchAsyncTask.MATCH_DATA), MatchAsyncTask.MatchData.class);
            List<ArrayList<MatchModel>> alData = data.data;
            //Toast.makeText(context, "Match Size " + alData.get(0).size(), Toast.LENGTH_SHORT).show();
            alAllData.clear();
            alAllData.addAll(alData.get(0));
            filterData2();
            //
            ArrayList<MatchModel> alDialogList = alData.get(1);
            filterDataDialog(alDialogList);
        }
    };

    //
    //
    private void filterDataDialog(ArrayList<MatchModel> data) {
        //
        alDialog.clear();
        //
        if (TextUtils.isEmpty(searchText)) {
            for (MatchModel model1 : data) {
                //
                DialogModel dm = new DialogModel(model1.chatId, model1.name, model1.url,
                        model1.byName, model1.createOn, model1.relationType, model1.rel_id,
                        model1.rel_user_id, model1.byUserId, model1.reason, model1.urlBy, model1.user1ChatId, model1.user2ChatId);
                dm.count = model1.count;
                //
                alDialog.add(dm);
            }
        } else {
            for (MatchModel model1 : alAllData) {
                if (model1.name.contains(searchText)) {
                    //
                    DialogModel dm = new DialogModel(model1.chatId, model1.name, model1.url,
                            model1.byName, model1.createOn, model1.relationType, model1.rel_id,
                            model1.rel_user_id, model1.byUserId, model1.reason, model1.urlBy, model1.user1ChatId, model1.user2ChatId);
                    dm.count = model1.count;
                    //
                    alDialog.add(dm);
                }
            }
        }
        //
        for (DialogModel model : alDialog) {
            Log.d(TAG, "Chat icon count " + model.count);
        }
        //
        adapterDialog.notifyDataSetChanged();
        //
        initCounts();
    }

    //
    private void filterData2() {
        //
        matchCount = 0;
        //
        data.clear();
        if (TextUtils.isEmpty(searchText)) {
            for (MatchModel model1 : alAllData) {
                data.add(model1);
                if (!model1.isRead) {
                    matchCount++;
                }
            }
        } else {
            for (MatchModel model1 : alAllData) {
                if (model1.name.contains(searchText)) {
                    data.add(model1);
                    if (!model1.isRead) {
                        matchCount++;
                    }
                }
            }
        }
        //Toast.makeText(getContext(), "Match Size New " + data.size(), Toast.LENGTH_SHORT).show();
        adapter.notifyDataSetChanged();
        //
        initCounts();
    }


    //
    @Override
    public void onPause() {
        super.onPause();
        //
        getActivity().unregisterReceiver(brMatch);
        //
        getActivity().unregisterReceiver(brMessage);
        //
        if (timer != null)
            timer.cancel();
    }

    //
    private void getMatchesListFromServer() {
        /*getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "MatchCalled", Toast.LENGTH_SHORT).show();
            }
        });*/
        matchCount = 0;
        //
        isRunning = true;
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        //
        ///pd = CustomProgress.show(getActivity(), null, false, false, null);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/new_match").setHeader("Authorization", "Basic " + basic).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.d("matches", result + "");
                        //pd.dismiss();
                        isRunning = false;
                        if (result != null) {
                            try {
                                JSONObject joResult = new JSONObject(result);
                                if (TextUtils.equals("success", joResult.getString("result"))) {
                                    alAllData.clear();
                                    if (joResult.has("new_match")) {
                                        JSONArray joNewMatch = joResult.getJSONArray("new_match");
                                        for (int i = 0; i < joNewMatch.length(); i++) {
                                            JSONObject joItem = joNewMatch.getJSONObject(i);
                                            String fn = joItem.getString("rel_first_name");
                                            String ln = joItem.getString("rel_last_name");
                                            //
                                            String byName = joItem.getString("by_first_name") + " " + joItem.getString("by_last_name");
                                            /*//
                                            Date d = null;
                                            try {
                                                d = df.parse(joItem.getString("created_on"));
                                            } catch (ParseException e1) {

                                            }
                                            //
                                            String createdOn = null;
                                            if (d != null) {
                                                createdOn = DateUtils.formatDateTime(getActivity(), d.getTime(), DateUtils.FORMAT_ABBREV_ALL);
                                            }*/
                                            //
                                            String createdOn = JUtils.getDateInDotFormat(joItem.getString("created_on"));
                                            //
                                            Log.d("MyCreateOn", createdOn + "hhkkkjj");
                                            //
                                            MatchModel model = new MatchModel(fn + " " + ln, fn, joItem.getString("rel_user_image"),
                                                    joItem.getInt("rel_chat_id"), byName, createdOn, joItem.getInt("relation_type"),
                                                    joItem.getInt("read_flag"), joItem.getInt("rel_id"), joItem.getInt("rel_user_id"),
                                                    joItem.getInt("by_user_id"), joItem.getString("reason"),
                                                    joItem.optString("by_user_image"), joItem.optString("by_fb_id"),
                                                    joItem.optString("rel_identifier"), joItem.optString("by_identifier"),
                                                    joItem.optString("rel_fb_id"), joItem.optInt("user1_chat_id"),
                                                    joItem.optInt("user2_chat_id"));
                                            //
                                            //Toast.makeText(getActivity(), "" + joItem.optString("rel_identifier").contains("ll"), Toast.LENGTH_SHORT).show();
                                            //
                                            alAllData.add(model);
                                        }
                                        /*if (isMergeNow) mergerDialogMatches();
                                        else isMergeNow = true;*/
                                    }
                                    //
                                    filterData();
                                    //initCounts();
                                    //else
                                    //Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                                } else {
                                    //
                                    getMatchesListFromServer();
                                    Log.d("ServerError", "ServerError");
                                    //
                                    /*builderError.setErrorMessage(joResult.getString("message"));
                                    adError.show();*/
                                    //Toast.makeText(getActivity(), "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                                    //
                                    /*if (isMergeNow) mergerDialogMatches();
                                    else isMergeNow = true;*/
                                    //
                                }
                            } catch (JSONException e1) {
                                ///
                                getMatchesListFromServer();
                                Log.d("ResultException", "" + e1);
                                //
                                /*builderError.setErrorMessage(getString(R.string.error_no_response));
                                adError.show();*/
                                //Toast.makeText(getActivity(), "" + e1, Toast.LENGTH_SHORT).show();
                                //
                                /*if (isMergeNow) mergerDialogMatches();
                                else isMergeNow = true;*/
                                //
                            }
                        } else {
                            getMatchesListFromServer();
                            Log.d("ResultNull", "" + e);
                            /*builderError.setErrorMessage(getString(R.string.error_no_response));
                            adError.show();*/
                            //Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                            //
                            /*if (isMergeNow) mergerDialogMatches();
                            else isMergeNow = true;*/
                            //
                        }
                    }
                });
    }

    private void initCounts() {
        //
        tvMatchesCount.setText(Html.fromHtml("<html><body>Matches (<font color='#8DC73F'>"
                + matchCount + "</font>)</body></html>"));
        //
        if (data.size() > 0) {
            llMatches.setVisibility(View.VISIBLE);
            llMatchesEmpty.setVisibility(View.GONE);
        } else {
            llMatches.setVisibility(View.GONE);
            llMatchesEmpty.setVisibility(View.VISIBLE);
        }
        //
        if (alDialog.size() > 0) {
            llMessages.setVisibility(View.VISIBLE);
            llMessagesEmpty.setVisibility(View.GONE);
            //
            int totalCount = 0;
            for (DialogModel model : alDialog) {
                totalCount += model.count;
            }
            //
            tvMessageCount.setTextColor(getResources().getColor(android.R.color.darker_gray));
            tvMessageCount.setText(Html.fromHtml("<html><body>Messages (<font color='#8DC73F'>" + totalCount
                    + "</font>)</body></html>"));
            tvMessageCount.setVisibility(View.VISIBLE);
            /*if (spUser.getBoolean("is_chat_init", false))
                loadDialogsFromQb();*/
        } else {
            llMessages.setVisibility(View.GONE);
            llMessagesEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onMatchClicked(int position) {
        MatchModel model = data.get(position);
        if (model.isRead) {//goto profile
            /*Intent intent = new Intent(getActivity(), FeedProfileActivity.class);
            intent.putExtra("user_id", model.rel_user_id + "");*/
            //
            FeedModel model1 = new FeedModel(model.rel_user_id, model.name, "", model.url);
            model1.isMatched = true;
            model1.chatId = model.chatId;
            //Toast.makeText(getActivity(), "" + model.relIdentifier.contains("ll"), Toast.LENGTH_SHORT).show();
            /*if (TextUtils.isEmpty(model.relIdentifier)) {
                Toast.makeText(getActivity(), "My Null Identifier", Toast.LENGTH_SHORT).show();
            }*/
            model1.identifier = model.relIdentifier;
            //
            listener.onProfileClicked(model1);
            /*intent.putExtra("feed_model", new Gson().toJson(model1));
            startActivity(intent);*/
        } else {//goto chat
            markReadMatch(model);
        }
    }

    //


    //
    FeedProfileActivity.OnProfileClickListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (FeedProfileActivity.OnProfileClickListener) context;
    }

    //
    private void callChat(MatchModel model) {
        if (spUser.getBoolean("is_chat_init", false)) {
            JSession.chatUserName = model.name;
            JSession.introzeId = model.rel_user_id + "";
            JSession.urlRel = model.url;
            openChat(model.chatId);
        } else
            Toast.makeText(getActivity(), "Chat initializing please wait...", Toast.LENGTH_SHORT).show();
    }

    //
    private BroadcastReceiver brMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //
            tvMessageCount.setTextColor(getResources().getColor(android.R.color.darker_gray));
            tvMessageCount.setText(Html.fromHtml("<html><body>Messages (<font color='#8DC73F'>" + intent.getIntExtra("count", 0)
                    + "</font>)</body></html>"));
            tvMessageCount.setVisibility(View.VISIBLE);
            //
        }
    };

    public void loadDialogsFromQb() {
        tvMessageCount.setTextColor(getResources().getColor(android.R.color.darker_gray));
        tvMessageCount.setText(Html.fromHtml("<html><body>Messages (<font color='#8DC73F'>0</font>)</body></html>"));
        tvMessageCount.setVisibility(View.VISIBLE);
        //
        QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
        requestBuilder.setLimit(100);

        QBChatService.getChatDialogs(QBDialogType.PRIVATE, requestBuilder, new QBEntityCallback<ArrayList<QBChatDialog>>() {
            @Override
            public void onSuccess(ArrayList<QBChatDialog> dialogs, Bundle args) {
                int totalEntries = args.getInt("total_entries");
                Log.d("TotalEntries", totalEntries + "");
                int totalCounts = 0;
                //
                for (QBChatDialog dialog : dialogs) {
                    //
                    Log.d(TAG, "Chat Count " + dialog.getUnreadMessageCount());
                    //
                    if (dialog.getUnreadMessageCount() > 0) {
                        DialogModel model = containChatId(dialog.getOccupants().get(1), dialog.getOccupants().get(0));
                        if (model != null) {
                            model.count = dialog.getUnreadMessageCount();
                            //
                            Log.d(TAG, "Chat Count Inner " + model.count);
                            //
                            totalCounts += model.count;
                        } else {
                            Log.d(TAG, "Chat Count Model test " + model);
                        }
                    }
                    //alDialog.add(new DialogModel(dialog.getOccupants().get(1), dialog.getUnreadMessageCount()));
                    Log.d("ChatIdDialog", dialog.getOccupants().get(1) + "");
                }
                //
                adapterDialog.notifyDataSetChanged();
                //
                /*Intent intent = new Intent("message_count");
                intent.putExtra("count", totalCounts);
                getActivity().sendBroadcast(intent);*/
                /*//

                 */
                /*tvMessageCount.setTextColor(getResources().getColor(android.R.color.darker_gray));
                tvMessageCount.setText(Html.fromHtml("<html><body>Messages (<font color='#8DC73F'>" + totalCounts
                        + "</font>)</body></html>"));
                tvMessageCount.setVisibility(View.VISIBLE);*/
                //
               /* if (isMergeNow) mergerDialogMatches();
                else if (isForDialogUpdate) updateDialogForUnreadMessage();
                else isMergeNow = true;*/
                //
            }

            @Override
            public void onError(QBResponseException errors) {
                Log.d("DialogError", "" + errors);
                //pd.dismiss();
            }
        });
    }

    //
    private void mergerDialogMatches() {
        //
        for (Iterator<DialogModel> it = alDialog.iterator(); it.hasNext(); ) {
            DialogModel dm = it.next();
            MatchModel tempModel = null;//containChatId(dm.chatId);
            if (tempModel != null) {
                dm.setDetail(tempModel.name, tempModel.url, tempModel.byName, tempModel.createOn, tempModel.relationType,
                        tempModel.rel_id, tempModel.rel_user_id);
            } else {
                it.remove();
            }
        }
        //
        /*for (DialogModel model : alDialog) {
            MatchModel tempModel = containChatId(model.chatId);
            if (tempModel != null) {
                model.setDetail(tempModel.name, tempModel.url, tempModel.byName, tempModel.createOn, tempModel.relationType,
                        tempModel.rel_user_id);
            } else {
                alDialog.remove(model);
            }
        }*/
        adapterDialog.notifyDataSetChanged();
        isMergeNow = false;
        isForDialogUpdate = true;
        //
        pd.dismiss();
    }

    //
    private DialogModel containChatId(int chatId1, int chatId2) {
        ArrayList<Integer> chatIds = new ArrayList<>();
        chatIds.add(chatId1);
        chatIds.add(chatId2);
        //
        Log.d(TAG, "Chat Id First Dialog Chat Ids " + chatIds.toString());
        //
        for (DialogModel model : alDialog) {
            //
            Log.d(TAG, "Chat Id First " + model.user1ChatId + " Chat Id Second " + model.user2ChatId);
            //
            if (chatIds.contains(model.user1ChatId) && chatIds.contains(model.user2ChatId)) {
                return model;
            }
            /*if (model.chatId == chatId) {
                return model;
            }*/
        }
        return null;
    }

    //
    private void updateDialogForUnreadMessage() {

    }

    //
    public void openChat(int chatId) {
        //
        if (pd == null) pd = CustomProgress.show(getActivity(), null, false, false, null);
        else pd.show();
        //
        ArrayList<QBUser> selectedUsers = new ArrayList<>();
        //
        selectedUsers.add(new QBUser(chatId));
        //
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {
                        pd.dismiss();
                        ChatActivity.startForResult(getActivity(), 5000, dialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        pd.dismiss();
                        Log.d("DialogError", "" + e);
                        //Toast.makeText(MainActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    //
    @Override
    public void onDialogClicked(int position, boolean isForChat) {
        //
        DialogModel model = alDialog.get(position);
        if (isForChat) {
            //
            if (spUser.getBoolean("is_chat_init", false)) {
                //
                JSession.chatUserName = model.name;
                JSession.introzeId = model.rel_user_id + "";
                JSession.urlRel = model.url;
                Log.d(TAG, "Dialog Chat Id " + model.chatId);
                openChat(model.chatId);
            } else {
                builderError.setErrorMessage("Chat initializing please wait...");
                adError.show();
                //Toast.makeText(getActivity(), "Chat initializing please wait...", Toast.LENGTH_SHORT).show();
            }//
            /*JSession.chatUserName = model.name;
            JSession.introzeId = model.rel_user_id + "";
            openChat(model.chatId);*/
        } else {
            gotoProfile(model);
        }
    }

    @Override
    public void onRelationshipClicked(int position) {
        //
        DialogModel model = alDialog.get(position);
        //
        builder.setValues(JUtils.getTwoUpperChar(model.byName), model.reason, model.urlBy);
        ad.show();
    }

    //
    public void gotoProfile(DialogModel model) {
        //
        //Intent intent = new Intent(getActivity(), FeedProfileActivity.class);

        //intent.putExtra("user_id", model.rel_user_id + "");
        //
        FeedModel model1 = new FeedModel(model.byUserId, model.byName, "", model.urlBy);
        listener.onProfileClicked(model1);

        //
        //intent.putExtra("feed_model", new Gson().toJson(model1));
        //startActivity(intent);
    }

    //
    private void markReadMatch(final MatchModel model) {
        //
        Log.d("RElId", model.rel_id + " Rel Id");
        //
        Ion.with(this).load("PUT", AppUrls.MAIN + "/mark_read").setHeader("Authorization", JUtils.getHeader(spUser))
                .setBodyParameter("rel_id", model.rel_id + "").asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("MarkResult", "" + result);
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            callChat(model);
                            ((HomeActivity) getActivity()).getMatchCount();
                        } else {
                            builderError.setErrorMessage(joResult.getString("message"));
                            adError.show();
                        }
                    } catch (JSONException e1) {
                        builderError.setErrorMessage(getString(R.string.error_no_response));
                        adError.show();
                    }
                } else {
                    builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();
                }
            }
        });
    }

    @Override
    public void dialogDismiss() {
        ad.dismiss();
    }
}
