package com.wd.introze.async;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.repository.server.rf.RFClient;
import com.wd.introze.repository.server.rf.RFService;
import com.wd.introze.repository.server.rf.model.badge.BadgeUnreadCount;
import com.wd.introze.repository.server.rf.model.badge.RFAccountUnreadCount;
import com.wd.introze.repository.server.rf.model.badge.RFActivityUnreadCount;


import java.io.IOException;

import retrofit2.Call;

/**
 * Created by flair on 01-10-2016
 */

public class BadgeCountAsyncTask extends AsyncTask<String, Void, String> {
    //
    public static final String BADGE_COUNT_RECEIVER = "badge_count_receiver";
    public static final String BADGE_COUNT = "badge_count";
    //
    private Context context;
    //
    private RFService service = RFClient.getClient().create(RFService.class);

    public BadgeCountAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        //
        String auth = params[0];
        //
        int forUCount = getForUCount(auth);
        int byUCount = getByUCount(auth);
        int accountCount = getAccountCount(auth);
        //
        BadgeUnreadCount unreadCount = new BadgeUnreadCount(accountCount, forUCount, byUCount);
        //
        return new Gson().toJson(unreadCount);
    }

    @Override
    protected void onPostExecute(String badgeCount) {
        super.onPostExecute(badgeCount);
        //
        Intent intent = new Intent(BADGE_COUNT_RECEIVER);
        intent.putExtra(BADGE_COUNT, badgeCount);
        context.sendBroadcast(intent);
    }

    //
    private int getForUCount(String auth) {
        //
        if (!JConnectionUtils.isConnectedToInternet(context)) return 0;
        //
        Call<RFActivityUnreadCount> call = service.getForUCount(auth);
        try {
            retrofit2.Response<RFActivityUnreadCount> response = call.execute();
            if (response.isSuccessful()) {
                RFActivityUnreadCount unreadCount = response.body();
                return unreadCount.unreadCount;
            } else {
                getForUCount(auth);
            }
        } catch (IOException e) {
            getForUCount(auth);
        }
        return 0;
    }

    //
    private int getByUCount(String auth) {
        //
        if (!JConnectionUtils.isConnectedToInternet(context)) return 0;
        //
        Call<RFActivityUnreadCount> call = service.getByUCount(auth);
        try {
            retrofit2.Response<RFActivityUnreadCount> response = call.execute();
            if (response.isSuccessful()) {
                RFActivityUnreadCount unreadCount = response.body();
                return unreadCount.unreadCount;
            } else {
                //getByUCount(auth);
            }
        } catch (IOException e) {
            getByUCount(auth);
        }
        return 0;
    }

    //
    private int getAccountCount(String auth) {
        //
        if (!JConnectionUtils.isConnectedToInternet(context)) return 0;
        //
        Call<RFAccountUnreadCount> call = service.getAccountCount(auth);
        try {
            retrofit2.Response<RFAccountUnreadCount> response = call.execute();
            if (response.isSuccessful()) {
                RFAccountUnreadCount unreadCount = response.body();
                return unreadCount.unreadRecCount;
            } else {
                getAccountCount(auth);
            }
        } catch (IOException e) {
            getAccountCount(auth);
        }
        return 0;
    }
}
