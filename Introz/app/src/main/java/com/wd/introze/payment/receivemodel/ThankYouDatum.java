
package com.wd.introze.payment.receivemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ThankYouDatum {

    @SerializedName("amount")
    @Expose
    public Integer amount;
    @SerializedName("your_first_name")
    @Expose
    public String yourFirstName;
    @SerializedName("your_last_name")
    @Expose
    public String yourLastName;
    @SerializedName("your_id")
    @Expose
    public Integer yourId;
    @SerializedName("by_user_id")
    @Expose
    public Integer byUserId;
    @SerializedName("by_first_name")
    @Expose
    public String byFirstName;
    @SerializedName("by_last_name")
    @Expose
    public String byLastName;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("by_read")
    @Expose
    public Integer byRead;
    @SerializedName("to_read")
    @Expose
    public Integer toRead;
    @SerializedName("thank_you_id")
    @Expose
    public Integer thankYouId;
    @SerializedName("message")
    @Expose
    public String message;

}
