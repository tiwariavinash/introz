
package com.wd.introze.repository.server.rf.model.badge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RFActivityUnreadCount {

    @SerializedName("result")
    @Expose
    public String result;
    @SerializedName("read_count")
    @Expose
    public Integer readCount;
    @SerializedName("unread_count")
    @Expose
    public Integer unreadCount;
}
