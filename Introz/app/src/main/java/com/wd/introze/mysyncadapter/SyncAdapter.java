/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wd.introze.mysyncadapter;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.wd.introze.AppUrls;
import com.wd.introze.SuccessModel;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JUtils;
import com.wd.introze.repository.server.rf.RFClient;
import com.wd.introze.repository.server.rf.RFService;
import com.wd.introze.repository.server.rf.model.user.Email;
import com.wd.introze.repository.server.rf.model.user.User;
import com.wd.introze.sync.Add;
import com.wd.introze.sync.Contacts;
import com.wd.introze.sync.Delete;
import com.wd.introze.sync.SyncModel;
import com.wd.introze.sync.Update;
import com.wd.introze.tables.TableContacts;
import com.wd.introze.tables.TableEmail;
import com.wd.introze.tables.TablePhone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Define a sync adapter for the app.
 * <p/>
 * <p>This class is instantiated in {@link SyncService}, which also binds SyncAdapter to the system.
 * SyncAdapter should only be initialized in SyncService, never anywhere else.
 * <p/>
 * <p>The system calls onPerformSync() via an RPC call through the IBinder object supplied by
 * SyncService.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    public static final String TAG = "SyncWeb";
    Context context;
    //
    private SharedPreferences spUser;
    private boolean isSyncing = false;
    //
    private boolean isGetIntrozeUser = false;
    //
    private int firstSyncCount;
    //
    private String userPhone;
    private ArrayList<String> userEmails = new ArrayList<>();
    //
    private boolean isGetIntrozeUserFirstTime = true;
    private int count = 5;

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.context = context;
        //
        spUser = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        //
        context.getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, new MyContentObserver());
    }

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        this.context = context;
    }

    /**
     * Called by the Android system in response to a request to run the sync adapter. The work
     * required to read data from the network, parse it, and store it in the content provider is
     * done here. Extending AbstractThreadedSyncAdapter ensures that all methods within SyncAdapter
     * run on a background thread. For this reason, blocking I/O and other long-running tasks can be
     * run <em>in situ</em>, and you don't have to set up a separate thread for them.
     * .
     * <p/>
     * <p>This is where we actually perform any work required to perform a sync.
     * {@link AbstractThreadedSyncAdapter} guarantees that this will be called on a non-UI thread,
     * so it is safe to peform blocking I/O here.
     * <p/>
     * <p>The syncResult argument allows you to pass information back to the method that triggered
     * the sync.
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        Log.d(TAG, "Beginning network synchronization");
        if (!JConnectionUtils.isConnectedToInternet(context)) return;
        //
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Here, thisActivity is the current activity
            Timer timer = new Timer("introze_sync");
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    //if (!isGetIntrozeUserFirstTime) getRFIntrozeUser();
                    //getIntrozeUsers();
                    //
                    checkForSync();
                }
            }, new Date(), 1000 * 120);
            //}
        } else {
            //
            Log.d(TAG, "IsGetIntrozeUserFirsmTime" + isGetIntrozeUserFirstTime);
            //
            //if (!isGetIntrozeUserFirstTime) getRFIntrozeUser();
            //getIntrozeUsers();
            //
            checkForSync();
        }
    }

    //
    private void sendMyBroadcast(String permission) {
        Log.d("Permission", permission + "");
        Intent notificationIntentDialog = new Intent("permission_access");
        notificationIntentDialog.putExtra("permission", permission);
        context.sendBroadcast(notificationIntentDialog);
    }

    //
    private void checkForSync() {
        //
        Log.d("is_first_time", spUser.getBoolean("is_first_time_sync", true) + " " + isSyncing);
        //
        /*if (!spUser.getBoolean("is_contact_sync_db", false))
            insertIntoDBFirstTime();
        else*/
        if (spUser.getBoolean("is_verified", false) && !isSyncing && !spUser.getBoolean("is_first_time_sync", true) &&
                spUser.getBoolean("is_contact_sync_db", false)) {
            Log.d("FirstTimeNot", "NotFirstTime");
            isSyncing = true;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //
                    syncAgain();
                }
            }).start();
        } else if (spUser.getBoolean("is_verified", false) && !isSyncing && spUser.getBoolean("is_contact_sync_db", false)) {
            Log.d("FirstTime", "FirstTime");
            isSyncing = true;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //
                    /*if (!spUser.getBoolean("is_contact_sync_db", false))
                        insertIntoDBFirstTime();
                    else*/
                    syncFirstTime();
                }
            }).start();
        }
        //Toast.makeText(context, "Contact Sync Completed", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "Network synchronization complete");
    }

    //
    private void updateContact(final String updateGson) {
        Log.d("UpdateContacts", "UpdateContacts");
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(updateGson);//spUser.getString("again_update", ""));
        //
        Ion.with(context).load("POST", AppUrls.MAIN + "/contact_sync3").setTimeout(120000).setHeader("Authorization", "Basic " + basic)
                .setHeader("Accept", "application/json").setHeader("Content-type", "application/json")
                .setJsonObjectBody(jo).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {

                Log.d("UpdateContact", result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //
                            JSONObject joFinalResult = new JSONObject(updateGson).getJSONObject("contacts");
                            //
                            JSONArray jaAdd = joFinalResult.optJSONArray("add");
                            if (jaAdd != null) {
                                for (int i = 0; i < jaAdd.length(); i++) {
                                    JSONObject joItem = jaAdd.getJSONObject(i);
                                    //
                                    //
                                    TableContacts contacts = new TableContacts();
                                    String id = joItem.getString("identifier");
                                    //
                                    contacts.identifier = Long.parseLong(id);
                                    //
                                    contacts.firstName = joItem.getString("first_name");
                                    contacts.lastName = joItem.getString("last_name");
                                    //contacts.isIntroze = joItem.getInt("introze_user") == 1 ? true : false;
                                    //contacts.introzeUserId = joItem.getInt("user_id");
                                    //
                                    contacts.save();
                                    //
                                    JSONArray jaPhone = joItem.optJSONArray("phone");
                                    if (jaPhone != null) {
                                        for (int iPhone = 0; iPhone < jaPhone.length(); iPhone++) {
                                            TablePhone modelPhone = new TablePhone();
                                            modelPhone.phone = jaPhone.getString(iPhone);
                                            modelPhone.contacts = contacts;
                                            modelPhone.save();
                                        }
                                    }
                                    //
                                    //
                                    JSONArray jaEmail = joItem.optJSONArray("email");
                                    if (jaEmail != null) {
                                        for (int iEmail = 0; i < jaEmail.length(); iEmail++) {
                                            TableEmail modelEmail = new TableEmail();
                                            modelEmail.email = jaEmail.getString(iEmail);
                                            modelEmail.contacts = contacts;
                                            modelEmail.save();
                                        }
                                    }
                                }
                            }
                            JSONArray jaUpdate = joFinalResult.optJSONArray("update");
                            if (jaUpdate != null) {
                                for (int i = 0; i < jaUpdate.length(); i++) {
                                    JSONObject joItem = jaUpdate.getJSONObject(i);
                                    //
                                    String id = joItem.getString("identifier");
                                    deleteFromDB(id);
                                    //
                                    TableContacts contacts = new TableContacts();
                                    //
                                    contacts.identifier = Long.parseLong(id);
                                    contacts.contactId = joItem.getInt("contact_id");
                                    //
                                    contacts.firstName = joItem.getString("first_name");
                                    contacts.lastName = joItem.getString("last_name");
                                    //contacts.isIntroze = joItem.getInt("introze_user") == 1 ? true : false;
                                    //contacts.introzeUserId = joItem.getInt("user_id");
                                    //
                                    contacts.save();
                                    //
                                    JSONArray jaPhone = joItem.optJSONArray("phone");
                                    if (jaPhone != null) {
                                        for (int iPhone = 0; iPhone < jaPhone.length(); iPhone++) {
                                            TablePhone modelPhone = new TablePhone();
                                            modelPhone.phone = jaPhone.getString(iPhone);
                                            modelPhone.contacts = contacts;
                                            modelPhone.save();
                                        }
                                    }
                                    //
                                    //
                                    JSONArray jaEmail = joItem.optJSONArray("email");
                                    if (jaEmail != null) {
                                        for (int iEmail = 0; i < jaEmail.length(); iEmail++) {
                                            TableEmail modelEmail = new TableEmail();
                                            modelEmail.email = jaEmail.getString(iEmail);
                                            modelEmail.contacts = contacts;
                                            modelEmail.save();
                                        }
                                    }
                                    //
                                }
                            }
                            JSONArray jaDelete = joFinalResult.optJSONArray("delete");
                            if (jaDelete != null) {
                                for (int i = 0; i < jaDelete.length(); i++) {
                                    //
                                    JSONObject joDeleteItem = jaDelete.getJSONObject(i);
                                    String id = joDeleteItem.getString("contact_id");
                                    deleteFromDB(id);
                                }
                            }
                            --gsonCount;
                            if (gsonCount == 0) {
                                isSyncing = false;
                                getIntrozeUsers();
                            }
                        } else {
                            --gsonCount;
                            if (gsonCount == 0) {
                                isSyncing = false;
                            }
                            if (joResult.has("message")) {
                                //Toast.makeText(context, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            } else if (joResult.has("errorType")) {
                                //Toast.makeText(context, "" + joResult.getString("errorType"), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (JSONException e1) {
                        --gsonCount;
                        if (gsonCount == 0) {
                            isSyncing = false;
                        }
                    }
                } else {
                    --gsonCount;
                    if (gsonCount == 0) {
                        isSyncing = false;
                    }
                }
            }
        });
    }

    //
    private void deleteFromDB(String id) {
        //
        List<TableContacts> c = new Select().from(TableContacts.class).where("Identifier =?",
                Long.parseLong(id)).execute();
        //
        if (c.size() > 0) {
            new com.activeandroid.query.Delete().from(TablePhone.class).where("Contacts =?",
                    c.get(0).getId()).execute(); //Long.parseLong(id);
            //
            new com.activeandroid.query.Delete().from(TableEmail.class).where("Contacts =?",
                    c.get(0).getId()).execute();
        }
        c.get(0).delete();
    }

    //
    private Timer timer;

    //
    private void syncFirstTime() {
        ArrayList<CModel> alData = getContactsFromPhoneBook();
        //
        ArrayList<List<CModel>> alDataBatch = getBatches(alData, 1000);
        //
        int max = alDataBatch.size();
        //
        firstSyncCount = max;
        //
        RFService service = RFClient.getClient().create(RFService.class);
        //
        for (int i = 0; i < max; i++) {
            List<CModel> listDeleteTemp = new ArrayList<>();
            List<CModel> listUpdateTemp = new ArrayList<>();
            //
            List<CModel> listNewTemp = alDataBatch.get(i);
            //
            //syncContact(convertToJson(listNewTemp, listDeleteTemp, listUpdateTemp));
            rfSyncFirst(service, new Gson().fromJson(convertToJson(listNewTemp, listDeleteTemp, listUpdateTemp), SyncModel.class));
        }
        //
        //
        if (firstSyncCount <= 0) {
            spUser.edit().putBoolean("is_first_time_sync", false).commit();
            getRFIntrozeUser();
        }
        isSyncing = false;
        //
        //
        /*if (max <= 0)
            isSyncing = false;
        else {
            //getIntrozeUsersFirstTime();
            getRFIntrozeUser();
        }*/
    }

    //
    private void rfSyncFirst(RFService service, SyncModel model) {
        if (!JConnectionUtils.isConnectedToInternet(context)) return;
        //
        Call<SuccessModel> call = service.onContactSync2(JUtils.getHeader(spUser), model);
        try {
            SuccessModel successModel = call.execute().body();
            if (successModel != null)
                if (TextUtils.equals("success", successModel.result)) {
                    --firstSyncCount;
                    Log.d(TAG, "Success " + "Sync Success");
                } else {
                    rfSyncFirst(service, model);
                }
        } catch (IOException e) {
            rfSyncFirst(service, model);
        }
        //
    }

    //
    private void getRFIntrozeUser() {
        RFService service = RFClient.getClient().create(RFService.class);
        Call<ResponseBody> call = service.getIntrozeUser(JUtils.getHeader(spUser));
        try {
            String result = call.execute().body().string();
            JSONObject joResult = new JSONObject(result);
            if (TextUtils.equals("success", joResult.getString("result"))) {
                //
                JSONArray joData = joResult.optJSONArray("data");
//
                if (joData != null) {
                    for (int i = 0; i < joData.length(); i++) {
                        //
                        JSONObject ii = joData.getJSONObject(i);
                        String id = ii.getString("identifier");
                        //
                        try {
                            long lid = Long.parseLong(id);
                            List<TableContacts> items = new Select().from(TableContacts.class).where("Identifier =?", lid).execute();
                            if (items.size() > 0) {
                                TableContacts model = items.get(0);
                                model.contactId = ii.getInt("contact_id");
                                model.introzeUserId = ii.getInt("introze_user_id");
                                model.isIntroze = true;
                                model.isMatched = ii.optInt("matched") == 1 ? true : false;
                                //
                                String urlPic = ii.optString("file_url");
                                String fbId = ii.getString("fb_id");
                                if (TextUtils.isEmpty(urlPic)) {
                                    if (!TextUtils.equals("0", fbId)) {
                                        model.urlPic = JUtils.getFacebookProfilePictureUrl(fbId);
                                    }
                                } else {
                                    model.urlPic = AppUrls.MAIN + "/" + urlPic;
                                }
                                //
                                model.chatId = ii.optInt("chat_id");
                                //
                                model.save();
                            }
                        } catch (NumberFormatException nfe) {

                        }
                    }
                    //
                }
                //
                count--;
                if ((joData == null || joData.length() <= 0) && count >= 0) {
                    getRFIntrozeUser();
                } else {
                    //isGetIntrozeUserFirstTime = false;
                    //
                    Intent intent = new Intent("load_contacts_from_db");
                    context.sendBroadcast(intent);
                }
            } else {
                count--;
                if (count >= 0) {
                    getRFIntrozeUser();
                } /*else {
                    isGetIntrozeUserFirstTime = false;
                }*/
            }
        } catch (IOException e) {
            count--;
            if (count >= 0) {
                getRFIntrozeUser();
            } /*else {
                isGetIntrozeUserFirstTime = false;
            }*/
        } catch (JSONException e) {
            count--;
            if (count >= 0) {
                getRFIntrozeUser();
            } /*else {
                isGetIntrozeUserFirstTime = false;
            }*/
        }
    }

    //
    private void syncContact(final String data) {
        Log.d("SyncGsonFTime", data + "");
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(data);
        //
        Ion.with(context).load("POST", AppUrls.MAIN + "/contact_sync2").setTimeout(120000).setHeader("Authorization", "Basic " + basic)
                .setHeader("Accept", "application/json").setHeader("Content-type", "application/json")
                .setJsonObjectBody(jo).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {

                Log.d("SyncResult", result + "");
                if (e == null) {
                    //spUser.edit().putBoolean("is_syncing", false).commit();
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //
                            --firstSyncCount;
                            if (firstSyncCount == 0) {
                                spUser.edit().putBoolean("is_first_time_sync", false).commit();
                                isSyncing = false;
                                //insertIntoDBFirstTime();
                                getIntrozeUsersFirstTime();
                                //
                                /*Intent intent = new Intent("load_contacts_from_db");
                                context.sendBroadcast(intent);*/
                                Log.d("isfts", spUser.getBoolean("is_first_time_sync", true) + "");
                            }
                            //Toast.makeText(context, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                            //
                            //
                            /*if (timer == null) {
                                timer = new Timer("get_introze_user");
                                timer.scheduleAtFixedRate(new TimerTask() {
                                    @Override
                                    public void run() {

                                    }
                                }, new Date(), 120000);
                            }*/
                            //
                            //spUser.edit().putBoolean("is_synced", true).commit();
                            /*spUser.edit().putBoolean("is_contact_sync", true).commit();
                            spUser.edit().putBoolean("is_first_time_sync", false).commit();*/
                        } else {
                            //
                            --firstSyncCount;
                            if (firstSyncCount <= 0) {
                                isSyncing = false;
                                isGetIntrozeUserFirstTime = false;
                            } else
                                syncContact(data);
                        }
                    } catch (JSONException e1) {
                        //
                        --firstSyncCount;
                        if (firstSyncCount <= 0) {
                            isSyncing = false;
                            isGetIntrozeUserFirstTime = false;
                        } else
                            syncContact(data);
                        //Toast.makeText(context, "" + e1, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //
                    --firstSyncCount;
                    if (firstSyncCount <= 0) {
                        isSyncing = false;
                        isGetIntrozeUserFirstTime = false;
                    } else
                        syncContact(data);
                    Log.d("HomeError", e + "");
                    //Toast.makeText(context, "" + e, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private void getIntrozeUsers() {
        //
        if (isGetIntrozeUser) {
            return;
        }
        isGetIntrozeUser = true;

        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(context).load("GET", AppUrls.MAIN + "/get_introze_users").setHeader("Authorization", "Basic " + basic).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("Synciusers", result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //
                            //int contactCount = joResult.getInt("total_contacts");
                            //
                            JSONArray joData = joResult.optJSONArray("data");
//
                            if (joData != null) {
                                for (int i = 0; i < joData.length(); i++) {
                                    //
                                    JSONObject ii = joData.getJSONObject(i);
                                    String id = ii.getString("identifier");
                                    //
                                    //TableContacts model = Model.load(TableContacts.class, Long.parseLong(id));
                                    try {
                                        long lid = Long.parseLong(id);
                                        List<TableContacts> items = new Select().from(TableContacts.class).where("Identifier =?", lid).execute();
                                        if (items.size() > 0) {
                                            TableContacts model = items.get(0);
                                            model.contactId = ii.getInt("contact_id");
                                            model.introzeUserId = ii.getInt("introze_user_id");
                                            model.isIntroze = true;
                                            model.isMatched = ii.optInt("matched") == 1 ? true : false;
                                            //
                                            String urlPic = ii.optString("file_url");
                                            String fbId = ii.getString("fb_id");
                                            if (TextUtils.isEmpty(urlPic)) {
                                                if (!TextUtils.equals("0", fbId)) {
                                                    model.urlPic = JUtils.getFacebookProfilePictureUrl(fbId);
                                                }
                                            } else {
                                                model.urlPic = AppUrls.MAIN + "/" + urlPic;
                                            }
                                            //
                                            model.chatId = ii.optInt("chat_id");
                                            //
                                            model.save();
                                        }
                                    } catch (NumberFormatException nfe) {

                                    }
                                }
                                isGetIntrozeUser = false;
                                //
                                Intent intent = new Intent("load_contacts_from_db");
                                context.sendBroadcast(intent);
                                //
                            } else {
                                isGetIntrozeUser = false;
                                getIntrozeUsers();
                            }

                        } else {
                            isGetIntrozeUser = false;
                            getIntrozeUsers();
                        }
                    } catch (JSONException e1) {
                        isGetIntrozeUser = false;
                        getIntrozeUsers();
                    }
                } else {
                    isGetIntrozeUser = false;
                    getIntrozeUsers();
                }
            }
        });
    }

    //
    //
    private void getIntrozeUsersFirstTime() {
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(context).load("GET", AppUrls.MAIN + "/get_introze_users").setHeader("Authorization", "Basic " + basic)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("SynciusersFirst", result + "");
                if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            //
                            //int contactCount = joResult.getInt("total_contacts");
                            //
                            JSONArray joData = joResult.optJSONArray("data");
//
                            if (joData != null) {
                                for (int i = 0; i < joData.length(); i++) {
                                    //
                                    JSONObject ii = joData.getJSONObject(i);
                                    String id = ii.getString("identifier");
                                    //
                                    //TableContacts model = Model.load(TableContacts.class, Long.parseLong(id));
                                    try {
                                        long lid = Long.parseLong(id);
                                        List<TableContacts> items = new Select().from(TableContacts.class).where("Identifier =?", lid).execute();
                                        if (items.size() > 0) {
                                            TableContacts model = items.get(0);
                                            model.contactId = ii.getInt("contact_id");
                                            model.introzeUserId = ii.getInt("introze_user_id");
                                            model.isIntroze = true;
                                            model.isMatched = ii.optInt("matched") == 1 ? true : false;
                                            //
                                            String urlPic = ii.optString("file_url");
                                            String fbId = ii.getString("fb_id");
                                            if (TextUtils.isEmpty(urlPic)) {
                                                if (!TextUtils.equals("0", fbId)) {
                                                    model.urlPic = JUtils.getFacebookProfilePictureUrl(fbId);
                                                }
                                            } else {
                                                model.urlPic = AppUrls.MAIN + "/" + urlPic;
                                            }
                                            //
                                            model.chatId = ii.optInt("chat_id");
                                            //
                                            model.save();
                                        }
                                    } catch (NumberFormatException nfe) {

                                    }
                                }
                                //
                            }
                            //
                            count--;
                            if ((joData == null || joData.length() <= 0) && count >= 0) {
                                getIntrozeUsersFirstTime();
                            } else {
                                isGetIntrozeUserFirstTime = false;
                                //
                                Intent intent = new Intent("load_contacts_from_db");
                                context.sendBroadcast(intent);
                            }
                        } else {
                            count--;
                            if (count >= 0) {
                                getIntrozeUsersFirstTime();
                            } else {
                                isGetIntrozeUserFirstTime = false;
                            }
                        }
                    } catch (JSONException e1) {
                        count--;
                        if (count >= 0) {
                            getIntrozeUsersFirstTime();
                        } else {
                            isGetIntrozeUserFirstTime = false;
                        }
                    }
                } else {
                    count--;
                    if (count >= 0) {
                        getIntrozeUsersFirstTime();
                    } else {
                        isGetIntrozeUserFirstTime = false;
                    }
                }
            }
        });
    }

    //
    private void getPhoneNumber(long id, List<String> phone) {
        /*if (Integer.parseInt(cur.getString(
                cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
            Cursor pCur = context.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                    new String[]{id}, null);
        */
        List<TablePhone> phone1 = new Select().from(TablePhone.class).where("Contacts =?", id).execute();
        for (TablePhone p : phone1) {
            // Do something with phones
            phone.add(p.phone);//pCur.getString(pCur.getColumnIndex("data1")));
            //
            //Log.d("PhoneNumber", pCur.getString(pCur.getColumnIndex("data1")) + "");
           /* }
            pCur.close();*/
        }
    }

    //
    private void getEmail(long id, List<String> alEmail) {
        List<TableEmail> emails = new Select().from(TableEmail.class).where("Contacts =?", id).execute();
        for (TableEmail e : emails) {
            // This would allow you get several email addresses
            // if the email addresses were stored in an array
            String email = e.email;
            //
            alEmail.add(email);
            /*String emailType = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));*/
        }
    }


    /**
     * if (!isSyncing) {
     * startContactSync();
     * //
     * }
     */

    //
    private void startContactSync() {
        spUser.edit().putBoolean("is_contact_sync", false).commit();
        spUser.edit().putBoolean("is_contact_sync_db", false).commit();
        //
        Log.d("SyncDBObserver", "Started");
        //
        new Thread(new Runnable() {
            @Override
            public void run() {
                //if (spUser.getBoolean("is_first_time_sync", true))
                //insertIntoDBFirstTime();
                //else syncAgain();
            }
        }).start();
    }

    //
    private void syncAgain() {
        Log.d("SyncAgain", "SyncAgain");
        ArrayList<CModel> alPhoneBookContacts = getContactsFromPhoneBook();
        //List<TableContacts> alDBContacts = new Select().from(TableContacts.class).execute();
        //
        ArrayList<CModel> alNewContacts = new ArrayList<>();
        ArrayList<CModel> alUpdatedContacts = new ArrayList<>();
        ArrayList<CModel> alDeleteContacts = new ArrayList<>();
        //
        for (CModel model : alPhoneBookContacts) {
            List<TableContacts> alDBContacts = new Select().from(TableContacts.class).where("Identifier =?", Integer.parseInt(model.id)).execute();
            if (alDBContacts.size() > 0) {
                if (isUpdated(model, alDBContacts.get(0))) {
                    model.contactId = Integer.parseInt(model.id); //alDBContacts.get(0).contactId;
                    alUpdatedContacts.add(model);
                }
            } else alNewContacts.add(model);
        }
        //
        List<TableContacts> alDBContacts = new Select().from(TableContacts.class).execute();
        for (TableContacts contact : alDBContacts) {
            boolean temp = false;
            for (CModel model : alPhoneBookContacts) {
                if (contact.identifier == Integer.parseInt(model.id)) temp = true;
            }
            if (!temp) {
                CModel cModel = new CModel(contact.identifier + "");//contact.identifier + "", "0", contact.firstName + " " + contact.lastName);
                alDeleteContacts.add(cModel);
            }
        }
        /**/
        //
        /*int newBatchSize = alNewContacts.size() / 2000;
        newBatchSize = (alNewContacts.size() % 2000) > 0 ? newBatchSize + 1 : newBatchSize;*/
        ArrayList<List<CModel>> alNewBatch = getBatches(alNewContacts, 1000);
        ArrayList<List<CModel>> alDeleteBatch = getBatches(alDeleteContacts, 1000);
        ArrayList<List<CModel>> alUpdateBatch = getBatches(alUpdatedContacts, 1000);
        //Log.d("GsonAgain", new Gson().toJson(model));
        //
        int max = Math.max(alNewBatch.size(), Math.max(alDeleteBatch.size(), alUpdateBatch.size()));
        //
        String[] gson = new String[max];
        //
        for (int i = 0; i < max; i++) {
            List<CModel> listNewTemp = new ArrayList<>();
            List<CModel> listDeleteTemp = new ArrayList<>();
            List<CModel> listUpdateTemp = new ArrayList<>();
            //
            if (i < alNewBatch.size()) {
                listNewTemp = alNewBatch.get(i);
            }
            if (i < alDeleteBatch.size()) {
                listDeleteTemp = alDeleteBatch.get(i);
            }
            if (i < alUpdateBatch.size()) {
                listUpdateTemp = alUpdateBatch.get(i);
            }
            //
            gson[i] = convertToJson(listNewTemp, listDeleteTemp, listUpdateTemp);
            //
            Log.d("FullGson", gson[i] + "");
        }
        //
        gsonCount = gson.length;
        //
        RFService service = RFClient.getClient().create(RFService.class);
        //
        for (String g : gson) {
            Log.d("FullGsonAgain", g + "");
            //updateContact(g);
            rfUpdateContact(service, new Gson().fromJson(g, SyncModel.class));
        }
        //
        //if (gsonCount == 0) {
        getRFIntrozeUser();
        //}
        isSyncing = false;
        //if (gson.length <= 0) isSyncing = false;
    }

    //
    private void rfUpdateContact(RFService service, SyncModel model) {
        if (!JConnectionUtils.isConnectedToInternet(context)) return;
        //
        Call<SuccessModel> call = service.onContactSync3(JUtils.getHeader(spUser), model);
        try {
            SuccessModel successModel = call.execute().body();
            if (TextUtils.equals("success", successModel.result)) {
                //--gsonCount;
                rfUpdateToDB(new Gson().toJson(model));
            } else {
                rfUpdateContact(service, model);
            }
        } catch (IOException e) {
            rfUpdateContact(service, model);
        }
        //

    }

    //
    private void rfUpdateToDB(String updateGson) {

        try {
            JSONObject joFinalResult = new JSONObject(updateGson).getJSONObject("contacts");

            //
            JSONArray jaAdd = joFinalResult.optJSONArray("add");
            if (jaAdd != null) {
                for (int i = 0; i < jaAdd.length(); i++) {
                    JSONObject joItem = jaAdd.getJSONObject(i);
                    //
                    //
                    TableContacts contacts = new TableContacts();
                    String id = joItem.getString("identifier");
                    //
                    contacts.identifier = Long.parseLong(id);
                    //
                    contacts.firstName = joItem.getString("first_name");
                    contacts.lastName = joItem.getString("last_name");
                    //contacts.isIntroze = joItem.getInt("introze_user") == 1 ? true : false;
                    //contacts.introzeUserId = joItem.getInt("user_id");
                    //
                    contacts.save();
                    //
                    JSONArray jaPhone = joItem.optJSONArray("phone");
                    if (jaPhone != null) {
                        for (int iPhone = 0; iPhone < jaPhone.length(); iPhone++) {
                            TablePhone modelPhone = new TablePhone();
                            modelPhone.phone = jaPhone.getString(iPhone);
                            modelPhone.contacts = contacts;
                            modelPhone.save();
                        }
                    }
                    //
                    //
                    JSONArray jaEmail = joItem.optJSONArray("email");
                    if (jaEmail != null) {
                        for (int iEmail = 0; i < jaEmail.length(); iEmail++) {
                            TableEmail modelEmail = new TableEmail();
                            modelEmail.email = jaEmail.getString(iEmail);
                            modelEmail.contacts = contacts;
                            modelEmail.save();
                        }
                    }
                }
            }
            JSONArray jaUpdate = joFinalResult.optJSONArray("update");
            if (jaUpdate != null) {
                for (int i = 0; i < jaUpdate.length(); i++) {
                    JSONObject joItem = jaUpdate.getJSONObject(i);
                    //
                    String id = joItem.getString("identifier");
                    deleteFromDB(id);
                    //
                    TableContacts contacts = new TableContacts();
                    //
                    contacts.identifier = Long.parseLong(id);
                    contacts.contactId = joItem.getInt("contact_id");
                    //
                    contacts.firstName = joItem.getString("first_name");
                    contacts.lastName = joItem.getString("last_name");
                    //contacts.isIntroze = joItem.getInt("introze_user") == 1 ? true : false;
                    //contacts.introzeUserId = joItem.getInt("user_id");
                    //
                    contacts.save();
                    //
                    JSONArray jaPhone = joItem.optJSONArray("phone");
                    if (jaPhone != null) {
                        for (int iPhone = 0; iPhone < jaPhone.length(); iPhone++) {
                            TablePhone modelPhone = new TablePhone();
                            modelPhone.phone = jaPhone.getString(iPhone);
                            modelPhone.contacts = contacts;
                            modelPhone.save();
                        }
                    }
                    //
                    //
                    JSONArray jaEmail = joItem.optJSONArray("email");
                    if (jaEmail != null) {
                        for (int iEmail = 0; i < jaEmail.length(); iEmail++) {
                            TableEmail modelEmail = new TableEmail();
                            modelEmail.email = jaEmail.getString(iEmail);
                            modelEmail.contacts = contacts;
                            modelEmail.save();
                        }
                    }
                    //
                }
            }
            JSONArray jaDelete = joFinalResult.optJSONArray("delete");
            if (jaDelete != null) {
                for (int i = 0; i < jaDelete.length(); i++) {
                    //
                    JSONObject joDeleteItem = jaDelete.getJSONObject(i);
                    String id = joDeleteItem.getString("contact_id");
                    deleteFromDB(id);
                }
            }
            /*--gsonCount;
            if (gsonCount == 0) {
                isSyncing = false;
                getIntrozeUsers();
            }*/
        } catch (JSONException e) {

        }
    }

    //
    private int gsonCount;

    //
    public ArrayList<List<String>> getDeleteBatches(ArrayList<String> collection, int batchSize) {
        int i = 0;
        ArrayList<List<String>> batches = new ArrayList<>();
        while (i < collection.size()) {
            int nextInc = Math.min(collection.size() - i, batchSize);
            List<String> batch = collection.subList(i, i + nextInc);
            batches.add(batch);
            i = i + nextInc;
        }
        return batches;
    }

    //
    public ArrayList<List<CModel>> getBatches(ArrayList<CModel> collection, int batchSize) {
        int i = 0;
        ArrayList<List<CModel>> batches = new ArrayList<>();
        while (i < collection.size()) {
            int nextInc = Math.min(collection.size() - i, batchSize);
            List<CModel> batch = collection.subList(i, i + nextInc);
            batches.add(batch);
            i = i + nextInc;
        }
        return batches;
    }

    //
    private String convertToJson(List<CModel> alNewContacts, List<CModel> alDeleteContacts, List<CModel> alUpdatedContacts) {
        //
        Contacts contacts = new Contacts();
        //
        List<Add> adds = contacts.add;
        List<Update> update = contacts.update;
        List<Delete> delete = contacts.delete;
        //
        SyncModel model = new SyncModel();
        model.contacts = contacts;
        //
        updateContact(update, alUpdatedContacts);
        deleteContact(delete, alDeleteContacts);
        addContact(adds, alNewContacts);
        //
        return new Gson().toJson(model);
    }

    //
    private void addContact(List<Add> adds, List<CModel> alAddContacts) {
        if (alAddContacts.size() > 0) {
            for (CModel con : alAddContacts) {
                //
                Add add = new Add();
                List<String> email = add.email;
                List<String> phone = add.phone;
                //
                long id = Integer.parseInt(con.id);
                //
                add.identifier = id + "";
                //
                //String name = con.displayName;
                //
                String[] name = con.displayName.split(" ");
                //
                if (name.length > 1) {
                    add.last_name = name[name.length - 1];
                    //
                    String temp = name[0];
                    for (int i = 1; i < name.length - 1; i++) {
                        temp = temp + " " + name[i];
                    }
                    add.first_name = temp;
                } else {
                    add.first_name = con.displayName;
                    add.last_name = "";
                }
                //
                //if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                //Query phone here.  Covered next
                //getPhoneNumber(con.getId(), phone);
                for (String phone1 : con.phones) {
                    phone.add(phone1);
                }
                //}
                //
                //getEmail(con.getId(), email);
                for (String email1 : con.emails) {
                    email.add(email1);
                }
                //
                adds.add(add);
            }
            /*spUser.edit().putBoolean("is_contact_sync", true).commit();
            spUser.edit().putBoolean("is_first_time_sync", false).commit();*/
        }
    }

    //
    private void deleteContact(List<Delete> deletes, List<CModel> alDeleteContacts) {
        if (alDeleteContacts.size() > 0) {
            for (CModel con : alDeleteContacts) {
                //
                com.wd.introze.sync.Delete delete = new com.wd.introze.sync.Delete();
                delete.contact_id = con.id;
                //List<String> email = delete.email;
                //List<String> phone = delete.phone;
                //
                //long id = Integer.parseInt(con.id);
                //
                //delete.identifier = con + "";
                //
                //delete.contact_id = con.;
                //String name = con.displayName;
                //
                //String[] name = con.displayName.split(" ");
                //
                /*if (name.length > 1) {
                    delete.last_name = name[name.length - 1];
                    //
                    String temp = name[0];
                    for (int i = 1; i < name.length - 1; i++) {
                        temp = temp + " " + name[i];
                    }
                    delete.first_name = temp;
                } else {
                    delete.first_name = con.displayName;
                    delete.last_name = "";
                }
                //
                //if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                //Query phone here.  Covered next
                //getPhoneNumber(con.getId(), phone);
                for (String phone1 : con.phones) {
                    phone.add(phone1);
                }
                //}
                //
                //getEmail(con.getId(), email);
                for (String email1 : con.emails) {
                    email.add(email1);
                }*/
                //
                deletes.add(delete);
            }
            /*spUser.edit().putBoolean("is_contact_sync", true).commit();
            spUser.edit().putBoolean("is_first_time_sync", false).commit();*/
        }
    }

    //
    private void updateContact(List<Update> update, List<CModel> alUpdatedContacts) {
        if (alUpdatedContacts.size() > 0) {
            for (CModel con : alUpdatedContacts) {
                //
                Update update1 = new Update();
                List<String> email = update1.email;
                List<String> phone = update1.phone;
                //
                long id = Integer.parseInt(con.id);
                //
                update1.identifier = id + "";
                //
                update1.contact_id = con.contactId;
                //String name = con.displayName;
                //
                String[] name = con.displayName.split(" ");
                //
                if (name.length > 1) {
                    update1.last_name = name[name.length - 1];
                    //
                    String temp = name[0];
                    for (int i = 1; i < name.length - 1; i++) {
                        temp = temp + " " + name[i];
                    }
                    update1.first_name = temp;
                } else {
                    update1.first_name = con.displayName;
                    update1.last_name = "";
                }
                //
                //if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                //Query phone here.  Covered next
                //getPhoneNumber(con.getId(), phone);
                for (String phone1 : con.phones) {
                    phone.add(phone1);
                }
                //}
                //
                //getEmail(con.getId(), email);
                for (String email1 : con.emails) {
                    email.add(email1);
                }
                //
                update.add(update1);
            }
            /*spUser.edit().putBoolean("is_contact_sync", true).commit();
            spUser.edit().putBoolean("is_first_time_sync", false).commit();*/
        }
    }

    //
    private boolean isUpdated(CModel cModel, TableContacts dbModel) {
        if (!TextUtils.equals(cModel.displayName, (dbModel.firstName + " " + dbModel.lastName).trim())) {
            Log.d("SyncName", "" + cModel.displayName + "\t" + (dbModel.firstName + " " + dbModel.lastName).trim());
            return true;
        }
        //
        List<TablePhone> dbModelPhone = new Select().from(TablePhone.class).where("Contacts =?", dbModel.getId()).execute();
        if (cModel.phones.size() != dbModelPhone.size()) {
            Log.d("SyncSize", cModel.phones.size() + "\t" + dbModelPhone.size());
            return true;
        } else if (dbModelPhone.size() > 0) {
            Log.d("SyncPhoneSize", "" + dbModelPhone.size());
            //
            boolean isPhoneForUpdate = isPhoneUpdated(cModel, dbModelPhone);
            if (isPhoneForUpdate) return isPhoneForUpdate;
        }
        //
        List<TableEmail> dbModelEmail = new Select().from(TableEmail.class).where("Contacts =?", dbModel.getId()).execute();
        if (cModel.emails.size() != dbModelEmail.size()) {
            //Log.d("SyncEmail", "SyncEmail");
            return true;
        } else {
            //Log.d("SyncEmailElse", "SyncEmailElse");
            return isEmailUpdated(cModel, dbModelEmail);
        }
    }

    //
    private boolean isEmailUpdated(CModel model, List<TableEmail> tableEmail) {
        for (TableEmail email : tableEmail) {
            if (!model.emails.contains(email.email)) return true;
        }
        return false;
    }

    //
    private boolean isPhoneUpdated(CModel model, List<TablePhone> tablePhone) {
        for (TablePhone phone : tablePhone) {
            if (!model.phones.contains(phone.phone)) {
                Log.d("SyncMyPhone", "" + phone.phone);
                return true;
            }
        }
        return false;
    }

    //
    private ArrayList<CModel> getContactsFromPhoneBook() {
        //
        /*HomeActivity.ProfileModel profileModel = new Gson().fromJson(spUser.getString("profile_detail", ""),
                HomeActivity.ProfileModel.class);
        if (profileModel != null) {
            userPhone = profileModel.phones;
            userEmails.addAll(profileModel.email);
        }*/
        String userDetail = spUser.getString(User.class.getName(), null);
        if (!TextUtils.isEmpty(userDetail)) {
            User user = new Gson().fromJson(userDetail, User.class);
            userPhone = user.userDetails.phone;
            for (Email e : user.userDetails.emails) {
                userEmails.add(e.email);
            }
            userEmails.add(user.userDetails.email);
            //userEmails.addAll(profileModel.email);
        }
        //
        Log.d("UserPhone", userPhone + "");
        //
        ContentResolver cr = context.getContentResolver();
        //
        Cursor curContact = cr.query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts.HAS_PHONE_NUMBER}, null, null, null);
        //
        ArrayList<CModel> alContacts = new ArrayList<>();
        //
        ArrayList<String> alNames = new ArrayList<>();
        //
        //
        while (curContact.moveToNext()) {
            String displayName = curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            if (!alNames.contains(displayName) && !TextUtils.isEmpty(displayName) && isAlpha(displayName)
                    && !JUtils.isEmail(displayName)) {
                alNames.add(displayName);
                //
                String _id = curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts._ID));
                //
                String[] data = getContactDetails(context, _id);
                //
                if (data != null) {
                    //
                    CModel model = new CModel(curContact.getString(
                            curContact.getColumnIndex(ContactsContract.Contacts._ID))
                            ,
                            curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)),
                            curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                    //
                    for (String phone : new Gson().fromJson(data[0], String[].class)) {
                        model.phones.add(phone);
                    }
                    //
                    for (String email : new Gson().fromJson(data[1], String[].class)) {
                        model.emails.add(email);
                    }
                    //
                    alContacts.add(model);
                }
            }
        }
        /*while (curContact.moveToNext()) {
            String _id = curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts._ID));
            //
            Cursor cur = cr.query(ContactsContract.RawContacts.CONTENT_URI, null, ContactsContract.RawContacts.CONTACT_ID + "=?",
                    new String[]{_id}, null);
            //
            CModel model = null;
            //
            if (cur.moveToNext()) {
                String accountName = cur.getString(cur.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_NAME));
                String displayName = curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (//(TextUtils.equals(accountName, "Phone") || TextUtils.equals(accountName, "vnd.sec.contact.phone")) &&
                        !TextUtils.isEmpty(displayName) && isAlpha(displayName)) {
                    //
                    //Log.d("DisplayName", displayName + " " + isAlpha(displayName));
                    //
                    model = new CModel(curContact.getString(
                            curContact.getColumnIndex(ContactsContract.Contacts._ID))
                            ,
                            curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)),
                            curContact.getString(curContact.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                    //
                }
            }
            cur.close();
            //
            if (model != null) {
                if (!TextUtils.equals(model.hasPhoneNumber, "0")) {
                    getPhoneNumber2(_id, model);
                }
                getEmail2(_id, model);
                //
                if (!model.isUser)
                    alContacts.add(model);
            }
        }*/
        curContact.close();
        //
        userPhone = null;
        userEmails.clear();
        //
        return alContacts;
    }

    //
    public String[] getContactDetails(Context context, String _id) {
        Cursor cursorDetail = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{ContactsContract.Data.MIMETYPE, ContactsContract.Data.DATA1},
                ContactsContract.Data.CONTACT_ID + "=?", new String[]{_id}, null);
        //
        ArrayList<String> phones = new ArrayList<>();
        ArrayList<String> emails = new ArrayList<>();
        //
        while (cursorDetail.moveToNext()) {
            String item = cursorDetail.getString(cursorDetail.getColumnIndex(ContactsContract.Data.DATA1));
            //
            if (TextUtils.equals(cursorDetail.getString(cursorDetail.getColumnIndex(ContactsContract.Data.MIMETYPE)),
                    ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
                String phone = formatPhone(item);
                if (TextUtils.equals(phone, userPhone)) {
                    cursorDetail.close();
                    return null;
                }
                if (!phones.contains(phone)) {
                    phones.add(phone);
                }
            } else if (TextUtils.equals(cursorDetail.getString(cursorDetail.getColumnIndex(ContactsContract.Data.MIMETYPE)),
                    ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
                if (userEmails.contains(item)) {
                    cursorDetail.close();
                    return null;
                }
                if (!emails.contains(item)) {
                    emails.add(item);
                }
            }
        }
        cursorDetail.close();
        if (phones.size() <= 0 && emails.size() <= 0) return null;
        return new String[]{new Gson().toJson(phones), new Gson().toJson(emails)};
    }

    private void insertIntoDBFirstTime() {
        new com.activeandroid.query.Delete().from(TableEmail.class).where("Email <>?", "-1").execute();
        new com.activeandroid.query.Delete().from(TablePhone.class).where("Phone <>?", "-1").execute();
        new com.activeandroid.query.Delete().from(TableContacts.class).where("Identifier <>?", -1).execute();
        //
        ArrayList<CModel> name = getContactsFromPhoneBook();
        //
        for (CModel model : name) {
            insertContact(model);
        }
        //
        spUser.edit().putBoolean("is_contact_sync_db", true).commit();
        //
        Log.d("SyncDBSyncContact2", "Completed");
        //SyncUtils.CreateSyncAccount(context);
        Intent intent = new Intent("load_contacts_from_db");
        context.sendBroadcast(intent);
        //
        //syncFirstTime();
    }

    public boolean isAlpha(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (Character.isLetter(c)) {
                return true;
            }
        }
        return false;
    }

    //
    private void insertContact(CModel model) {
        //
        TableContacts contacts = new TableContacts();
        String id = model.id;
        //
        contacts.identifier = Long.parseLong(id);
        //
        String[] name = model.displayName.split(" ");
        //
        if (name.length > 1) {
            contacts.lastName = name[name.length - 1];
            //
            String temp = name[0];
            for (int i = 1; i < name.length - 1; i++) {
                temp = temp + " " + name[i];
            }
            contacts.firstName = temp;
        } else {
            contacts.firstName = model.displayName;
            contacts.lastName = "";
        }
        //
        contacts.save();
        //
        ArrayList<String> phones = model.phones;
        for (int i = 0; i < phones.size(); i++) {
            TablePhone modelPhone = new TablePhone();
            modelPhone.phone = phones.get(i);
            modelPhone.contacts = contacts;
            modelPhone.save();
        }
        //
        //
        ArrayList<String> emails = model.emails;
        for (int i = 0; i < emails.size(); i++) {
            TableEmail modelEmail = new TableEmail();
            modelEmail.email = emails.get(i);
            modelEmail.contacts = contacts;
            modelEmail.save();
        }
        //
    }

    //
    /*private void getEmail2(String id, CModel model) {
        Cursor emailCur = context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{id}, null);
        while (emailCur.moveToNext()) {
            // This would allow you get several email addresses
            // if the email addresses were stored in an array
            String email = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            //
            if (userEmails != null && userEmails.contains(email)) model.isUser = true;
            //
            model.emails.add(email);
        }
        emailCur.close();
    }*/

    //
    /*private void getPhoneNumber2(String id, CModel model) {
        //
        Cursor pCur = context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                new String[]{id}, null);
        //
        ArrayList<String> temp = new ArrayList<>();
        //
        while (pCur.moveToNext()) {
            // Do something with phones
            String phone = formatPhone(pCur.getString(pCur.getColumnIndex("data1")));
            if (!temp.contains(phone)) {
                if (userPhone != null && TextUtils.equals(userPhone, phone)) model.isUser = true;
                model.phones.add(phone);
                //
                temp.add(phone);
                //
            }
        }
        pCur.close();
    }*/

    //
    private String formatPhone(String phone) {
        String temp = phone.replaceAll("[^0-9]", "").replace(" ", "");
        if (temp.length() > 10) {
            temp = temp.substring(temp.length() - 10);
        }
        return temp;
    }

    //
    public class CModel {
        //
        //public boolean isUser;
        //
        public String id;
        public String hasPhoneNumber;
        public String displayName;
        //
        public int contactId;
        //
        public ArrayList<String> phones = new ArrayList<>();
        public ArrayList<String> emails = new ArrayList<>();

        //
        public CModel() {
        }

        //
        public CModel(String id) {
            this.id = id;
        }

        //
        public CModel(String id, String hasPhoneNumber, String displayName) {
            this.id = id;
            this.hasPhoneNumber = hasPhoneNumber;
            this.displayName = displayName;
        }
    }

    //
    public class MyContentObserver extends ContentObserver {
        public MyContentObserver() {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            checkForSync();
            Log.e("PhonebookUpdated", "~~~~~~PhonebookUpdated" + selfChange);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }
    }
}
