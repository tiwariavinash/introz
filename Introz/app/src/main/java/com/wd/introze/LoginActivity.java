package com.wd.introze;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.login.widget.LoginButton;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JConnectionUtils;
import com.wd.introze.jutil.JErrorTextWatcher;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by admin on 2016-07-14
 */
public class LoginActivity extends FBLoginActivity {
    //
    private SharedPreferences.Editor editorUser;
    //
    private EditText etEmail, etPassword;
    private TextView tvErrorEmail, tvErrorPassword;
    //
    private ProgressDialog pd;
    //
    private SharedPreferences spUser;
    //
    TextView tvConnectWith, tv1, tv2;
    String fontPath = "sourcesanspro-light.ttf";
    Typeface tfText;
    //
    String fontPathHeader = "itcavantgardestd-bold.ttf";
    Typeface tfHeader;
    //
    private AlertDialog adError;
    private JSimpleBuilder builderError;
    //
    LinearLayout linLayOnForgotPass;
    CustomProgress customProgress;
    LinearLayout llSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //
        TypefaceHelper.typeface(this);
        //
        pd = new ProgressDialog(this);
        pd.setMessage("Please wait...");
        //
        builderError = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
        spUser = getSharedPreferences("user", MODE_PRIVATE);
        editorUser = spUser.edit();
        //
        tvErrorEmail = (TextView) findViewById(R.id.tvErrorEmail);
        tvErrorPassword = (TextView) findViewById(R.id.tvErrorPassword);
        //
        etEmail = (EditText) findViewById(R.id.etEmail);
        etEmail.addTextChangedListener(new JErrorTextWatcher(tvErrorEmail, "Please Enter Email Address"));
        //
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPassword.addTextChangedListener(new JErrorTextWatcher(tvErrorPassword, "Please Enter Password"));

        //
        linLayOnForgotPass = (LinearLayout) findViewById(R.id.linLayOnForgotPass);
        if (!Validate.isEmailValid(etEmail.getText().toString().trim())) {
            etEmail.addTextChangedListener(new JErrorTextWatcher(tvErrorEmail, "Please Enter Valid Email Address"));
        }

        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tfText = Typeface.createFromAsset(getAssets(), fontPath);
        tv1.setTypeface(tfText);
        tv2.setTypeface(tfText);
        //
        //
        loginButton = (LoginButton) findViewById(R.id.login_button);
        /*loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        loginButton.setReadPermissions(Arrays.asList("public_profile","email"));//"user_friends");
        //loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, callback);

        linLayOnForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });
        //
        llSignUp = (LinearLayout) findViewById(R.id.llSignUp);
        llSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });
        //
    }

    /*//
    private void registerDevice(String deviceToken, final boolean mobileVerified) {
        String device_os = System.getProperty("os.version");
        String device_model = "" + android.os.Build.MODEL;
        //
        TelephonyManager telephonyManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + telephonyManager.getDeviceId();
        tmSerial = "" + telephonyManager.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        //
        String deviceType = "Android";
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/device").setHeader("Authorization", "Basic " + basic)
                .setBodyParameter("device_unique_id", deviceId).setBodyParameter("device_type", deviceType)
                .setBodyParameter("device_model", device_model).setBodyParameter("device_os", device_os)
                .setBodyParameter("device_token", deviceToken).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                //
                if (mobileVerified) {
                    //Toast.makeText(LoginActivity.this, "phone is not verified", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                } else {
                    Intent intent = new Intent(LoginActivity.this, PhoneVerificationActivity.class);
                    startActivity(intent);
                    finish();
                }
                //Log.d("DeviceRegistration", result + "");
                *//*if (e == null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                    } catch (JSONException e1) {

                    }
                } else Toast.makeText(LoginActivity.this, "" + e, Toast.LENGTH_SHORT).show();*//*
            }
        });
    }
*/
    //
    public void onClickBtnSignUp(View view) {
        //startActivity(new Intent(this, SignUpActivity.class));
    }

    //
    public void onClickTvForgotPassword(View view) {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
        //finish();
    }

    //
    public void onClickTvSignUp(View view) {
        startActivity(new Intent(this, SignUpActivity.class));
        finish();
    }

    //
    public void onClickBtnLogin(View view) {
        //
        if (!JConnectionUtils.isConnectedToInternet(this)) {
            //
            showAlertDialog("Alert", getString(R.string.error_no_response));//getString(R.string.no_internet_connection));
            /*builderError.setErrorMessage(getString(R.string.no_internet_connection));
            adError.show();
            TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));*/
            return;
        }
        //
        String email = etEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            tvErrorEmail.setText("Please Enter Email Address");
            //Toast.makeText(LoginActivity.this, "Please enter email id", Toast.LENGTH_SHORT).show();
            return;
        }
        //
        if (!Validate.isEmailValid(etEmail.getText().toString().trim())) {
            tvErrorEmail.setText("Please Enter Valid Email Address");
            return;
        }
        //
        String password = etPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            tvErrorPassword.setText("Please Enter Password");
            //Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.length() < 6 || password.length() > 32) {
            tvErrorPassword.setText("Password must be 6 to 32 characters long");
            return;
        }

        //
        //  pd.show();
        customProgress = CustomProgress.show(LoginActivity.this, "", false, false, null);
        Ion.with(this).load(AppUrls.MAIN + "/login").setBodyParameter("email", email).setBodyParameter("password", password)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                // pd.dismiss();
                Log.d("LoginResult", result + "");
                customProgress.dismiss();
                if (result != null) {
                    try {
                        JSONObject joResult = new JSONObject(result);
                        //
                        if (TextUtils.equals("success", joResult.getString("result"))) {
                            JSONObject joUser = joResult.getJSONObject("user");
                            //
                            isVerified = joUser.getInt("mobile_verified") == 1 ? true : false;
                            editorUser.putBoolean("mobile_verified", isVerified).commit();
                            //
                            editorUser.putString("user_id", joUser.getInt("user_id") + "")
                                    .putString("user_token", joUser.getString("user_token"));
                            if (joUser.has("user_chat_id"))
                                editorUser.putInt("chat_id", joUser.getInt("user_chat_id"));
                            editorUser.commit();
                            //
                            afterLoginSuccessStartActivity();
                            //
                        } else {
                            showAlertDialog("Error", joResult.getString("message"));
                            /*builderError.setErrorMessage(joResult.getString("message"));
                            adError.show();
                            TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));*/
                        }
                    } catch (JSONException e1) {
                        /*builderError.setErrorMessage(getString(R.string.error_no_response));
                        adError.show();*/
                        showAlertDialog("Error", getString(R.string.error_no_response));
                        //Toast.makeText(LoginActivity.this, "" + e1.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    /*builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();*/
                    showAlertDialog("Error", getString(R.string.error_no_response));
                    //Toast.makeText(LoginActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //
    private void showAlertDialog(String title, String message) {
        builderError.setErrorMessage(title, message);
        adError.show();
        TypefaceHelper.typeface(adError.getButton(DialogInterface.BUTTON_POSITIVE));
    }
}
