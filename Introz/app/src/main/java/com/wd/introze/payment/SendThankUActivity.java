package com.wd.introze.payment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.norbsoft.typefacehelper.TypefaceHelper;
import com.wd.introze.AppUrls;
import com.wd.introze.R;
import com.wd.introze.SendMoneyActivity;
import com.wd.introze.customloader.CustomProgress;
import com.wd.introze.jutil.JSimpleBuilder;
import com.wd.introze.jutil.JUtils;

/**
 * Created by flair on 12-09-2016
 */
public class SendThankUActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    //
    private final String TAG = "SendThankU";
    //
    private EditText etMessage;
    //
    private TextView tvShortName, tvNameDesc;
    //
    private SimpleDraweeView ivProfile;
    //
    private int[] money = {5, 10, 20, 50, 75, 100};
    //
    private ToggleButton[] buttons = new ToggleButton[6];
    //
    private int currentPosition = -1;
    //
    private String userName;
    private String userPic;
    private int userId;
    //
    private SharedPreferences spUser;
    //
    private JSimpleBuilder builderError;
    private AlertDialog adError;
    //
    private CustomProgress pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_thanku);
        TypefaceHelper.typeface(this);
        //
        tvShortName = (TextView) findViewById(R.id.tvShortName);
        tvNameDesc = (TextView) findViewById(R.id.tvNameDesc);
        ivProfile = (SimpleDraweeView) findViewById(R.id.ivProfile);
        //
        TextView tvHeader = (TextView) findViewById(R.id.txtHeader);
        tvHeader.setTextSize(12);
        tvHeader.setText("SEND THANK YOU $$$");
        //
        Intent intent = getIntent();
        userName = intent.getStringExtra("user_name");
        userPic = intent.getStringExtra("url_user_pic");
        userId = intent.getIntExtra("user_id", 0);
        //
        spUser = getSharedPreferences("user", MODE_PRIVATE);
        //
        tvNameDesc.setText(JUtils.uppercaseFirstLetters(userName) + "'" + getString(R.string.send_thank_u));
        //
        setUserPic();
        //
        int[] btnResources = {R.id.btnOne, R.id.btnTwo, R.id.btnThree, R.id.btnFive, R.id.btnSix, R.id.btnSeven};
        for (int i = 0; i < btnResources.length; i++) {
            buttons[i] = (ToggleButton) findViewById(btnResources[i]);
            buttons[i].setTextOff("$" + money[i]);
            buttons[i].setTextOn("$" + money[i]);
            buttons[i].setOnCheckedChangeListener(this);
            buttons[i].setChecked(false);
        }
        //
        etMessage = (EditText) findViewById(R.id.etMessage);
        //
        builderError = new JSimpleBuilder(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adError.dismiss();
            }
        });
        adError = builderError.create();
        //adError.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    //
    public void onClickBtnSendThanks(View view) {
        //
        if (currentPosition == -1) {
            builderError.setErrorMessage("Alert", "Please select amount to send");
            adError.show();
            return;
        }
        //
        getFeedDetails();
        /*String userDetail = spUser.getString(SendMoneyActivity.PaymentDetail.class.getName(), null);
        if (userDetail == null) {
            //getCardDetail();
        } else getFeedDetails();*/ //gotoAct();
        //gotoAct();
    }

    //
    private void swapAct() {
        String userDetail = spUser.getString(SendMoneyActivity.PaymentDetail.class.getName(), null);
        if (userDetail == null) {
            getCardDetail();
        } else gotoAct();
    }

    //
    ConfirmActivity.FeedDetail feesDetail;

    //
    private void getFeedDetails() {
        //
        pd = CustomProgress.show(this, null, false, false, null);
        //
        Ion.with(this).load("POST", AppUrls.MAIN + "/fee_details").setHeader("Authorization", JUtils.getHeader(spUser))
                .setBodyParameter("amount", money[currentPosition] + "").asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d(TAG, "Result Fees Detail" + result);
                pd.dismiss();
                if (e == null) {
                    feesDetail = new Gson().fromJson(result, ConfirmActivity.FeedDetail.class);
                    if (TextUtils.equals("success", feesDetail.result)) {
                        //
                        swapAct();
                        /*tvProcessingFee.setText("Processing Fee (" + feedDetail.fees + "%):");
                        tvProcessingFeed.setText("$" + (amount * Float.parseFloat(feedDetail.fees)) / 100);
                        tvAmountCharge.setText("$" + feedDetail.totalAmount);*/

                    } else {
                        builderError.setErrorMessage(getString(R.string.error_no_response));
                        adError.show();
                    }
                } else {
                    builderError.setErrorMessage(getString(R.string.error_no_response));
                    adError.show();
                    Log.d(TAG, "");
                }
            }
        });
    }

    //
    private void gotoAct() {
        //
        String message = etMessage.getText().toString();
        //
        String userDetail = spUser.getString(SendMoneyActivity.PaymentDetail.class.getName(), null);
        if (userDetail == null) {
            Intent intent = new Intent(this, SendMoneyActivity.class);
            //
            intent.putExtra("user_name", userName);
            intent.putExtra("amount", money[currentPosition]);
            intent.putExtra("message", message);
            intent.putExtra("user_id", userId);
            //
            intent.putExtra("fees_detail", new Gson().toJson(feesDetail));
            //
            intent.putExtra("is_from_thank_u", true);
            startActivity(intent);
            finish();
            return;
        } else {
            SendMoneyActivity.PaymentDetail model = new Gson().fromJson(userDetail, SendMoneyActivity.PaymentDetail.class);
            //
            Intent intent = new Intent(this, ConfirmActivity.class);
            intent.putExtra("user_name", userName);
            intent.putExtra("amount", money[currentPosition]);
            intent.putExtra("message", message);
            intent.putExtra("user_id", userId);
            intent.putExtra("last_4", model.last4);
            //
            intent.putExtra("fees_detail", new Gson().toJson(feesDetail));
            //

            startActivity(intent);

            finish();
        }
    }

    //
    private void setUserPic() {
        //
        ControllerListener controllerListener = new BaseControllerListener() {
            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                tvShortName.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
                tvShortName.setText(JUtils.getTwoUpperChar(userName));
                tvShortName.setVisibility(View.VISIBLE);
            }
        };
        //
        DraweeController controller = Fresco.newDraweeControllerBuilder().setControllerListener(controllerListener)
                .setUri(userPic).build();
        ivProfile.setController(controller);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            if (currentPosition != -1)
                buttons[currentPosition].setChecked(false);
            switch (buttonView.getId()) {
                case R.id.btnOne:
                    currentPosition = 0;
                    break;
                case R.id.btnTwo:
                    currentPosition = 1;
                    break;
                case R.id.btnThree:
                    currentPosition = 2;
                    break;
                case R.id.btnFive:
                    currentPosition = 3;
                    break;
                case R.id.btnSix:
                    currentPosition = 4;
                    break;
                case R.id.btnSeven:
                    currentPosition = 5;
                    break;
            }
            //
            buttons[currentPosition].setTextColor(getResources().getColor(android.R.color.white));
        } else {
            buttons[currentPosition].setTextColor(getResources().getColor(R.color.text));
        }
    }

    //
    //
    public void getCardDetail() {
        //
        if (pd == null) pd = CustomProgress.show(this, null, false, false, null);
        else pd.show();
        //
        byte[] auth = (spUser.getString("user_id", "") + ":" + spUser.getString("user_token", "")).getBytes();
        String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
        //
        Ion.with(this).load("GET", AppUrls.MAIN + "/get_cc").setHeader("Authorization", "Basic " + basic).asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.d("CardDetail", result + "");
                        pd.dismiss();
                        if (result != null) {
                            //
                            SendMoneyActivity.PaymentDetail model = new Gson().fromJson(result, SendMoneyActivity.PaymentDetail.class);
                            //
                            if (TextUtils.equals("success", model.result)) {
                                //Toast.makeText(SendMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();
                                if (model.month == null || model.year == null || model.name == null) {
                                    /*builderError.setErrorMessage(model.message);
                                    adError.show();*/
                                } else {
                                    saveBankDetail(model);
                                }
                                gotoAct();
                            } else {
                                builderError.setErrorMessage(model.message);
                                adError.show();
                            }
                            //Toast.makeText(SendMoneyActivity.this, "" + joResult.getString("message"), Toast.LENGTH_SHORT).show();

                        } else {
                            builderError.setErrorMessage(getString(R.string.error_no_response));
                            adError.show();
                        }
                        //Toast.makeText(SendMoneyActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //
    //
    private void saveBankDetail(SendMoneyActivity.PaymentDetail model) {
        spUser.edit().putString(SendMoneyActivity.PaymentDetail.class.getName(), new Gson().toJson(model)).commit();
    }
}
