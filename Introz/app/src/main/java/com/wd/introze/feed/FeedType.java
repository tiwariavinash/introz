package com.wd.introze.feed;

/**
 * Created by flair on 21-07-2016
 */
public interface FeedType {
    int BUSINESS = 0;
    int DATING = 1;
    int FRIENDSHIP = 2;
    int MUTUAL_FRIEND = 3;
}
