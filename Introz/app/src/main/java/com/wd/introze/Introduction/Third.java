package com.wd.introze.Introduction;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.wd.introze.LoginActivity;
import com.wd.introze.MainActivity;
import com.wd.introze.R;

/**
 * Created by admin on 28-10-2015.
 */
public class Third extends Fragment {
    Button btnSignUp, btnLogin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_three, null);
        //
        //Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "gotham_rounded_bold.otf");
        //
        //((TextView) view.findViewById(R.id.tv)).setTypeface(tf);
        btnSignUp = (Button) view.findViewById(R.id.btnSignUp);
        btnLogin = (Button) view.findViewById(R.id.btnLogin);
        //
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return view;
    }
}
