package com.wd.introze;

import com.activeandroid.Configuration;
import com.activeandroid.content.ContentProvider;
import com.wd.introze.tables.TableContacts;
import com.wd.introze.tables.TableEmail;
import com.wd.introze.tables.TablePhone;

public class DBContentProvider extends ContentProvider {
    @Override
    protected Configuration getConfiguration() {
        super.getConfiguration();
        //Configuration.Builder configs = new Configuration.Builder(getContext()).addModelClass(S3UrlTable.class);
        Configuration.Builder configs = new Configuration.Builder(getContext());
        configs.addModelClass(TableContacts.class);
        configs.addModelClass(TableEmail.class);
        configs.addModelClass(TablePhone.class);

        return configs.create();
    }
}
