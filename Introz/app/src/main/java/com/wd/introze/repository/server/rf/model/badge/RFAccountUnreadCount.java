package com.wd.introze.repository.server.rf.model.badge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by flair on 01-10-2016
 */

public class RFAccountUnreadCount {
    @SerializedName("result")
    @Expose
    public String result;
    @SerializedName("unread_count")
    @Expose
    public Integer unreadCount;
    @SerializedName("unread_sent_count")
    @Expose
    public Integer unreadSentCount;
    @SerializedName("unread_rec_count")
    @Expose
    public Integer unreadRecCount;
    @SerializedName("amount")
    @Expose
    public Integer amount;
}
