package com.wd.introze.repository.server.rf;

import com.wd.introze.AppUrls;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Gajendra on 9/24/2016
 */

public class RFClient {
    //"http://introzedev.us-east-1.elasticbeanstalk.com/"
    public static final String BASE_URL = AppUrls.MAIN;
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
