package com.quickblox.sample.chat.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.sample.chat.utils.Consts;
import com.quickblox.sample.chat.utils.SharedPreferencesUtil;
import com.quickblox.sample.chat.utils.chat.ChatHelper;
import com.quickblox.sample.core.ui.activity.CoreSplashActivity;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.wd.introze.R;

import java.util.Random;

public class SplashActivity extends CoreSplashActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //test
        //Random random = new Random();
        //JUser.setFullNameParent(this, random.nextInt(100) + "" + random.nextInt(100) + "" + random.nextInt(100));
        //test

        if (SharedPreferencesUtil.hasQbUser()) {
            //proceedToTheNextActivityWithDelay();
            //return;
        }

        createSession();
    }

    @Override
    protected String getAppName() {
        return getString(R.string.splash_app_title);
    }

    @Override
    protected void proceedToTheNextActivity() {
        if (SharedPreferencesUtil.hasQbUser()) {
            //DialogsActivity.start(this);
            startActivity(new Intent(this, MainActivity.class));
        } else {
            LoginActivity.start(this);
        }
        finish();
    }

    private void createSession() {
        QBAuth.createSessionV2(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession result, Bundle params) {
                if (SharedPreferencesUtil.hasQbUser()) {
                    proceedToTheNextActivityWithDelay();
                    return;
                }
                signup();
                //proceedToTheNextActivity();
            }

            @Override
            public void onError(QBResponseException e) {
                showSnackbarError(null, R.string.splash_create_session_error, e, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        createSession();
                    }
                });
            }
        });
    }

    private void signup() {
        Random random = new Random();
        int a = random.nextInt(1000);
        int b = random.nextInt(1000);
       QBUser user = new QBUser(a + b + "", Consts.QB_USERS_PASSWORD);
        //user.setExternalId("45345");
        //user.setFacebookId("100233453457767");
        //user.setTwitterId("182334635457");
        user.setPassword(Consts.QB_USERS_PASSWORD);
        user.setEmail(a + b + "" + "@gmail.com");
        user.setFullName(a + b + "");
        user.setPhone(a + b + "");
        //user.setCustomData("dfdds");

        //
        StringifyArrayList<String> tags = new StringifyArrayList<>();
        //tags.add("webrtcusers");
        tags.add("teguser");
        user.setTags(tags);
        //user.setWebsite("www.mysite.com");


        try {
            user=  QBUsers.signUpV2(user);
            user.setPassword(Consts.QB_USERS_PASSWORD);
            login(user);
        } catch (QBResponseException e) {
            e.printStackTrace();
        }

//        QBUsers.s(user, new QBEntityCallback<QBUser>()
//        {
//            @Override
//            public void onSuccess(QBUser user, Bundle args) {
//                //proceedToTheNextActivity();
//                //final QBUser user = (QBUser) parent.getItemAtPosition(position);
//                // We use hardcoded password for all users for test purposes
//                // Of course you shouldn't do that in your app
//                user.setPassword(Consts.QB_USERS_PASSWORD);
//                login(user);
//                //profileImage(user);
//                Toast.makeText(SplashActivity.this, "OnSuccess", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onError(QBResponseException errors) {
//                user.setPassword(Consts.QB_USERS_PASSWORD);
//                login(user);
//                //Toast.makeText(SplashActivity.this, "OnError " + errors.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    private void login(final QBUser user) {
        //final QBUser user2 = new QBUser();
        //user.setEmail("mohit.schouhan09@gmail.com");
        user.setPassword(Consts.QB_USERS_PASSWORD);
        ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_login);
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                SharedPreferencesUtil.saveQbUser(user);

                //DialogsActivity.start(SplashActivity.this);
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();

                //ProgressDialogFragment.hide(getSupportFragmentManager());
                //profileImage(user);
            }

            @Override
            public void onError(QBResponseException e) {
                ProgressDialogFragment.hide(getSupportFragmentManager());
               /* ErrorUtils.showSnackbar(userListView, R.string.login_chat_login_error, e,
                        R.string.dlg_retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                login(user);
                            }
                        });*/
                Toast.makeText(SplashActivity.this, "OnError Login : " + e, Toast.LENGTH_SHORT).show();
            }
        });
    }
}