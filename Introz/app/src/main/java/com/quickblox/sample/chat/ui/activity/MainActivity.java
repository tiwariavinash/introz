package com.quickblox.sample.chat.ui.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.chat.utils.SharedPreferencesUtil;
import com.quickblox.sample.chat.utils.chat.ChatHelper;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.wd.introze.R;

import java.util.ArrayList;

/**
 * Created by flair on 12-08-2016.
 */
public class MainActivity extends AppCompatActivity {
    //
    EditText etId, etMessage;

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qb_activity_main);
        //
        etId = (EditText) findViewById(R.id.etId);
        etMessage = (EditText) findViewById(R.id.etMessage);
    }

    //
    public void sendMessage(View view) {
        //
        QBUsers.getUserV2(Integer.parseInt(etId.getText().toString()), new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
//
                Toast.makeText(MainActivity.this, "User success " + qbUser.getId(), Toast.LENGTH_SHORT).show();
                //
                ArrayList<QBUser> selectedUsers = new ArrayList<>();
                selectedUsers.add(qbUser);
                selectedUsers.add(SharedPreferencesUtil.getQbUser());
                //
                ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                        new QBEntityCallback<QBChatDialog>() {
                            @Override
                            public void onSuccess(QBChatDialog dialog, Bundle args) {
                                ChatActivity.startForResult(MainActivity.this, 5000, dialog);
                            }

                            @Override
                            public void onError(QBResponseException e) {
                                Toast.makeText(MainActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                );
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("userproblem", "" + e);
            }
        });

    }
}
